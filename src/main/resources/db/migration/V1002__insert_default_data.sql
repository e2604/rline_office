INSERT INTO statuses (name)
VALUES ('Новая'),
       ('В работе'),
       ('Выполнена'),
       ('Просрочена'),
       ('Отменена');

INSERT INTO roles (role)
VALUES ('ADMIN'),
       ('MANAGER'),
       ('ACCOUNTANT'),
       ('EXECUTOR'),
       ('USER');

INSERT INTO priorities (name)
VALUES ('Низкий'),
       ('Средний'),
       ('Высокий');

INSERT INTO application_types (name)
VALUES ('На монтаж'),
       ('На доставку');

INSERT INTO positions (name)
VALUES ('Администратор'),
       ('Офис-менеджер'),
       ('Бухгалтер'),
       ('Исполнитель'),
       ('Ведущий инженер'),
       ('Инженер'),
       ('Техник-монтажник'),
       ('Главный конструктор'),
       ('Производитель работ'),
       ('Электромонтер'),
       ('Курьер');

INSERT INTO counterparty_types (name)
VALUES ('Физическое лицо'),
       ('Юридическое лицо');

INSERT INTO device_statuses (name)
VALUES ('Новая'),
       ('В работе'),
       ('Исправно'),
       ('Сгорел');

INSERT INTO document_types (name)
VALUES ('Общий'),
       ('АВР');

INSERT INTO document_statuses (name)
VALUES ('Не определен'),
       ('Определен');

INSERT INTO users (name, surname, email, password, phone, chat_id, iin, role_id, position_id)
VALUES ('Admin', 'Admin', 'admin@gmail.com', '$2a$04$mNMJCFz23EqQTJCxC02ICOtYtZCQaHzHzXGrpXCdxtXKr3upT.P5a',
        '+7(700)000-00-00', null, '000000000000', 1, 1)