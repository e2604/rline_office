CREATE TABLE statuses
(
    id   SERIAL PRIMARY KEY,
    name varchar(128) NOT NULL
);

CREATE TABLE roles
(
    id   SERIAL PRIMARY KEY,
    role varchar(128) NOT NULL
);

CREATE TABLE priorities
(
    id   SERIAL PRIMARY KEY,
    name varchar(128) NOT NULL
);

CREATE TABLE application_types
(
    id   SERIAL PRIMARY KEY,
    name varchar(128) NOT NULL
);

CREATE TABLE positions
(
    id   SERIAL PRIMARY KEY,
    name varchar(128) NOT NULL
);


CREATE TABLE users
(
    id          SERIAL PRIMARY KEY,
    name        varchar(128) COLLATE pg_catalog."default"       NOT NULL,
    surname     varchar(128) COLLATE pg_catalog."default"       NOT NULL,
    email       varchar(128) COLLATE pg_catalog."default"       NOT NULL UNIQUE,
    password    varchar(128) COLLATE pg_catalog."default"       NOT NULL,
    phone       varchar(128) COLLATE pg_catalog."default",
    chat_id     bigint,
    iin         varchar(12) COLLATE pg_catalog."default" UNIQUE NOT NULL,
    role_id     integer                                         NOT NULL,
    position_id integer                                         NOT NULL,
    enabled     boolean                                         NOT NULL DEFAULT TRUE,
    CONSTRAINT fk_position
        FOREIGN KEY (position_id)
            REFERENCES positions (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_role
        FOREIGN KEY (role_id)
            REFERENCES roles (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE counterparty_types
(
    id   SERIAL PRIMARY KEY,
    name varchar(128) NOT NULL
);

CREATE TABLE counterparties
(
    id                   SERIAL PRIMARY KEY,
    name                 varchar(128) NOT NULL,
    minio_name           varchar(512),
    description          varchar(256),
    folder               varchar(256),
    address              varchar(256) NOT NULL,
    bin                  varchar(12)  NOT NULL,
    contact_name         varchar(128) NOT NULL,
    phone_number         varchar(128) NOT NULL,
    counterparty_type_id integer      NOT NULL,
    CONSTRAINT fk_counterparty_type
        FOREIGN KEY (counterparty_type_id)
            REFERENCES counterparty_types (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE object_companies
(
    id      SERIAL PRIMARY KEY,
    name    varchar(128) NOT NULL,
    address varchar(128) NOT NULL
);

CREATE TABLE object_locations
(
    id              SERIAL PRIMARY KEY,
    address         varchar(256) NOT NULL,
    name            varchar(128),
    folder          varchar(128),
    contact_name    varchar(128) NOT NULL,
    phone_number    varchar(128) NOT NULL,
    minio_name      varchar(512),
    counterparty_id integer      NOT NULL,
    object_id       integer,
    CONSTRAINT fk_counterparty
        FOREIGN KEY (counterparty_id)
            REFERENCES counterparties (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_object
        FOREIGN KEY (object_id)
            REFERENCES object_companies (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE projects
(
    id           SERIAL PRIMARY KEY,
    name         varchar(128) COLLATE pg_catalog."default" NOT NULL,
    description  varchar(512),
    created_date timestamp                                 NOT NULL,
    closed_date  timestamp,
    status_id    integer                                   NOT NULL,
    CONSTRAINT fk_status
        FOREIGN KEY (status_id)
            REFERENCES statuses (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE device_types
(
    id           SERIAL PRIMARY KEY,
    name         varchar(128) NOT NULL,
    author_id    integer      NOT NULL,
    created_date timestamp    NOT NULL,
    description  varchar(512),
    CONSTRAINT fk_author
        FOREIGN KEY (author_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE device_statuses
(
    id   SERIAL PRIMARY KEY,
    name varchar(128) NOT NULL
);

CREATE TABLE devices
(
    id               SERIAL PRIMARY KEY,
    author_id        integer      NOT NULL,
    created_date     timestamp    NOT NULL,
    name             varchar(128) NOT NULL,
    folder           varchar(128) NOT NULL,
    minio_name       varchar(512),
    description      varchar(512),
    type_id          integer      NOT NULL,
    location_id      integer      NOT NULL,
    inventory_number varchar(128),
    serial_number    varchar(128),
    status_id        integer      NOT NULL,
    CONSTRAINT fk_author
        FOREIGN KEY (author_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_type
        FOREIGN KEY (type_id)
            REFERENCES device_types (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_status
        FOREIGN KEY (status_id)
            REFERENCES device_statuses (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_local
        FOREIGN KEY (location_id)
            REFERENCES object_locations (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE device_comments
(
    id           SERIAL PRIMARY KEY,
    device_id    integer   NOT NULL,
    author_id    integer   NOT NULL,
    comment      varchar(512),
    created_date timestamp NOT NULL,
    CONSTRAINT fk_device
        FOREIGN KEY (device_id)
            REFERENCES devices (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_author
        FOREIGN KEY (author_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE device_histories
(
    id          SERIAL PRIMARY KEY,
    device_id   integer,
    description varchar(512),
    status_id   integer,
    CONSTRAINT fk_device
        FOREIGN KEY (device_id)
            REFERENCES devices (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_status
        FOREIGN KEY (status_id)
            REFERENCES device_statuses (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE applications
(
    id                 SERIAL PRIMARY KEY,
    author_id          integer                                   NOT NULL,
    executor_id        integer,
    created_date       timestamp                                 NOT NULL,
    name               varchar(128) COLLATE pg_catalog."default" NOT NULL,
    minio_name         varchar(512),
    folder             varchar(128),
    description        varchar(512),
    counterparty_id    integer,
    object_location_id integer,
    project_id         integer,
    client_price       double precision,
    executor_price     double precision,
    payer_id           integer,
    status_id          integer,
    planned_start_date timestamp,
    planned_due_date   timestamp,
    actual_start_date  timestamp,
    closed_date        timestamp,
    type_id            integer,
    priority_id        integer,
    is_active          boolean                                   NOT NULL DEFAULT TRUE,
    CONSTRAINT fk_author
        FOREIGN KEY (author_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_executor
        FOREIGN KEY (executor_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_location
        FOREIGN KEY (object_location_id)
            REFERENCES object_locations (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_status
        FOREIGN KEY (status_id)
            REFERENCES statuses (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_project
        FOREIGN KEY (project_id)
            REFERENCES projects (id) MATCH SIMPLE
            ON UPDATE CASCADE
            ON DELETE NO ACTION,
    CONSTRAINT fk_payer
        FOREIGN KEY (payer_id)
            REFERENCES counterparties (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_type
        FOREIGN KEY (type_id)
            REFERENCES application_types (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_priority
        FOREIGN KEY (priority_id)
            REFERENCES priorities (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE application_histories
(
    id                 SERIAL PRIMARY KEY,
    application_id     integer,
    status_id          integer,
    description        varchar(512),
    executor_id        integer,
    priority_id        integer,
    planned_start_date timestamp,
    planned_due_date   timestamp,
    client_price       double precision,
    executor_price     double precision,
    CONSTRAINT fk_application
        FOREIGN KEY (application_id)
            REFERENCES applications (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_status
        FOREIGN KEY (status_id)
            REFERENCES statuses (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_executor
        FOREIGN KEY (executor_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_priority
        FOREIGN KEY (priority_id)
            REFERENCES priorities (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE application_status_histories
(
    id             SERIAL PRIMARY KEY,
    application_id integer   NOT NULL,
    status_id      integer   NOT NULL,
    start_date     timestamp NOT NULL,
    CONSTRAINT fk_application
        FOREIGN KEY (application_id)
            REFERENCES applications (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_status
        FOREIGN KEY (status_id)
            REFERENCES statuses (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE application_comments
(
    id             SERIAL PRIMARY KEY,
    application_id integer                                    NOT NULL,
    created_date   timestamp                                  NOT NULL,
    comment_text   varchar(1024) COLLATE pg_catalog."default" NOT NULL,
    author_id      integer                                    NOT NULL,
    CONSTRAINT fk_application
        FOREIGN KEY (application_id)
            REFERENCES applications (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_author
        FOREIGN KEY (author_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE document_statuses
(
    id   SERIAL PRIMARY KEY,
    name varchar(128) NOT NULL

);

CREATE TABLE document_types
(
    id   SERIAL PRIMARY KEY,
    name varchar(128) NOT NULL
);

CREATE TABLE folders
(
    id             SERIAL PRIMARY KEY,
    name           varchar(128) NOT NULL,
    page_name      varchar(128) NOT NULL,
    auto           boolean      NOT NULL DEFAULT false,
    fix            boolean      NOT NULL DEFAULT false,
    document_count integer,
    size           integer      NOT NULL
);

CREATE TABLE documents
(
    id                 SERIAL PRIMARY KEY,
    name               varchar(256) NOT NULL,
    exchange           varchar(256),
    error              boolean      NOT NULL DEFAULT false,
    previous_name      varchar(256),
    folder_id          integer      NOT NULL,
    style              varchar(256),
    path               varchar(256),
    note               varchar(256),
    size               integer,
    author_id          integer      NOT NULL,
    created_date       timestamp    NOT NULL,
    document_status_id integer      NOT NULL,
    document_type      varchar(256),
    is_change          boolean      NOT NULL DEFAULT false,
    mark_delete        boolean      NOT NULL DEFAULT false,
    counterparty_id    integer,
    location_id        integer,
    device_id          integer,
    application_id     integer,

    CONSTRAINT fk_folder
        FOREIGN KEY (folder_id)
            REFERENCES folders (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_author
        FOREIGN KEY (author_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_document_status
        FOREIGN KEY (document_status_id)
            REFERENCES document_statuses (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_counterparty
        FOREIGN KEY (counterparty_id)
            REFERENCES "counterparties" (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_location
        FOREIGN KEY (location_id)
            REFERENCES object_locations (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_device
        FOREIGN KEY (device_id)
            REFERENCES devices (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_application
        FOREIGN KEY (application_id)
            REFERENCES applications (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE actions
(
    id           SERIAL PRIMARY KEY,
    user_id      integer   NOT NULL,
    created_date timestamp NOT NULL,
    action       smallint  NOT NULL,
    document_id  integer   NOT NULL,
    CONSTRAINT fk_user
        FOREIGN KEY (user_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_document
        FOREIGN KEY (document_id)
            REFERENCES documents (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE document_histories
(
    id                 SERIAL PRIMARY KEY,
    document_id        integer,
    created_date       timestamp,
    editor_id          integer,
    document_status_id integer,
    CONSTRAINT fk_document
        FOREIGN KEY (document_id)
            REFERENCES documents (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_editor
        FOREIGN KEY (editor_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_status
        FOREIGN KEY (document_status_id)
            REFERENCES document_statuses (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE document_comments
(
    id           SERIAL PRIMARY KEY,
    document_id  integer                                    NOT NULL,
    created_date timestamp                                  NOT NULL,
    comment_text varchar(1024) COLLATE pg_catalog."default" NOT NULL,
    author_id    integer                                    NOT NULL,
    CONSTRAINT fk_author
        FOREIGN KEY (author_id)
            REFERENCES users (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
    CONSTRAINT fk_document
        FOREIGN KEY (document_id)
            REFERENCES documents (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE common_tags
(
    id          SERIAL PRIMARY KEY,
    document_id integer NOT NULL,
    link        varchar(512),
    CONSTRAINT fk_document
        FOREIGN KEY (document_id)
            REFERENCES documents (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE application_devices
(
    id             SERIAL PRIMARY KEY,
    application_id integer not null,
    device_id      integer not null
);

CREATE TABLE histories
(
    id            SERIAL PRIMARY KEY,
    author_name   varchar(128) NOT NULL,
    created_date  timestamp    NOT NULL,
    entity_id     integer      NOT NULL,
    entity_type   varchar(128),
    entity_action varchar(128)
);

CREATE TABLE application_participants
(
    id             SERIAL PRIMARY KEY,
    application_id integer not null,
    participant_id integer not null
);

CREATE TABLE documents_trash
(
    id          SERIAL PRIMARY KEY,
    name        varchar(1024) NOT NULL,
    minio_id    varchar(512),
    date_delete timestamp     NOT NULL,
    bucket      varchar(512)  NOT NULL,
    link        varchar(1024) NOT NULL
);