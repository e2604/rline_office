<#import "../index.ftlh" as base>

<@base.layout title="Создание устройства">

    <div id="layout-wrapper">
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <#-- ДИРЕКТОРИЯ СТРАНИЦЫ-->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18" key="t-create-device">Создать Устройство</h4>
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item" key="t-devices">Устройства</li>
                                        <li class="breadcrumb-item active" key="t-create-device">Создать Устройство</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <#-- ФОРМА СОЗДАНИЯ-->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title mb-4" key="t-create-new-device">Создать Новое Устройство</h4>
                                    <form action="/device/add" method="post">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <input hidden name="author" id="author" type="number"
                                                       value=${(principal.getName())!''}>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label for="name" class="col-form-label col-lg-2 fw-bold"
                                                   key="t-name-field">Наименование:</label>
                                            <div class="col-lg-10">
                                                <input minlength="3" maxlength="60" required id="name" name="name"
                                                       type="text" class="form-control"
                                                       placeholder="Введите наименование">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="type" class="col-form-label col-lg-2 fw-bold"
                                                   key="t-type-field" style="padding-top: 22px">Тип:</label>
                                            <div class="col-lg-8 d-flex align-items-center">
                                                <#if types?has_content>
                                                    <select name="type" id="type" class="form-select text-muted">
                                                        <option selected value="" class="text-muted"
                                                                key="t-choose-type">Выберите тип
                                                        </option>
                                                        <#list types as type>
                                                            <option value=${type.getId()}>${type.getName()}</option>
                                                        </#list>
                                                    </select>
                                                </#if>
                                            </div>
                                            <#-- кнопка для СОЗДАНИЯ ОБЪЕКТА-->
                                            <div class="col-lg-2" style="padding: 14px 12px">
                                                <button type="button" class="btn btn-primary font-size-12"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#addDeviceType" style="width: 138px"
                                                        key="t-create-type">Создать Тип
                                                </button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="location" class="col-form-label col-lg-2 fw-bold"
                                                   key="t-location-field" style="padding-top: 22px">Location:</label>
                                            <div class="col-lg-8 d-flex align-items-center">
                                                <#if locations?has_content>
                                                    <select name="location" id="location"
                                                            class="form-select text-muted">
                                                        <option selected value="" key="t-choose-location">Выберите
                                                            локацию
                                                        </option>
                                                        <#list locations as location>
                                                            <option value=${location.id}>${location.address}</option>
                                                        </#list>
                                                    </select>
                                                </#if>
                                            </div>
                                            <#-- кнопка для СОЗДАНИЯ ЛОКАЦИИ-->
                                            <div class="col-lg-2" style="padding: 14px 12px">
                                                <button type="button" class="btn btn-primary font-size-12"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#addLocation" style="width: 138px"
                                                        key="t-create-location">Создать Локацию
                                                </button>
                                            </div>
                                        </div>
                                        <div class="row mb-2 mt-3">
                                            <label for="serialNumber" class="col-form-label col-lg-2 fw-bold"
                                                   key="t-serial-number-field">Серийный Номер:</label>
                                            <div class="col-lg-10">
                                                <input maxlength="16" required id="serialNumber"
                                                       name="serialNumber" type="text"
                                                       class="form-control"
                                                       placeholder="Введите серийный номер">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <#if statuses?has_content>
                                                    <#list statuses as status>
                                                        <label for="status">
                                                            <input hidden name="status" id="status" type="number" value=1>
                                                        </label>
                                                    </#list>
                                                </#if>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label for="inventoryNumber" class="col-form-label col-lg-2 fw-bold"
                                                   key="t-inventory-number-field">Инвентарный Номер:</label>
                                            <div class="col-lg-10">
                                                <input maxlength="12" required id="inventoryNumber"
                                                       name="inventoryNumber"
                                                       type="text"
                                                       class="form-control"
                                                       placeholder="Введите инвентарный номер">
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <label for="description" class="col-form-label col-lg-2 fw-bold"
                                                   key="t-description-field">Описание:</label>
                                            <div class="col-lg-10">
                                                <textarea class="form-control" name="description" id="description"
                                                          rows="5" required placeholder="Введите описание" minlength="3"
                                                          maxlength="200"></textarea>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end">
                                            <div class="col-lg-10">
                                                <button type="submit" class="btn btn-primary" key="t-create-device">
                                                    Создать Устройство
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <#-- модальное окно ДОБАВЛЕНИЯ ЛОКАЦИИ-->
                                    <div class="modal fade" id="addLocation" data-bs-backdrop="static"
                                         data-bs-keyboard="false" tabindex="-1"
                                         aria-labelledby="addLocationLabel" aria-hidden="true"
                                         style="background-color: rgba(0,0,0,0.62);">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h6 class="modal-title text-uppercase fw-bold"
                                                        id="addLocation" key="t-create-new-location">создать новую локацию</h6>
                                                    <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="/location/add-from-device" method="post">
                                                        <div class="mb-2">
                                                            <label class="form-label" style="font-weight: bold"
                                                                   for="address"
                                                                   key="t-address-field">Адрес:</label>
                                                            <input minlength="3" maxlength="50" type="text"
                                                                   class="form-control"
                                                                   name="address" id="address"
                                                                   placeholder="Введите адрес">
                                                        </div>
                                                        <div class="mb-2">
                                                            <label class="form-label" style="font-weight: bold"
                                                                   for="contactName"
                                                                   key="t-contact-person-field">Контактное
                                                                Лицо:</label>
                                                            <input minlength="3" maxlength="30" type="text"
                                                                   name="contactName"
                                                                   class="form-control" id="contactName"
                                                                   placeholder="Введите фамилию и имя контактного лица">
                                                        </div>
                                                        <div class="mb-2">
                                                            <label class="form-label" style="font-weight: bold"
                                                                   for="phoneNumber"
                                                                   key="t-phone-field">Телефон:</label>
                                                            <input type="text" name="phoneNumber"
                                                                   class="form-control" id="phoneNumber"
                                                                   placeholder="+7(700)000-00-00">
                                                        </div>
                                                        <div class="mb-3">
                                                            <label style="font-weight: bold" for="object"
                                                                   key="t-object-field">Объект:</label>
                                                            <#if objects?has_content>
                                                                <div>
                                                                    <select class="form-select mb-3 text-muted"
                                                                            name="objectId" id="object">
                                                                        <option value="" selected
                                                                                key="t-choose-object">Выберите объект
                                                                        </option>
                                                                        <#list objects as object>
                                                                            <option value=${object.id}>${object.name}</option>
                                                                        </#list>
                                                                    </select>
                                                                </div>
                                                            </#if>
                                                        </div>
                                                        <div class="mb-3">
                                                            <label style="font-weight: bold" for="counterparty"
                                                                   key="t-counterparty-field">Контрагент:</label>
                                                            <#if counterparties?has_content>
                                                                <div>
                                                                    <select class="form-select text-muted mb-3"
                                                                            name="counterpartyId"
                                                                            id="counterparty">
                                                                        <option value="" selected
                                                                                key="t-choose-counterparty">Выберите контрагента
                                                                        </option>
                                                                        <#list counterparties as counterparty>
                                                                            <option value=${counterparty.id}>${counterparty.name}</option>
                                                                        </#list>
                                                                    </select>
                                                                </div>
                                                            </#if>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary btn-sm"
                                                                key="t-create-location">Создать Локацию
                                                        </button>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary btn-sm"
                                                            data-bs-target="#addObject"
                                                            data-bs-toggle="modal" data-bs-dismiss="modal"
                                                            key="t-create-object">
                                                        Создать Объект
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <#-- модальное окно ДОБАВЛЕНИЯ ОБЪЕКТА-->
                                    <div class="modal fade" id="addObject" data-bs-backdrop="static"
                                         data-bs-keyboard="false" tabindex="-1"
                                         aria-labelledby="addObjectLabel"
                                         aria-hidden="true"
                                         style="background-color: rgba(0,0,0,0.62);">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h6 class="modal-title text-uppercase fw-bold"
                                                        id="addObject" key="t-create-new-object">создать новый объект</h6>
                                                    <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="/object/add-from-device" method="post">
                                                        <div class="mb-2">
                                                            <label class="form-label" style="font-weight: bold"
                                                                   for="name" key="t-object-field">Объект:</label>
                                                            <input minlength="3" maxlength="30" type="text"
                                                                   class="form-control"
                                                                   name="name" id="name"
                                                                   placeholder="Введите наименование">
                                                        </div>
                                                        <div class="mb-2">
                                                            <label class="form-label" style="font-weight: bold"
                                                                   for="address"
                                                                   key="t-address-field">Адрес:</label>
                                                            <input minlength="3" maxlength="50" type="text"
                                                                   name="address"
                                                                   class="form-control" id="address"
                                                                   placeholder="Введите адрес">
                                                        </div>
                                                        <button type="submit" class="btn btn-primary btn-sm mt-2"
                                                                key="t-create-object">Создать Объект
                                                        </button>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary btn-sm"
                                                            data-bs-target="#addLocation"
                                                            data-bs-toggle="modal"
                                                            data-bs-dismiss="modal"
                                                            key="t-back-to-location">
                                                        Назад к Локации
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <#-- модальное окно ДОБАВЛЕНИЯ ТИПА УСТРОЙСТВА-->
                                    <div class="modal fade" id="addDeviceType" data-bs-backdrop="static"
                                         data-bs-keyboard="false" tabindex="-1"
                                         aria-labelledby="addDeviceTypeLabel" aria-hidden="true"
                                         style="background-color: rgba(0,0,0,0.62);">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h6 class="modal-title text-uppercase fw-bold"
                                                        id="addDeviceTypeLabel" key="t-create-new-type">создать новый тип</h6>
                                                    <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <form id="deviceForm"
                                                          action="/device/add-device-type"
                                                          method="post">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col-lg-10">
                                                                    <input hidden name="author" id="author"
                                                                           type="number"
                                                                           value=${(principal.getName())!''}>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mb-2">
                                                            <label class="form-label" style="font-weight: bold"
                                                                   for="name" key="t-name-field">Наименование:</label>
                                                            <input minlength="3" maxlength="38"
                                                                   type="text"
                                                                   class="form-control"
                                                                   name="name" id="name"
                                                                   placeholder="Введите наименование">
                                                        </div>
                                                        <div class="mb-4">
                                                            <label class="form-label" style="font-weight: bold"
                                                                   for="description"
                                                                   key="t-description-field">Описание:</label>
                                                            <textarea name="description"
                                                                      class="form-control"
                                                                      id="description" minlength="3" maxlength="200"
                                                                      placeholder="Введите описание"></textarea>
                                                        </div>
                                                        <div class="d-flex justify-content-end">
                                                            <button type="submit"
                                                                    class="btn btn-primary btn-sm"
                                                                    key="t-create-type">Создать Тип
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/assets/js/imask.js"></script>
    <script>
        const phoneNumber = document.getElementById('phoneNumber');
        const maskOptions = {
            mask: '+{7}(700)000-00-00'
        };
        IMask(phoneNumber, maskOptions);
    </script>

<#-- создание типа устройства без перезагрузки, но новые данные не подтягиваются в текущий список-->
<#--    <script>-->
<#--        const deviceForm = document.getElementById("deviceForm")-->
<#--        deviceForm.addEventListener("submit",(e)=>{-->
<#--            e.preventDefault()-->
<#--            const formData = new FormData(e.currentTarget)-->
<#--            fetch("/device/add-device-type",{-->
<#--                method:"POST",-->
<#--                body:formData-->
<#--            }).then((response)=>{-->
<#--                return response.text()-->
<#--            }).then(html=>{-->
<#--                document.body.innerHTML = ""-->
<#--                document.body.innerHTML = html;-->
<#--            })-->
<#--        })-->
<#--    </script>-->

</@base.layout>