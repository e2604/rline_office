<#import "../index.ftlh" as base>

<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]/>

<@base.layout title="Обзор проекта">

    <div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <#-- ДИРЕКТОРИЯ СТРАНИЦЫ-->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18" key="t-project-overview">Обзор проекта</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item" key="t-projects">Проекты</li>
                                <li class="breadcrumb-item"><a href="/project/list" key="t-project-list"
                                                               data-toggle="tooltip"
                                                               title="Перейти к списку проектов">Список Проектов</a>
                                </li>
                                <li class="breadcrumb-item active" key="t-project-overview">Обзор Проекта</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <#-- КАРТОЧКА ПРОЕКТА-->
                <div class="col-md-4">
                    <div class="card" style="border-top: 3px solid; border-radius: 0">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <img src="assets/images/entities/project.png" alt="project"
                                     class="avatar-sm">
                                <h4 class="card-header bg-transparent"
                                    style="max-width: 260px;">${project.name}</h4>
                            </div>
                            <#-- СОСТОЯНИЕ ПРОЕКТА-->
                            <#if project.closedDate?has_content>
                                <h5 class="text-success mt-3 font-size-14">Проект закрыт</h5>
                            <#else>
                                <h5 class="text-warning mt-3 font-size-14">Проект в работе</h5>
                            </#if>
                            <#-- ЗАКРЫТЬ ПРОЕКТ-->
                            <div class="mt-4">
                                <#if project.closedDate?? == false>
                                    <form action="/project/overview/${project.id}/close" method="post">
                                        <button type="submit" class="btn btn-success btn-sm font-size-13"
                                                style="width: 80px" key="t-close" title="Закрыть проект">Закрыть
                                        </button>
                                    </form>
                                </#if>
                            </div>
                            <#-- ОПИСАНИЕ ПРОЕКТА-->
                            <div>
                                <h5 class="font-size-15 my-3" key="t-project-details">Детали Проекта:</h5>
                                <h5 class="font-size-14 fw-bold" key="t-description-field">Описание:</h5>
                                <h5 class="font-size-14">${project.description}</h5>
                                <#-- ДАТЫ ПРОЕКТА-->
                                <div class="mt-4">
                                    <div class="d-flex align-items-center">
                                        <i class="bx bx-calendar me-1 text-primary fw-bold"></i>
                                        <h5 class="font-size-14 mb-0 fw-bold" key="t-project-created-date">Дата создания</h5>
                                    </div>
                                    <p>${project.createdDate}</p>
                                    <#if project.closedDate?has_content>
                                        <div class="d-flex align-items-center">
                                            <i class="bx bx-calendar me-1 text-primary fw-bold"></i>
                                            <h5 class="font-size-14 mb-0 fw-bold" key="t-closed-date">Дата закрытия</h5>
                                        </div>
                                        <p>${project.closedDate}</p>
                                    </#if>
                                </div>
                            </div>
                            <#-- кнопки РЕДАКТИРОВАНИЯ и УДАЛЕНИЯ ПРОЕКТА-->
                            <@security.authorize access="hasAnyAuthority('ADMIN', 'MANAGER')">
                                <#if !project.closedDate?has_content>
                                    <div class="d-flex flex-column">
                                        <div class="d-flex justify-content-end mb-2">
                                            <button type="submit" class="btn btn-primary"
                                                    data-bs-target="#editProject"
                                                    data-bs-toggle="modal" key="t-edit"
                                                    style="width: 85px">
                                                Изменить
                                            </button>
                                        </div>
                                        <#--                                        <div class="d-flex justify-content-end">-->
                                        <#--                                            <button type="submit" class="btn btn-danger"-->
                                        <#--                                                    data-bs-target="#deleteProject" name="delete"-->
                                        <#--                                                    data-bs-toggle="modal" key="t-delete"-->
                                        <#--                                                    style="width: 85px">-->
                                        <#--                                                Удалить-->
                                        <#--                                            </button>-->
                                        <#--                                        </div>-->
                                    </div>
                                </#if>
                            </@security.authorize>
                            <#-- модальное окно РЕДАКТИРОВАНИЯ-->
                            <div class="modal fade" id="editProject"
                                 data-bs-backdrop="static"
                                 data-bs-keyboard="false" tabindex="-1"
                                 aria-labelledby="editProjectLabel" aria-hidden="true"
                                 style="background-color: rgba(0,0,0,0.62);">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="d-sm-flex">
                                                <h6 class="modal-title text-uppercase fw-bold pe-1"
                                                    id="editProject" key="t-edit">
                                                    Изменить</h6>
                                                <h6 class="modal-title text-uppercase fw-bold text-info">${project.name}</h6>
                                            </div>
                                            <button type="button" class="btn-close"
                                                    data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="/project/projects-overview/${project.id}/update"
                                                  method="post">
                                                <div class="mb-2">
                                                    <label class="form-label" style="font-weight: bold"
                                                           for="name"
                                                           key="t-name-field">Наименование:</label>
                                                    <input minlength="3" maxlength="50" type="text"
                                                           class="form-control"
                                                           name="name" id="name"
                                                           value="${(project.name)!''}">
                                                </div>
                                                <div class="mb-4">
                                                    <label class="form-label" style="font-weight: bold"
                                                           for="description"
                                                           key="t-description-field">Описание:</label>
                                                    <textarea
                                                            name="description"
                                                            class="form-control"
                                                            rows="4"
                                                            cols="50"
                                                            minlength="3" maxlength="200">${(project.description)!""}</textarea>
                                                </div>
                                                <div class="d-flex justify-content-end">
                                                    <button type="submit"
                                                            class="btn btn-primary" style="width: 100px"
                                                            key="t-edit">Изменить
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <#-- модальное окно УДАЛЕНИЯ-->
                            <div class="modal fade" id="deleteProject"
                                 data-bs-backdrop="static"
                                 data-bs-keyboard="false" tabindex="-1"
                                 aria-labelledby="deleteProjectLabel"
                                 aria-hidden="true" style="background-color: rgba(0,0,0,0.62)">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <form action="/project/overview/${project.id}/delete"
                                              method="post">
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="deleteProject"
                                                    key="t-delete-project">Вы действительно хотите
                                                    удалить проект?</h5>
                                                <button type="button"
                                                        class="btn-close"
                                                        data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button"
                                                        class="btn btn-danger"
                                                        data-bs-dismiss="modal"
                                                        key="t-no">Нет
                                                </button>
                                                <button type="submit"
                                                        class="btn btn-success"
                                                        key="t-yes">Да
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <#-- ЛИСТ ЗАЯВОК-->
                <div class="col-md-8">
                    <div class="card"
                         style="border-top: 3px solid; border-radius: 0; max-height: 760px; overflow-y: auto; overflow-x: hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <img src="assets/images/entities/application.png" alt="application"
                                     class="avatar-sm">
                                <h4 class="ps-2 bg-transparent m-0" key="t-applications">
                                    Заявки</h4>
                            </div>
                            <#if projects.getApplicationsCount(project.id) = 0>
                                <h5 class="font-size-14 mt-4" key="t-project-no-applications">У проекта нет
                                    ни одной заявки</h5>
                            <#else>
                                <div class="d-flex align-items-center mt-4">
                                    <h5 class="mb-0"><span class="ps-2 badge badge-soft-info font-size-12"
                                                           key="t-total-applications">Всего
                                                                заявок:</span></h5>
                                    <h5 class="mb-0">
                                        <span class="ms-1 badge badge-soft-info font-size-12">${projects.getApplicationsCount(project.id)}</span>
                                    </h5>
                                </div>
                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col" key="t-name">Наименование</th>
                                            <th scope="col" key="t-status">Статус</th>
                                            <th scope="col" key="t-executor">Исполнитель</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <#if projects.getApplicationsByProjectId(project.id)?has_content>
                                            <#list projects.getApplicationsByProjectId(project.id) as application>
                                                <tr>
                                                    <td>#{application?counter}</td>
                                                    <td><h5 class="font-size-14 m-0"><a
                                                                    href="/applications/${application.id}"
                                                                    class="text-info"
                                                                    data-toggle="tooltip"
                                                                    title="Перейти к профилю заявки">${application.name}</a>
                                                        </h5></td>
                                                    <td>
                                                        <#if application.status.id = 1>
                                                            <span class="badge bg-warning font-size-12">${application.status.name}</span>
                                                        <#elseif application.status.id = 2>
                                                            <span class="badge bg-primary font-size-12">${application.status.name}</span>
                                                        <#elseif application.status.id = 3>
                                                            <span class="badge bg-success font-size-12">${application.status.name}</span>
                                                        <#elseif application.status.id = 4>
                                                            <span class="badge bg-danger font-size-12">${application.status.name}</span>
                                                        <#else>
                                                            <span class="badge bg-secondary font-size-12">${application.status.name}</span>
                                                        </#if>
                                                    </td>
                                                    <td><h5 class="font-size-14 m-0"><a
                                                                    href="/user/profile/${application.executor.id}"
                                                                    class="table"
                                                                    data-toggle="tooltip"
                                                                    title="Перейти к профилю исполнителя">${application.executor.fullName}</a>
                                                        </h5></td>
                                                </tr>
                                            </#list>
                                        </#if>
                                        </tbody>
                                    </table>
                                </div>
                            </#if>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</@base.layout>