<#import "../index.ftlh" as base >

<@base.layout title="Создание заявки">

    <div id="layout-wrapper">
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <#-- ДИРЕКТОРИЯ СТРАНИЦЫ-->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18" key="t-create-application">Создать Заявку</h4>
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item" key="t-applications">Заявки</li>
                                        <li class="breadcrumb-item active" key="t-create-application">Создать Заявку
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <#-- ФОРМА СОЗДАНИЯ-->
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title mb-4" key="t-create-new-application">Создать Новую Заявку</h4>
                                    <form name="myForm" method="post" action="/applications">
                                        <div class="mb-3">
                                            <input hidden name="author" id="author" type="number"
                                                   value=${(principal.getName())!''}>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="name" class="form-label fw-bold" key="t-name-field">Наименование:</label>
                                                    <input minlength="3" maxlength="50" type="text" name="name"
                                                           class="form-control" id="name" required
                                                           placeholder="Введите наименование">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="project" class="form-label fw-bold"
                                                           key="t-project-field">Проект:</label>
                                                    <select id="project" name="project"
                                                            class="form-select project-select text-muted">
                                                        <option disabled selected value key="t-choose-project">Выберите проект
                                                        </option>
                                                        <#list projects as project>
                                                            <option value="${project.id}">${project.name}</option>
                                                        </#list>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="counterparty" class="form-label fw-bold" key="t-customer-field">Заказчик:</label>
                                                    <select id="counterparty" name="counterparty"
                                                            class="form-select counterparty-select text-muted" required>
                                                        <option disabled selected value key="t-choose-customer">Выберите заказчика
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="location" class="form-label fw-bold"
                                                           key="t-address-field">Адрес:</label>
                                                    <select id="location" name="location"
                                                            class="form-select location-select text-muted" required>
                                                        <option key="t-address-empty">Адреса появятся после выбора заказчика
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="type" class="form-label fw-bold" key="t-type-field">Тип:</label>
                                                    <select id="type" name="type" class="form-select type-select text-muted"
                                                            required>
                                                        <option disabled selected value key="t-choose-type">
                                                            Выберите тип
                                                        </option>
                                                        <#list types as type>
                                                            <option value="${type.id}">${type.name}</option>
                                                        </#list>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="priority" class="form-label fw-bold"
                                                           key="t-priority-field">Приоритет:</label>
                                                    <select id="priority" name="priority"
                                                            class="form-select priority-select text-muted" required>
                                                        <option disabled selected value
                                                                key="t-choose-priority">Выберите приоритет
                                                        </option>
                                                        <#list priorities as priority>
                                                            <option value="${priority.id}">${priority.name}</option>
                                                        </#list>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="executor" class="form-label fw-bold"
                                                           key="t-executor-field">Исполнитель:</label>
                                                    <select id="executor" name="executor" class="form-select text-muted" required>
                                                        <option disabled selected value key="t-choose-executor">Выберите исполнителя
                                                        </option>
                                                        <#list users.getAll() as user>
                                                            <#if user.position.id == 4>
                                                                <option value="${user.id}">${user.getName()} ${user.getSurname()}</option>
                                                            </#if>
                                                        </#list>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="executorPrice" class="form-label fw-bold"
                                                           key="t-executor-price-field">Оплата исполнителю:</label>
                                                    <input type="text" name="executorPrice" class="form-control"
                                                           id="executorPrice" placeholder="Введите сумму для исполнителя" required
                                                           maxlength="7">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="payer" class="form-label fw-bold" key="t-payer-field">Плательщик:</label>
                                                    <select id="payer" name="payer" class="form-select payer-select text-muted"
                                                            required>
                                                        <option disabled selected value key="t-choose-payer">Выберите плательщика
                                                        </option>
                                                        <#list counterparties as counterparty>
                                                            <option value="${counterparty.id}">${counterparty.name}</option>
                                                        </#list>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="clientPrice" class="form-label fw-bold" key="t-customer-price-field">Оплата от заказчика:</label>
                                                    <input type="text" name="clientPrice" class="form-control"
                                                           id="clientPrice" placeholder="Введите сумму для заказчика" required
                                                           maxlength="7">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="planedStartDate" class="form-label fw-bold" key="t-planned-start-date-field">Дата планируемого начала:</label>
                                                    <input type="date" name="planedStartDate" class="form-control"
                                                           id="planedStartDate" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="planedDueDate" class="form-label fw-bold" key="t-due-date-field">Срок выполнения:</label>
                                                    <input type="date" name="planedDueDate" class="form-control"
                                                           id="planedDueDate" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 d-flex flex-column">
                                                    <label for="participants" class="form-label fw-bold" key="t-participants-field">Участники:</label>
                                                    <select id="participants" name="participants"
                                                            class="select2 form-select select2-multiple text-muted"
                                                            multiple="multiple" data-placeholder="Выберите участников">
                                                        <#list users.getAllParticipants() as user>
                                                            <option value="${user.id}">${user.fullName} - ${user.position.name}</option>
                                                        </#list>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3 d-flex flex-column">
                                                    <label for="devices" class="form-label fw-bold"
                                                           key="t-devices-field">Устройства:</label>
                                                    <select id="devices" name="devices"
                                                            class="select2 form-control form-select select2-multiple text-muted"
                                                            multiple="multiple" data-placeholder="Выберите устройства">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="description" class="form-label fw-bold"
                                                   key="t-description-field">Описание:</label>
                                            <textarea minlength="3" maxlength="200" class="form-control"
                                                      name="description" id="description" rows="5" required
                                                      placeholder="Введите описание"></textarea>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-primary w-md" key="t-create-application">Создать Заявку
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/assets/js/application-create.js"></script>

<#--    <script src="/assets/js/imask.js"></script>-->
<#--    <script>-->
<#--        let priceMask = IMask(-->
<#--            document.getElementById('executorPrice'),-->
<#--            {-->
<#--                mask: Number,-->
<#--                min: 0,-->
<#--                max: 999_999,-->
<#--                thousandsSeparator: ' '-->
<#--            });-->
<#--    </script>-->

<#--    <script>-->
<#--        let priceMask2 = IMask(-->
<#--            document.getElementById('clientPrice'),-->
<#--            {-->
<#--                mask: Number,-->
<#--                min: 0,-->
<#--                max: 999_999,-->
<#--                thousandsSeparator: ' '-->
<#--            });-->
<#--    </script>-->

</@base.layout>