let current_page = 1;
let records_per_page = 2;
let objJson = [];
const allUrl = '/applications/allComments';

function showAllList() {
    fetch(allUrl).then(response => response.json().then(counterparties => showAll2(counterparties)));
}

async function showAll2(json) {
    let test = [];
    test = json;
    objJson = [];
    objJson = test;
    await changePage2(1);
}

showAllList();

function getCount(id) {
    let count = 0;
    const size = fetch('http://localhost:8888/apiDocument/counterFilesCount/' + id).then(response => response.json()).then(result => count = result);
    return count; //не ставит резултат fetch запроса
}

function prevPage2() {
    if (current_page > 1) {
        current_page--;
        changePage2(current_page);
    }
}

function nextPage2() {
    if (current_page < numPages()) {
        current_page++;
        changePage2(current_page);
    }
}

function changePage2(page) {
    let btn_next2 = document.getElementById("btn_next2");
    let btn_prev2 = document.getElementById("btn_prev2");
    let page_span2 = document.getElementById("page1");
    let fileCount = document.querySelector(".app-all-list");

    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    fileCount.innerHTML = "";
    let a = 1;
    for (let i = (page - 1) * records_per_page; i < (page * records_per_page) && i < objJson.length; i++) {

        let classes = "mb-0 badge font-size-11 ";
        if (objJson[i].status.id === 1) {
            classes += "bg-warning";
        } else if (objJson[i].status.id === 2) {
            classes += "bg-primary";
        } else if (objJson[i].status.id === 3) {
            classes += "bg-success";
        } else if (objJson[i].status.id === 4) {
            classes += "bg-danger";
        } else {
            classes += "bg-secondary";
        }

        let priorities = "mb-0 badge font-size-11 ";
        if (objJson[i].priority.id === 1) {
            priorities += "bg-primary";
        } else if (objJson[i].priority.id === 2) {
            priorities += "bg-warning";
        } else {
            priorities += "bg-danger";
        }

        fileCount.innerHTML += `<tr>
                                            <td class="ps-4">${a++}</td>
                                            <td><h5 class="text-truncate font-size-14 mb-0" style="max-width: 240px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap">
                                            <a href="/applications/${objJson[i].id}" class="table" data-toggle="tooltip" title="Перейти к профилю заявки">${objJson[i].name}</a></h5></td>
                                            <td><h6 class="${classes}">${objJson[i].status.name}</h6></td>
                                            <td><h6 class="${priorities}">${objJson[i].priority.name}</h6></td>
                                            <td><p class="mb-0">${objJson[i].fullExecutorName}</p></td>
                                            <td><p class="mb-0">${objJson[i].planedDueDate}</p></td>
                                            <td><p class="mb-0" style="max-width: 240px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap">${objJson[i].location.address}</p></td>
                                            </tr>`;
        page_span2.innerHTML = page + "/" + numPages();
    }
}

function numPages() {
    return Math.ceil(objJson.length / records_per_page);
}

const postData = async (url, data) => {
    const find = fetch(url + data).then(temp => temp.json()).then(response => refreshList(response));
}

function refreshList(json) {
    let test = [];
    test = json;
    objJson = [];
    objJson = test;
    changePage2(1);
}

function isEmpty(str) {
    return str.trim() === '';
}

window.onload = function () {
    changePage2(1);
};