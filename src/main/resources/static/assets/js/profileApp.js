let current_page = 1;
let records_per_page = 8;
let objJson = [];
const searchUrl = window.location.pathname + '/apiApp/search/';
const allUrl = '/apiApp/all';

function showAllList(){
    fetch(window.location.pathname + allUrl).then(response => response.json().then(counterparties => showAll2(counterparties)));
}

async function showAll2(json) {
    let test = [];
    test = json;
    objJson = [];
    objJson = test;
    await changePage2(1);
}

showAllList();


function getCount(id) {
    let count = 0;
    const size = fetch('http://localhost:8888/apiDocument/counterFilesCount/' + id).then(response => response.json()).then(result => count = result);
    return count; //не ставит резултат fetch запроса
}

function prevPage2() {
    if (current_page > 1) {
        current_page--;
        changePage2(current_page);
    }
}

function nextPage2() {
    if (current_page < numPages()) {
        current_page++;
        changePage2(current_page);
    }
}


function changePage2(page) {
    let btn_next2 = document.getElementById("btn_next2");
    let btn_prev2 = document.getElementById("btn_prev2");
    let page_span2 = document.getElementById("page1");

    let fileCount = document.querySelector(".user-app-profile");

    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();


    fileCount.innerHTML = "";
    let a = 1;
    for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < objJson.length; i++) {
        fileCount.innerHTML += `<!-- <table class="table table-nowrap table-hover mb-0">-->
<!--                                    <thead>-->
<!--                                    <tr>-->
<!--                                        <th scope="col">#</th>-->
<!--                                        <th scope="col">Заявка</th>-->
<!--                                        <th scope="col">Статус</th>-->
<!--                                        <th scope="col">Приоритет</th>-->
<!--                                        <th scope="col">Сделать до</th>-->
<!--                                        <th scope="col">Кто?</th>-->
<!--                                    </tr>-->
<!--                                    </thead>-->
<!--                                    <tbody>-->
                                            <tr>
                                                <td class="align-middle ps-3">${a++}</td>
                                                <td><h5 class="font-size-14 m-0" style="max-width: 150px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap"><a
                                                                href="/applications/${objJson[i].id}"
                                                                class="text-dark">${objJson[i].name}</a></h5></td>
                                                
                                                    <td class="align-middle">
                                                        <h6 class="mb-0"><span class="badge bg-info">${objJson[i].status.name}</span></h6>
                                                    </td>
                                               
                                                    <td class="align-middle">
                                                        <h6 class="mb-0"><span class="badge bg-primary">${objJson[i].priority.name}</span></h6>
                                                    </td>
                                                
                                                <td class="align-middle">${objJson[i].planedDueDate}</td>
<!--                                                <#if application.getExecutor().id = user.id>-->
<!--                                                    <td class="align-middle">-->
<!--                                                        <h6 class="mb-0"><span class="badge bg-primary">Исполнитель</span></h6>-->
<!--                                                    </td>-->
<!--                                                <#elseif application.getExecutor().id != user.id>-->
<!--                                                    <td class="align-middle">-->
<!--                                                        <h6 class="mb-0"><span class="badge bg-info">Участник</span></h6>-->
<!--                                                    </td>-->
<!--                                                </#if>-->
                                            </tr>
                                
<!--                                    </tbody>-->
<!--                                </table>-->`;
        page_span2.innerHTML = page + "/" + numPages();
    }


}

function numPages() {
    return Math.ceil(objJson.length / records_per_page);
}


let formDiv = document.querySelector(".form-search");

let formNew = document.createElement('form');
formNew.method = 'post';
formNew.classList.add('mt-4', 'mt-sm-0', 'float-sm-end', 'd-flex', 'align-items-center', 'form-input-js');
let divSearch = document.createElement('div');
divSearch.classList.add('search-box', 'mb-2', 'me-2');
formNew.append(divSearch);
let divPosition = document.createElement('div');
divPosition.classList.add('position-relative');
divSearch.append(divPosition);
let inputSearch = document.createElement('input');
inputSearch.classList.add('form-control', 'bg-light', 'border-light', 'rounded');
inputSearch.type = 'text';
inputSearch.name = 'search';
inputSearch.placeholder = 'Search...';
divPosition.append(inputSearch);
let iSea = document.createElement('i');
iSea.classList.add('bx', 'bx-search-alt', 'search-icon');
divPosition.append(iSea);
let buttonForm = document.createElement('button');
buttonForm.type ='submit';
buttonForm.innerText = 'Найти';
buttonForm.style = 'min-width: 90px; margin-bottom: 8px;';
buttonForm.classList.add("btn", "btn-primary", "waves-effect", "waves-light");
formNew.append(buttonForm);
formDiv.append(formNew);

formNew.addEventListener('submit',(e) =>{
    e.preventDefault();
    let form = e.target;
    let formData = new FormData(form);
    let data = Object.fromEntries(formData);
    let post = data['search'];
    if (isEmpty(post)){
        const find = postData(allUrl, post);
    }else {
        const find2 = postData(searchUrl, post);
    }

})

const postData = async (url,data) =>{
    const find = fetch(url + data).then(temp => temp.json()).then(response=>refreshList(response));
}

function refreshList(json){
    let test = [];
    test = json;
    objJson = [];
    objJson = test;
    changePage2(1);
}


function isEmpty(str) {
    return str.trim() === '';
}

window.onload = function () {
    changePage2(1);
};


// const postData = async (url,data) =>{
//     const res = await fetch(url,{
//         method:"GET",
//         headers: {
//             'Content-type': 'application/json'
//         },
//         body:data
//     });
//     console.log(res);
//     return await res.json();
//
// }