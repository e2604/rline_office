let current_page = 1;
let records_per_page = 8;
let objJson = [];
const searchUrl ='/apiAppUser/searchUsers/';
const allUrl = '/apiAppUser/allUsers';

function showAllList(){
    fetch(allUrl).then(response => response.json().then(counterparties => showAll2(counterparties)));
}

async function showAll2(json) {
    let test = [];
    test = json;
    objJson = [];
    objJson = test;
    await changePage2(1);
}

showAllList();

function getCount(id) {
    let count = 0;
    const size = fetch('http://localhost:8888/apiDocument/counterFilesCount/' + id).then(response => response.json()).then(result => count = result);
    return count; //не ставит резултат fetch запроса
}

function prevPage2() {
    if (current_page > 1) {
        current_page--;
        changePage2(current_page);
    }
}

function nextPage2() {
    if (current_page < numPages()) {
        current_page++;
        changePage2(current_page);
    }
}

function changePage2(page) {
    let btn_next2 = document.getElementById("btn_next2");
    let btn_prev2 = document.getElementById("btn_prev2");
    let page_span2 = document.getElementById("page1");
    let fileCount = document.querySelector(".user-all-list");
    document.getElementById("total").innerHTML = objJson.length.toString();

    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    fileCount.innerHTML = "";
    let a = 1;
    for (let i = (page - 1) * records_per_page; i < (page * records_per_page) && i < objJson.length; i++) {
        fileCount.innerHTML += `<tr>
                                                <td class="ps-4">${a++}</td>
                                                <td><h5 class="font-size-14 mb-0"><a href="/user/profile/${objJson[i].id}" class="text-info" data-toggle="tooltip" title="Перейти к профилю пользователя">${objJson[i].fullName}</a></h5></td>
                                                <td><h5 class="font-size-14 mb-0">${objJson[i].phone}</h5></td>
                                                <td><h5 class="font-size-14 mb-0">${objJson[i].email}</h5></td>
                                                <td><h5 class="font-size-14 mb-0">${objJson[i].position.name}</h5></td>
                                                <td><h5 class="font-size-14 mb-0">${objJson[i].role.role}</h5></td>
                                            </tr>`;
        page_span2.innerHTML = page + "/" + numPages();
    }
}

function numPages() {
    return Math.ceil(objJson.length / records_per_page);
}

let formDiv = document.querySelector(".form-search");

let formNew = document.createElement('form');
formNew.method = 'post';
formNew.classList.add('mt-4', 'mt-sm-0', 'float-sm-end', 'd-flex', 'align-items-center', 'form-input-js');
let divSearch = document.createElement('div');
divSearch.classList.add('search-box', 'mb-2', 'me-2');
formNew.append(divSearch);
let divPosition = document.createElement('div');
divPosition.classList.add('position-relative');
divSearch.append(divPosition);
let inputSearch = document.createElement('input');
inputSearch.classList.add('form-control', 'bg-light', 'border-light', 'rounded');
inputSearch.style = 'padding-left: 30px';
inputSearch.type = 'text';
inputSearch.name = 'search';
inputSearch.placeholder = 'Поиск...';
inputSearch.key = 't-search';
divPosition.append(inputSearch);
let iSea = document.createElement('i');
iSea.classList.add('bx', 'bx-search-alt', 'search-icon');
divPosition.append(iSea);
let buttonForm = document.createElement('button');
buttonForm.type ='submit';
buttonForm.innerText = 'Найти';
buttonForm.style = 'min-width: 90px; margin-bottom: 8px;';
buttonForm.classList.add("btn", "btn-primary", "waves-effect", "waves-light");
formNew.append(buttonForm);
formDiv.append(formNew);

formNew.addEventListener('submit',(e) =>{
    e.preventDefault();
    let form = e.target;
    let formData = new FormData(form);
    let data = Object.fromEntries(formData);
    let post = data['search'];
    if (isEmpty(post)){
        const find = postData(allUrl, post);
    }else {
        const find2 = postData(searchUrl, post);
    }
})

const postData = async (url,data) =>{
    const find = fetch(url + data).then(temp => temp.json()).then(response=>refreshList(response));
}

function refreshList(json){
    let test = [];
    test = json;
    objJson = [];
    objJson = test;
    changePage2(1);
}

function isEmpty(str) {
    return str.trim() === '';
}

window.onload = function () {
    changePage2(1);
};

// const postData = async (url,data) =>{
//     const res = await fetch(url,{
//         method:"GET",
//         headers: {
//             'Content-type': 'application/json'
//         },
//         body:data
//     });
//     console.log(res);
//     return await res.json();
//
// }