let current_page = 1;
let records_per_page = 10;
let objJson = [];
let intermediate = [];
let searchSave = [];
let emptySearch = true;
let link = "";
//
// const searchUrl = '/apiDocument/documentSearch/';
const searchUrlPost = '/apiDocument/documentSearch';
const allUrl = '/apiDocument/all';
const deleteUrl = '/apiDocument/removeObject/';
const downloadUrl = '/apiDocument/download/';
const exNameUrl = '/apiDocument/ex-name';
const linkUrl = '/apiDocument/link/';
const uploadUrl = '/apiDocument/upload-admin/';
const sendUrl = '/apiDocument/transfer';
const putMarkUrl = '/apiDocument/putMark/';
let info = document.querySelector('.message-info');

function showAllList() {
    fetch(allUrl).then(response => response.json().then(counterparties => showAll2(counterparties)));
}

async function showAll2(json) {
    let test = [];
    test = json;
    intermediate = [];
    objJson = [];
    objJson = test;
    objJson.sort((a, b) => a.createdDate - b.createdDate);
    intermediate = test;
    await changePage2(1);
}


function getCount(id) {
    let count = 0;
    const size = fetch('http://localhost:8888/apiDocument/counterFilesCount/' + id).then(response => response.json()).then(result => count = result);
    return count; //не ставит резултат fetch запроса
}

function prevPage2() {
    if (current_page > 1) {
        current_page--;
        changePage2(current_page);
    }
}

function nextPage2() {
    if (current_page < numPages()) {
        current_page++;
        changePage2(current_page);
    }
}

async function changePage2(page) {
    let btn_next2 = document.getElementById("btn_next2");
    let btn_prev2 = document.getElementById("btn_prev2");
    let page_span2 = document.getElementById("page1");

    let fileCount = document.querySelector(".files-table");

    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();


    fileCount.innerHTML = "";

    for (let i = (page - 1) * records_per_page; i < (page * records_per_page) && i < objJson.length; i++) {
        let trMain = document.createElement('tr');
        let tdImage = document.createElement('td');
        let tdDate = document.createElement('td');
        let tdAuthor = document.createElement('td');
        let tdCounter = document.createElement('td');
        let tdStatus = document.createElement('td');
        let tdSize = document.createElement('td');
        let tdAction = document.createElement('td');
        tdAction.style = "display:flex; justify-content: space-around; max-width:290px;align-items: center;";

        let aDocImage = document.createElement('a');
        let counterPage = document.createElement('a');
        let spanStatus = document.createElement('span');
        spanStatus.innerText = objJson[i].status.name;

        //Вывод папки нужной


        if (objJson[i].application!=null){
            counterPage.href = "/applications/" + objJson[i].application.id;
        }else if(objJson[i].device!=null){
            counterPage.href = "/device/overview/" + objJson[i].device.id;
        }else if(objJson[i].location!=null){
            counterPage.href = "/location/overview/" + objJson[i].location.id;
        }else if(objJson[i].counterparty!=null){
            counterPage.href = "/counterparty/overview/" + objJson[i].counterparty.id;
        }else {
            counterPage.href = "/folderView/" + objJson[i].folder.name;
        }

        //end show folder

        let iImage = document.createElement('i');
        let iMark = document.createElement('i');
        iMark.classList.add("mdi", "mdi-delete-clock", "font-size-18", "align-middle", "text-danger", "me-2")

        if (objJson[i].markDelete === true) {
            aDocImage.append(iMark);
        }

        let linkDocument = document.createElement('a');
        linkDocument.innerText = objJson[i].exchange;
        linkDocument.href = "/documentView/" + objJson[i].id;
        linkDocument.style = "text-decoration:none";
        aDocImage.append(iImage);
        aDocImage.append(linkDocument);


        let aDownload = document.createElement('a');
        aDownload.style = "text-decoration: none;";

        if (objJson[i].counterparty === null) {
            aDownload.href = "/apiDocument/download/" + objJson[i].id + "/" + objJson[i].folder.name + "/" + objJson[i].name;
        } else {
            aDownload.href = "/apiDocument/download/" + objJson[i].id + "/" + objJson[i].folder.name + "/" + objJson[i].name;
        }

        let buttonDownload = document.createElement('button');
        buttonDownload.title = "Скачать";
        buttonDownload.style = "height: 20px; display: flex; justify-content: center; align-items: center; min-width: 40px;"
        buttonDownload.classList.add("btn", "btn-secondary", "waves-effect", "waves-light", "w-sm");
        // buttonDownload.innerText = "Download";
        let iButtonDownload = document.createElement('i');
        iButtonDownload.classList.add("mdi", "mdi-download", "d-block", "font-size-6")
        buttonDownload.append(iButtonDownload);
        aDownload.append(buttonDownload);



        let buttonLink = document.createElement('button');
        buttonLink.style = "height: 20px; display: flex; justify-content: center; align-items: center; min-width: 40px; border: 1px solid;"
        buttonLink.classList.add("btn", "btn-link", "waves-effect", "waves-light", "w-sm");
        buttonLink.id = "sa-position";
        buttonLink.title = "Получить ссылку на документ";
        // buttonDownload.innerText = "Download";
        let iButtonLink = document.createElement('i');
        iButtonLink.classList.add("mdi", "mdi-dot-net", "d-block", "font-size-16")
        buttonLink.append(iButtonLink);

        let buttonMarkDelete = document.createElement('button');
        buttonMarkDelete.style = "height: 20px; display: flex; justify-content: center; align-items: center; min-width: 40px;"
        buttonMarkDelete.classList.add("btn", "btn-warning", "waves-effect", "waves-light", "w-sm");
        // buttonDownload.innerText = "Download";
        let iButtonMarkDelete = document.createElement('i');
        iButtonMarkDelete.classList.add("mdi", "mdi-delete-alert", "d-block", "font-size-6")
        buttonMarkDelete.append(iButtonMarkDelete);
        buttonMarkDelete.title = "Поставить/снять поментку на удаление"


        let formSendFile = document.createElement('form');
        let inputFileInfo = document.createElement('input');
        let inputFileBucket = document.createElement('input');
        let inputFileExchangeName = document.createElement('input');
        let inputDocId = document.createElement('input');

        formSendFile.append(inputFileInfo);
        inputFileInfo.hidden = true;
        inputFileInfo.type = "text";
        inputFileInfo.value = objJson[i].name;
        inputFileInfo.name = "fileName";


        formSendFile.append(inputFileBucket);
        inputFileBucket.hidden = true;
        inputFileBucket.type = "text";
        inputFileBucket.value = objJson[i].folder.name;
        inputFileBucket.name = "folder";

        formSendFile.append(inputFileExchangeName);
        inputFileExchangeName.hidden = true;
        inputFileExchangeName.type = "text";
        inputFileExchangeName.value = objJson[i].exchange;
        inputFileExchangeName.name = "exName";

        formSendFile.append(inputDocId);
        inputDocId.hidden = true;
        inputDocId.type = "number";
        inputDocId.value = objJson[i].id;
        inputDocId.name = "docId";


        let buttonSend = document.createElement('button');
        buttonSend.title = "Оправить";
        buttonSend.id = "sa-params";
        buttonSend.type = "submit";
        buttonSend.style = "height: 20px; display: flex; justify-content: center; align-items: center; min-width: 40px;";
        buttonSend.classList.add("btn", "btn-info", "waves-effect", "waves-light", "w-sm");
        let iButtonSend = document.createElement('i');
        iButtonSend.classList.add("mdi", "mdi-send", "d-block", "font-size-6")
        buttonSend.append(iButtonSend);
        formSendFile.append(buttonSend);

        formSendFile.addEventListener('submit', (e) => {
            e.preventDefault();
            e.stopPropagation();
            let form = e.target;
            let formData = new FormData(form);
            let data = Object.fromEntries(formData);
            let jsonData = JSON.stringify(data);

            let jsonParseFile = JSON.parse(jsonData);

            let inputFileName = document.querySelector(".name-file");
            let inputFolderName = document.querySelector(".folder-name");
            let inputExcahngeName = document.querySelector(".ex-name");
            let inputID = document.querySelector(".doc-id");

            inputFileName.placeholder = jsonParseFile.fileName;

            inputFileName.value = jsonParseFile.fileName;
            inputFolderName.value = jsonParseFile.folder;
            inputExcahngeName.value = jsonParseFile.exName;
            inputID.value = jsonParseFile.docId;

        })

        function onClickEvent(e) {
            try {
                console.log(e.target.value); // or `arguments[0].target.value`
                let inputFileName = document.querySelector(".name-file");
                inputFileName.placeholder = e.name;
            } catch (error) {
                console.log(error);
            }
        }

        let buttonDelete = document.createElement('button');
        buttonDelete.title = "Удалить";
        buttonDelete.id = "sa-params";
        buttonDelete.style = "height: 20px; display: flex; justify-content: center; align-items: center; min-width: 40px;";
        buttonDelete.classList.add("btn", "btn-danger", "waves-effect", "waves-light", "w-sm");
        let iButtonDelete = document.createElement('i');
        iButtonDelete.classList.add("mdi", "mdi-trash-can", "d-block", "font-size-6")
        buttonDelete.append(iButtonDelete);


        if (objJson[i].status.name.toLowerCase() === "определен") {
            spanStatus.classList.add("badge", "bg-success");
        } else {
            spanStatus.classList.add("badge", "bg-warning");
        }

        if (objJson[i].type.toUpperCase() === "IMAGE/PNG") {
            iImage.classList.add("mdi", "mdi-image", "font-size-16", "align-middle", "text-success", "me-2");
        } else if (objJson[i].type.toLowerCase() === "application/pdf") {
            iImage.classList.add("mdi", "mdi-file-pdf", "font-size-16", "align-middle", "text-danger", "me-2");
        } else if (objJson[i].type.toLowerCase() === "image/jpeg") {
            iImage.classList.add("mdi", "mdi-image", "font-size-16", "align-middle", "text-warning", "me-2");
        } else if (objJson[i].type.toLowerCase() === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            iImage.classList.add("mdi", "mdi-microsoft-excel", "font-size-16", "align-middle", "text-success", "me-2");
        } else if (objJson[i].type.toLowerCase() === "video/mp4") {
            iImage.classList.add("mdi", "mdi-motion-play-outline", "font-size-16", "align-middle", "text-danger", "me-2");
        }else if(objJson[i].type.toLowerCase() === "text/plain"){
            iImage.classList.add("mdi", "mdi-text-box", "font-size-16", "align-middle", "text-bg-soft", "me-2");
        }else if(objJson[i].type.toLowerCase() === "audio/mpeg"){
            iImage.classList.add("mdi", "mdi-playlist-music", "font-size-16", "align-middle", "text-danger", "me-2");
        }else if(objJson[i].type.toLowerCase() === "application/msword"||objJson[i].type.toLowerCase() === "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
            iImage.classList.add("mdi", "mdi-file-word", "font-size-16", "align-middle", "text-primary", "me-2");
        }else if(objJson[i].type.toLowerCase() === "application/octet-stream"){
            iImage.classList.add("mdi", "mdi-mdi-file-alert-outline", "font-size-16", "align-middle", "text-bg-gradient", "me-2");
        }else {
            iImage.classList.add("mdi", "mdi-file-question-outline", "font-size-16", "align-middle", "text-warning", "me-2");
        }


        aDocImage.classList.add("text-dark", "fw-medium")
        tdImage.style = "max-width: 150px;overflow: hidden;text-overflow: ellipsis; white-space: nowrap"
        tdImage.append(aDocImage);
        tdDate.innerText = objJson[i].createdDate.substring(0, 10);
        // tdAuthor.innerText = objJson[i].author.name;
        tdAuthor.innerText = objJson[i].author.name;

        //имя сущности
        // counterPage.append(objJson[i].folder.pageName);

        if (objJson[i].application!=null){
            counterPage.append(objJson[i].application.name)
        }else if(objJson[i].device!=null){
            counterPage.append(objJson[i].device.name)
        }else if(objJson[i].location!=null){
            counterPage.append(objJson[i].location.name)
        }else if(objJson[i].counterparty!=null){
            counterPage.append(objJson[i].counterparty.name)
        }else {
            counterPage.append(objJson[i].folder.pageName);
        }
///
        //end name Entity
        tdCounter.append(counterPage);
        tdStatus.append(spanStatus);
        tdAction.append(buttonLink, aDownload, buttonDelete, buttonMarkDelete, formSendFile);

        tdSize.innerText = Math.floor(objJson[i].size * 1000 / 1000 / 1000) + "Kb";
        trMain.append(tdImage, tdDate, tdAuthor, tdCounter, tdStatus, tdSize, tdAction);
        fileCount.append(trMain);


        buttonDelete.addEventListener('click', function () {
            Swal.fire({
                title: "Are you sure?",
                text: "Вы уверены что хотите удалить документ: " + objJson[i].exchange,
                icon: "warning",
                showCancelButton: !0,
                confirmButtonText: "Да, удалить!",
                cancelButtonText: "Нет, отменить!",
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ms-2 mt-2",
                buttonsStyling: !1
            }).then(function (t) {
                t.value ? Swal.fire({
                        title: "Deleted!",
                        text: "Your file has been deleted.",
                        icon: "success"
                    }).then(removeDocument.bind(null, deleteUrl, objJson[i].id, objJson[i].folder.name, objJson[i].name))
                    // }).then(removeDocument.bind(null, deleteUrl, objJson[i].id, objJson[i].counterparty.minioName, objJson[i].name))
                    : t.dismiss === Swal.DismissReason.cancel && Swal.fire({
                    title: "Отмена",
                    text: "Ваш файл в безопасности :)",
                    icon: "error",
                })
            });
        });
        document.execCommand('copy');
        buttonLink.addEventListener('click', function () {
            let copyText = getLink(objJson[i].id);
            Swal.fire({
                position: "center",
                icon: "success",
                title: "Ссылка на документ полученна",
                showConfirmButton: !1,
                timer: 1500
            })
        });


        buttonMarkDelete.addEventListener('click',
            markDocument.bind(null, objJson[i].id)
        );

        page_span2.innerHTML = page + "/" + numPages();

    }


}

function numPages() {
    return Math.ceil(objJson.length / records_per_page);
}

let formDiv = document.querySelector(".form-search");

let formNew = document.createElement('form');
formNew.method = 'post';
formNew.classList.add('mt-4', 'mt-sm-0', 'float-sm-end', 'd-flex', 'align-items-center', 'form-input-js');
let divSearch = document.createElement('div');
divSearch.classList.add('search-box', 'me-2');
formNew.append(divSearch);
let divPosition = document.createElement('div');
divPosition.classList.add('position-relative');
divPosition.style = "min-width: 150px;";
divSearch.append(divPosition);
let inputSearch = document.createElement('input');
inputSearch.classList.add('form-control', 'bg-light', 'border-light', 'rounded');
inputSearch.type = 'text';
inputSearch.name = 'search';
inputSearch.placeholder = 'Search...';
divPosition.append(inputSearch);
let iSea = document.createElement('i');
iSea.classList.add('bx', 'bx-search-alt', 'search-icon');
divPosition.append(iSea);
let buttonForm = document.createElement('button');
buttonForm.type = 'submit';
buttonForm.innerText = 'Найти';
buttonForm.classList.add("btn", "btn-primary", "waves-effect", "waves-light");

let divDate = document.createElement('div');
divDate.style = "display: flex; flex-direction: row; flex-wrap:wrap; margin-right: 5px;";

divDate.innerHTML = `
                                    <div style="margin-right: 5px">
                                       
                                                            <input style="max-width: 145px" type="date" name="planedStartDate" class="form-control" id="planedStartDate">
                                                        </div>
                                                        <div>
                                                          
                                                            <input style="max-width: 145px" type="date" name="planedDueDate" class="form-control" id="planedDueDate">
                                                        </div>
                                                        
                                                            `;


let divForButton = document.createElement('div');
divForButton.append(buttonForm);
let generalFormSearchDiv = document.createElement('div');
generalFormSearchDiv.classList.add('kraiden');
generalFormSearchDiv.style = "display: flex;" + "flex-direction: row;" + "justify-content: space-around;" + "align-items: center;" + "max-height: 150px;" + "flex-wrap: wrap;";
formNew.append(generalFormSearchDiv);

generalFormSearchDiv.append(divDate, divSearch, divForButton);

formDiv.append(formNew);

let transferForm = document.querySelector("#transfer-file");
transferForm.addEventListener("submit",async (e) => {
    e.preventDefault();
    e.stopPropagation();
    let form = e.target;
    let formData = new FormData(form);
    let data = Object.fromEntries(formData);

    let response = await postSend(sendUrl, data).then(response => response.text());
    console.log(response);
    if (response.toLowerCase() === "такой документ присутствует у принимающей стороны" ||
        response.toLowerCase() === "file error :(" ||
        response.toLowerCase() === "отсутсвуют корректные вводные данные" ||
        response.toLowerCase() === "error данные не корректы"){

        Swal.fire({
            position: "center",
            icon: "error",
            title: response,
            showConfirmButton: !1,
            timer: 1500
        })
    }else {
        Swal.fire({
            position: "center",
            icon: "success",
            title: response,
            showConfirmButton: !1,
            timer: 1500
        })
    }

})


const postSend = async (url, data) => {
    return await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    });

};




formNew.addEventListener('submit', async (e) => {
    e.preventDefault();
    e.stopPropagation();
    let form = e.target;
    let formData = new FormData(form);
    let data = Object.fromEntries(formData);
    if (formData.entries().next().done) {
        emptySearch = true;
    } else {
        emptySearch = false;
    }
    console.log("Has is info?: " + formData.entries().next().done);
    searchSave = data;
    postSearch(searchUrlPost, data).then(response => response.json().then(documents => refreshList(documents)));

});

const postSearch = async (url, data) => {
    return await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    });
};



async function downloadDocument(minioName, filename, id) {
    const messageDownload = await fetch(downloadUrl + id + "/" + minioName + "/" + filename).then(message => message.text().then(alarm => console.log(alarm)));
}

async function removeDocument(url, id, minioName, filename) {
    const messageDelete = await fetch(url + id + "/" + minioName + "/" + filename).then(message => message.text()).then(alarm => console.log(alarm));
    const newListDelete = await fetch(allUrl).then(response => response.json().then(counterparties => refreshList(counterparties)));
}

async function getLink(id) {
    let linkDocument = await fetch(linkUrl + id).then(response => response.text()).then(data => copyToClipboard(data));
}


function copyToClipboard(text) {
    if (window.clipboardData && window.clipboardData.setData) {
        // Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
        return window.clipboardData.setData("Text", text);

    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
        var textarea = document.createElement("textarea");
        textarea.textContent = text;
        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in Microsoft Edge.
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
        } catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            return prompt("Copy to clipboard: Ctrl+C, Enter", text);
        } finally {
            document.body.removeChild(textarea);
        }
    }
}


async function markDocument(id) {
    // const messageDelete = await fetch(putMarkUrl + id).then(message => message.text()).then(alarm => console.log(alarm));
    const messageDelete = await fetch(putMarkUrl + id).then(message => message.text()).then(alarm => console.log(alarm));
    if (emptySearch === true) {
        const newListDelete = await fetch(allUrl).then(response => response.json()).then(counterparties => refreshList(counterparties));
    } else {
        postSearch(searchUrlPost, searchSave).then(response => response.json().then(documents => refreshList(documents)));
    }
}

// const postData = async (data) => {
//     const find = await fetch(searchUrl + data).then(temp => temp.json()).then(response => refreshList(response));
// }

async function refreshList(json) {
    let test = [];
    test = json;
    objJson = [];
    objJson = test;
    objJson.sort((a, b) => a.createdDate - b.createdDate);
    intermediate = test;
    await changePage2(current_page);
}

async function refreshListIntermedia() {

    // objJson = intermediate;

    await changePage2(1);
}

function isEmpty(str) {
    return str.trim() === '';
}

window.onload = function () {
    showAllList();
    changePage2(1);

};


// let response = await fetch(searchUrlPost, {
//     method: 'POST',
//     headers: {
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify(data)
//
// }).then(response =>response.json().then(documents=>refreshList(documents)));


// let result = await response.json();
// await fetch(allUrl).then(response => response.json().then(counterparties => refreshList(counterparties)));


// const postData = async (url,data) =>{
//     const res = await fetch(url,{
//         method:"GET",
//         headers: {
//             'Content-type': 'application/json'
//         },
//         body:data
//     });
//     console.log(res);
//     return await res.json();
//
// }


// $.ajax({
//     type:'POST',
//
// })
// let formEx = document.querySelector('.ex-name');
//     formEx.addEventListener('submit',async( e)=>{
//         e.preventDefault();
//         e.stopPropagation();
//         let form = e.target;
//         let formData = new FormData(form);
//         let data = Object.fromEntries(formData);
//         console.log(data);
//
//
//         let response =  await fetch(exNameUrl, {
//             method: 'POST',
//             body: data,
//             headers: {
//                 'Content-type':'application/json'
//             }
//         }).then(data =>console.log(data));
//
//         let result = await response.json();
//         await fetch(allUrl).then(response => response.json().then(counterparties => refreshList(counterparties)));
//
//     });

//
// const json = JSON.stringify(Object.fromEntries(formData.entries()));
// formNew.addEventListener('submit', (e) => {
//     e.preventDefault();
//     let form = e.target;
//     let formData = new FormData(form);
//     let data = Object.fromEntries(formData);
//     let post = data['search'];
//     if (isEmpty(post)) {
//         const find = postData(allUrl, post);
//     } else {
//         const find2 = postData(searchUrl, post);
//     }
//
// });

// const postData = async (url, data) => {
//     const find = await fetch(url + data).then(temp => temp.json()).then(response => refreshList(response));
// }