
const deleteUrl = '/histories/delete-all';

const buttonDelete = document.querySelector('.delete-hst');

buttonDelete.addEventListener('click', function () {
    Swal.fire({
        title: "Are you sure?",
        text: "Вы уверены что хотите удалить всю Историю",
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "Да, удалить!",
        cancelButtonText: "Нет, отменить!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ms-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        t.value ? Swal.fire({
                title: "Deleted!",
                text: "История действий удалена",
                icon: "success"
            }).then(removeDocument())
            : t.dismiss === Swal.DismissReason.cancel && Swal.fire({
            title: "Отмена",
            text: "Ваш файл в безопасности :)",
            icon: "error",
        })
    });
});


async function removeDocument() {
    const messageDelete = await fetch(deleteUrl).then(message => message.text()).then(alarm => console.log(alarm));
    setTimeout(reload, 1300)
}

async function reload(){
    location.reload();
}