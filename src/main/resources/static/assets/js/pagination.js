let current_page = 1;
let records_per_page = 6;
let objJson = [];
const searchUrl = '/folder/specialSearch/';
const allUrl = '/folder/all_auto';
const folderCreateURL = '/folder/folder-new';

function showAllList(){
    fetch(allUrl).then(response => response.json().then(counterparties => showAll2(counterparties)));
}

async function showAll2(json) {
    let test = [];
    test = json;
    objJson = [];
    objJson = test;
    await changePage2(1);
}

showAllList();


function getCount(id) {
    let count = 0;
    const size = fetch('http://localhost:8888/apiDocument/counterFilesCount/' + id).then(response => response.json()).then(result => count = result);
    return count; //не ставит резултат fetch запроса
}

function prevPage2() {
    if (current_page > 1) {
        current_page--;
        changePage2(current_page);
    }
}

function nextPage2() {
    if (current_page < numPages()) {
        current_page++;
        changePage2(current_page);
    }
}


function changePage2(page) {
    let btn_next2 = document.getElementById("btn_next2");
    let btn_prev2 = document.getElementById("btn_prev2");
    let page_span2 = document.getElementById("page1");

    let fileCount = document.querySelector(".file-count");

    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();


    fileCount.innerHTML = "";

    for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < objJson.length; i++) {
        fileCount.innerHTML += `<div class="col-xl-4 col-sm-6">
                                                            <div class="card shadow-none border">
                                                                <div class="card-body p-3">
                                                                    <div class="">
                                                                        <div class="float-end ms-2">
                                                                            <div class="dropdown mb-2">
                                                                                <a class="font-size-16 text-muted" role="button" data-bs-toggle="dropdown" aria-haspopup="true">
                                                                                    <i class="mdi mdi-dots-horizontal"></i>
                                                                                </a>

                                                                                <div class="dropdown-menu dropdown-menu-end">
                                                                                    <a class="dropdown-item" href="#">Open</a>
                                                                                    <a class="dropdown-item" href="#">Edit</a>
                                                                                    <a class="dropdown-item" href="#">Rename</a>
                                                                                    <div class="dropdown-divider"></div>
                                                                                    <a class="dropdown-item" href="#">Remove</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="avatar-xs me-3 mb-3">
                                                                            <div class="avatar-title bg-transparent rounded">
                                                                                <i class="bx bxs-folder font-size-24 text-warning"></i>
                                                                            </div>
                                                                        </div>
                                                                        <div class="d-flex">
                                                                            <div class="overflow-hidden me-auto">
                                                                                <h5 class="font-size-14 text-truncate mb-1"><a href="/folderView/${objJson[i].name}" class="text-body">${objJson[i].pageName}</a></h5>
                                                                                <p class="text-muted text-truncate mb-0">${objJson[i].documentCount}</p>
                                                                            </div>
                                                                            <div class="align-self-end ms-2">
                                                                                <p class="text-muted mb-0">${objJson[i].size/100000}kb</p>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>`;
        page_span2.innerHTML = page + "/" + numPages();
    }


}

function numPages() {
    return Math.ceil(objJson.length / records_per_page);
}


let formDiv = document.querySelector(".form-search");

let formNew = document.createElement('form');
formNew.method = 'post';
formNew.classList.add('mt-4', 'mt-sm-0', 'float-sm-end', 'd-flex', 'align-items-center', 'form-input-js');
let divSearch = document.createElement('div');
divSearch.classList.add('search-box', 'mb-2', 'me-2');
formNew.append(divSearch);
let divPosition = document.createElement('div');
divPosition.classList.add('position-relative');
divSearch.append(divPosition);
let inputSearch = document.createElement('input');
inputSearch.classList.add('form-control', 'bg-light', 'border-light', 'rounded');
inputSearch.type = 'text';
inputSearch.name = 'search';
inputSearch.placeholder = 'Search...';
divPosition.append(inputSearch);
let iSea = document.createElement('i');
iSea.classList.add('bx', 'bx-search-alt', 'search-icon');
divPosition.append(iSea);
let buttonForm = document.createElement('button');
buttonForm.type ='submit';
buttonForm.innerText = 'Найти';
buttonForm.style = 'min-width: 90px; margin-bottom: 8px;';
buttonForm.classList.add("btn", "btn-primary", "waves-effect", "waves-light");
formNew.append(buttonForm);
formDiv.append(formNew);

formNew.addEventListener('submit',(e) =>{
    e.preventDefault();
    let form = e.target;
    let formData = new FormData(form);
    let data = Object.fromEntries(formData);
    let post = data['search'];
    if (isEmpty(post)){
        const find = postData(allUrl, post);
    }else {
        const find2 = postData(searchUrl, post);
    }

})

const postData = async (url,data) =>{
    const find = fetch(url + data).then(temp => temp.json()).then(response=>refreshList(response));
}

function refreshList(json){
    let test = [];
    test = json;
    objJson = [];
    objJson = test;
    changePage2(1);
}


function isEmpty(str) {
    return str.trim() === '';
}


let transferForm = document.querySelector("#create-folder");
transferForm.addEventListener("submit",async (e) => {
    e.preventDefault();
    e.stopPropagation();
    let form = e.target;
    let formData = new FormData(form);
    let data = Object.fromEntries(formData);
    let jsonData = JSON.stringify(data);
    let jsonParseFile = JSON.parse(jsonData);
    let data2 = jsonParseFile.name;

    let response = await postSend(folderCreateURL, data2).then(response => response.text());
    console.log(response);

    if (response.toLowerCase()==="нельзя использовать дефолтные значения"||
        response.toLowerCase()==="минимальное кол-во 3 символа"||
        response.toLowerCase()==="такое название папку уже используется"||
        response.toLowerCase()==="что то пошло не так, при создании папки, проверьте входные данные"){
        Swal.fire({
            position: "center",
            icon: "success",
            title: response,
            showConfirmButton: !1,
            timer: 1500
        })
    }else {
        Swal.fire({
            position: "center",
            icon: "success",
            title: response,
            showConfirmButton: !1,
            timer: 1500
        })
        setTimeout(reload, 1300)
    }

})

async function reload(){
    location.replace("/file-manager");
}

const postSend = async (url, data) => {
    return await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: data,
    });

};

window.onload = function () {
    changePage2(1);
};


// const postData = async (url,data) =>{
//     const res = await fetch(url,{
//         method:"GET",
//         headers: {
//             'Content-type': 'application/json'
//         },
//         body:data
//     });
//     console.log(res);
//     return await res.json();
//
// }