const linkCounterparties = '/api/counterparties';
const linkLocations = '/api/locations';

let objJson = [];
let locationsData = [];
let devicesData =[];

let devicesOptions = document.getElementById('devices');

function getAllCounterparties(){
    fetch(linkCounterparties).then(response => response.json().then(counterparties => showAllCounterparties(counterparties)));
}

function showAllCounterparties(data){
    let test = [];
    test = data;
    objJson = [];
    objJson = test;
    let counterpartyOptions = document.getElementById('counterparty');
    objJson.forEach(function (counterparty){
        counterpartyOptions.innerHTML += `
            <option value="${counterparty.id}">${counterparty.name}</option>
        `
    });
}

getAllCounterparties();

function getAllLocationsByCounterparty(counterpartyId){
    fetch(linkCounterparties+"/"+counterpartyId+"/locations").then(response => response.json().then(locations => showLocations(locations)));
}

function showLocations(data){
    locationsData = data;
    let locationOptions = document.getElementById('location');
    locationOptions.innerHTML = "";
    locationOptions.innerHTML = `
        <option disabled selected value>Выберите адрес</option>
    `;
    locationsData.forEach(function (location){
        locationOptions.innerHTML += `
            <option value="${location.id}">${location.address}</option>
        `
    });
}

function getAllDevicesByLocation(locationId){
    fetch(linkLocations+"/"+locationId+"/devices").then(response => response.json().then(devices => showDevices(devices)));
}

function showDevices(data){
    devicesData = data;
    devicesOptions.innerHTML = "";
    devicesData.forEach(function (device){
        devicesOptions.innerHTML += `
            <option value="${device.id}">${device.name}</option>
        `
    });
}

const counterpartiesSelect = myForm.counterparty;

function changeLocations(){
    devicesOptions.innerHTML = "";
    let selectedOption = counterpartiesSelect.options[counterpartiesSelect.selectedIndex];
    getAllLocationsByCounterparty(selectedOption.value);
}

counterpartiesSelect.addEventListener("change", changeLocations);

const locationsSelect = myForm.location;

function changeDevices(){
    let selectedOption = locationsSelect.options[locationsSelect.selectedIndex];
    getAllDevicesByLocation(selectedOption.value);
}

locationsSelect.addEventListener("change", changeDevices);
