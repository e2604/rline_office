let divMinioCounter = document.querySelector(".js-minio");

let divCol12 = document.createElement('div');
divCol12.classList.add('col-12');
divMinioCounter.append(divCol12);

let divCard = document.createElement('div');
divCard.classList.add('card');
divMinioCounter.append(divCard);

let divCardBody = document.createElement('div');
divCardBody.classList.add('card-body');
divMinioCounter.append(divCardBody);

let h4 = document.createElement('h4');
h4.innerText = "My filesDrop";
h4.classList.add('card-title');
divCardBody.append(h4);

let form = document.createElement('form');
form.method = "post";
form.action = "/counterpartyRest/upload";
form.enctype = 'multipart/form-data';

divCardBody.append(form);

let inputFile = document.createElement('input');
inputFile.type = 'file';
inputFile.multiple = 'multiple';
inputFile.name = 'multipartFile';
form.append(inputFile);

let inputMinioName = document.createElement('input');
inputMinioName.type = 'text';
inputMinioName.name = "bucketName";
inputMinioName.hidden = true;
form.append(inputMinioName);

// Пример отправки POST запроса:
async function postData(url = '/counterpartyRest/upload', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return await response.json(); // parses JSON response into native JavaScript objects
}

postData('https://example.com/answer', { answer: 42 })
    .then((data) => {
        console.log(data); // JSON data parsed by `response.json()` call
    });