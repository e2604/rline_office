const myFolderURL = '/folder/file-upload'+ window.location.pathname;

let formUpload = document.querySelector('.upper-form-put');

formUpload.addEventListener("submit",async (e) => {
    e.preventDefault();
    e.stopPropagation();

    let file = document.querySelector("#formFile").files[0];
    const formData = new FormData();

    formData.append("multipartFile", file);

    let response = await postUpload(formData).then(response => response.text());
    if (response.toLowerCase() === "в этой папке присутствует аналогичный файл" ||
        response.toLowerCase() === "произошла ошибка с историей" ||
        response.toLowerCase() === "невозможно загрузить объект в файловое хранилище" ||
        response.toLowerCase() === "данной папки не существует в файловом хранилище"||
        response.toLowerCase() === "файл отсутствует"||
        response.toLowerCase() === "максимальный размер файла не должне превышать 10MB"||
        response.toLowerCase() === "неизвестный тип файла"||
        response.toLowerCase() === "unknown error"||
        response.toLowerCase()==="Не разрешенный символ в имени файла"){
        Swal.fire({
            position: "center",
            icon: "error",
            title: response,
            showConfirmButton: !1,
            timer: 1500
        })
    }else {
        Swal.fire({
            position: "center",
            icon: "success",
            title: response,
            showConfirmButton: !1,
            timer: 1500
        })
    }

});


const postUpload = async (form) => {
    return await fetch(myFolderURL, {
        method: 'POST',
        body: form
    });

};
