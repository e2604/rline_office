const myFolderURL = '/folder/file-upload' + window.location.pathname;

let formUpload = document.querySelector('.upper-form-put');

formUpload.addEventListener("submit", async (e) => {
    e.preventDefault();
    e.stopPropagation();

    let file = document.querySelector("#formFile").files[0];
    const formData = new FormData();
    formData.append("multipartFile", file);

    let response = await postUpload(formData).then(response => response.text());
    if (response.toLowerCase() === "В данной папке присутствует аналогичный файл" ||
        response.toLowerCase() === "Произошла ошибка с историей" ||
        response.toLowerCase() === "Невозможно загрузить объект в файловое хранилище" ||
        response.toLowerCase() === "Данной папки не существует в файловом хранилище" ||
        response.toLowerCase() === "Файл отсутствует" ||
        response.toLowerCase() === "Максимальный размер файла не должен превышать 10MB" ||
        response.toLowerCase() === "Неизвестный тип файла" ||
        response.toLowerCase() === "Unknown error" ||
        response.toLowerCase() === "Наименование файла содержит недопустимый символ") {
        Swal.fire({
            position: "center",
            icon: "error",
            title: response,
            showConfirmButton: !1,
            timer: 1500
        })
    } else {
        Swal.fire({
            position: "center",
            icon: "success",
            title: response,
            showConfirmButton: !1,
            timer: 1500
        })
        setTimeout(reload, 1300)
    }
});

async function reload() {
    location.reload();
}

const postUpload = async (form) => {
    return await fetch(myFolderURL, {
        method: 'POST',
        body: form
    });
};