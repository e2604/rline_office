package com.example.office;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.util.Collections;

@SpringBootApplication
public class OfficeApplication {
	public static void main(String[] args) {
		SpringApplication.run(OfficeApplication.class, args);
	}

	public OfficeApplication(FreeMarkerConfigurer freeMarkerConfigurer) {
		freeMarkerConfigurer
				.getTaglibFactory()
				.setClasspathTlds(Collections.singletonList("/META-INF/security.tld"));
	}
}