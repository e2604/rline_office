package com.example.office.DTO;

import com.example.office.model.application.Type;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TypeDTO {

    private Integer id;

    private String name;

    public static TypeDTO from(Type type) {
        return TypeDTO.builder()
                .id(type.getId())
                .name(type.getName())
                .build();
    }
}