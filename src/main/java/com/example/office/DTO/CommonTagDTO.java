package com.example.office.DTO;

import com.example.office.model.application.CommonTag;
import com.example.office.model.document.Document;
import lombok.*;

import java.util.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommonTagDTO {

    private Integer id;
    private List<UserDTO> user;
    private String link;
    private Document document;

    public static CommonTagDTO from(CommonTag tag) {

        List<UserDTO> usersDTO = new ArrayList<>();
        for (int i = 0; i < tag.getUser().size(); i++) {
            UserDTO user = UserDTO.from(tag.getUser().get(i));
            usersDTO.add(user);
        }

        return CommonTagDTO
                .builder()
                .id(tag.getId())
                .user(usersDTO)
                .document(tag.getDocument())
                .link(tag.getLink())
                .build();
    }
}