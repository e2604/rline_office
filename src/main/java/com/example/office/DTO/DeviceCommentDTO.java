package com.example.office.DTO;

import com.example.office.model.device.*;
import com.example.office.model.user.User;
import lombok.*;

import java.time.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceCommentDTO {

    private Integer id;
    private Device device;
    private User author;
    private String comment;
    private LocalDateTime createdDate;

    public static DeviceCommentDTO from(DeviceComment comment) {
        return DeviceCommentDTO
                .builder()
                .id(comment.getId())
                .device(comment.getDevice())
                .author(comment.getAuthor())
                .comment(comment.getComment())
                .createdDate(comment.getCreatedDate())
                .build();
    }
}