package com.example.office.DTO;

import com.example.office.model.user.Position;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PositionDTO {

    private Integer id;

    private String name;

    public static PositionDTO from(Position position) {
        return PositionDTO.builder()
                .id(position.getId())
                .name(position.getName())
                .build();
    }
}