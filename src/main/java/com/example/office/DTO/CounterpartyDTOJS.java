package com.example.office.DTO;

import com.example.office.model.сounterparty.Counterparty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CounterpartyDTOJS {

    private Integer id;
    private String name;
    private String minioName;
    private String folder;
    private String description;
    private String address;
    private String bin;
    private String contactName;
    private String phoneNumber;
    private int type;

    public static CounterpartyDTOJS fromJS(Counterparty counterparty) {
        return CounterpartyDTOJS
                .builder()
                .id(counterparty.getId())
                .name(counterparty.getName())
                .folder(counterparty.getFolder())
                .minioName(counterparty.getMinioName())
                .description(counterparty.getDescription())
                .address(counterparty.getAddress())
                .bin(counterparty.getBin())
                .contactName(counterparty.getContactName())
                .phoneNumber(counterparty.getPhoneNumber())
                .build();
    }
}
