package com.example.office.DTO;

import com.example.office.model.document.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocumentStatusDTO {

    private int id;
    private String name;
    private List<Document> documents;

    public static DocumentStatusDTO from(DocumentStatus status) {
        return DocumentStatusDTO
                .builder()
                .id(status.getId())
                .name(status.getName())
                .build();
    }
}