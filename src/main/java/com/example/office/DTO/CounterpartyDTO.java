package com.example.office.DTO;

import com.example.office.model.сounterparty.*;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CounterpartyDTO {

    private Integer id;
    private String name;
    private String minioName;
    private String folder;
    private String description;
    private String address;
    private String bin;
    private String contactName;
    private String phoneNumber;
    private CounterpartyTypeDTO type;

    public static CounterpartyDTO from(Counterparty counterparty) {

        return CounterpartyDTO
                .builder()
                .id(counterparty.getId())
                .name(counterparty.getName())
                .folder(counterparty.getFolder())
                .minioName(counterparty.getMinioName())
                .description(counterparty.getDescription())
                .address(counterparty.getAddress())
                .bin(counterparty.getBin())
                .contactName(counterparty.getContactName())
                .phoneNumber(counterparty.getPhoneNumber())
                .type(CounterpartyTypeDTO.from(counterparty.getType()))
                .build();
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof CounterpartyDTO)) return false;
//        CounterpartyDTO that = (CounterpartyDTO) o;
//        return getId().equals(that.getId()) && getName().equals(that.getName()) && getMinioName().equals(that.getMinioName()) && Objects.equals(getDescription(), that.getDescription()) && getAddress().equals(that.getAddress()) && getBin().equals(that.getBin()) && getContactName().equals(that.getContactName()) && getPhoneNumber().equals(that.getPhoneNumber()) && getType().equals(that.getType());
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(getId(), getName(), getMinioName(), getDescription(), getAddress(), getBin(), getContactName(), getPhoneNumber(), getType());
//    }
}