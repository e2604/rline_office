package com.example.office.DTO;

import com.example.office.model.application.Application;
import com.example.office.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserApplicationDTO {

    private Integer id;

    private Application application;
    private User user;
}
