package com.example.office.DTO;

import com.example.office.model.user.*;
import lombok.*;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class UserDTO {

    private Integer id;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private String password;
    private Long chatId;
    private String iin;
    private RoleDTO role;
    private PositionDTO position;
    private boolean enabled;

    public static UserDTO from(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getEmail())
                .password(user.getPassword())
                .phone(user.getPhone())
                .chatId(user.getChatId())
                .iin(user.getIin())
                .role(RoleDTO.from(user.getRole()))
                .position(PositionDTO.from(user.getPosition()))
                .enabled(user.isEnabled())
                .build();
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public String getFullName() {
        return name + " " + surname;
    }
}