package com.example.office.DTO;

import com.example.office.model.сounterparty.*;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CounterpartyTypeDTO {
    private Integer id;
    private String name;

    public static CounterpartyTypeDTO from(CounterpartyType type) {
        return CounterpartyTypeDTO
                .builder()
                .id(type.getId())
                .name(type.getName())
                .build();
    }
}