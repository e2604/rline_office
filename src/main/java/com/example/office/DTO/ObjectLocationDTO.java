package com.example.office.DTO;

import com.example.office.model.object.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ObjectLocationDTO {

    private Integer id;
    private String address;
    private String name;
    private String folder;
    private String contactName;
    private String phoneNumber;
    private String minioName;
    private CounterpartyDTO counterparty;
    private ObjectCompanyDTO object;

    public static ObjectLocationDTO from(ObjectLocation location) {
        if (location.getObject() != null) {
            return ObjectLocationDTO
                    .builder()
                    .id(location.getId())
                    .address(location.getAddress())
                    .name(location.getName())
                    .folder(location.getFolder())
                    .contactName(location.getContactName())
                    .phoneNumber(location.getPhoneNumber())
                    .minioName(location.getMinioName())
                    .counterparty(CounterpartyDTO.from(location.getCounterparty()))
                    .object(ObjectCompanyDTO.from(location.getObject()))
                    .build();
        } else {
            return ObjectLocationDTO
                    .builder()
                    .id(location.getId())
                    .address(location.getAddress())
                    .name(location.getName())
                    .folder(location.getFolder())
                    .contactName(location.getContactName())
                    .phoneNumber(location.getPhoneNumber())
                    .minioName(location.getMinioName())
                    .counterparty(CounterpartyDTO.from(location.getCounterparty()))
                    .build();
        }
    }
}