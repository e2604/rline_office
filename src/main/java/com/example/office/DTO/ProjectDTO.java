package com.example.office.DTO;

import com.example.office.model.application.Application;
import com.example.office.model.project.*;
import lombok.*;

import java.time.*;
import java.util.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectDTO {

    private Integer id;
    private String name;
    private String description;
    private LocalDate createdDate;
    private LocalDate closedDate;
    private StatusDTO status;

    private List<Application> applications;

    public static ProjectDTO from(Project project) {

        return ProjectDTO
                .builder()
                .id(project.getId())
                .name(project.getName())
                .description(project.getDescription())
                .createdDate(project.getCreatedDate())
                .closedDate(project.getClosedDate())
                .status(StatusDTO.from(project.getStatus()))
//                .comments(commentsDTO)
//                .documents(documentsDTO)
//                .applications(applicationsDTO)
                .build();
    }
}