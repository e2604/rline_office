package com.example.office.DTO;

import com.example.office.model.application.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

import java.time.*;
import java.time.format.*;
import java.util.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApplicationDTO {
    private Integer id;
    private UserDTO author;
    private UserDTO executor;
    private LocalDateTime createdDate;
    private String name;
    private String folder;
    private String minioName;
    private String description;
    private List<DeviceDTO> devices;
    private ObjectLocationDTO location;
    private CounterpartyDTO counterparty;
    private ProjectDTO project;
    private Double clientPrice;
    private Double executorPrice;
    private CounterpartyDTO payer;
    private StatusDTO status;
    private LocalDate planedStartDate;
    private LocalDate planedDueDate;
    private LocalDateTime actualStartDate;
    private LocalDateTime closedDate;
    private TypeDTO type;
    private PriorityDTO priority;
    private boolean isActive;

    public static ApplicationDTO from(Application application) {
        if (application.getProject() != null) {
            return ApplicationDTO.builder()
                    .id(application.getId())
                    .author(UserDTO.from(application.getAuthor()))
                    .executor(UserDTO.from(application.getExecutor()))
                    .createdDate(application.getCreatedDate())
                    .name(application.getName())
                    .folder(application.getFolder())
                    .minioName(application.getMinioName())
                    .description(application.getDescription())
                    .location(ObjectLocationDTO.from(application.getLocation()))
                    .counterparty(CounterpartyDTO.from(application.getCounterparty()))
                    .project(ProjectDTO.from(application.getProject()))
                    .clientPrice(application.getClientPrice())
                    .executorPrice(application.getExecutorPrice())
                    .payer(CounterpartyDTO.from(application.getPayer()))
                    .planedStartDate(application.getPlanedStartDate())
                    .planedDueDate(application.getPlanedDueDate())
                    .actualStartDate(application.getActualStartDate())
                    .closedDate(application.getClosedDate())
                    .type(TypeDTO.from(application.getType()))
                    .priority(PriorityDTO.from(application.getPriority()))
                    .isActive(application.isActive())
                    .status(StatusDTO.from(application.getStatus()))
                    .build();
        } else {
            return ApplicationDTO.builder()
                    .id(application.getId())
                    .author(UserDTO.from(application.getAuthor()))
                    .executor(UserDTO.from(application.getExecutor()))
                    .createdDate(application.getCreatedDate())
                    .name(application.getName())
                    .folder(application.getFolder())
                    .minioName(application.getMinioName())
                    .description(application.getDescription())
                    .location(ObjectLocationDTO.from(application.getLocation()))
                    .counterparty(CounterpartyDTO.from(application.getCounterparty()))
                    .clientPrice(application.getClientPrice())
                    .executorPrice(application.getExecutorPrice())
                    .payer(CounterpartyDTO.from(application.getPayer()))
                    .planedStartDate(application.getPlanedStartDate())
                    .planedDueDate(application.getPlanedDueDate())
                    .actualStartDate(application.getActualStartDate())
                    .closedDate(application.getClosedDate())
                    .type(TypeDTO.from(application.getType()))
                    .priority(PriorityDTO.from(application.getPriority()))
                    .isActive(application.isActive())
                    .status(StatusDTO.from(application.getStatus()))
                    .build();
        }
    }

    public String getFullExecutorName() {
        return executor.getName() + " " + executor.getSurname();
    }

    public String getCreatedDateJS() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return formatter.format(createdDate);
    }
}