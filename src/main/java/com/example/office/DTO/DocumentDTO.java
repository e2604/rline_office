package com.example.office.DTO;

import com.example.office.model.document.*;
import com.example.office.model.folder.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

import java.time.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude
public class DocumentDTO {

    private int id;
    private String name;
    private String exchange;
    private String note;
    private String path;
    private String style;
    private boolean error;
    private String previousName;
    private FolderDTO folder;
    private long size;
    private UserDTO author;
    private LocalDateTime createdDate;
    private DocumentStatusDTO status;
    private String type;
    private boolean isChange;
    private boolean markDelete;
    private CounterpartyDTO counterparty;
    private DeviceDTO device;
    private ObjectLocationDTO location;
    private ApplicationDTO application;

    public static DocumentDTO from(Document document) {
        if (document.getLocation()==null&&document.getDevice()==null&&document.getApplication()==null&&document.getCounterparty()==null){
            return DocumentDTO
                    .builder()
                    .id(document.getId())
                    .name(document.getName())
                    .exchange(document.getExchange())
                    .note(document.getNote())
                    .style(document.getStyle())
                    .size(document.getSize())
                    .path(document.getPath())
                    .error(document.isError())
                    .previousName(document.getPreviousName())
                    .folder(FolderDTO.from(document.getFolder()))
                    .author(UserDTO.from(document.getAuthor()))
                    .createdDate(document.getCreatedDate())
                    .status(DocumentStatusDTO.from(document.getStatus()))
                    .type(document.getType())
                    .isChange(document.isChange())
                    .markDelete(document.isMarkDelete())
                    .build();
        }else if (document.getLocation()==null&&document.getApplication()==null){
            return DocumentDTO
                    .builder()
                    .id(document.getId())
                    .name(document.getName())
                    .exchange(document.getExchange())
                    .note(document.getNote())
                    .style(document.getStyle())
                    .size(document.getSize())
                    .path(document.getPath())
                    .error(document.isError())
                    .previousName(document.getPreviousName())
                    .folder(FolderDTO.from(document.getFolder()))
                    .author(UserDTO.from(document.getAuthor()))
                    .createdDate(document.getCreatedDate())
                    .status(DocumentStatusDTO.from(document.getStatus()))
                    .type(document.getType())
                    .isChange(document.isChange())
                    .markDelete(document.isMarkDelete())
                    .counterparty(CounterpartyDTO.from(document.getCounterparty()))
                    .build();
        }else if(document.getApplication()!=null){
            return DocumentDTO
                    .builder()
                    .id(document.getId())
                    .name(document.getName())
                    .exchange(document.getExchange())
                    .note(document.getNote())
                    .style(document.getStyle())
                    .size(document.getSize())
                    .path(document.getPath())
                    .error(document.isError())
                    .previousName(document.getPreviousName())
                    .folder(FolderDTO.from(document.getFolder()))
                    .author(UserDTO.from(document.getAuthor()))
                    .createdDate(document.getCreatedDate())
                    .status(DocumentStatusDTO.from(document.getStatus()))
                    .type(document.getType())
                    .isChange(document.isChange())
                    .markDelete(document.isMarkDelete())
                    .counterparty(CounterpartyDTO.from(document.getCounterparty()))
                    .application(ApplicationDTO.from(document.getApplication()))
                    .build();
        }else if (document.getLocation()!=null&&document.getDevice()==null){
            return DocumentDTO
                    .builder()
                    .id(document.getId())
                    .name(document.getName())
                    .exchange(document.getExchange())
                    .note(document.getNote())
                    .style(document.getStyle())
                    .size(document.getSize())
                    .path(document.getPath())
                    .error(document.isError())
                    .previousName(document.getPreviousName())
                    .folder(FolderDTO.from(document.getFolder()))
                    .author(UserDTO.from(document.getAuthor()))
                    .createdDate(document.getCreatedDate())
                    .status(DocumentStatusDTO.from(document.getStatus()))
                    .type(document.getType())
                    .isChange(document.isChange())
                    .markDelete(document.isMarkDelete())
                    .counterparty(CounterpartyDTO.from(document.getCounterparty()))
                    .location(ObjectLocationDTO.from(document.getLocation()))
                    .build();
        }else if (document.getLocation()!=null&&document.getDevice()!=null){
            return DocumentDTO
                    .builder()
                    .id(document.getId())
                    .name(document.getName())
                    .exchange(document.getExchange())
                    .note(document.getNote())
                    .style(document.getStyle())
                    .size(document.getSize())
                    .path(document.getPath())
                    .error(document.isError())
                    .previousName(document.getPreviousName())
                    .folder(FolderDTO.from(document.getFolder()))
                    .author(UserDTO.from(document.getAuthor()))
                    .createdDate(document.getCreatedDate())
                    .status(DocumentStatusDTO.from(document.getStatus()))
                    .type(document.getType())
                    .isChange(document.isChange())
                    .markDelete(document.isMarkDelete())
                    .counterparty(CounterpartyDTO.from(document.getCounterparty()))
                    .location(ObjectLocationDTO.from(document.getLocation()))
                    .device(DeviceDTO.from(document.getDevice()))
                    .build();
        }else {
            return DocumentDTO
                    .builder()
                    .id(document.getId())
                    .name(document.getName())
                    .exchange(document.getExchange())
                    .note(document.getNote())
                    .style(document.getStyle())
                    .size(document.getSize())
                    .path(document.getPath())
                    .error(document.isError())
                    .previousName(document.getPreviousName())
                    .folder(FolderDTO.from(document.getFolder()))
                    .author(UserDTO.from(document.getAuthor()))
                    .createdDate(document.getCreatedDate())
                    .status(DocumentStatusDTO.from(document.getStatus()))
                    .type(document.getType())
                    .isChange(document.isChange())
                    .markDelete(document.isMarkDelete())
                    .counterparty(CounterpartyDTO.from(document.getCounterparty()))
                    .location(ObjectLocationDTO.from(document.getLocation()))
                    .device(DeviceDTO.from(document.getDevice()))
                    .application(ApplicationDTO.from(document.getApplication()))
                    .build();
        }
    }

    public long getRoundedKBSizeDTO() {
        return size/1000;
    }

    public int getRoundedMBSizeDTO() {
        double newValue = size;
        double sizeInDouble = newValue / 1000000;
        return (int) Math.round(sizeInDouble);
    }
}