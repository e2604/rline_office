package com.example.office.DTO;

import com.example.office.model.user.Role;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RoleDTO {

    private Integer id;

    private String role;

    public static RoleDTO from(Role role) {
        return RoleDTO.builder()
                .id(role.getId())
                .role(role.getRole())
                .build();
    }
}