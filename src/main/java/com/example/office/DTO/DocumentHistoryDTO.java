package com.example.office.DTO;

import com.example.office.model.document.*;
import lombok.*;

import java.time.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocumentHistoryDTO {

    private Integer id;
    private DocumentDTO document;
    private LocalDateTime createdDate;
    private UserDTO editor;
//    private DocumentStatusDTO status;

    public static DocumentHistoryDTO from(DocumentHistory history) {
        return DocumentHistoryDTO
                .builder()
                .id(history.getId())
                .document(DocumentDTO.from(history.getDocument()))
                .createdDate(history.getCreatedDate())
                .editor(UserDTO.from(history.getEditor()))
//                .status(DocumentStatusDTO.from(history.getStatus()))
                .build();
    }
}