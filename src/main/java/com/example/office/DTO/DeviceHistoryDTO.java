package com.example.office.DTO;

import com.example.office.model.device.*;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceHistoryDTO {

    private Integer id;
    private DeviceDTO device;
    private String description;
    private DeviceStatusDTO status;

    public static DeviceHistoryDTO from(DeviceHistory history) {
        return DeviceHistoryDTO
                .builder()
                .id(history.getId())
                .device(DeviceDTO.from(history.getDevice()))
                .description(history.getDescription())
                .status(DeviceStatusDTO.from(history.getStatus()))
                .build();
    }
}