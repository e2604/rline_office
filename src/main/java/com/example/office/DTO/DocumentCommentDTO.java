package com.example.office.DTO;

import com.example.office.model.document.*;
import com.example.office.model.user.User;
import lombok.*;

import java.time.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocumentCommentDTO {

    private Integer id;
    private String commentText;
    private LocalDateTime createdDate;
    private User author;
    private Document document;

    public static DocumentCommentDTO from(DocumentComment comment) {
        return DocumentCommentDTO
                .builder()
                .id(comment.getId())
                .commentText(comment.getCommentText())
                .createdDate(comment.getCreatedDate())
                .author(comment.getAuthor())
                .document(comment.getDocument())
                .build();
    }
}