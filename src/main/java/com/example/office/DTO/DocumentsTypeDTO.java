package com.example.office.DTO;

import com.example.office.model.document.*;
import lombok.*;

import java.util.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocumentsTypeDTO {

    private Integer id;
    private String name;
    private List<DocumentDTO> documents;

    public static DocumentsTypeDTO from(DocumentsType type) {

        List<DocumentDTO> documentsDTO = new ArrayList<>();
        for (int i = 0; i < type.getDocuments().size(); i++) {
            DocumentDTO document = DocumentDTO.from(type.getDocuments().get(i));
            documentsDTO.add(document);
        }

        return DocumentsTypeDTO
                .builder()
                .id(type.getId())
                .name(type.getName())
                .documents(documentsDTO)
                .build();
    }
}