package com.example.office.DTO;

import com.example.office.model.device.*;
import lombok.*;

import java.time.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceTypeDTO {

    private Integer id;
    private String name;
    private UserDTO author;
    private LocalDateTime createdDate;
    private String description;

    public static DeviceTypeDTO from(DeviceType type) {
        return DeviceTypeDTO.builder()
                .id(type.getId())
                .name(type.getName())
                .author(UserDTO.from(type.getAuthor()))
                .createdDate(type.getCreatedDate())
                .description(type.getDescription())
                .build();
    }
}