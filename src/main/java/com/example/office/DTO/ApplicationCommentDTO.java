package com.example.office.DTO;

import com.example.office.model.application.*;
import com.example.office.model.user.User;
import lombok.*;

import java.time.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationCommentDTO {

    private Integer id;
    private String commentText;
    private LocalDateTime createdDate; //для отображения даты к комментарию
    private User author;
    private Application application;

    public static ApplicationCommentDTO from(ApplicationComment comment){
        return ApplicationCommentDTO
                .builder()
                .id(comment.getId())
                .author(comment.getAuthor())
                .commentText(comment.getCommentText())
                .createdDate(comment.getCreatedDate())
                .application(comment.getApplication())
                .build();
    }
}