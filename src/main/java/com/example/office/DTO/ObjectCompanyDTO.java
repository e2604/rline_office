package com.example.office.DTO;

import com.example.office.model.document.Document;
import com.example.office.model.object.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@JsonInclude( JsonInclude.Include. NON_NULL )
public class ObjectCompanyDTO {

    private Integer id;
    private String name;
    private String address;

    public static ObjectCompanyDTO from(ObjectCompany object) {

            return ObjectCompanyDTO
                    .builder()
                    .id(object.getId())
                    .name(object.getName())
                    .address(object.getAddress())
                    .build();
        }
    }

