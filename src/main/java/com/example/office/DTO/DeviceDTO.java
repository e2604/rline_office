package com.example.office.DTO;

import com.example.office.model.device.*;
import lombok.*;

import java.time.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceDTO {

    private Integer id;
    private UserDTO author;
    private LocalDateTime createdDate;
    private String name;
    private String folder;
    private String minioName;
    private String description;
    private DeviceTypeDTO type;
    private ObjectLocationDTO location;
    private String inventoryNumber;
    private String serialNumber;
    private DeviceStatusDTO status;

    public static DeviceDTO from(Device device) {
        return DeviceDTO
                .builder()
                .id(device.getId())
                .author(UserDTO.from(device.getAuthor()))
                .createdDate(device.getCreatedDate())
                .name(device.getName())
                .folder(device.getFolder())
                .minioName(device.getMinioName())
                .description(device.getDescription())
                .type(DeviceTypeDTO.from(device.getType()))
                .location(ObjectLocationDTO.from(device.getLocation()))
                .inventoryNumber(device.getInventoryNumber())
                .serialNumber(device.getSerialNumber())
                .status(DeviceStatusDTO.from(device.getStatus()))
                .build();
    }

    public String getFullName() {
        return author.getName() + " " + author.getSurname();
    }
}