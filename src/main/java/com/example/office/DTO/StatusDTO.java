package com.example.office.DTO;

import com.example.office.model.application.Application;
import com.example.office.model.application.Status;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StatusDTO {

    private Integer id;

    private String name;
    private List<Application> applications;

    public static StatusDTO from(Status status) {
        return StatusDTO.builder()
                .id(status.getId())
                .name(status.getName())
                .build();
    }
}