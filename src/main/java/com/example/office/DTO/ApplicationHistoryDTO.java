package com.example.office.DTO;

import com.example.office.model.application.*;
import lombok.*;

import java.time.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class ApplicationHistoryDTO {

    private Integer id;
    private ApplicationDTO application;
    private StatusDTO status;
    private String description;
    private UserDTO executor;
    private PriorityDTO priority;
    private LocalDateTime plannedStartDate;
    private LocalDateTime plannedDueDate;
    private Double clientPrice;
    private Double executorPrice;

    public static ApplicationHistoryDTO from(ApplicationHistory history) {
        return ApplicationHistoryDTO
                .builder()
                .id(history.getId())
                .application(ApplicationDTO.from(history.getApplication()))
                .status(StatusDTO.from(history.getStatus()))
                .description(history.getDescription())
                .executor(UserDTO.from(history.getExecutor()))
                .priority(PriorityDTO.from(history.getPriority()))
                .plannedStartDate(history.getPlannedStartDate())
                .plannedDueDate(history.getPlannedDueDate())
                .clientPrice(history.getClientPrice())
                .build();
    }
}