package com.example.office.DTO;

import com.example.office.model.application.*;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PriorityDTO {

    private Integer id;

    private String name;

    public static PriorityDTO from(Priority priority) {
        return PriorityDTO.builder()
                .id(priority.getId())
                .name(priority.getName())
                .build();
    }
}