package com.example.office.DTO;

import com.example.office.model.application.Application;
import com.example.office.model.application.Priority;
import com.example.office.model.application.Status;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
public class UserRequests {
    private Application application;

    private final Integer id;
    private final Integer applicationId;
    private final String applicationName;
    private final Status status;
    private final Priority priority;
    private final LocalDate planedDueDate;
    private final UserApplicationDTO userApplicationDTO;

    public UserRequests(Integer id, String applicationName, String statusName, Integer priorityId, String priorityName, LocalDate planedDueDate, Integer executerId, String executerName, List<Integer> participantsId, List<String> participantsName, UserApplicationDTO userApplicationDTO) {
        this.id = id;
        this.applicationId = application.getId();
        this.applicationName = application.getName();
        this.status = application.getStatus();
        this.priority = application.getPriority();
        this.planedDueDate = application.getPlanedDueDate();
        this.userApplicationDTO = userApplicationDTO;
    }
}
