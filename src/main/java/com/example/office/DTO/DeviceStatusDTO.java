package com.example.office.DTO;

import com.example.office.model.device.DeviceStatus;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceStatusDTO {

    private Integer id;
    private String name;

    public static DeviceStatusDTO from(DeviceStatus status) {
        return DeviceStatusDTO
                .builder()
                .id(status.getId())
                .name(status.getName())
                .build();
    }
}