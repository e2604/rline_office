package com.example.office.quartz;

import com.example.office.service.ApplicationService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@DisallowConcurrentExecution
public class ApplicationCheck implements Job {
    @Autowired
    private ApplicationService service;

    @Override
    public void execute(JobExecutionContext context) {
        service.sendReminder();
    }
}