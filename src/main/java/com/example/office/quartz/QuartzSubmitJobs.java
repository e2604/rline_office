package com.example.office.quartz;

import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

@Configuration
public class QuartzSubmitJobs {
    private static final String CRON_EVERY_DAY_AT_9AM = "0 0 0,9 ? * * *";

    @Bean(name = "reminder")
    public JobDetailFactoryBean jobMemberStats() {
        return QuartzConfig.createJobDetail(ApplicationCheck.class, "Reminder Job");
    }

    @Bean(name = "reminderTrigger")
    public CronTriggerFactoryBean triggerMemberClassStats(@Qualifier("reminder") JobDetail jobDetail) {
        return QuartzConfig.createCronTrigger(jobDetail, CRON_EVERY_DAY_AT_9AM, "Reminder Trigger");
    }

}