//package com.example.office.telegramBot.bot;
//
//import com.example.office.DTO.UserDTO;
//import com.example.office.model.user.User;
//import com.example.office.model.user.repository.UserRepository;
//import lombok.*;
//import lombok.extern.log4j.Log4j2;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
//import org.telegram.telegrambots.bots.TelegramLongPollingBot;
//import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
//import org.telegram.telegrambots.meta.api.objects.Message;
//import org.telegram.telegrambots.meta.api.objects.Update;
//import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
//

//МЕТОД РЕАЛИЗОВАН В USERSERVICE!----------------------------------------------------------------------------------------------------------------------
//@Log4j2
//@Service
//@PropertySource("classpath:telegram.properties")
//public class Bot extends TelegramLongPollingBot {
//
//    private final UserRepository userRepository;
//
//    public Bot(UserRepository userRepository) {
//        this.userRepository = userRepository;
//    }
//
//
//    public UserDTO findByIin(String iin){
//        UserDTO dto;
//        try {
//            User user = userRepository.findByIin(iin);
//            dto = UserDTO.from(user);
//            return dto;
//        }catch (NullPointerException e){
//            e.printStackTrace();
//            dto = new UserDTO();
//        }
//        return dto;
//    }
//
//    @Value("${bot.name}")
//    private String botName;
//
//    @Value("${bot.token}")
//    private String botToken;
//
//    @Override
//    public String getBotUsername() {
//        return botName; //Имя бота
//    }
//
//    @Override
//    public String getBotToken() {
//        return botToken; //токен бота, который создан через BotFather
//    }
//
//    @SneakyThrows
//    @Override
//    public void onUpdateReceived(Update update) {
//        Message message = update.getMessage();
//
//        if (message.getText().equals("/start")) {
//            execute(
//                    SendMessage.builder()
//                            .chatId(message.getChatId().toString())
//                            .text("Доброго времени суток!\n\nПожалуйста введите ваш ИИН, если вас зарегистрировали в системе офиса.")
//                            .build());
//            return;
//        }
//
//        if (message.getText().length() != 12 || !message.getText().matches("[0-9]{12}")) {
//            execute(
//                    SendMessage.builder()
//                            .chatId(message.getChatId().toString())
//                            .text("Пожалуйста введите ваш ИИН! \n\nВам будут высланы ваши Логин и Пароль.\n\n\n" +
//                                    "Если это сообщение продолжает выходить, пожалуйста обратитесь к Администратору, за созданием аккаунта")
//                            .build());
//            return;
//        }
//
//        UserDTO find = findByIin(message.getText());
//        long chatId = update.getMessage().getChatId();
//        if (message.getText().equals(find.getIin())) {
//            find.setChatId(chatId);
//            userRepository.save(User.from(find));
//            execute(
//                    SendMessage.builder()
//                            .chatId(message.getChatId().toString())
//                            .text("Ваша информация обновлена!\n\nДобро пожаловать в коллектив!\n\nВаши данные для входа: \nЛогин: " + find.getEmail() + "\nПароль: " + find.getPassword() + "\n\nПри создании вам заявки, вы будете уведомлены!")
//                            .build());
//        }
//    }
//
//    public void sendMessage(String chatId, String textMessage){
//        SendMessage sendMessage = new SendMessage();
//        sendMessage.setChatId(chatId);
//        sendMessage.setText(textMessage);
//
//        try {
//            execute(sendMessage);
//        } catch (TelegramApiException e) {
//            e.printStackTrace();
//        }
//    }
//}
