package com.example.office.exception;

import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@Setter
@ResponseStatus(HttpStatus.NOT_FOUND)
@AllArgsConstructor
public class LocationNotFoundException extends RuntimeException {
    String exceptionMessage;
}