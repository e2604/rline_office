package com.example.office.log;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class LombokLoggingController {

    @RequestMapping
    public String projectLogging(){
        log.trace("Вызов разнообразных блокирующих и асинхронных операций");
        log.debug("Debug");
        log.info("Разовая операция");
        log.warn("Странный формат запроса");
        log.error("Ошибка");

        return "Просмотрите журнал логов...";
    }
}
