package com.example.office.model.history.mainHistory;

import com.example.office.model.application.repository.*;
import com.example.office.model.device.DeviceType;
import com.example.office.model.device.repository.*;
import com.example.office.model.document.repository.*;
import com.example.office.model.history.Essence;
import com.example.office.model.object.repository.*;
import com.example.office.model.project.repository.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import com.example.office.model.сounterparty.repository.*;
import lombok.*;
import org.springframework.stereotype.*;

import java.security.*;
import java.util.*;

@Service
@AllArgsConstructor
public class MainHistoryService {

    private final MainHistoryRepository mainHistoryRepository;
    private final UserRepository userRepository;
    private final DeviceRepository deviceRepository;
    private final CounterpartyRepository counterpartyRepository;
    private final ObjectLocationRepository objectLocationRepository;
    private final ObjectCompanyRepository objectCompanyRepository;
    private final ProjectRepository projectRepository;
    private final ApplicationRepository applicationRepository;
    private final DocumentRepository documentRepository;
    private final DeviceTypeRepository deviceTypeRepository;

    public List<History> getThirtyActions() {
        List<History> histories = new ArrayList<>();

        var listAll = mainHistoryRepository.findAll();
        try {
            for (int i = 0; i < listAll.size(); i++) {
                if (i > 30) {
                    break;
                }
                if (listAll.get(i).getEssence().equals(Essence.COUNTERPARTY.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = counterpartyRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getCounter(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.PROJECT.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = projectRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getProject(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.DEVICE.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = deviceRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getDevice(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.APPLICATION.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = applicationRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getApp(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.DOCUMENT.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = documentRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getDocument(listAll.get(i), user.getFullName(), object));
                } else if (listAll.get(i).getEssence().equals(Essence.LOCATION.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = objectLocationRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getLocationCompany(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.OBJECT.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = objectCompanyRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getObjectCompany(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.DEVICETYPE.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var deviceType = deviceTypeRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getDeviceType(listAll.get(i), user, deviceType));
                } else if (listAll.get(i).getEssence().equals(Essence.USER.getName())) {
                    var author = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var user = userRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getUser(listAll.get(i), author, user));
                }
            }
            histories.sort(Comparator.comparing(History::getCreatedDate).reversed());
            return histories;
        } catch (Exception e) {
            System.out.println("Ошибка с историей" + e.getMessage());
            return histories;
        }
    }

    public List<History> getDocumentHistory(Integer id, Principal principal) {
        List<History> histories = new ArrayList<>();
        List<History> historyFiveElements = new ArrayList<>();
        var listAll = mainHistoryRepository.findAll();

        for (MainHistory mainHistory : listAll) {
            if (mainHistory.getEssence().equals(Essence.DOCUMENT.getName()) && mainHistory.getIdEntity() == id) {
                var user = userRepository.findByEmail(principal.getName()).orElseThrow();
                var object = documentRepository.findById(id).orElseThrow();
                histories.add(History.getDocument(mainHistory, user.getFullName(), object));
            }
        }
        histories.sort(Comparator.comparing(History::getCreatedDate).reversed());
        for (int i = 0; i < histories.size(); i++) {
            if (i > 4) {
                break;
            }
            historyFiveElements.add(histories.get(i));
        }
        return historyFiveElements;
    }

    public List<History> getAllDocumentHistory(Principal principal) {
        List<History> histories = new ArrayList<>();
        List<History> historyFiveElements = new ArrayList<>();
        var listAll = mainHistoryRepository.findAll();
        try {
            for (MainHistory mainHistory : listAll) {
                if (mainHistory.getEssence().equals(Essence.DOCUMENT.getName())) {
                    var user = userRepository.findByEmail(principal.getName()).orElseThrow();
                    var object = documentRepository.findById(mainHistory.getIdEntity()).orElseThrow();
                    histories.add(History.getDocument(mainHistory, user.getFullName(), object));
                }
            }
            histories.sort(Comparator.comparing(History::getCreatedDate).reversed());
            for (int i = 0; i < histories.size(); i++) {
                if (i > 4) {
                    break;
                }
                historyFiveElements.add(histories.get(i));
            }
            return historyFiveElements;
        } catch (NoSuchElementException e) {
            System.out.println("Ошибка с историей: " + e.getMessage());
            return new ArrayList<>();
        }
    }

    public List<History> getAllActions() {
        List<History> histories = new ArrayList<>();

        var listAll = mainHistoryRepository.findAll();
        try {
            for (int i = 0; i < listAll.size(); i++) {
                if (listAll.get(i).getEssence().equals(Essence.COUNTERPARTY.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = counterpartyRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getCounter(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.PROJECT.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = projectRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getProject(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.DEVICE.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = deviceRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getDevice(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.APPLICATION.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = applicationRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getApp(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.DOCUMENT.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = documentRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getDocument(listAll.get(i), user.getFullName(), object));
                } else if (listAll.get(i).getEssence().equals(Essence.LOCATION.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = objectLocationRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getLocationCompany(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.OBJECT.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    var object = objectCompanyRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getObjectCompany(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.DEVICETYPE.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    DeviceType object = deviceTypeRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getDeviceType(listAll.get(i), user, object));
                } else if (listAll.get(i).getEssence().equals(Essence.USER.getName())) {
                    var user = userRepository.findByEmail(listAll.get(i).getAuthorName()).orElseThrow();
                    User object = userRepository.findById(listAll.get(i).getIdEntity()).orElseThrow();
                    histories.add(History.getUser(listAll.get(i), user, object));
                }
            }
            histories.sort(Comparator.comparing(History::getCreatedDate).reversed());
            return histories;
        } catch (Exception e) {
            System.out.println("Ошибка с историей" + e.getMessage());
            return histories;
        }
    }

    public String deleteALl() {
        try {
            mainHistoryRepository.deleteAll();
            return "History deleted";
        } catch (Exception e) {
            System.out.println("Проблема с удалением истории: " + e.getMessage());
            return "Не получилось удалить историю";
        }
    }
}