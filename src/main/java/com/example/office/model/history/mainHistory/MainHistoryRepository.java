package com.example.office.model.history.mainHistory;

import com.example.office.model.history.Essence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MainHistoryRepository extends JpaRepository<MainHistory,Integer> {
    List<History> getAllByEssence(Essence essence);
}
