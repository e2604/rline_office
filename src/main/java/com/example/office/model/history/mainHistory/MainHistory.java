package com.example.office.model.history.mainHistory;

import com.example.office.model.history.*;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@Table(name = "histories")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class MainHistory{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "author_name")
    private String authorName;
    @Column(name = "created_date")
    private LocalDateTime createdDate;
    @Column(name = "entity_id")
    private int idEntity;
    @Column(name = "entity_type")
    private String essence;
    @Column(name = "entity_action")
    private String action;

    public static MainHistory createFromCounter(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.COUNTERPARTY.getName())
                .action(Action.CREATED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteFromCounter(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.COUNTERPARTY.getName())
                .action(Action.DELETE.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory openFromCounter(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.COUNTERPARTY.getName())
                .action(Action.OPEN.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory errorFromCounter(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.COUNTERPARTY.getName())
                .action(Action.ERROR.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editCounterparty(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.COUNTERPARTY.getName())
                .action(Action.EDIT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory createFromLocation(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.LOCATION.getName())
                .action(Action.CREATED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteFromLocation(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.LOCATION.getName())
                .action(Action.DELETE.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteLocationFromObject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.LOCATION.getName())
                .action(Action.DELETEFROMOBJECTLIST.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteLocationFromCounterparty(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.LOCATION.getName())
                .action(Action.DELETEFROMLIST.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory openFromLocation(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.LOCATION.getName())
                .action(Action.OPEN.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory errorFromLocation(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.LOCATION.getName())
                .action(Action.ERROR.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editLocation(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.LOCATION.getName())
                .action(Action.EDIT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editLocationFromCounterparty(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.LOCATION.getName())
                .action(Action.EDITFROMCOUNTERPARTY.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editLocationFromObject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.LOCATION.getName())
                .action(Action.EDITFROMOBJECT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory createFromObject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.OBJECT.getName())
                .action(Action.CREATED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteFromObject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.OBJECT.getName())
                .action(Action.DELETE.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory openFromObject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.OBJECT.getName())
                .action(Action.OPEN.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory errorFromObject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.OBJECT.getName())
                .action(Action.ERROR.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editObject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.OBJECT.getName())
                .action(Action.EDIT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory createFromProject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.PROJECT.getName())
                .action(Action.CREATED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteFromProject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.PROJECT.getName())
                .action(Action.CREATED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory openFromProject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.PROJECT.getName())
                .action(Action.OPEN.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory errorFromProject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.PROJECT.getName())
                .action(Action.ERROR.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editProject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.PROJECT.getName())
                .action(Action.EDIT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory closedProject(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.PROJECT.getName())
                .action(Action.COMPLETED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory createFromDevice(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICE.getName())
                .action(Action.CREATED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteFromDevice(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICE.getName())
                .action(Action.DELETE.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory openFromDevice(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICE.getName())
                .action(Action.OPEN.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory errorFromDevice(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICE.getName())
                .action(Action.ERROR.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory commentFromDevice(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICE.getName())
                .action(Action.COMMENT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editDevice(String author, Integer idEntity) {
        return MainHistory
                .builder()
                .authorName(author)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICE.getName())
                .action(Action.EDIT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deviceInProgress(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICE.getName())
                .action(Action.START.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deviceDone(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICE.getName())
                .action(Action.COMPLETED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deviceBurned(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICE.getName())
                .action(Action.BURNED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory createDeviceType(String author, Integer idEntity) {
        return MainHistory
                .builder()
                .authorName(author)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICETYPE.getName())
                .action(Action.CREATED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteDeviceType(String author, Integer idEntity) {
        return MainHistory
                .builder()
                .authorName(author)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICETYPE.getName())
                .action(Action.DELETE.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editDeviceType(String author, Integer idEntity) {
        return MainHistory
                .builder()
                .authorName(author)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DEVICETYPE.getName())
                .action(Action.EDIT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory createFromApplication(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.APPLICATION.getName())
                .action(Action.CREATED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteFromApplication(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.APPLICATION.getName())
                .action(Action.DELETE.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory commentFromApplication(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.APPLICATION.getName())
                .action(Action.COMMENT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editApplication(String author, Integer idEntity) {
        return MainHistory.builder()
                .authorName(author)
                .createdDate(LocalDateTime.now())
                .essence(Essence.APPLICATION.getName())
                .action(Action.EDIT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory commentFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.COMMENT.getText())
                .idEntity(idEntity)
                .build();
    }
    public static MainHistory cancelFromApplication(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.APPLICATION.getName())
                .action(Action.CANCEL.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory startFromApplication(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.APPLICATION.getName())
                .action(Action.START.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory completeFromApplication(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.APPLICATION.getName())
                .action(Action.COMPLETED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory openFromApplication(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.APPLICATION.getName())
                .action(Action.OPEN.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory errorFromApplication(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.APPLICATION.getName())
                .action(Action.ERROR.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory uploadFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.UPLOAD.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory openFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.OPEN.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory linkFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.LINK.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory downloadFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.DOWNLOAD.getText())
                .idEntity(idEntity)
                .build();
    }

    public MainHistory renameDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.RENAME.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory sendDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.SEND.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.DELETE.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory createFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.ADD.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory errorFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.ERROR.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory renameFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.RENAME.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory markFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.MARK.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory markDeleteFromDocument(String authorName, Integer idEntity) {
        return MainHistory.builder()
                .authorName(authorName)
                .createdDate(LocalDateTime.now())
                .essence(Essence.DOCUMENT.getName())
                .action(Action.MARKDELETE.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory addUser(String author, Integer idEntity) {
        return MainHistory
                .builder()
                .authorName(author)
                .createdDate(LocalDateTime.now())
                .essence(Essence.USER.getName())
                .action(Action.CREATED.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editUser(String author, Integer idEntity) {
        return MainHistory
                .builder()
                .authorName(author)
                .createdDate(LocalDateTime.now())
                .essence(Essence.USER.getName())
                .action(Action.EDIT.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory editUserPassword(String author, Integer idEntity) {
        return MainHistory
                .builder()
                .authorName(author)
                .createdDate(LocalDateTime.now())
                .essence(Essence.USER.getName())
                .action(Action.EDITPASSWORD.getText())
                .idEntity(idEntity)
                .build();
    }

    public static MainHistory deleteUser(String author, Integer idEntity) {
        return MainHistory
                .builder()
                .authorName(author)
                .createdDate(LocalDateTime.now())
                .essence(Essence.USER.getName())
                .action(Action.DELETE.getText())
                .idEntity(idEntity)
                .build();
    }
}