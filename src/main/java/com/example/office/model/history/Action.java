package com.example.office.model.history;

public enum Action {
    ADD("Добавил"),
    CREATED("Создал"),
    CREATEDFROMDEVICE("Создал со страницы устройства"),
    DELETE("Удалил"),
    DELETEFROMOBJECTLIST("Удалил из листа объекта"),
    DELETEFROMLIST("Удалил из списка локаций"),
    OPEN("Открыл"),
    LINK("Получил ссылку"),
    DOWNLOAD("Скачал"),
    RENAME("Переименовал"),
    SEND("Отправил"),
    HASENTERED("Зашел в систему"),
    UPLOAD("Загрузил"),
    ERROR("Ошибка"),
    COMMENT("Прокомментировал"),
    MARK("Поставил пометку на удаление"),
    MARKDELETE("Убрал пометку на удаление"),
    START("Начал"),
    CANCEL("Отменил"),
    COMPLETED("Завершил"),
    EDIT("Изменил"),
    EDITFROMCOUNTERPARTY("Изменил в профиле контрагента"),
    EDITFROMOBJECT("Изменил в профиле объекта"),
    EDITPASSWORD("Обновил пароль"),
    BURNED("Сгорело");

    private String text;

    Action(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}