package com.example.office.model.history.mainHistory;


import com.example.office.model.application.*;
import com.example.office.model.device.*;
import com.example.office.model.document.*;
import com.example.office.model.object.*;
import com.example.office.model.project.*;
import com.example.office.model.user.*;
import com.example.office.model.сounterparty.*;
import lombok.*;

import java.time.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class History {

    private int id;
    private User user;
    private String author;
    private LocalDateTime createdDate;
    private String essence;
    private String action;
    private Object object;

    public static History getCounter(MainHistory mainHistory, User user,Counterparty object){
        return History.builder()
                .action(mainHistory.getAction())
                .essence(mainHistory.getEssence())
                .user(user)
                .object(object)
                .createdDate(mainHistory.getCreatedDate())
                .build();
    }

    public static History getProject(MainHistory mainHistory, User user, Project object){
        return History.builder()
                .action(mainHistory.getAction())
                .essence(mainHistory.getEssence())
                .user(user)
                .object(object)
                .createdDate(mainHistory.getCreatedDate())
                .build();
    }

    public static History getDevice(MainHistory mainHistory, User user, Device object){
        return History.builder()
                .action(mainHistory.getAction())
                .essence(mainHistory.getEssence())
                .user(user)
                .object(object)
                .createdDate(mainHistory.getCreatedDate())
                .build();
    }

    public static History getApp(MainHistory mainHistory, User user, Application object){
        return History.builder()
                .action(mainHistory.getAction())
                .essence(mainHistory.getEssence())
                .user(user)
                .object(object)
                .createdDate(mainHistory.getCreatedDate())
                .build();
    }

    public static History getObjectCompany(MainHistory mainHistory, User user, ObjectCompany object){
        return History.builder()
                .action(mainHistory.getAction())
                .essence(mainHistory.getEssence())
                .user(user)
                .object(object)
                .createdDate(mainHistory.getCreatedDate())
                .build();
    }

    public static History getLocationCompany(MainHistory mainHistory, User user, ObjectLocation object){
        return History.builder()
                .action(mainHistory.getAction())
                .essence(mainHistory.getEssence())
                .user(user)
                .object(object)
                .createdDate(mainHistory.getCreatedDate())
                .build();
    }

    public static History getDocument(MainHistory mainHistory, String author, Document object){
        return History.builder()
                .action(mainHistory.getAction())
                .essence(mainHistory.getEssence())
                .author(author)
                .object(object)
                .createdDate(mainHistory.getCreatedDate())
                .build();
    }

    public static History getDeviceType(MainHistory mainHistory, User user, DeviceType object) {
        return History.builder()
                .action(mainHistory.getAction())
                .essence(mainHistory.getEssence())
                .user(user)
                .object(object)
                .createdDate(mainHistory.getCreatedDate())
                .build();
    }

    public static History getUser(MainHistory mainHistory, User user, User object) {
        return History.builder()
                .action(mainHistory.getAction())
                .essence(mainHistory.getEssence())
                .user(user)
                .object(object)
                .createdDate(mainHistory.getCreatedDate())
                .build();
    }
}