package com.example.office.model.history;

public enum Essence  {
    APPLICATION("Заявка"),
    DOCUMENT("Документ"),
    LOCATION("Локация"),
    OBJECT("Объект"),
    PROJECT("Проект"),
    DEVICE("Устройство"),
    DEVICETYPE("Тип устройства"),
    COUNTERPARTY("Контрагент"),
    USER("Юзер");

    private String name;

    Essence(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
