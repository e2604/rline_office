package com.example.office.model.history.mainHistory;

import com.example.office.model.application.Application;
import com.example.office.model.device.Device;
import com.example.office.model.document.Document;
import com.example.office.model.history.Action;
import com.example.office.model.history.Essence;
import com.example.office.model.object.ObjectCompany;
import com.example.office.model.object.ObjectLocation;
import com.example.office.model.project.Project;
import com.example.office.model.сounterparty.Counterparty;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface MainHistoryAction {
   static MainHistory createFromCounter(String userName, int idEntity){
       return MainHistory.builder()
               .authorName(userName)
               .createdDate(LocalDateTime.now())
               .essence(Essence.COUNTERPARTY.getName())
               .action(Action.CREATED.getText())
               .idEntity(idEntity)
               .build();
   };
    MainHistory deleteFromCounter(int userId, int idEntity);
    MainHistory openFromCounter(int userId, int idEntity);
    MainHistory errorFromCounter(int userId, int idEntity);

    MainHistory createFromLocation(int userId, int idEntity);
    MainHistory deleteFromLocation(int userId, int idEntity);
    MainHistory openFromLocation(int userId, int idEntity);
    MainHistory errorFromLocation(int userId, int idEntity);

    MainHistory createFromObject(int userId, int idEntity);
    MainHistory deleteFromObject(int userId, int idEntity);
    MainHistory openFromObject(int userId, int idEntity);
    MainHistory errorFromObject(int userId, int idEntity);

    MainHistory createFromProject(int userId, int idEntity);
    MainHistory deleteFromProject(int userId, int idEntity);
    MainHistory openFromProject(int userId, int idEntity);
    MainHistory errorFromProject(int userId, int idEntity);

    MainHistory createFromDevice(int userId, int idEntity);
    MainHistory deleteFromDevice(int userId, int idEntity);
    MainHistory openFromDevice(int userId, int idEntity);
    MainHistory errorFromDevice(int userId, int idEntity);

    MainHistory createFromApplication(int userId, int idEntity);
    MainHistory deleteFromApplication(int userId, int idEntity);
    MainHistory openFromApplication(int userId, int idEntity);
    MainHistory errorFromApplication(int userId, int idEntity);

    MainHistory uploadFromDocument(int userId, int idEntity);
    MainHistory openFromDocument( int userId, int idEntity);
    MainHistory linkFromDocument(int userId, int idEntity);
    MainHistory downloadFromDocument(int userId, int idEntity);
    MainHistory renameDocument(int userId, int idEntity);
    MainHistory sendDocument(int userId, int idEntity);
    MainHistory deleteFromDocument(int userId, int idEntity);
    MainHistory createFromDocument(int userId, int idEntity);
    MainHistory errorFromDocument(int userId, int idEntity);
}
