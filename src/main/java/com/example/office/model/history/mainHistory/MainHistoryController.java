package com.example.office.model.history.mainHistory;

import com.example.office.DTO.DocumentDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/histories")
@AllArgsConstructor
public class MainHistoryController {

    private MainHistoryService mainHistoryService;

    @GetMapping("/delete-all")
    public String getFolderAllDocument() {
       return mainHistoryService.deleteALl();
    }
}
