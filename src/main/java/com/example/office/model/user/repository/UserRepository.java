package com.example.office.model.user.repository;

import com.example.office.model.user.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import java.util.*;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByChatId(Long chatId); // для телеграм бота
    User findByIin (String iin);
    Optional<User> findByName(String name);
//    List <User> findAllApplicationId(Integer id);
    Optional<User> findByEmail (String email);
    List<User> findUserByNameIgnoreCaseContains(String text);

//    Optional<User> findUserByEmail(String email);

//    @Query("SELECT user FROM User user WHERE user.email = :email")
//    User getUserByEmail(@Param("email") String email);

    boolean existsByEmail(String email);
    boolean existsByIin(String iin);

    User findTopByOrderByIdDesc();
}