package com.example.office.model.user.repository;

import com.example.office.model.user.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
}