package com.example.office.model.user.repository;

import com.example.office.model.user.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends JpaRepository<Position, Integer> {
    Position findByName(String name);
    void deleteByName(String name);
}