package com.example.office.model.user;

import com.example.office.DTO.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.springframework.security.core.*;
import org.springframework.security.core.authority.*;
import org.springframework.security.core.userdetails.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;

@Getter
@Setter
@Entity
@Builder
@ToString
@NoArgsConstructor
@Table(name = "users")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 2, max = 20, message = "Имя пользователя должно содержать не менее 3 и не более 20 знаков")
    private String name;

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "фамилия пользователя должна содержать не менее 2 и не более 20 знаков")
    private String surname;

    @Column(unique = true)
    @Email(message = "Email address has invalid format: ${validatedValue}")
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Адрес электронной почты должен содержать не менее 3 и не более 20 знаков, таких как, латинские буквы, цифры и точки")
    @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")
    private String email;

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 8, max = 12, message = "Длина пароля должна быть не менее 8 и не более 30 знаков, включать латинские буквы, цифры и специальные символы")
    private String password;

    @NotBlank(message = "Данное поле не может быть пустым")
    private String phone;

    private Long chatId;  //Для телеграма

    @Column(unique = true)
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 12, max = 12, message = "Длина ИИН не может быть менее 12 знаков")
    private String iin;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "position_id")
    private Position position;

    private boolean enabled = true;

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "executor")
//    private List<Application> executorApplications;

    public static User from(UserDTO user) {
        return User.builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getEmail())
                .password(user.getPassword())
                .phone(user.getPhone())
                .chatId(user.getChatId())
                .iin(user.getIin())
                .role(Role.from(user.getRole()))
                .position(Position.from(user.getPosition()))
                .enabled(user.isEnabled())
                .build();
    }

    public String getFullName() {
        return name + " " + surname;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("FULL"));
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}