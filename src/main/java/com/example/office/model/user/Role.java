package com.example.office.model.user;

import com.example.office.DTO.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "roles")
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@ToString
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование роли должно содержать не менее 3 и не более 20 знаков")
    private String role;

    public static Role from(RoleDTO role) {
        return Role.builder()
                .id(role.getId())
                .role(role.getRole())
                .build();
    }
}