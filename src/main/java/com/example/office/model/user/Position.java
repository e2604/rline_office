package com.example.office.model.user;

import com.example.office.DTO.PositionDTO;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "positions")
@Entity
@ToString
public class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должности должно содержать не менее 3 и не более 20 знаков")
    private String name;

    public static Position from(PositionDTO position) {
        return Position.builder()
                .id(position.getId())
                .name(position.getName())
                .build();
    }
}