package com.example.office.model.application;

import com.example.office.DTO.TypeDTO;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "application_types")
@Entity
public class Type {
    @Id
    private Integer id;

    private String name;

    public static Type from(TypeDTO type) {
        return Type.builder()
                .id(type.getId())
                .name(type.getName())
                .build();
    }
}