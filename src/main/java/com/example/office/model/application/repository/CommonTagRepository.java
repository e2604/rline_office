package com.example.office.model.application.repository;

import com.example.office.model.application.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import java.util.List;

@Repository
public interface CommonTagRepository extends JpaRepository<CommonTag, Integer> {
    List<CommonTag> findAllById(Integer id);

    List<CommonTag> findAllByUserId(Integer id);
}