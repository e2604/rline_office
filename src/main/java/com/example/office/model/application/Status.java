package com.example.office.model.application;

import com.example.office.DTO.StatusDTO;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Builder
@Table(name = "statuses")
@Entity
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@ToString
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    private String name;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
    private List<Application> applications;

    public static Status from(StatusDTO status) {
        return Status.builder()
                .id(status.getId())
                .name(status.getName())
                .build();
    }
}