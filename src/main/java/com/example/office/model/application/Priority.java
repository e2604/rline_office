package com.example.office.model.application;

import com.example.office.DTO.*;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "priorities")
@Entity
public class Priority {
    @Id
    private Integer id;

    private String name;

    public static Priority from(PriorityDTO priority) {
        return Priority.builder()
                .id(priority.getId())
                .name(priority.getName())
                .build();
    }
}