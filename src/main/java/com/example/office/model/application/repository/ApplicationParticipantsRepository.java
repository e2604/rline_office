package com.example.office.model.application.repository;

import com.example.office.model.application.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;
import java.util.*;

@Repository
public interface ApplicationParticipantsRepository extends JpaRepository<ApplicationParticipants, Integer> {
    List<ApplicationParticipants> getApplicationParticipantsByApplicationId(Integer applicationId);
    List<ApplicationParticipants> getApplicationParticipantsByParticipantId(Integer applicationId);
//    void deleteApplicationParticipantsByParticipantAndApplication(User participant, Application id);
//    ApplicationParticipants findApplicationParticipantsByApplicationIdAndParticipantId(int applicationId, int participantId);
}