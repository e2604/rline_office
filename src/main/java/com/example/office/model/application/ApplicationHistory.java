package com.example.office.model.application;

import com.example.office.DTO.*;
import com.example.office.model.user.*;
import lombok.*;
import org.springframework.format.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.*;

@Getter
@Setter
@Builder
@Table(name = "application_histories")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private Application application;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id")
    private Status status;
    @Size(min = 3, max = 200, message = "Описание должно содержать не менее 3 и не более 200 знаков")
    private String description;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "executor_id")
    private User executor;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "priority_id")
    private Priority priority;
    @Column(name = "planned_start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime plannedStartDate;
    @Column(name = "planned_due_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime plannedDueDate;
    @Column(name = "client_price")
    private Double clientPrice;

    public static ApplicationHistory from(ApplicationHistoryDTO history) {
        return ApplicationHistory
                .builder()
                .id(history.getId())
                .application(Application.from(history.getApplication()))
                .status(Status.from(history.getStatus()))
                .description(history.getDescription())
                .executor(User.from(history.getExecutor()))
                .priority(Priority.from(history.getPriority()))
                .plannedStartDate(history.getPlannedStartDate())
                .plannedDueDate(history.getPlannedDueDate())
                .clientPrice(history.getClientPrice())
                .build();
    }
}