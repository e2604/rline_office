package com.example.office.model.application;

import com.example.office.DTO.*;
import com.example.office.model.device.*;
import com.example.office.model.document.*;
import com.example.office.model.object.*;
import com.example.office.model.project.*;
import com.example.office.model.user.*;
import com.example.office.model.сounterparty.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.springframework.format.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "applications")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Application {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id")
    private User author;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "executor_id")
    private User executor;
    private String folder;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "created_date")
    private LocalDateTime createdDate = LocalDateTime.now();

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    private String name;

    @Column(name = "minio_name")
    private String minioName;

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 200, message = "Описание должно содержать не менее 3 и не более 200 знаков")
    private String description;

    @OneToMany
    @JoinTable(name = "application_devices", joinColumns = @JoinColumn(name = "application_id"), inverseJoinColumns = @JoinColumn(name = "device_id"))
    private List<Device> devices;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "object_location_id")
    private ObjectLocation location;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "counterparty_id")
    private Counterparty counterparty;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    @Column(name = "client_price")
    private Double clientPrice;

    @Column(name = "executor_price")
    private Double executorPrice;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "payer_id")
    private Counterparty payer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id")
    private Status status;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "planned_start_date")
    private LocalDate planedStartDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "planned_due_date")
    private LocalDate planedDueDate;

    @Column(name = "actual_start_date")
    private LocalDateTime actualStartDate;

    @Column(name = "closed_date")
    private LocalDateTime closedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id")
    private Type type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "priority_id")
    private Priority priority;

    @Column(name = "is_active")
    private boolean isActive = true;

    @OneToMany
    @JoinTable(name = "application_participants", joinColumns = @JoinColumn(name = "application_id"), inverseJoinColumns = @JoinColumn(name = "participant_id"))
    private List<User> participants;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "application", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Document> documents;

    public static Application from(ApplicationDTO application) {

        if (application.getProject() != null) {
            return Application.builder()
                    .id(application.getId())
                    .author(User.from(application.getAuthor()))
                    .executor(User.from(application.getExecutor()))
                    .createdDate(application.getCreatedDate())
                    .name(application.getName())
                    .minioName(application.getMinioName())
                    .folder(application.getFolder())
                    .description(application.getDescription())
                    .location(ObjectLocation.from(application.getLocation()))
                    .counterparty(Counterparty.from(application.getCounterparty()))
                    .project(Project.from(application.getProject()))
                    .clientPrice(application.getClientPrice())
                    .executorPrice(application.getExecutorPrice())
                    .payer(Counterparty.from(application.getPayer()))
                    .planedStartDate(application.getPlanedStartDate())
                    .planedDueDate(application.getPlanedDueDate())
                    .actualStartDate(application.getActualStartDate())
                    .closedDate(application.getClosedDate())
                    .type(Type.from(application.getType()))
                    .priority(Priority.from(application.getPriority()))
                    .isActive(application.isActive())
                    .status(Status.from(application.getStatus()))
                    .build();
        } else {
            return Application.builder()
                    .id(application.getId())
                    .author(User.from(application.getAuthor()))
                    .executor(User.from(application.getExecutor()))
                    .createdDate(application.getCreatedDate())
                    .name(application.getName())
                    .minioName(application.getMinioName())
                    .folder(application.getFolder())
                    .description(application.getDescription())
                    .location(ObjectLocation.from(application.getLocation()))
                    .counterparty(Counterparty.from(application.getCounterparty()))
                    .clientPrice(application.getClientPrice())
                    .executorPrice(application.getExecutorPrice())
                    .payer(Counterparty.from(application.getPayer()))
                    .planedStartDate(application.getPlanedStartDate())
                    .planedDueDate(application.getPlanedDueDate())
                    .actualStartDate(application.getActualStartDate())
                    .closedDate(application.getClosedDate())
                    .type(Type.from(application.getType()))
                    .priority(Priority.from(application.getPriority()))
                    .isActive(application.isActive())
                    .status(Status.from(application.getStatus()))
                    .build();
        }
    }

    public String getFullExecutorName() {
        return executor.getName() + " " + executor.getSurname();
    }

    public String getCreatedDateJS() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return formatter.format(createdDate);
    }

    public List<Document> getDocumentsByReversedId() {
        List<Document> applicationDocuments = documents;
        applicationDocuments.sort(Comparator.comparing(Document::getId).reversed());
        return applicationDocuments;
    }
}