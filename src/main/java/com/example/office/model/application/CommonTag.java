package com.example.office.model.application;

import com.example.office.DTO.*;
import com.example.office.model.document.Document;
import com.example.office.model.user.*;
import lombok.*;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Builder
@Table(name = "common_tags")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class CommonTag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "document_id")
    private Document document;

    @OneToMany(fetch = FetchType.LAZY)
    private List<User> user;
    private String link;

    public static CommonTag from(CommonTagDTO tag) {

        List<User> userList = new ArrayList<>();
        for (int i = 0; i < tag.getUser().size(); i++) {
            User user = User.from(tag.getUser().get(i));
            userList.add(user);
        }

        return CommonTag
                .builder()
                .id(tag.getId())
                .user(userList)
                .document(tag.getDocument())
                .link(tag.getLink())
                .build();
    }
}