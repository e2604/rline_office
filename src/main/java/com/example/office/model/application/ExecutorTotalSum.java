package com.example.office.model.application;

public interface ExecutorTotalSum {
    Double getTotalSum();
    String getMonth();
}