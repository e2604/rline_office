package com.example.office.model.application.repository;

import com.example.office.model.application.ApplicationDevices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationDevicesRepository extends JpaRepository<ApplicationDevices, Integer> {
    List<ApplicationDevices> getApplicationDevicesByApplicationId(int id);
}
