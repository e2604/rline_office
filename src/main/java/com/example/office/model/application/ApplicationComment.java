package com.example.office.model.application;

import com.example.office.DTO.*;
import com.example.office.model.user.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.*;

@Getter
@Setter
@Builder
@Table(name = "application_comments")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationComment {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;
    @Column(name = "comment_text")
    @Size(min = 3, max = 512, message = "Максимальное количество символов - 512")
    private String commentText;

    @Column(name = "created_date")
    private LocalDateTime createdDate;
    @NotNull(message = "Данное поле не может быть пустым")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private User author;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private Application application;

//    public static ApplicationComment from(ApplicationCommentDTO comment){
//        return ApplicationComment
//                .builder()
//                .id(comment.getId())
//                .author(User.from(comment.getAuthor()))
//                .commentText(comment.getCommentText())
//                .createdDate(comment.getCreatedDate())
//                .application(Application.from(comment.getApplication()))
//                .build();
//    }
}