package com.example.office.model.application;

import com.example.office.model.user.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "application_participants")
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicationParticipants {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "application_id")
    @OneToOne
    private Application application;

    @JoinColumn(name = "participant_id")
    @OneToOne
    private User participant;
}
