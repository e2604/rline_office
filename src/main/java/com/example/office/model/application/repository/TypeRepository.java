package com.example.office.model.application.repository;

import com.example.office.model.application.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeRepository extends JpaRepository<Type, Integer> {
    Type findByName(String name);

    void deleteByName(String name);
}