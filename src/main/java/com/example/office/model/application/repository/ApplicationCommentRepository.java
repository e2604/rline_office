package com.example.office.model.application.repository;

import com.example.office.model.application.*;
import com.example.office.model.user.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import java.util.*;

@Repository
public interface ApplicationCommentRepository extends JpaRepository<ApplicationComment, Integer> {
//    Optional<ApplicationComment> findAllByAuthor(User user);
//    void deleteCommentByAuthor(User user);
    List<ApplicationComment> findAllByApplicationId(Integer id);
    Page<ApplicationComment>findAllByApplicationId(Integer id,Pageable pageable);

}