package com.example.office.model.application.repository;

import com.example.office.model.application.Priority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriorityRepository extends JpaRepository<Priority, Integer> {
    Priority findByName(String name);

    void deleteByName(String name);
}