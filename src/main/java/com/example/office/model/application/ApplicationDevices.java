package com.example.office.model.application;

import com.example.office.model.device.Device;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "application_devices")
@Entity
public class ApplicationDevices {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "application_id")
    @OneToOne
    private Application application;

    @JoinColumn(name = "device_id")
    @OneToOne
    private Device device;
}
