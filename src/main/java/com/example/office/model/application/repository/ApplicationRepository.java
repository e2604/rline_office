package com.example.office.model.application.repository;

import com.example.office.model.application.*;
import com.example.office.model.user.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import javax.annotation.*;
import java.util.*;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Integer> {
    @NotNull
    Optional<Application> findById(@NotNull Integer id);
    List<Application> findAllByProjectId(Integer id);
    List<Application> findAllByStatusId(Integer id);
    List<Application> findAllByExecutorId(Integer id);
    List<Application> findAllByParticipantsContains(User user);
    Application findTopByOrderByIdDesc();
    Page<Application> findAll(@Nullable Specification<Application> specification, Pageable pageable);
    List<Application> findApplicationsByNameIgnoreCaseContains(String text);
    List<Application> findApplicationByNameIgnoreCaseContainsAndExecutorOrParticipantsContains(String text, User executor, User participants);
    List<Application> findApplicationByNameIgnoreCaseContains(String text);
//    List<Application> findApplicationByNameIgnoreCaseContainsAnd(String text, User executor, List<User> participants);
//    List<Application> findByExecutorOrParticipants(int id);

    @Query(value="SELECT SUM(executor_price) AS totalSum, " +
            " CASE extract(month FROM closed_date) " +
            " WHEN 1 THEN 'Январь' " +
            " WHEN 2 THEN 'Февраль' " +
            " WHEN 3 THEN 'Март' " +
            " WHEN 4 THEN 'Апрель' " +
            " WHEN 5 THEN 'Май' " +
            " WHEN 6 THEN 'Июнь' " +
            " WHEN 7 THEN 'Июль' " +
            " WHEN 8 THEN 'Август' " +
            " WHEN 9 THEN 'Сентябрь' " +
            " WHEN 10 THEN 'Октябрь' " +
            " WHEN 11 THEN 'Ноябрь' " +
            " WHEN 12 THEN 'Декабрь'" +
            " END AS month FROM applications WHERE executor_id = ?1 and status_id = 3 " + //сумма складывается по id исполнителя с закрытой заявкой (id статуса = 3) за каждый месяц
            " GROUP BY extract(month FROM closed_date)" +
            " ORDER BY extract(month FROM closed_date)", nativeQuery = true)
    List<ExecutorTotalSum> getTotalSum(Integer executorId);

    @Query(value="SELECT SUM(executor_price) AS totalSum, " +
            " CASE extract(month FROM closed_date) " +
            " WHEN 1 THEN 'Январь' " +
            " WHEN 2 THEN 'Февраль' " +
            " WHEN 3 THEN 'Март' " +
            " WHEN 4 THEN 'Апрель' " +
            " WHEN 5 THEN 'Май' " +
            " WHEN 6 THEN 'Июнь' " +
            " WHEN 7 THEN 'Июль' " +
            " WHEN 8 THEN 'Август' " +
            " WHEN 9 THEN 'Сентябрь' " +
            " WHEN 10 THEN 'Октябрь' " +
            " WHEN 11 THEN 'Ноябрь' " +
            " WHEN 12 THEN 'Декабрь'" +
            " END AS month FROM applications WHERE executor_id = ?1 and status_id = 3 " + //сумма складывается по id исполнителя с закрытой заявкой (id статуса = 3) за каждый месяц
            " AND extract(month FROM closed_date) = extract(month from current_date)" +
            " GROUP BY month", nativeQuery = true)
    ExecutorTotalSum getTotalMonthSum(Integer executorId);

    @Query(value="SELECT * FROM applications WHERE status_id = 3 " +
            " ORDER BY closed_date DESC " +
            " LIMIT 10", nativeQuery = true) //последние 10 заявок с id статуса = 3 (закрытые) и отсортированные по дате закрытия в обратном порядке
    List<Application> findTop10ByStatusId(Integer statusId);
}