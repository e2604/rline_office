package com.example.office.model.document.repository;

import com.example.office.model.document.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

@Repository
public interface DocumentTypeRepository extends JpaRepository<DocumentsType, Integer> {
}