package com.example.office.model.document.repository;

import com.example.office.model.application.ApplicationComment;
import com.example.office.model.document.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

@Repository
public interface DocumentCommentRepository extends JpaRepository<DocumentComment, Integer> {
    Page<DocumentComment> findAllByDocumentId(Integer id, Pageable pageable);
}