package com.example.office.model.document;

import com.example.office.DTO.*;
import com.example.office.model.user.*;
import lombok.*;
import javax.persistence.*;
import java.time.*;

@Getter
@Setter
@Builder
@Table(name = "document_histories")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class DocumentHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id")
    private Document document;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "editor_id")
    private User editor;//редактор

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "document_status_id")
//    private DocumentStatus status;

    public static DocumentHistory from(DocumentHistoryDTO history) {
        return DocumentHistory
                .builder()
                .id(history.getId())
                .document(Document.from(history.getDocument()))
                .createdDate(history.getCreatedDate())
                .editor(User.from(history.getEditor()))
//                .status(DocumentStatus.from(history.getStatus()))
                .build();
    }
}