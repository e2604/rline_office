package com.example.office.model.document;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TransferDocument {
    private String fileName;
    private String folderName;
    private String exchangeName;
    private int docId;
    public String entity;

}
