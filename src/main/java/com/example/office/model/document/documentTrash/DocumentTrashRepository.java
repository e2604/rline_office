package com.example.office.model.document.documentTrash;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentTrashRepository extends JpaRepository<DocumentTrash,Integer> {
}
