package com.example.office.model.document;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class RenameDocumentClass {
    private int idContr;
    private String exName;
}
