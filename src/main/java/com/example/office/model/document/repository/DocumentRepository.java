package com.example.office.model.document.repository;

import com.example.office.model.application.Application;
import com.example.office.model.document.*;
import com.example.office.model.folder.FolderDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Integer> {
    List<Document> findByCounterpartyId(int id);
    List<Document> findDocumentByExchangeIgnoreCaseContains(String text);
    List<Document> findDocumentByExchangeIgnoreCaseContainsAndCreatedDateAfter(String text, LocalDate localDate);
    List<Document> findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBefore(String text,LocalDate localDate);
    List<Document> findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBetween(String text,LocalDate start,LocalDate finish);
    List<Document> findDocumentByCreatedDateBetween(LocalDate start, LocalDate finish);
    List<Document> findDocumentByCreatedDateAfter(LocalDate localDate);
    List<Document> findDocumentByCreatedDateBefore(LocalDate localDate);
    List<Document> findAllByMarkDeleteTrue();
    List<Document> findAllByDeviceId(int id);
    Document findFirstByOrderByCreatedDateDesc();
    Document findTopByOrderByIdDesc();

    List<Document> findDocumentByExchangeIgnoreCaseContainsAndMarkDeleteTrue(String text);
    List<Document> findDocumentByExchangeIgnoreCaseContainsAndCreatedDateAfterAndMarkDeleteTrue(String text, LocalDate localDate);
    List<Document> findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBeforeAndMarkDeleteTrue(String text,LocalDate localDate);
    List<Document> findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBetweenAndMarkDeleteTrue(String text,LocalDate start,LocalDate finish);
    List<Document> findDocumentByCreatedDateBetweenAndMarkDeleteTrue(LocalDate start, LocalDate finish);
    List<Document> findDocumentByCreatedDateAfterAndMarkDeleteTrue(LocalDate localDate);
    List<Document> findDocumentByCreatedDateBeforeAndMarkDeleteTrue(LocalDate localDate);


    List<Document> findDocumentByExchangeIgnoreCaseContainsAndFolderName(String text, String folderName);
    List<Document> findDocumentByExchangeIgnoreCaseContainsAndCreatedDateAfterAndFolderName(String text, LocalDate localDate, String folderName);
    List<Document> findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBeforeAndFolderName(String text,LocalDate localDate, String folderName);
    List<Document> findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBetweenAndFolderName(String text,LocalDate start,LocalDate finish, String folderName);
    List<Document> findDocumentByCreatedDateBetweenAndFolderName(LocalDate start, LocalDate finish, String folderName);
    List<Document> findDocumentByCreatedDateAfterAndFolderName(LocalDate localDate, String folderName);
    List<Document> findDocumentByCreatedDateBeforeAndFolderName(LocalDate localDate, String folderName);
    List<Document> findAllByFolderName(String folderName);
}