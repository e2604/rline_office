package com.example.office.model.document;

import com.example.office.DTO.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Builder
@Table(name = "document_statuses")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DocumentStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
    private List<Document> documents;

    public static DocumentStatus from(DocumentStatusDTO status) {
        return DocumentStatus
                .builder()
                .id(status.getId())
                .name(status.getName())
                .build();
    }

}