package com.example.office.model.document;

import com.example.office.DTO.*;
import com.example.office.model.folder.*;
import com.example.office.model.application.*;

import com.example.office.model.device.*;
import com.example.office.model.object.*;
import com.example.office.model.user.*;
import com.example.office.model.сounterparty.*;
import lombok.*;

import javax.persistence.*;
import java.time.*;
import java.util.*;

@Getter
@Setter
@Builder
@Table(name = "documents")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", unique = true, nullable = false)
//    @NotBlank(message = "Данное поле не может быть пустым")
//    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    private String name;
    private String exchange;
    private boolean error;
    @Column(name = "previous_name")
    private String previousName;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "folder_id")
    private Folder folder;
    private String style;
    private String path;
    private String note;
    private long size;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private User author;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_status_id")
    private DocumentStatus status;

    @Column(name = "document_type")
    private String type;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "document",cascade = CascadeType.ALL)
    private List<CommonTag> tags;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "document",cascade = CascadeType.ALL)
    private List<DocumentComment> comments;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "document",cascade = CascadeType.ALL)
    private List<DocumentHistory> histories;

    @Column(name = "is_change")
    private boolean isChange = false;

    @Column(name = "mark_delete")
    private boolean markDelete = false;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "counterparty_id")
    private Counterparty counterparty;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    private ObjectLocation location;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "device_id")
    private Device device;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private Application application;

    public static Document from(DocumentDTO document) {
        return Document
                .builder()
                .id(document.getId())
                .name(document.getName())
                .exchange(document.getExchange())
                .note(document.getNote())
                .path(document.getPath())
                .size(document.getSize())
                .error(document.isError())
                .previousName(document.getPreviousName())
                .folder(Folder.from(document.getFolder()))
                .author(User.from(document.getAuthor()))
                .createdDate(document.getCreatedDate())
                .status(DocumentStatus.from(document.getStatus()))
                .type(document.getType())
                .isChange(document.isChange())
                .markDelete(document.isMarkDelete())
                .counterparty(Counterparty.from(document.getCounterparty()))
                .location(ObjectLocation.from(document.getLocation()))
                .device(Device.from(document.getDevice()))
                .application(Application.from(document.getApplication()))
                .build();
    }

    public long getRoundedKBSize() {
        return size/1000;
    }

    public int getRoundedMBSize() {
        double newValue = size;
        double sizeInDouble = newValue / 1000000;
        return (int) Math.round(sizeInDouble);
    }
}