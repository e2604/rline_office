package com.example.office.model.document;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UploadDocument {
    private int formRadios;
    private MultipartFile multipartFile;
}
