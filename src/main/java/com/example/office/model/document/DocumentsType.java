package com.example.office.model.document;

import com.example.office.DTO.*;
import lombok.*;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Builder
@Table(name = "document_types")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class DocumentsType {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int id;
    private String name;
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "type")
    private List<Document> documents;

}