package com.example.office.model.document;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class SearchClass {
    private String search;
    private LocalDate planedStartDate;
    private LocalDate planedDueDate;
}
