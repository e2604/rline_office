package com.example.office.model.document;

import com.example.office.DTO.*;
import com.example.office.model.user.*;
import lombok.*;

import javax.persistence.*;
import java.time.*;

@Getter
@Setter
@Builder
@Table(name = "document_comments")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class DocumentComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "comment_text")
    private String commentText;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private User author;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id")
    private Document document;

//    public static DocumentComment from(DocumentCommentDTO comment) {
//        return DocumentComment
//                .builder()
//                .id(comment.getId())
//                .commentText(comment.getCommentText())
//                .createdDate(comment.getCreatedDate())
//                .author(User.from(comment.getAuthor()))
//                .document(Document.from(comment.getDocument()))
//                .build();
//    }
}