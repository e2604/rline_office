package com.example.office.model.document.documentTrash;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Getter
@Setter
@Builder
@Table(name = "documents_trash")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocumentTrash {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @Column(name = "minio_id")
    private String minioID; //UUID
    @Column(name = "date_delete")
    private LocalDate dateDelete;
    private String bucket;
    private String link;

    public long getLeftDays(){
        var days =ChronoUnit.DAYS.between(dateDelete, LocalDate.now());
        var done = 6-days;
        if (done<=0){
            return 0;
        }else {
            return done;
        }
    }
}
