package com.example.office.model.device.repository;

import com.example.office.model.device.*;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import javax.annotation.*;
import java.util.*;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Integer> {
    Device findTopByOrderByIdDesc();
    Page<Device> findAll(@Nullable Specification<Device> specification, Pageable pageable);
    List<Device> findAllByLocationId(Integer id);
    List<Device> findDevicesByNameIgnoreCaseContains(String text);
}