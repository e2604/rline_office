package com.example.office.model.device;

import com.example.office.model.user.*;
import lombok.*;
import org.springframework.format.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.*;
import java.time.format.*;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "device_comments")
public class DeviceComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "device_id")
    private Device device;

    @NotNull(message = "Данное поле не может быть пустым")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private User author;

    @Size(min = 3, max = 512, message = "Максимальное количество символов - 512")
    private String comment;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdDate;

//    public static DeviceComment from(DeviceCommentDTO comment) {
//        return DeviceComment
//                .builder()
//                .id(comment.getId())
//                .device(Device.from(comment.getDevice()))
//                .author(User.from(comment.getAuthor()))
//                .comment(comment.getComment())
//                .createdDate(comment.getCreatedDate())
//                .build();
//    }

    public String getCreatedDateJS() {
        DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return timeFormat.format(createdDate);
    }
}