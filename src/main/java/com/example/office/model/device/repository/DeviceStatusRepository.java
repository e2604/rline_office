package com.example.office.model.device.repository;

import com.example.office.model.device.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

@Repository
public interface DeviceStatusRepository extends JpaRepository<DeviceStatus, Integer> {
}