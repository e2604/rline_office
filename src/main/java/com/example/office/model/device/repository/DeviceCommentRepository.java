package com.example.office.model.device.repository;

import com.example.office.model.application.ApplicationComment;
import com.example.office.model.device.DeviceComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface DeviceCommentRepository extends JpaRepository <DeviceComment,Integer> {

    List<DeviceComment> findAllByDeviceId(Integer id);
}
