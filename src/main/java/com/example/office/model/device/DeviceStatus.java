package com.example.office.model.device;

import com.example.office.DTO.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Getter
@Setter
@Builder
@Table(name = "device_statuses")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class DeviceStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    private String name;

    public static DeviceStatus from(DeviceStatusDTO status) {
        return DeviceStatus
                .builder()
                .id(status.getId())
                .name(status.getName())
                .build();
    }
}