package com.example.office.model.device;

import com.example.office.DTO.*;
import com.example.office.model.document.*;
import com.example.office.model.object.*;
import com.example.office.model.user.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.*;
import java.util.*;

@Getter
@Setter
@Builder
@Table(name = "devices")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "Данное поле не может быть пустым")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private User author;

    private String folder;

    @NotNull(message = "Данное поле не может быть пустым")
    @Column(name = "created_date")
    private LocalDateTime createdDate = LocalDateTime.now();

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    private String name;

    @Column(name = "minio_name")
    private String minioName;

    @Size(min = 3, max = 200, message = "Описание должно содержать не менее 3 и не более 200 знаков")
    private String description;

    @NotNull(message = "Данное поле не может быть пустым")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id")
    private DeviceType type;

    @NotNull(message = "Данное поле не может быть пустым")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    private ObjectLocation location;

    @Column(name = "inventory_number")
    @Size(max = 10, message = "Максимальное количество символов - 10")
    private String inventoryNumber;

    @Column(name = "serial_number")
    @Size(max = 10, message = "Максимальное количество символов - 10")
    private String serialNumber;

    @NotNull(message = "Данное поле не может быть пустым")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id")
    private DeviceStatus status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "device",cascade = CascadeType.ALL)
    private List<Document> documents;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "device", cascade = CascadeType.ALL)
    private List<DeviceHistory> histories;

    public static Device from(DeviceDTO device) {
        return Device
                .builder()
                .id(device.getId())
                .author(User.from(device.getAuthor()))
                .createdDate(device.getCreatedDate())
                .folder(device.getFolder())
                .name(device.getName())
                .minioName(device.getMinioName())
                .description(device.getDescription())
                .type(DeviceType.from(device.getType()))
                .location(ObjectLocation.from(device.getLocation()))
                .inventoryNumber(device.getInventoryNumber())
                .serialNumber(device.getSerialNumber())
                .status(DeviceStatus.from(device.getStatus()))
                .build();
    }
}