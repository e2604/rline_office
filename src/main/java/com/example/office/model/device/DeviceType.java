package com.example.office.model.device;

import com.example.office.DTO.*;
import com.example.office.model.user.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.*;

@Getter
@Setter
@Builder
@Table(name = "device_types")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class DeviceType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    private String name;

    @NotNull(message = "Данное поле не может быть пустым")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private User author;

    @NotNull(message = "Данное поле не может быть пустым")
    @Column(name = "created_date")
    private LocalDateTime createdDate = LocalDateTime.now();

    @Size(min = 3, max = 200, message = "Описание должно содержать не менее 3 и не более 200 знаков")
    private String description;

    public static DeviceType from(DeviceTypeDTO type) {
        return DeviceType.builder()
                .id(type.getId())
                .name(type.getName())
                .author(User.from(type.getAuthor()))
                .createdDate(type.getCreatedDate())
                .description(type.getDescription())
                .build();
    }
}