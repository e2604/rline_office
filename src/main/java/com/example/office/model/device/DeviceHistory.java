package com.example.office.model.device;

import com.example.office.DTO.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@Table(name = "device_histories")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class DeviceHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "device_id")
    private Device device;

    @Size(min = 3, max = 200, message = "Описание должно содержать не менее 3 и не более 200 знаков")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id")
    private DeviceStatus status;

    public static DeviceHistory from(DeviceHistoryDTO history) {
        return DeviceHistory
                .builder()
                .id(history.getId())
                .device(Device.from(history.getDevice()))
                .description(history.getDescription())
                .status(DeviceStatus.from(history.getStatus()))
                .build();
    }
}