package com.example.office.model.folder.repository;

import com.example.office.model.folder.Folder;
import com.example.office.model.сounterparty.Counterparty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FolderRepository extends JpaRepository<Folder,Integer> {
    Optional<Folder> findByName(String name);
    List<Folder> findByPageNameIgnoreCaseContains(String text);
    List<Folder> findAllByAutoTrue();
    List<Folder> findAllByAutoFalse();
    boolean existsByName (String name);

}
