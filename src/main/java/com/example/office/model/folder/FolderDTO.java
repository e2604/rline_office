package com.example.office.model.folder;

import com.example.office.model.document.Document;
import lombok.*;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FolderDTO {

    private int id;
    private String name;
    private boolean auto;
    private String pageName;
    private int documentCount;
    private boolean fix;
    private long size;

    public static FolderDTO from(Folder folder) {

        return FolderDTO
                .builder()
                .id(folder.getId())
                .name(folder.getName())
                .pageName(folder.getPageName())
                .size(folder.getSize())
                .documentCount(folder.getDocumentCount())
                .auto(folder.isAuto())
                .fix(folder.isFix())
                .build();
    }


}
