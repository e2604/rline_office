package com.example.office.model.folder;

import com.example.office.DTO.CounterpartyDTO;
import com.example.office.model.document.Document;
import com.example.office.model.сounterparty.Counterparty;
import com.example.office.model.сounterparty.CounterpartyType;
import lombok.*;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Builder
@Table(name = "folders")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Folder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @Column(name = "page_name")
    private String pageName;
    private boolean auto;
    private boolean fix;
    private long size;
    @Column(name = "document_count")
    private int documentCount;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "folder")
    private List<Document> documents;

    public static Folder from(FolderDTO folder) {
        return Folder
                .builder()
                .id(folder.getId())
                .name(folder.getName())
                .pageName(folder.getPageName())
                .size(folder.getSize())
                .documentCount(folder.getDocumentCount())
                .auto(folder.isAuto())
                .fix(folder.isFix())
                .build();
    }
}
