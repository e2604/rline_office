package com.example.office.model.project;

import com.example.office.DTO.*;
import com.example.office.model.application.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.springframework.format.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.*;
import java.util.*;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@Table(name = "projects")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "Имя Проекта не должно быть пустым")
    @Size(min = 4, message = "Имя проекта не может быть менее 4 символов")
    private String name;

    private String description;

    @Column(name = "created_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate createdDate;

    @Column(name = "closed_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate closedDate;

    @JoinColumn(name = "status_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull(message = "Данное поле не может быть пустым")
    private Status status;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Application> applications;

    public static Project from(ProjectDTO project) {
        return Project
                .builder()
                .id(project.getId())
                .name(project.getName())
                .createdDate(project.getCreatedDate())
                .closedDate(project.getClosedDate())
                .status(Status.from(project.getStatus()))
                .build();
    }
}