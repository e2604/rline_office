package com.example.office.model.project.repository;

import com.example.office.model.project.Project;
import com.example.office.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> , PagingAndSortingRepository <Project,Integer>{
    Optional<List<Project>> getProjectByStatusId(int statusId);
    Optional<Project> getProjectByName(String name);
    Page<Project> findAllByName(String name,Pageable pageable);
    Page<Project>findAll(@Nullable Specification<Project> specification, Pageable pageable);

    List<Project> findProjectByNameIgnoreCaseContains(String text);
}
