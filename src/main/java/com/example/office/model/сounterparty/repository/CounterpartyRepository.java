package com.example.office.model.сounterparty.repository;

import com.example.office.model.device.Device;
import com.example.office.model.project.Project;
import com.example.office.model.сounterparty.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import java.util.List;

@Repository
public interface CounterpartyRepository extends JpaRepository<Counterparty, Integer> {
    List<Counterparty> findCounterpartyByNameContains(String text);
    Counterparty findTopByOrderByIdDesc();
    boolean existsByMinioName(String minioName);

    List<Counterparty> findCounterpartiesByNameIgnoreCaseContains(String text);
}