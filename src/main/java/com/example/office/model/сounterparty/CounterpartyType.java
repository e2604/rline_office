package com.example.office.model.сounterparty;

import com.example.office.DTO.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "counterparty_types")
@Entity
public class CounterpartyType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    private String name;

    public static CounterpartyType from(CounterpartyTypeDTO type) {
        return CounterpartyType
                .builder()
                .id(type.getId())
                .name(type.getName())
                .build();
    }
}