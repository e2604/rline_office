package com.example.office.model.сounterparty.repository;

import com.example.office.model.сounterparty.CounterpartyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CounterpartyTypeRepository extends JpaRepository<CounterpartyType,Integer> {
}
