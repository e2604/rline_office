package com.example.office.model.сounterparty;

import com.example.office.DTO.*;
import com.example.office.model.document.*;
import com.example.office.model.object.*;
import lombok.*;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;

@Getter
@Setter
@Builder
@Table(name = "counterparties")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Counterparty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    @Unique
    private String name;
    @Column(name = "minio_name")
    private String minioName;
    @Size(min = 3, max = 200, message = "Описание должно содержать не менее 3 и не более 200 знаков")
    private String description;
    private String folder;
    @NotBlank(message = "Данное поле не может быть пустым")
    private String address;
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 12, max = 12, message = "Длина БИН должна быть равна 12 знакам")
    private String bin;
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Имя сотрудника должно содержать не менее 3 и не более 20 знаков")
    @Column(name = "contact_name")
    private String contactName;
    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 11, max = 11, message = "Длина номера телефона должна быть равна 11 знакам")
    @Column(name = "phone_number")
    private String phoneNumber;

    @NotNull(message = "Данное поле не может быть пустым")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "counterparty_type_id")
    private CounterpartyType type;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "counterparty",cascade = CascadeType.ALL)
    private List<Document> documents;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "counterparty",cascade = CascadeType.ALL)
    private List<ObjectLocation> locations;

    public static Counterparty from(CounterpartyDTO counterparty) {

        return Counterparty
                .builder()
                .id(counterparty.getId())
                .name(counterparty.getName())
                .folder(counterparty.getFolder())
                .minioName(counterparty.getMinioName())
                .description(counterparty.getDescription())
                .address(counterparty.getAddress())
                .bin(counterparty.getBin())
                .contactName(counterparty.getContactName())
                .phoneNumber(counterparty.getPhoneNumber())
                .type(CounterpartyType.from(counterparty.getType()))
                .build();
    }

    public static Counterparty fromJS(CounterpartyDTOJS counterparty) {
        return Counterparty
                .builder()
                .id(counterparty.getId())
                .name(counterparty.getName())
                .folder(counterparty.getFolder())
                .minioName(counterparty.getMinioName())
                .description(counterparty.getDescription())
                .address(counterparty.getAddress())
                .bin(counterparty.getBin())
                .contactName(counterparty.getContactName())
                .phoneNumber(counterparty.getPhoneNumber())
                .build();
    }


}