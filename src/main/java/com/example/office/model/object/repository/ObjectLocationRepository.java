package com.example.office.model.object.repository;

import com.example.office.model.object.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import java.util.*;

@Repository
public interface ObjectLocationRepository extends JpaRepository<ObjectLocation, Integer> {
    ObjectLocation findByAddress(String address);
    List<ObjectLocation> findByCounterpartyId(Integer id);
    ObjectLocation findTopByOrderByIdDesc();
    List<ObjectLocation> findAllByObjectId(Integer objectId);

    List<ObjectLocation> findLocationsByNameIgnoreCaseContains(String search);
}