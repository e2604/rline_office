package com.example.office.model.object.repository;

import com.example.office.model.object.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import java.util.*;

@Repository
public interface ObjectCompanyRepository extends JpaRepository<ObjectCompany,Integer> {
    ObjectCompany findTopByOrderByIdDesc();
    List<ObjectCompany> findObjectsByNameIgnoreCaseContains(String search);
}