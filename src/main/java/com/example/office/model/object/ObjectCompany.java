package com.example.office.model.object;

import com.example.office.DTO.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;

@Getter
@Setter
@Builder
@Table(name = "object_companies") //ObjectCompany - Тойота, а ObjectLocation будет хранить в себе все адреса Тойота центров, например, Тойота Центр на Абая, Тойота Центр на Аль-Фараби
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ObjectCompany { //в объекте уже будет локация(Пусть добавляется по мере необходимости)

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    private String name;

    @NotBlank(message = "Данное поле не может быть пустым")
    @Size(min = 3, max = 20, message = "Наименование должно содержать не менее 3 и не более 20 знаков")
    private String address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "object")
    private List<ObjectLocation> locations;

    public static ObjectCompany from(ObjectCompanyDTO object) {
            return ObjectCompany
                    .builder()
                    .id(object.getId())
                    .name(object.getName())
                    .address(object.getAddress())
                    .build();
    }

    public int getCountLocations(){
        return locations.size();
    }

    public List<ObjectLocation> getAllReversedLocations() {
        List<ObjectLocation> reversedLocations = locations;
        reversedLocations.sort(Comparator.comparing(ObjectLocation::getId).reversed());
        return reversedLocations;
    }
}