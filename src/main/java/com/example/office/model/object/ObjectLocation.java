package com.example.office.model.object;

import com.example.office.DTO.*;
import com.example.office.model.application.*;
import com.example.office.model.device.*;
import com.example.office.model.document.*;
import com.example.office.model.сounterparty.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Builder
@Table(name = "object_locations")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ObjectLocation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String address;
    private String name;
    private String folder;

    @Column(name = "contact_name")
    private String contactName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "minio_name")
    private String minioName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "counterparty_id")

    private Counterparty counterparty;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "object_id")
    @ToString.Exclude
    private ObjectCompany object;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    private List<Device> devices;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    @JsonIgnore
    private List<Application> applications;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location", cascade = CascadeType.ALL)
    private List<Document> documents;

    public static ObjectLocation from(ObjectLocationDTO location) {

        if (location.getObject() != null) {
            return ObjectLocation
                    .builder()
                    .id(location.getId())
                    .address(location.getAddress())
                    .folder(location.getFolder())
                    .contactName(location.getContactName())
                    .phoneNumber(location.getPhoneNumber())
                    .name(location.getName())
                    .minioName(location.getMinioName())
                    .counterparty(Counterparty.from(location.getCounterparty()))
                    .object(ObjectCompany.from(location.getObject()))
                    .build();
        } else {
            return ObjectLocation
                    .builder()
                    .id(location.getId())
                    .address(location.getAddress())
                    .folder(location.getFolder())
                    .contactName(location.getContactName())
                    .phoneNumber(location.getPhoneNumber())
                    .name(location.getName())
                    .minioName(location.getMinioName())
                    .counterparty(Counterparty.from(location.getCounterparty()))
                    .build();
        }
    }

    public int getCountDevices() {
        return devices.size();
    }

    public int getCountDocuments() {
        return documents.size();
    }
}