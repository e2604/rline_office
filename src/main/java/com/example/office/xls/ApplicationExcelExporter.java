package com.example.office.xls;

import com.example.office.DTO.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class ApplicationExcelExporter {
    private final XSSFWorkbook workbook;
    private XSSFSheet xssfSheet;
    private final List<ApplicationDTO> listUsers;

    public ApplicationExcelExporter(List<ApplicationDTO> listUsers) {
        this.listUsers = listUsers;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        xssfSheet = workbook.createSheet("Список заявок");

        Row row = xssfSheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(14);
        style.setFont(font);

        createCell(row, 0, "ID", style);
        createCell(row, 1, "Наименование", style);
        createCell(row, 2, "Статус", style);
        createCell(row, 3, "Приоритет", style);
        createCell(row, 4, "Тип", style);
        createCell(row, 5, "Дата создания", style);
        createCell(row, 6, "Срок выполнения", style);
        createCell(row, 7, "Исполнитель", style);
        createCell(row, 8, "Сумма исполнителю", style);
        createCell(row, 9, "Контрагент", style);
        createCell(row, 10, "Оплата от заказчика", style);
        createCell(row, 11, "Сумма от заказчика", style);
        createCell(row, 12, "Локация", style);
        createCell(row, 13, "Описание", style);
    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        xssfSheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(12);
        style.setFont(font);

        for (ApplicationDTO application : listUsers) {
            Row row = xssfSheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, application.getId(), style);
            createCell(row, columnCount++, application.getName(), style);
            createCell(row, columnCount++, application.getStatus().getName(), style);
            createCell(row, columnCount++, application.getPriority().getName(), style);
            createCell(row, columnCount++, application.getType().getName(), style);
            createCell(row, columnCount++, application.getCreatedDateJS(), style);
            createCell(row, columnCount++, application.getPlanedDueDate().toString(), style);
            createCell(row, columnCount++, application.getExecutor().getFullName(), style);
            createCell(row, columnCount++, application.getExecutorPrice().toString(), style);
            createCell(row, columnCount++, application.getCounterparty().getName(), style);
            createCell(row, columnCount++, application.getPayer().getName(), style);
            createCell(row, columnCount++, application.getClientPrice().toString(), style);
            createCell(row, columnCount++, application.getLocation().getName(), style);
            createCell(row, columnCount, application.getDescription(), style);
        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }
}