package com.example.office.xls;

import com.example.office.DTO.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.time.format.*;
import java.util.*;

public class DevicesExcelExport {
    private final XSSFWorkbook workbook;
    private XSSFSheet xssfSheet;
    private final List<DeviceDTO> devices;

    public DevicesExcelExport(List<DeviceDTO> devices) {
        this.devices = devices;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        xssfSheet = workbook.createSheet("Список устройств");

        Row row = xssfSheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(14);
        style.setFont(font);

        createCell(row, 0, "ID", style);
        createCell(row, 1, "Наименование", style);
        createCell(row, 2, "Тип устройства", style);
        createCell(row, 3, "Инвентарный номер", style);
        createCell(row, 4, "Серийный номер", style);
        createCell(row, 5, "Описание", style);
        createCell(row, 6, "Дата создания", style);
        createCell(row, 7, "Объект", style);
        createCell(row, 8, "Автор", style);
    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        xssfSheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(12);
        style.setFont(font);

        for (DeviceDTO device : devices) {
            Row row = xssfSheet.createRow(rowCount++);
            int columnCount = 0;

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            createCell(row, columnCount++, device.getId(), style);
            createCell(row, columnCount++, device.getName(), style);
            createCell(row, columnCount++, device.getType().getName(), style);
            createCell(row, columnCount++, device.getInventoryNumber(), style);
            createCell(row, columnCount++, device.getSerialNumber(), style);
            createCell(row, columnCount++, device.getDescription(), style);
            createCell(row, columnCount++, device.getCreatedDate().format(formatter), style);
            createCell(row, columnCount++, device.getLocation().getName(), style);
            createCell(row, columnCount++, device.getFullName(), style);
        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }
}