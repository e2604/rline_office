package com.example.office.xls;

import com.example.office.DTO.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class CounterpartyExcelExporter {
    private final XSSFWorkbook xssfWorkbook;
    private XSSFSheet xssfSheet;
    private final List<CounterpartyDTO> listCounterparties;

    public CounterpartyExcelExporter(List<CounterpartyDTO> listCounterparties) {
        this.listCounterparties = listCounterparties;
        xssfWorkbook = new XSSFWorkbook();
    }

    private void headerLine() {
        xssfSheet = xssfWorkbook.createSheet("Список контрагентов");
        Row rowObj = xssfSheet.createRow(0);
        CellStyle style = xssfWorkbook.createCellStyle();
        XSSFFont font = xssfWorkbook.createFont();
        font.setBold(true);
        font.setFontHeight(14);
        style.setFont(font);

        createCell(rowObj, 0, "ID", style);
        createCell(rowObj, 1, "Контрагент", style);
        createCell(rowObj, 2, "БИН/ИНН", style);
        createCell(rowObj, 3, "Контактное лицо", style);
        createCell(rowObj, 4, "Телефон", style);
        createCell(rowObj, 5, "Адрес", style);
        createCell(rowObj, 6, "Тип", style);
    }

    public void createCell(Row row, Integer columnCount, Object value, CellStyle style) {
        xssfSheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void dataLines() {
        int rowCount = 1;
        CellStyle style = xssfWorkbook.createCellStyle();
        XSSFFont font = xssfWorkbook.createFont();
        font.setFontHeight(12);
        style.setFont(font);

        for (CounterpartyDTO counterparty : listCounterparties) {
            Row rowObj = xssfSheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(rowObj, columnCount++, counterparty.getId(), style);
            createCell(rowObj, columnCount++, counterparty.getName(), style);
            createCell(rowObj, columnCount++, counterparty.getBin(), style);
            createCell(rowObj, columnCount++, counterparty.getContactName(), style);
            createCell(rowObj, columnCount++, counterparty.getPhoneNumber(), style);
            createCell(rowObj, columnCount++, counterparty.getAddress(), style);
            createCell(rowObj, columnCount, counterparty.getType().getName(), style);
        }
    }

    public void export(HttpServletResponse response) throws IOException {
        headerLine();
        dataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        xssfWorkbook.write(outputStream);
        xssfWorkbook.close();
        outputStream.close();
    }
}