package com.example.office.xls;

import com.example.office.DTO.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class ProjectExcelExporter {
    private final XSSFWorkbook workbook;
    private XSSFSheet projectSheet;
    private final List<ProjectDTO> projects;

    public ProjectExcelExporter(List<ProjectDTO> projects) {
        this.projects = projects;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        projectSheet = workbook.createSheet("Список проектов");
        Row row = projectSheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(14);
        style.setFont(font);

        createCell(row, 0, "ID", style);
        createCell(row, 1, "Проект", style);
        createCell(row, 2, "Статус проекта", style);
        createCell(row, 3, "Дата создания", style);
        createCell(row, 4, "Описание", style);
    }

    private void createCell(Row row, Integer columnCount, Object value, CellStyle style) {
        projectSheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(12);
        style.setFont(font);

        for (ProjectDTO projectDTO : projects) {
            Row row = projectSheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, projectDTO.getId(), style);
            createCell(row, columnCount++, projectDTO.getName(), style);
            createCell(row, columnCount++, projectDTO.getStatus().getName(), style);
            createCell(row, columnCount++, projectDTO.getCreatedDate().toString(), style);
            createCell(row, columnCount, projectDTO.getDescription(), style);
        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }
}