package com.example.office.xls;

import com.example.office.DTO.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class UserExcelExporter {
    private final XSSFWorkbook xssfWorkbook;
    private XSSFSheet xssfSheet;
    private final List<UserDTO> users;

    public UserExcelExporter(List<UserDTO> users) {
        this.users = users;
        xssfWorkbook = new XSSFWorkbook();
    }

    private void headerLine() {
        xssfSheet = xssfWorkbook.createSheet("Список пользователей");
        Row rowObj = xssfSheet.createRow(0);
        CellStyle style = xssfWorkbook.createCellStyle();
        XSSFFont font = xssfWorkbook.createFont();
        font.setBold(true);
        font.setFontHeight(14);
        style.setFont(font);

        createCell(rowObj, 0, "ID", style);
        createCell(rowObj, 1, "Имя Фамилия", style);
        createCell(rowObj, 2, "Телефон", style);
        createCell(rowObj, 3, "Должность", style);
        createCell(rowObj, 4, "Роль", style);
    }

    public void createCell(Row row, Integer columnCount, Object value, CellStyle style) {
        xssfSheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void dataLines() {
        int rowCount = 1;
        CellStyle style = xssfWorkbook.createCellStyle();
        XSSFFont font = xssfWorkbook.createFont();
        font.setFontHeight(12);
        style.setFont(font);

        for (UserDTO user : users) {
            Row rowObj = xssfSheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(rowObj, columnCount++, user.getId(), style);
            createCell(rowObj, columnCount++, user.getFullName(), style);
            createCell(rowObj, columnCount++, user.getPhone(), style);
            createCell(rowObj, columnCount++, user.getPosition().getName(), style);
            createCell(rowObj, columnCount, user.getRole().getRole(), style);
        }
    }

    public void export(HttpServletResponse response) throws IOException {
        headerLine();
        dataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        xssfWorkbook.write(outputStream);
        xssfWorkbook.close();
        outputStream.close();
    }
}