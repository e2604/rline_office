package com.example.office.xls;

import com.example.office.DTO.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class ObjectExcelExporter {

    private final XSSFWorkbook xssfWorkbook;
    private final XSSFSheet xssfSheet;
    private final List<ObjectCompanyDTO> objectList;

    public ObjectExcelExporter(List<ObjectCompanyDTO> objectList) {
        this.objectList = objectList;
        xssfWorkbook = new XSSFWorkbook();
        xssfSheet = xssfWorkbook.createSheet("Список объектов");
    }

    private void headerLine() {

        Row rowObject = xssfSheet.createRow(0);
        Cell cell = rowObject.createCell(0);
        cell.setCellValue("ID");

        CellStyle style = xssfWorkbook.createCellStyle();
        XSSFFont font = xssfWorkbook.createFont();
        font.setBold(true);
        font.setFontHeight(14);
        style.setFont(font);

        createCell(rowObject, 0, "ID", style);
        createCell(rowObject, 1, "Объект", style);
        createCell(rowObject, 2, "Адрес", style);
//        createCell(rowObject, 3, "Локаций у объекта", style);
    }

    public void createCell(Row row, Integer columnCount, Object value, CellStyle style) {
        xssfSheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void dataLines() {
        int rowCount = 1;
        CellStyle style = xssfWorkbook.createCellStyle();
        XSSFFont font = xssfWorkbook.createFont();
        font.setFontHeight(12);
        style.setFont(font);

        for (ObjectCompanyDTO object : objectList) {
            Row rowObject = xssfSheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(rowObject, columnCount++, object.getId(), style);
            createCell(rowObject, columnCount++, object.getName(), style);
            createCell(rowObject, columnCount, object.getAddress(), style);
        }
    }

    public void export(HttpServletResponse response) throws IOException {
        headerLine();
        dataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        xssfWorkbook.write(outputStream);
        xssfWorkbook.close();
        outputStream.close();
    }
}