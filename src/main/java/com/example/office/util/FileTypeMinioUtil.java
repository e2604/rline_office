package com.example.office.util;

import cn.hutool.core.io.FileTypeUtil;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class FileTypeMinioUtil {

    private final static String IMAGE_TYPE = "image/";
    private final static String AUDIO_TYPE = "audio/";
    private final static String VIDEO_TYPE = "video/";
    private final static String APPLICATION_TYPE = "application/";
    private final static String TXT_TYPE = "text/";

    @SneakyThrows
    public static String getFileType(MultipartFile multipartFile) {


        InputStream inputStream;
        String type;

        inputStream = multipartFile.getInputStream();
        type = FileTypeUtil.getType(inputStream);
        if (type.equalsIgnoreCase("JPG") || type.equalsIgnoreCase("JPEG")
                || type.equalsIgnoreCase("GIF") || type.equalsIgnoreCase("PNG")
                || type.equalsIgnoreCase("PDF")) {
            return IMAGE_TYPE + type;
        }

        if (type.equalsIgnoreCase("mp3") || type.equalsIgnoreCase("CD")) {
            return AUDIO_TYPE + type;
        }

        if (type.equalsIgnoreCase("mp4")) {
            return VIDEO_TYPE + type;
        }

        if (type.equalsIgnoreCase("doc") || type.equalsIgnoreCase("docx")
        || type.equalsIgnoreCase("zip") || type.equalsIgnoreCase("jar")||type.equalsIgnoreCase("xls")) {
            return APPLICATION_TYPE + type;
        }

        if (type.equalsIgnoreCase("txt") || type.equalsIgnoreCase("rtf")) {
            return TXT_TYPE + type;
        }

        return null;
    }
}