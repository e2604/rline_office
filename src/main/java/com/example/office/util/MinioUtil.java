package com.example.office.util;

import com.example.office.configuration.MinioConfiguration;
import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
@AllArgsConstructor
public class MinioUtil {

    private final MinioClient minioClient;
    private final MinioConfiguration minioConfiguration;

    //загрузка файлов
    @SneakyThrows
    public void putObject(String bucketName, MultipartFile multipartFile, String filename, String fileType) {
        InputStream inputStream = new ByteArrayInputStream(multipartFile.getBytes());
        minioClient.putObject(
                PutObjectArgs
                        .builder()
                        .bucket(bucketName)
                        .object(filename)
                        .stream(inputStream, -1, minioConfiguration.getFileSize())
                        .contentType(fileType)
                        .build());
    }

    @SneakyThrows
    public String copyObject(String bucketName, String objectName, String newBucketName) {
        boolean flag = bucketExists(bucketName);
        if (flag) {
            StatObjectResponse statObject = statObject(bucketName, objectName);
            if (statObject != null && statObject.size() > 0) {
                try {
                    minioClient.copyObject(
                            CopyObjectArgs.builder()
                                    .bucket(newBucketName)
                                    .object(objectName)
                                    .source(
                                            CopySource.builder()
                                                    .bucket(bucketName)
                                                    .object(objectName)
                                                    .build())
                                    .build());
                }catch (Exception e){
                    System.out.println("Ошибка");
                    return "ObjectERROR";
                }
            }

            return "Отправлен";
        }else {
            return "Объекта не существует";
        }

    }


    @SneakyThrows
    public void putSubFolder(String bucketName, MultipartFile multipartFile, String filename, String fileType, String subFolderName) {
        InputStream inputStream = new ByteArrayInputStream(multipartFile.getBytes());
        minioClient.putObject(
                PutObjectArgs
                        .builder()
                        .bucket(bucketName)
                        .object(subFolderName + "/" + filename)
                        .stream(inputStream, -1, minioConfiguration.getFileSize())
                        .contentType(fileType)
                        .build());
    }

    @SneakyThrows
    public void putSubFolderFromDevice(String bucketName, MultipartFile multipartFile, String filename, String fileType, String locationName, String deviceName) {
        InputStream inputStream = new ByteArrayInputStream(multipartFile.getBytes());
        minioClient.putObject(
                PutObjectArgs
                        .builder()
                        .bucket(bucketName)
                        .object(locationName + "/" + deviceName + "/" + filename)
                        .stream(inputStream, -1, minioConfiguration.getFileSize())
                        .contentType(fileType)
                        .build());
    }

    @SneakyThrows
    public InputStream getObjectFromLocation(String bucketName, String objectName) {

        boolean flag = bucketExists(bucketName);

        if (flag) {
            StatObjectResponse statObject = statObject(bucketName, objectName);

            if (statObject != null && statObject.size() > 0) {
                return minioClient.getObject(
                        GetObjectArgs.builder()
                                .bucket(bucketName)
                                .object(objectName)
                                .build());
            }
        }

        return null;
    }

    //путь к объекту
    @SneakyThrows
    public String getObjectUrl(String bucketName, String objectName) {
        boolean flag = bucketExists(bucketName);
//        Map<String, String> reqParams = new HashMap<String, String>();
//        reqParams.put("application/msword", "application/json");
        String url = "";

        if (flag) {
            url = minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs
                            .builder()
                            .method(Method.GET)
                            .bucket(bucketName)
                            .object(objectName)
                            .expiry(6, TimeUnit.DAYS)
//                            .extraQueryParams(reqParams)
                            .build());
            System.out.println(url);
        }
        return url;
    }

    //проверка бакетов
    @SneakyThrows
    public boolean bucketExists(String bucketName) {
        boolean found = minioClient.bucketExists(
                BucketExistsArgs
                        .builder()
                        .bucket(bucketName)
                        .build());
        return found;
    }

    //лист наименований всех бакетов
    @SneakyThrows
    public List<String> listBucketNames() {
        List<Bucket> bucketList = listBuckets();
        List<String> bucketListName = new ArrayList<>();

        for (Bucket bucket : bucketList) {
            bucketListName.add(bucket.name());
        }

        return bucketListName;
    }

    @SneakyThrows
    public Iterable<Result<Item>> listDocumentsBucket(String bucketName) {
        return minioClient.listObjects(
                ListObjectsArgs.builder().bucket(bucketName).build());
    }

    @SneakyThrows
    public List<Bucket> listAllBucket() {
        return minioClient.listBuckets();
    }

    //лист всех бакетов
    @SneakyThrows
    private List<Bucket> listBuckets() {
        return minioClient.listBuckets();
    }

    //лист наименований всех объектов в бакете
    @SneakyThrows
    public List<String> listObjectNames(String bucketName) {
        List<String> listObjectNames = new ArrayList<>();

        boolean flag = bucketExists(bucketName);

        if (flag) {
            Iterable<Result<Item>> bucketObjects = listObjects(bucketName);
            for (Result<Item> result : bucketObjects) {
                Item item = result.get();
                listObjectNames.add(item.objectName());
            }
        } else {
            listObjectNames.add("Данная папка не существует");
        }

        return listObjectNames;
    }

    //лист всех объектов в бакете
    @SneakyThrows
    private Iterable<Result<Item>> listObjects(String bucketName) {
        boolean flag = bucketExists(bucketName);

        if (flag) {
            return minioClient.listObjects(
                    ListObjectsArgs
                            .builder()
                            .bucket(bucketName)
                            .build());
        }

        return null;
    }

    //создание бакета
    @SneakyThrows
    public boolean makeBucket(String bucketName) {
        boolean flag = bucketExists(bucketName);

        if (!flag) {
            minioClient.makeBucket(
                    MakeBucketArgs
                            .builder()
                            .bucket(bucketName)
                            .build());
            return true;
        } else {
            return false;
        }
    }

    //удаление бакета
    @SneakyThrows
    public boolean removeBucket(String bucketName) {
        boolean flag = bucketExists(bucketName);

        if (flag) {
            Iterable<Result<Item>> bucketObjects = listObjects(bucketName);

            for (Result<Item> result : bucketObjects) {
                Item item = result.get();

                //есть вложения, удаление не удалось
                if (item.size() > 0) {
                    return false;
                }
            }

            //удаление возможно, если бакет пуст
            minioClient.removeBucket(
                    RemoveBucketArgs
                            .builder()
                            .bucket(bucketName)
                            .build());

            flag = bucketExists(bucketName);

            if (!flag) {
                return true;
            }
        }

        return false;
    }

    //удаление объекта
    @SneakyThrows
    public boolean removeObject(String bucketName, String objectName) {
        boolean flag = bucketExists(bucketName);

        if (flag) {
            minioClient.removeObject(
                    RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
            return true;
        }

        return false;
    }

    @SneakyThrows
    public boolean downloadNewObject(String bucketName, String objectName, String fileName) {

        boolean flag = bucketExists(bucketName);

        if (flag) {
            minioClient.downloadObject(
                    DownloadObjectArgs.builder()
                            .bucket(bucketName)
                            .object(objectName)
                            .filename(fileName)
                            .build());
            return true;
        }
        return false;
    }

    //получение файла
    @SneakyThrows
    public InputStream getObject(String bucketName, String objectName) {
        boolean flag = bucketExists(bucketName);

        if (flag) {
            StatObjectResponse statObject = statObject(bucketName, objectName);

            if (statObject != null && statObject.size() > 0) {
                return minioClient.getObject(
                        GetObjectArgs.builder()
                                .bucket(bucketName)
                                .object(objectName)
                                .build());
            }
        }

        return null;
    }


    //метаданные объекта
    @SneakyThrows
    public StatObjectResponse statObject(String bucketName, String objectName) {
        boolean flag = bucketExists(bucketName);

        if (flag) {
            return minioClient.statObject(
                    StatObjectArgs
                            .builder()
                            .bucket(bucketName)
                            .object(objectName)
                            .build());
        }else {
            System.out.println("Нет такого документа");
            return null;
        }


    }
}