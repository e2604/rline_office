package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.configuration.*;
import com.example.office.model.application.*;
import com.example.office.model.device.*;
import com.example.office.model.document.*;
import com.example.office.model.application.repository.*;
import com.example.office.model.device.repository.*;
import com.example.office.model.document.documentTrash.*;
import com.example.office.model.document.repository.*;
import com.example.office.model.folder.*;
import com.example.office.model.folder.repository.*;
import com.example.office.model.history.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.object.*;
import com.example.office.model.object.repository.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import com.example.office.model.сounterparty.*;
import com.example.office.model.сounterparty.repository.*;
import io.minio.*;
import io.minio.errors.*;
import io.minio.messages.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.springframework.stereotype.*;
import org.springframework.web.multipart.*;

import java.io.*;
import java.security.*;
import java.time.*;
import java.util.*;

@Log4j2
@Service
@AllArgsConstructor
public class DocumentService {

    private final CounterpartyRepository counterpartyRepository;
    private final ObjectLocationRepository objectLocationRepository;
    private final UserRepository userRepository;
    private final DocumentStatusRepository statusRepository;
    private final MinioService minioService;
    private final DocumentRepository documentRepository;
    private final DeviceRepository deviceRepository;
    private final ApplicationRepository applicationRepository;
    private final MainHistoryRepository mainHistoryRepository;
    private final FolderRepository folderRepository;
    private final DocumentTrashRepository documentTrashRepository;

    public String validate(String name) {
        Translit translit = new Translit();//
        var trimName = name.trim();
        String regex = trimName.replaceAll("[-+.^:~_@#$&*()`!,]", "");
        var trimTwo = regex.trim();
        var nameExchange = trimTwo.replaceAll("\\s+", "-");
        var transliteName = translit.cyr2lat(nameExchange.toUpperCase());
        return transliteName.toLowerCase();
    }

    public int similarFiles(List<Document> listDocuments, Document element) {
        int yes = 0;
        for (Document document : listDocuments) {
            if (document.getName().equals(element.getName()) &&
                    document.getExchange().equals(element.getExchange()) &&
                    document.getSize() == element.getSize()) {
                yes = yes + 1;
            }
        }
        return yes;
    }

    public Folder findFolderAndIncreaseCount(String name, Long size) {
        var folder = folderRepository.findByName(name).orElseThrow();
        folder.setSize(folder.getSize() + size);
        folder.setDocumentCount(folder.getDocumentCount() + 1);
        folderRepository.save(folder);
        return folder;
    }

    public void saveCounterpartyDocument(MultipartFile file, Folder folder, Counterparty counterparty, User user) {
        var document = Document.builder()
                .name(file.getOriginalFilename())
                .folder(folder)
                .exchange(file.getOriginalFilename())
                .size(file.getSize())
                .author(user)
                .createdDate(LocalDateTime.now())
                .type(file.getContentType())
                .status(statusRepository.findById(2).orElseThrow())
                .isChange(false)
                .markDelete(false)
                .counterparty(counterparty)
                .build();
        documentRepository.save(document);
    }

    public void saveLocationDocument(MultipartFile file, Folder folder, ObjectLocation location, User user) {
        var document = Document.builder()
                .name(file.getOriginalFilename())
                .exchange(file.getOriginalFilename())
                .folder(folder)
                .size(file.getSize())
                .author(user)
                .createdDate(LocalDateTime.now())
                .type(file.getContentType())
                .status(statusRepository.findById(2).orElseThrow())
                .isChange(false)
                .markDelete(false)
                .counterparty(location.getCounterparty())
                .location(location)
                .build();
        documentRepository.save(document);
    }

    public void saveDeviceDocument(MultipartFile multipartFile, Folder folder, Device device, User user) {
        var document = Document.builder()
                .name(multipartFile.getOriginalFilename())
                .exchange(multipartFile.getOriginalFilename())
                .folder(folder)
                .size(multipartFile.getSize())
                .author(user)
                .createdDate(LocalDateTime.now())
                .type(multipartFile.getContentType())
                .status(statusRepository.findById(2).orElseThrow())
                .isChange(false)
                .markDelete(false)
                .counterparty(device.getLocation().getCounterparty())
                .location(device.getLocation())
                .device(device)
                .build();
        documentRepository.save(document);
    }

    public void saveApplicationDocument(MultipartFile multipartFile, Folder folder, Application application, User user) {
        var document = Document.builder()
                .name(multipartFile.getOriginalFilename())
                .exchange(multipartFile.getOriginalFilename())
                .folder(folder)
                .size(multipartFile.getSize())
                .author(user)
                .createdDate(LocalDateTime.now())
                .type(multipartFile.getContentType())
                .status(statusRepository.findById(2).orElseThrow())
                .isChange(false)
                .markDelete(false)
                .counterparty(application.getCounterparty())
                .application(application)
                .build();
        documentRepository.save(document);
    }

    public int getLastId() {
        return documentRepository.findTopByOrderByIdDesc().getId();
    }

    //Добавление по сущностям
    public void addDocument(int id, MultipartFile multipartFile, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        Counterparty counterparty = counterpartyRepository.findById(id).orElseThrow();
        var folder = findFolderAndIncreaseCount(counterpartyRepository.findById(id).orElseThrow().getMinioName(), multipartFile.getSize());
        saveCounterpartyDocument(multipartFile, folder, counterparty, user);
        var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
        mainHistoryRepository.save(history);
    }

    public void addDocumentFromLocation(int id, MultipartFile multipartFile, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var location = objectLocationRepository.findById(id).orElseThrow();
        var folder = findFolderAndIncreaseCount(location.getCounterparty().getMinioName(), multipartFile.getSize());
        saveLocationDocument(multipartFile, folder, location, user);
        var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
        mainHistoryRepository.save(history);
    }

    public void addDocumentFromDevice(int id, MultipartFile multipartFile, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var device = deviceRepository.findById(id).orElseThrow();
        var folder = findFolderAndIncreaseCount(device.getLocation().getCounterparty().getMinioName(), multipartFile.getSize());
        saveDeviceDocument(multipartFile, folder, device, user);
        var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
        mainHistoryRepository.save(history);
    }

    public void addDocumentFromApplication(int id, MultipartFile multipartFile, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var application = applicationRepository.findById(id).orElseThrow();
        var folder = findFolderAndIncreaseCount(application.getCounterparty().getMinioName(), multipartFile.getSize());
        saveApplicationDocument(multipartFile, folder, application, user);
        var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
        mainHistoryRepository.save(history);
    }
    //Добавление по сущностям(конец)

    public List<Document> getAll() {
        return documentRepository.findAll();
    }

    public List<Document> getResentDocuments() {

        List<Document> listNineElements = new ArrayList<>();
        var listAll = documentRepository.findAll();
        listAll.sort(Comparator.comparing(Document::getCreatedDate).reversed());

        for (int i = 0; i < listAll.size(); i++) {
            if (i > 9) {
                break;
            }
            listNineElements.add(listAll.get(i));
        }
        return listNineElements;
    }

    public int getCountFilesCounterparty(int id) {
        return documentRepository.findByCounterpartyId(id).size();
    }

    public Map<String, Long> getSizeFilesFromCategory() {
        Map<String, Long> mapSizes = new HashMap<>();
        mapSizes.put("images", 0L);
        mapSizes.put("document", 0L);
        mapSizes.put("audio", 0L);
        mapSizes.put("video", 0L);
        mapSizes.put("other", 0L);
        var listDocuments = documentRepository.findAll();
        for (Document listDocument : listDocuments) {
            var result = listDocument.getType().split("/")[0];
            switch (result.toLowerCase(Locale.ROOT)) {
                case "image":
                    mapSizes.put("images", mapSizes.get("images") + listDocument.getSize());
                    break;
                case "application":
                case "text":
                    mapSizes.put("document", mapSizes.get("document") + listDocument.getSize());
                    break;
                case "audio":
                    mapSizes.put("audio", mapSizes.get("audio") + listDocument.getSize());
                    break;
                case "video":
                    mapSizes.put("video", mapSizes.get("video") + listDocument.getSize());
                    break;
                default:
                    mapSizes.put("other", mapSizes.get("other") + listDocument.getSize());
                    break;
            }
        }
        return mapSizes;
    }

    public List<Document> getAllSort() {
        var listAll = documentRepository.findAll();
        listAll.sort(Comparator.comparing(Document::getCreatedDate).reversed());
        return listAll;
    }

    public List<DocumentDTO> getSearchListGlobal(SearchClass search) {

        if (search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() == null) {
            return remakeDTO(getAllSort());
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() == null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContains(search.getSearch()));
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() == null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndCreatedDateAfter(search.getSearch(), search.getPlanedStartDate()));
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBefore(search.getSearch(), search.getPlanedDueDate()));
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBetween(search.getSearch(), search.getPlanedStartDate(), search.getPlanedDueDate()));
        } else if (search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByCreatedDateBetween(search.getPlanedStartDate(), search.getPlanedDueDate()));
        } else if (search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() == null) {
            return remakeDTO(documentRepository.findDocumentByCreatedDateAfter(search.getPlanedStartDate()));
        } else if (search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByCreatedDateBefore(search.getPlanedDueDate()));
        } else {
            return remakeDTO(getAllSort());
        }
    }

    public Document getById(int id) {
        return documentRepository.findById(id).orElseThrow();
    }

    public String deleteDocument(Document document, String bucketName, String object) {

        boolean existFileOtherEntity = false;
        var documentRepo = documentRepository.findAll();
        int countDublicate = 0;
        for (Document value : documentRepo) {
            if (value.getName().equals(document.getName())
                    && value.getFolder().getName().equals(document.getFolder().getName())) {
                countDublicate = countDublicate + 1;
                if (countDublicate > 1) {
                    existFileOtherEntity = true;
                    break;
                }
            }
        }

        var mainHistory = mainHistoryRepository.findAll();
        for (MainHistory history : mainHistory) {
            if (history.getEssence().equals(Essence.DOCUMENT.getName()) && history.getIdEntity() == document.getId()) {
                mainHistoryRepository.delete(history);
            }
        }

        minioService.makeBucket("trash");
        var testBucket = minioService.isBucket("trash");
        if (testBucket) {
            minioService.copyObject(bucketName, object, "trash");
            var newLinkDoc = minioService.getObjectUrl("trash", object);
            var documentTrash = DocumentTrash.builder()
                    .bucket("trash")
                    .link(newLinkDoc)
                    .name(document.getExchange())
                    .dateDelete(LocalDate.from(LocalDateTime.now()))
                    .build();
            try {
                documentTrashRepository.save(documentTrash);
            } catch (Exception e) {
                System.out.println("Ошибка документа при удалении: " + e.getMessage());
            }
        }

        var folderNull = folderRepository.findByName(document.getFolder().getName()).orElseThrow();
        documentRepository.delete(document);
        folderNull.setDocumentCount(document.getFolder().getDocumentCount() - 1);
        folderNull.setSize(folderNull.getSize() - document.getSize());
        if (folderNull.getDocumentCount() <= 0) {
            folderNull.setDocumentCount(0);
        }

        if (folderNull.getSize() <= 0) {
            folderNull.setSize(0);
        }
        folderRepository.save(folderNull);

        if (existFileOtherEntity) {
            return "Объект удален";
        } else {
            return minioService.removeObject(bucketName, object) ? "Объект удален" : "Не удалось удалить объект";
        }
    }

    public String exchangeNameDocumentMVC(String name, int idoc, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        try {
            var document = documentRepository.findById(idoc).orElseThrow();
            document.setExchange(name);
            document.setChange(true);
            documentRepository.save(document);
            var history = MainHistory.renameFromDocument(user.getFullName(), document.getId());
            mainHistoryRepository.save(history);
            return "redirect:/file-manager-all";
        } catch (Exception ex) {
            return "Что то пошло не так";
        }
    }

    public String putMarkDelete(int id, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        try {
            var document = documentRepository.findById(id).orElseThrow();
            if (document.isMarkDelete()) {
                document.setMarkDelete(false);
                var history = MainHistory.markDeleteFromDocument(user.getFullName(), document.getId());
                mainHistoryRepository.save(history);
            } else {
                document.setMarkDelete(true);
                var history = MainHistory.markFromDocument(user.getFullName(), document.getId());
                mainHistoryRepository.save(history);
            }
            documentRepository.save(document);
            return "Пометка поставленна";
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "Что то пошло не так";
        }
    }

    public List<DocumentDTO> getSearchList(String search) {
        return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContains(search));
    }

    public String getLink(String bucketName, String objectName, int documentId, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var history = MainHistory.linkFromDocument(user.getFullName(), documentId);
        mainHistoryRepository.save(history);
        return minioService.getObjectUrl(bucketName, objectName);
    }

    public void putError(int id, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var document = documentRepository.findById(id).orElseThrow();
        document.setError(true);
        var history = MainHistory.errorFromDocument(user.getFullName(), document.getId());
        mainHistoryRepository.save(history);
        documentRepository.save(document);
    }

    public void putHistoryDownloadDocument(int documentId, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var history = MainHistory.downloadFromDocument(user.getFullName(), documentId);
        mainHistoryRepository.save(history);
    }

    public DocumentDTO getOneDocument(int id) {
        return DocumentDTO.from(documentRepository.findById(id).orElseThrow());
    }

    public void exchangeNameDocument(RenameDocumentClass renameDocumentClass, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var document = documentRepository.findById(renameDocumentClass.getIdContr()).orElseThrow();
        document.setExchange(renameDocumentClass.getExName());
        var history = MainHistory.renameFromDocument(user.getFullName(), document.getId());
        mainHistoryRepository.save(history);
        documentRepository.save(document);
    }

    public List<DocumentDTO> getAllSortDTO() {
        var done = remakeDTO(documentRepository.findAll());
        done.sort(Comparator.comparing(DocumentDTO::getCreatedDate).reversed());
        return done;
    }

    private List<DocumentDTO> remakeDTO(List<Document> list) {
        List<DocumentDTO> dtoList = new ArrayList<>();
        for (Document unit : list) {
            DocumentDTO documentDTO = DocumentDTO.from(unit);
            dtoList.add(documentDTO);
        }
        return dtoList;
    }

    public List<DocumentDTO> getMarkDeleteList() {
        return remakeDTO(documentRepository.findAllByMarkDeleteTrue());
    }

    public HashMap<DocumentDTO, DocumentDTO> getDuplicate() {
        List<Document> all = documentRepository.findAll();
        List<DocumentDTO> duplicates = new ArrayList<>(remakeDTO(all));
        HashMap<DocumentDTO, DocumentDTO> documentDTODocumentDTOHashMap = new HashMap<>();

        for (int i = 0; i < duplicates.size(); i++) {
            for (int z = i + 1; z < duplicates.size(); z++) {
                if (duplicates.get(i).getName().equals(duplicates.get(z).getName()) && duplicates.get(i).getSize() == duplicates.get(z).getSize()
                        || duplicates.get(i).getSize() == duplicates.get(z).getSize() && duplicates.get(i).getType().equals(duplicates.get(z).getType())) {
                    documentDTODocumentDTOHashMap.put(duplicates.get(i), duplicates.get(z));
                }                                                         //hashMap.get(document.nameDocument) = List<Folders>.add(Document.getFolder))
            }
        }
        return documentDTODocumentDTOHashMap;
    }

    public void deleteDocumentTrash(int id) {
        try {
            documentTrashRepository.delete(documentTrashRepository.findById(id).orElseThrow());
        } catch (Exception e) {
            System.out.println("Ошибка при удалении документа" + e.getMessage());
        }
    }

    public String getLinkTrash(String object) {
        return minioService.getObjectUrl("trash", object);
    }

//    public void addDocumentFromAdmin(int formRadios, MultipartFile multipartFile, String fileType) {
//
//        String bucketNameGeneral = "general";
//        String bucketNameAccountant = "accountant";
//
//        if (formRadios == 1) { //общий
//            var testFolder = folderRepository.existsByName(bucketNameGeneral);
//            if (testFolder) {
//                var folder = findFolderAndIncreaseCount(bucketNameGeneral, multipartFile.getSize());
//                try {
//                    minioService.putObject(multipartFile, bucketNameGeneral, fileType);
//                } catch (Exception e) {
//                    System.out.println("Errors upload admin: " + e);
//                }
//
//                var document = Document.builder()
//                        .name(multipartFile.getOriginalFilename())
//                        .folder(folder)
//                        .exchange(multipartFile.getOriginalFilename())
//                        .size(multipartFile.getSize())
//                        .style(bucketNameGeneral)
//                        .author(user)
//                        .createdDate(LocalDateTime.now())
//                        .type(fileType)
//                        .status(statusRepository.findById(1).orElseThrow())
//                        .isChange(false)
//                        .markDelete(false)
//                        .build();
//                documentRepository.save(document);
//
//                try {
//                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
//                    mainHistoryRepository.save(history);
//                } catch (Exception e) {
//                    System.out.println("С историей что то не то");
//                }
//            } else {
//                Folder folder = Folder.builder()
//                        .name(bucketNameGeneral)
//                        .size(0)
//                        .auto(false)
//                        .documentCount(0)
//                        .pageName(bucketNameGeneral)
//                        .build();
//                folder.setSize(folder.getSize() + fileSize);
//                folder.setDocumentCount(folder.getDocumentCount() + 1);
//                folderRepository.save(folder);
//
//                try {
//                    minioService.putObject(multipartFile, bucketNameGeneral, fileType);
//                } catch (Exception e) {
//                    System.out.println(e);
//                }
//
//                var document = Document.builder()
//                        .name(fileName)
//                        .folder(folder)
//                        .exchange(fileName)
//                        .size(fileSize)
//                        .style(bucketNameGeneral)
//                        .author(user)
//                        .createdDate(LocalDateTime.now())
//                        .type(fileType)
//                        .status(statusRepository.findById(1).orElseThrow())
//                        .isChange(false)
//                        .markDelete(false)
//                        .build();
//                documentRepository.save(document);
//
//                try {
//                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
//                    mainHistoryRepository.save(history);
//                } catch (Exception e) {
//                    System.out.println("С историей что то не то");
//                }
//            }
//        } else {
//            var testFolder = folderRepository.existsByName(bucketNameAccountant);
//            if (testFolder) {
//                var folder = folderRepository.findByName(bucketNameAccountant).orElseThrow();
//                folder.setSize(folder.getSize() + fileSize);
//                folder.setDocumentCount(folder.getDocumentCount() + 1);
//                folderRepository.save(folder);
//
//                try {
//                    minioService.putObject(multipartFile, bucketNameAccountant, fileType);
//                } catch (Exception e) {
//                    System.out.println(e);
//                }
//
//                var document = Document.builder()
//                        .name(fileName)
//                        .folder(folder)
//                        .exchange(fileName)
//                        .size(fileSize)
//                        .style(bucketNameAccountant)
//                        .author(user)
//                        .createdDate(LocalDateTime.now())
//                        .type(fileType)
//                        .status(statusRepository.findById(1).orElseThrow())
//                        .isChange(false)
//                        .markDelete(false)
//                        .build();
//                documentRepository.save(document);
//
//                try {
//                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
//                    mainHistoryRepository.save(history);
//                } catch (Exception e) {
//                    System.out.println("С историей что то не то");
//                }
//            } else {
//                Folder folder = Folder.builder()
//                        .name(bucketNameAccountant)
//                        .size(0)
//                        .auto(false)
//                        .documentCount(0)
//                        .pageName(bucketNameAccountant)
//                        .build();
//                folder.setSize(folder.getSize() + fileSize);
//                folder.setDocumentCount(folder.getDocumentCount() + 1);
//                folderRepository.save(folder);
//
//                try {
//                    minioService.putObject(multipartFile, bucketNameAccountant, fileType);
//                } catch (Exception e) {
//                    System.out.println(e);
//                }
//
//                var document = Document.builder()
//                        .name(fileName)
//                        .folder(folder)
//                        .exchange(fileName)
//                        .size(fileSize)
//                        .style(bucketNameAccountant)
//                        .author(user)
//                        .createdDate(LocalDateTime.now())
//                        .type(fileType)
//                        .status(statusRepository.findById(1).orElseThrow())
//                        .isChange(false)
//                        .markDelete(false)
//                        .build();
//                documentRepository.save(document);
//
//                try {
//                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
//                    mainHistoryRepository.save(history);
//                } catch (Exception e) {
//                    System.out.println("С историей что то не то");
//                }
//            }
//        }
//    }

    public String transfer(TransferDocument transferDocument, int idEntity, Principal principal) {
        try {
            var user = userRepository.findByEmail(principal.getName()).orElseThrow();
            var fileName = transferDocument.getFileName();
            var id = transferDocument.getDocId();
            var exchangeName = transferDocument.getExchangeName();
            var folderName = transferDocument.getFolderName();
            var bucket = ""; //куда положить файл
            var documentOld = documentRepository.findById(id).orElseThrow();

            String[] strings = {"CounterpartyDTO", "ObjectLocationDTO", "DeviceDTO", "ApplicationDTO",};

            for (int i = 0; i < strings.length; i++) {
                var indexJava = transferDocument.getEntity().indexOf(strings[i]);
                if (indexJava == -1) {
                    System.out.println("Не найдено");
                } else {
                    if (i == 0) {
                        var counterparty = counterpartyRepository.findById(idEntity).orElseThrow();
                        bucket = counterparty.getMinioName();

                        var similar = similarFiles(counterparty.getDocuments(), documentOld);
                        if (similar > 0) {
                            return "Такой документ присутствует у принимающей стороны";
                        }

                        var folder = findFolderAndIncreaseCount(bucket, documentOld.getSize());
                        if (bucket.equals(folderName)) {
                            var document = Document.builder()
                                    .name(fileName)
                                    .folder(folder)
                                    .exchange(exchangeName)
                                    .size(documentOld.getSize())
                                    .author(user)
                                    .createdDate(LocalDateTime.now())
                                    .type(documentOld.getType())
                                    .status(statusRepository.findById(2).orElseThrow())
                                    .isChange(false)
                                    .markDelete(false)
                                    .counterparty(counterparty)
                                    .build();
                            documentRepository.save(document);
                            documentRepository.save(document);
                            documentOld.setStatus(statusRepository.findById(2).orElseThrow());
                            documentRepository.save(documentOld);

                            try {
                                var history = MainHistory.sendDocument(user.getFullName(), documentOld.getId());
                                mainHistoryRepository.save(history);
                            } catch (Exception e) {
                                System.out.println("С историей что то не то");
                            }
                            return "Отправлено";
                        } else {
                            var result = minioService.copyObject(folderName, fileName, bucket);
                            if (result.equals("ObjectERROR") || result.equals("Объекта не существует")) {
                                return "File error :(";
                            } else {
                                var document = Document.builder()
                                        .name(fileName)
                                        .folder(folder)
                                        .exchange(exchangeName)
                                        .size(documentOld.getSize())
                                        .author(user)
                                        .createdDate(LocalDateTime.now())
                                        .type(documentOld.getType())
                                        .status(statusRepository.findById(2).orElseThrow())
                                        .isChange(false)
                                        .markDelete(false)
                                        .counterparty(counterparty)
                                        .build();
                                documentRepository.save(document);
                                documentOld.setStatus(statusRepository.findById(2).orElseThrow());
                                documentRepository.save(documentOld);

                                try {
                                    var history = MainHistory.sendDocument(user.getFullName(), documentOld.getId());
                                    mainHistoryRepository.save(history);
                                } catch (Exception e) {
                                    System.out.println("С историей что то не то");
                                }
                                return "Отправлено";
                            }
                        }
                    } else if (i == 1) {
                        var location = objectLocationRepository.findById(idEntity).orElseThrow();
                        bucket = location.getFolder();

                        var similar = similarFiles(location.getDocuments(), documentOld);
                        if (similar > 0) {
                            return "Такой документ присутствует у принимающей стороны";
                        }

                        var folder = findFolderAndIncreaseCount(bucket, documentOld.getSize());
                        if (bucket.equals(folderName)) {
                            var document = Document.builder()
                                    .name(fileName)
                                    .exchange(exchangeName)
                                    .folder(folder)
                                    .size(documentOld.getSize())
                                    .author(user)
                                    .createdDate(LocalDateTime.now())
                                    .type(documentOld.getType())
                                    .status(statusRepository.findById(2).orElseThrow())
                                    .isChange(false)
                                    .markDelete(false)
                                    .counterparty(location.getCounterparty())
                                    .location(location)
                                    .build();
                            documentRepository.save(document);
                            documentOld.setStatus(statusRepository.findById(2).orElseThrow());
                            documentRepository.save(documentOld);

                            try {
                                var history = MainHistory.sendDocument(user.getFullName(), documentOld.getId());
                                mainHistoryRepository.save(history);
                            } catch (Exception e) {
                                System.out.println("С историей что то не то");
                            }
                            return "Отправлено";
                        } else {
                            var result = minioService.copyObject(folderName, fileName, bucket);
                            if (result.equals("ObjectERROR") || result.equals("Объекта не существует")) {
                                return "File error :(";
                            } else {
                                var document = Document.builder()
                                        .name(fileName)
                                        .exchange(exchangeName)
                                        .folder(folder)
                                        .size(documentOld.getSize())
                                        .author(user)
                                        .createdDate(LocalDateTime.now())
                                        .type(documentOld.getType())
                                        .status(statusRepository.findById(2).orElseThrow())
                                        .isChange(false)
                                        .markDelete(false)
                                        .counterparty(location.getCounterparty())
                                        .location(location)
                                        .build();
                                documentRepository.save(document);
                                documentOld.setStatus(statusRepository.findById(2).orElseThrow());
                                documentRepository.save(documentOld);

                                try {
                                    var history = MainHistory.sendDocument(user.getFullName(), documentOld.getId());
                                    mainHistoryRepository.save(history);
                                } catch (Exception e) {
                                    System.out.println("С историей что то не то");
                                }
                                return "Отправлено";
                            }
                        }
                    } else if (i == 2) {
                        var device = deviceRepository.findById(idEntity).orElseThrow();
                        bucket = device.getFolder();

                        var similar = similarFiles(device.getDocuments(), documentOld);
                        if (similar > 0) {
                            return "Такой документ присутствует у принимающей стороны";
                        }

                        var folder = findFolderAndIncreaseCount(bucket, documentOld.getSize());

                        if (bucket.equals(folderName)) {
                            var document = Document.builder()
                                    .name(fileName)
                                    .exchange(exchangeName)
                                    .folder(folder)
                                    .size(documentOld.getSize())
                                    .author(user)
                                    .createdDate(LocalDateTime.now())
                                    .type(documentOld.getType())
                                    .status(statusRepository.findById(2).orElseThrow())
                                    .isChange(false)
                                    .markDelete(false)
                                    .counterparty(device.getLocation().getCounterparty())
                                    .location(device.getLocation())
                                    .device(device)
                                    .build();
                            documentRepository.save(document);
                            documentOld.setStatus(statusRepository.findById(2).orElseThrow());
                            documentRepository.save(documentOld);

                            try {
                                var history = MainHistory.sendDocument(user.getFullName(), documentOld.getId());
                                mainHistoryRepository.save(history);
                            } catch (Exception e) {
                                System.out.println("С историей что то не то");
                            }
                            return "Отправлено";
                        } else {
                            var result = minioService.copyObject(folderName, fileName, bucket);
                            if (result.equals("ObjectERROR") || result.equals("Объекта не существует")) {
                                return "File error :(";
                            } else {
                                var document = Document.builder()
                                        .name(fileName)
                                        .exchange(exchangeName)
                                        .folder(folder)
                                        .size(documentOld.getSize())
                                        .author(user)
                                        .createdDate(LocalDateTime.now())
                                        .type(documentOld.getType())
                                        .status(statusRepository.findById(2).orElseThrow())
                                        .isChange(false)
                                        .markDelete(false)
                                        .counterparty(device.getLocation().getCounterparty())
                                        .location(device.getLocation())
                                        .device(device)
                                        .build();

                                documentRepository.save(document);
                                documentOld.setStatus(statusRepository.findById(2).orElseThrow());
                                documentRepository.save(documentOld);

                                try {
                                    var history = MainHistory.sendDocument(user.getFullName(), documentOld.getId());
                                    mainHistoryRepository.save(history);
                                } catch (Exception e) {
                                    System.out.println("С историей что то не то");
                                }
                                return "Отправлено";
                            }
                        }
                    } else {
                        var application = applicationRepository.findById(idEntity).orElseThrow();
                        bucket = application.getFolder();

                        var similar = similarFiles(application.getDocuments(), documentOld);
                        if (similar > 0) {
                            return "Такой документ присутствует у принимающей стороны";
                        }
                        var folder = findFolderAndIncreaseCount(bucket, documentOld.getSize());

                        if (bucket.equals(folderName)) {
                            var document = Document.builder()
                                    .name(fileName)
                                    .exchange(exchangeName)
                                    .folder(folder)
                                    .size(documentOld.getSize())
                                    .author(user)
                                    .createdDate(LocalDateTime.now())
                                    .type(documentOld.getType())
                                    .status(statusRepository.findById(2).orElseThrow())
                                    .isChange(false)
                                    .markDelete(false)
                                    .counterparty(application.getCounterparty())
                                    .application(application)
                                    .build();
                            documentRepository.save(document);
                            documentOld.setStatus(statusRepository.findById(2).orElseThrow());
                            documentRepository.save(documentOld);

                            try {
                                var history = MainHistory.sendDocument(user.getFullName(), documentOld.getId());
                                mainHistoryRepository.save(history);
                            } catch (Exception e) {
                                System.out.println("С историей что то не то");
                            }
                            return "Отправлено";
                        } else {
                            var result = minioService.copyObject(folderName, fileName, bucket);
                            if (result.equals("ObjectERROR") || result.equals("Объекта не существует")) {
                                return "File error :(";
                            } else {
                                var document = Document.builder()
                                        .name(fileName)
                                        .exchange(exchangeName)
                                        .folder(folder)
                                        .size(documentOld.getSize())
                                        .author(user)
                                        .createdDate(LocalDateTime.now())
                                        .type(documentOld.getType())
                                        .status(statusRepository.findById(2).orElseThrow())
                                        .isChange(false)
                                        .markDelete(false)
                                        .counterparty(application.getCounterparty())
                                        .application(application)
                                        .build();
                                documentRepository.save(document);
                                documentOld.setStatus(statusRepository.findById(2).orElseThrow());
                                documentRepository.save(documentOld);

                                try {
                                    var history = MainHistory.sendDocument(user.getFullName(), documentOld.getId());
                                    mainHistoryRepository.save(history);
                                } catch (Exception e) {
                                    System.out.println("С историей что то не то");
                                }
                                return "Отправлено";
                            }
                        }
                    }
                }
            }
        } catch (NullPointerException e) {
            return "Отсутсвуют корректные вводные данные";
        }
        return "ERROR данные не корректы";
    }

    public List<DocumentDTO> getSearchListMarkDelete(SearchClass search) {
        if (search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() == null) {
            return getMarkDeleteList();
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() == null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndMarkDeleteTrue(search.getSearch()));
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() == null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndCreatedDateAfterAndMarkDeleteTrue(search.getSearch(), search.getPlanedStartDate()));
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBeforeAndMarkDeleteTrue(search.getSearch(), search.getPlanedDueDate()));
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBetweenAndMarkDeleteTrue(search.getSearch(), search.getPlanedStartDate(), search.getPlanedDueDate()));
        } else if (search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByCreatedDateBetweenAndMarkDeleteTrue(search.getPlanedStartDate(), search.getPlanedDueDate()));
        } else if (search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() == null) {
            return remakeDTO(documentRepository.findDocumentByCreatedDateAfterAndMarkDeleteTrue(search.getPlanedStartDate()));
        } else if (search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByCreatedDateBeforeAndMarkDeleteTrue(search.getPlanedDueDate()));
        } else {
            return getMarkDeleteList();
        }
    }

    public List<DocumentDTO> getFolderDocumentList(String folder) {
        return remakeDTO(documentRepository.findAllByFolderName(folder));
    }

    public List<DocumentDTO> getSearchListFolder(SearchClass search, String folderName) {
        if (search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() == null) {
            return getFolderDocumentList(folderName);
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() == null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndFolderName(search.getSearch(), folderName));
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() == null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndCreatedDateAfterAndFolderName(search.getSearch(), search.getPlanedStartDate(), folderName));
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBeforeAndFolderName(search.getSearch(), search.getPlanedDueDate(), folderName));
        } else if (!search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByExchangeIgnoreCaseContainsAndCreatedDateBetweenAndFolderName(search.getSearch(), search.getPlanedStartDate(), search.getPlanedDueDate(), folderName));
        } else if (search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByCreatedDateBetweenAndFolderName(search.getPlanedStartDate(), search.getPlanedDueDate(), folderName));
        } else if (search.getSearch().isEmpty() && search.getPlanedStartDate() != null && search.getPlanedDueDate() == null) {
            return remakeDTO(documentRepository.findDocumentByCreatedDateAfterAndFolderName(search.getPlanedStartDate(), folderName));
        } else if (search.getSearch().isEmpty() && search.getPlanedStartDate() == null && search.getPlanedDueDate() != null) {
            return remakeDTO(documentRepository.findDocumentByCreatedDateBeforeAndFolderName(search.getPlanedDueDate(), folderName));
        } else {
            return getFolderDocumentList(folderName);
        }
    }

    public List<Bucket> getAllBucketList() {
        return minioService.getBuckets();
    }

    public List<String> getAllDocumentsMinio(String bucketName) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {

        var iterator = minioService.getListObjects(bucketName);
        if (!iterator.iterator().hasNext()) {
            return new ArrayList<>();
        }
        List<String> names = new ArrayList<>();
        for (Result<Item> result : iterator) {
            names.add(result.get().objectName());
        }
        return names;
    }

    public int getCountDocumentsMinio(String bucketName) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {

        var iterator = minioService.getListObjects(bucketName);
        if (!iterator.iterator().hasNext()) {
            return 0;
        }
        int count = 0;
        for (Result<Item> result : iterator) {
            count++;
        }
        return count;
    }

    public int getCountDocumentsSystem(String bucketName) {
        try {
            var count = folderRepository.findByName(bucketName).orElseThrow();
            var size = count.getDocuments().size();
            System.out.println("COUNT: " + count);
            return size;
        } catch (Exception e) {
            System.out.println("ERROR COUNT DOCUMENT: " + e.getMessage());
            return 0;
        }
    }

    public boolean testIsDocument(String documentName, String bucketName) {
        try {
            var documents = folderRepository.findByName(bucketName).orElseThrow().getDocuments();
            return documents.stream().anyMatch(document -> document.getName().equals(documentName));
        } catch (Exception e) {
            System.out.println("IS TEST: " + e.getMessage());
            return false;
        }
    }

    public HashMap<String, List<String>> testList(List<String> minioList, String bucketName) {  //документы которые отсутствуют в minio, но есть в системе
        HashMap<String, List<String>> errors = new HashMap<>();

        try {
            var documents = folderRepository.findByName(bucketName).orElseThrow().getDocuments();
            List<String> pinkList = new ArrayList<>();
            for (Document document : documents) {
                pinkList.add(document.getName());
            }

            ArrayList<String> minioOut = new ArrayList<>(pinkList);
            minioOut.removeAll(minioList);

            ArrayList<String> systemOut = new ArrayList<>(minioList);
            systemOut.removeAll(pinkList);
            errors.put("Minio", minioOut);
            errors.put("System", systemOut);
            return errors;
        } catch (Exception e) {
            System.out.println("List error Test document uniq: " + e.getMessage());
            return new HashMap<>();
        }
    }

    public void deleteFromMinio(String bucketName, String fileName) {
        minioService.removeObject(bucketName, fileName);
    }

    public void deleteFromSystem(String bucketName, String fileName) {
        var folder = folderRepository.findByName(bucketName).orElseThrow();
        if (folder.getDocuments() != null) {
            for (int i = 0; i < folder.getDocuments().size(); i++) {
                if (folder.getDocuments().get(i).getName().equals(fileName)) {
                    var mainHistory = mainHistoryRepository.findAll();
                    for (MainHistory history : mainHistory) {
                        if (history.getEssence().equals(Essence.DOCUMENT.getName()) && history.getIdEntity() == folder.getDocuments().get(i).getId()) {
                            mainHistoryRepository.delete(history);
                        }
                    }
                    documentRepository.delete(folder.getDocuments().get(i));
                    break;
                }
            }
        }
    }

    public int getSimilarDocument(String bucketName) {
        try {
            int count = 0;
            var folder = folderRepository.findByName(bucketName).orElseThrow();
            if (folder.getDocuments() != null) {
                for (int i = 0; i < folder.getDocuments().size(); i++) {
                    for (int z = i + 1; z < folder.getDocuments().size(); z++) {
                        if (folder.getDocuments().get(i).getName().equals(folder.getDocuments().get(z).getName())) {
                            count++;
                        }
                    }
                }
            }
            return count;
        } catch (Exception e) {
            System.out.println("Проблема с выводом дубликатов в minioPage: " + e.getMessage());
            return 0;
        }
    }

    public String addDocumentFromFolderApi(String folderName, MultipartFile multipartFile, String fileType, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        String fileName = multipartFile.getOriginalFilename();
        var fileSize = multipartFile.getSize();
        var testFolder = folderRepository.existsByName(folderName);

        if (testFolder) {
            var folderOriginal = folderRepository.findByName(folderName).orElseThrow();
            var docTestExist = folderOriginal.getDocuments();
            if (docTestExist != null) {
                for (Document document : docTestExist) {
                    if (document.getName().equals(fileName) &&
                            document.getExchange().equals(fileName) &&
                            document.getSize() == fileSize) {
                        return "В этой папке присутствует аналогичный файл";
                    }
                }
            }
            try {
                var folder = findFolderAndIncreaseCount(folderName, multipartFile.getSize());
                minioService.putObject(multipartFile, folderName, fileType);

                var document = Document.builder()
                        .name(fileName)
                        .folder(folder)
                        .exchange(fileName)
                        .size(fileSize)
                        .style(folderName)
                        .author(user)
                        .createdDate(LocalDateTime.now())
                        .type(fileType)
                        .status(statusRepository.findById(1).orElseThrow())
                        .isChange(false)
                        .markDelete(false)
                        .build();
                documentRepository.save(document);

                try {
                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
                    mainHistoryRepository.save(history);
                } catch (Exception e) {
                    System.out.println("С историей что то не то");
                    return "Произошла ошибка с историей";
                }
                return "Загружено";
            } catch (Exception e) {
                System.out.println("Ошибка с загрузкой файла: " + e.getMessage());
                return "Невозможно загрузить объект в файловое хранилище";
            }
        } else {
            return "Данной папки не существует в файловом хранилище";
        }
    }

    public String addDocumentFromAdminApi(MultipartFile multipartFile, Principal principal) {
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        minioService.makeBucket("general");
        String bucketNameGeneral = "general";
        String fileType = multipartFile.getContentType();
        String fileName = multipartFile.getOriginalFilename();

        var fileSize = multipartFile.getSize();
        var testFolder = folderRepository.existsByName(bucketNameGeneral);

        if (testFolder) {
            var folderOriginal = folderRepository.findByName(bucketNameGeneral).orElseThrow();
            var docTestExist = folderOriginal.getDocuments();
            if (docTestExist != null) {
                for (Document document : docTestExist) {
                    if (document.getName().equals(fileName) &&
                            document.getExchange().equals(fileName) &&
                            document.getSize() == fileSize) {
                        return "В этой папке присутствует аналогичный файл";
                    }
                }
            }
            try {
                var folder = findFolderAndIncreaseCount(bucketNameGeneral, multipartFile.getSize());
                minioService.putObject(multipartFile, bucketNameGeneral, fileType);

                var document = Document.builder()
                        .name(fileName)
                        .folder(folder)
                        .exchange(fileName)
                        .size(fileSize)
                        .style(bucketNameGeneral)
                        .author(user)
                        .createdDate(LocalDateTime.now())
                        .type(fileType)
                        .status(statusRepository.findById(1).orElseThrow())
                        .isChange(false)
                        .markDelete(false)
                        .build();
                documentRepository.save(document);

                try {
                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
                    mainHistoryRepository.save(history);
                } catch (Exception e) {
                    System.out.println("С историей что то не то");
                    return "Произошла ошибка с историей";
                }
                return "Загружено";
            } catch (Exception e) {
                System.out.println("Ошибка с загрузкой файла: " + e.getMessage());
                return "Невозможно загрузить объект в файловое хранилище";
            }
        } else {
            return "Данной папки не существует в файловом хранилище";
        }
    }

    public String addDocumentFromCounterpartyApi(int id, MultipartFile multipartFile, String fileType, Principal principal) {
        var counterparty = counterpartyRepository.findById(id).orElseThrow();
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        String fileName = multipartFile.getOriginalFilename();
        var fileSize = multipartFile.getSize();
        var fileContentType = multipartFile.getContentType();
        var folder = folderRepository.findByName(counterpartyRepository.findById(id).orElseThrow().getMinioName()).orElseThrow();
        var testFolder = folderRepository.existsByName(folder.getName());

        if (testFolder) {
            var docTestExist = counterparty.getDocuments();
            if (docTestExist != null) {
                for (Document document : docTestExist) {
                    if (document.getName().equals(fileName) &&
                            document.getExchange().equals(fileName) &&
                            document.getSize() == fileSize && document.getLocation() == null && document.getDevice() == null && document.getApplication() == null) {
                        return "В этой папке присутствует аналогичный файл";
                    }
                }
            }
            try {
                folder.setSize(folder.getSize() + fileSize);
                folder.setDocumentCount(folder.getDocumentCount() + 1);
                folderRepository.save(folder);

                minioService.putObject(multipartFile, folder.getName(), fileType);

                var document = Document.builder()
                        .name(fileName)
                        .folder(folder)
                        .exchange(fileName)
                        .size(fileSize)
                        .author(user)
                        .createdDate(LocalDateTime.now())
                        .type(fileContentType)
                        .status(statusRepository.findById(2).orElseThrow())
                        .isChange(false)
                        .markDelete(false)
                        .counterparty(counterparty)
                        .build();
                documentRepository.save(document);
                try {
                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
                    mainHistoryRepository.save(history);
                } catch (Exception e) {
                    System.out.println("С историей что то не то");
                    return "Произошла ошибка с историей";
                }
                return "Загружено";
            } catch (Exception e) {
                System.out.println("Ошибка с загрузкой файла: " + e.getMessage());
                return "Невозможно загрузить объект в файловое хранилище";
            }
        } else {
            return "Данной папки не существует в файловом хранилище";
        }
    }

    public String addDocumentFromLocationApi(int id, MultipartFile multipartFile, String fileType, Principal principal) {
        var location = objectLocationRepository.findById(id).orElseThrow();
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        String fileName = multipartFile.getOriginalFilename();
        var fileSize = multipartFile.getSize();
        var fileContentType = multipartFile.getContentType();
        var folder = folderRepository.findByName(location.getCounterparty().getMinioName()).orElseThrow();
        var testFolder = folderRepository.existsByName(folder.getName());

        if (testFolder) {
            var docTestExist = location.getDocuments();
            if (docTestExist != null) {
                for (Document document : docTestExist) {
                    if (document.getName().equals(fileName) &&
                            document.getExchange().equals(fileName) &&
                            document.getSize() == fileSize) {
                        return "В этой папке присутствует аналогичный файл";
                    }
                }
            }
            try {
                folder.setSize(folder.getSize() + fileSize);
                folder.setDocumentCount(folder.getDocumentCount() + 1);
                folderRepository.save(folder);

                minioService.putObject(multipartFile, folder.getName(), fileType);

                var document = Document.builder()
                        .name(fileName)
                        .exchange(fileName)
                        .folder(folder)
                        .size(fileSize)
                        .author(user)
                        .createdDate(LocalDateTime.now())
                        .type(fileContentType)
                        .status(statusRepository.findById(2).orElseThrow())
                        .isChange(false)
                        .markDelete(false)
                        .counterparty(location.getCounterparty())
                        .location(location)
                        .build();
                documentRepository.save(document);

                try {
                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
                    mainHistoryRepository.save(history);
                } catch (Exception e) {
                    System.out.println("С историей что то не то");
                    return "Произошла ошибка с историей";
                }
                return "Загружено";
            } catch (Exception e) {
                System.out.println("Ошибка с загрузкой файла: " + e.getMessage());
                return "Невозможно загрузить объект в файловое хранилище";
            }
        } else {
            return "Данной папки не существует в файловом хранилище";
        }
    }

    public String addDocumentFromDeviceApi(int id, MultipartFile multipartFile, String fileType, Principal principal) {
        var device = deviceRepository.findById(id).orElseThrow();
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        String fileName = multipartFile.getOriginalFilename();
        var fileSize = multipartFile.getSize();
        var folder = folderRepository.findByName(device.getLocation().getCounterparty().getMinioName()).orElseThrow();
        var testFolder = folderRepository.existsByName(folder.getName());

        if (testFolder) {
            var docTestExist = device.getDocuments();
            if (docTestExist != null) {
                for (Document document : docTestExist) {
                    if (document.getName().equals(fileName) &&
                            document.getExchange().equals(fileName) &&
                            document.getSize() == fileSize) {
                        return "В этой папке присутствует аналогичный файл";
                    }
                }
            }
            try {
                folder.setSize(folder.getSize() + fileSize);
                folder.setDocumentCount(folder.getDocumentCount() + 1);
                folderRepository.save(folder);

                minioService.putObject(multipartFile, folder.getName(), fileType);

                var document = Document.builder()
                        .name(multipartFile.getOriginalFilename())
                        .exchange(multipartFile.getOriginalFilename())
                        .folder(folder)
                        .size(multipartFile.getSize())
                        .author(user)
                        .createdDate(LocalDateTime.now())
                        .type(multipartFile.getContentType())
                        .status(statusRepository.findById(2).orElseThrow())
                        .isChange(false)
                        .markDelete(false)
                        .counterparty(device.getLocation().getCounterparty())
                        .location(device.getLocation())
                        .device(device)
                        .build();
                documentRepository.save(document);

                try {
                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
                    mainHistoryRepository.save(history);
                } catch (Exception e) {
                    System.out.println("С историей что то не то");
                    return "Произошла ошибка с историей";
                }
                return "Загружено";
            } catch (Exception e) {
                System.out.println("Ошибка с загрузкой файла: " + e.getMessage());
                return "Невозможно загрузить объект в файловое хранилище";
            }
        } else {
            return "Данной папки не существует в файловом хранилище";
        }
    }

    public String addDocumentFromApplicationApi(int id, MultipartFile multipartFile, String fileType, Principal principal) {
        var application = applicationRepository.findById(id).orElseThrow();
        var user = userRepository.findByEmail(principal.getName()).orElseThrow();
        String fileName = multipartFile.getOriginalFilename();
        var fileSize = multipartFile.getSize();
        var folder = folderRepository.findByName(application.getCounterparty().getMinioName()).orElseThrow();
        var testFolder = folderRepository.existsByName(folder.getName());

        if (testFolder) {
            var docTestExist = application.getDocuments();
            if (docTestExist != null) {
                for (Document document : docTestExist) {
                    if (document.getName().equals(fileName) &&
                            document.getExchange().equals(fileName) &&
                            document.getSize() == fileSize) {
                        return "В этой папке присутствует аналогичный файл";
                    }
                }
            }
            try {
                folder.setSize(folder.getSize() + fileSize);
                folder.setDocumentCount(folder.getDocumentCount() + 1);
                folderRepository.save(folder);

                minioService.putObject(multipartFile, folder.getName(), fileType);

                var document = Document.builder()
                        .name(multipartFile.getOriginalFilename())
                        .exchange(multipartFile.getOriginalFilename())
                        .folder(folder)
                        .size(multipartFile.getSize())
                        .author(user)
                        .createdDate(LocalDateTime.now())
                        .type(multipartFile.getContentType())
                        .status(statusRepository.findById(2).orElseThrow())
                        .isChange(false)
                        .markDelete(false)
                        .counterparty(application.getCounterparty())
                        .application(application)
                        .build();
                documentRepository.save(document);

                try {
                    var history = MainHistory.createFromDocument(user.getFullName(), getLastId());
                    mainHistoryRepository.save(history);
                } catch (Exception e) {
                    System.out.println("С историей что то не то");
                    return "Произошла ошибка с историей";
                }
                return "Загружено";
            } catch (Exception e) {
                System.out.println("Ошибка с загрузкой файла: " + e.getMessage());
                return "Невозможно загрузить объект в файловое хранилище";
            }
        } else {
            return "Данной папки не существует в файловом хранилище";
        }
    }
}