package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.configuration.*;
import com.example.office.model.application.*;
import com.example.office.model.document.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.object.*;
import com.example.office.model.object.repository.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import com.example.office.model.сounterparty.*;
import com.example.office.model.сounterparty.repository.*;
import com.example.office.service.device.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.stereotype.*;

import java.security.*;
import java.util.*;
import java.util.stream.*;

@Service
@AllArgsConstructor
public class ObjectLocationService {

    private final ObjectLocationRepository objectLocationRepository;
    private final CounterpartyRepository counterpartyRepository;
    private final ObjectCompanyRepository objectCompanyRepository;
    private final MainHistoryRepository mainHistoryRepository;
    private final UserRepository userRepository;
    private final DeviceService deviceService;
    private final ApplicationDevicesService applicationDevicesService;

    public void add(ObjectLocationDTO objectLocationDTO, Principal principal) {

        Translit translit = new Translit();
        var trimName = objectLocationDTO.getAddress().trim();
        var nameExchange = trimName.replaceAll(" ", "-");

        var translitName = translit.cyr2lat(nameExchange.toUpperCase());
        var lowerName = translitName.toLowerCase();

        objectLocationDTO.setMinioName(lowerName);
        objectLocationDTO.setName(objectLocationDTO.getAddress());
        objectLocationDTO.setFolder(objectLocationDTO.getCounterparty().getFolder());
        objectLocationRepository.save(ObjectLocation.from(objectLocationDTO));

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var history = MainHistory.createFromLocation(user.getFullName(), getLastId());
        mainHistoryRepository.save(history);
    }

    public Counterparty getCounter(Integer counterpartyId) {
        return counterpartyRepository.findById(counterpartyId).orElseThrow();
    }

    public Integer getLastId() {
        return objectLocationRepository.findTopByOrderByIdDesc().getId();
    }

    public ObjectCompany getObject(Integer objectId) {
        return objectCompanyRepository.findById(objectId).orElseThrow();
    }

    public ObjectLocation getLocation(Integer locationId) {
        return objectLocationRepository.findById(locationId).orElseThrow();
    }

    public String deleteLocation(Integer id) {
        var counterId = objectLocationRepository.findById(id).orElseThrow().getCounterparty().getId();
        objectLocationRepository.delete(objectLocationRepository.findById(id).orElseThrow());
        var history = MainHistory.deleteFromLocation(userRepository.findById(1).orElseThrow().getName(), id);
        mainHistoryRepository.save(history);
        return "redirect:/counterparty/overview/" + counterId;
    }

    public ObjectLocation getOne(Integer id) {
        return objectLocationRepository.findById(id).orElseThrow();
    }

    public ObjectLocationDTO getById(Integer id) {
        return ObjectLocationDTO.from(objectLocationRepository.findById(id).orElseThrow());
    }

    public List<ObjectLocationDTO> getAll() {
        return remakeLocationDto(objectLocationRepository.findAll(Sort.by("id").descending()));
    }

    public List<ObjectLocationDTO> getAllForExcel() {
        return objectLocationRepository.findAll(Sort.by("id").ascending()).stream().map(ObjectLocationDTO::from).collect(Collectors.toList());
    }

    public List<ObjectLocation> getLocationList() {
        return objectLocationRepository.findAll();
    }

    private List<ObjectLocationDTO> remakeLocationDto(List<ObjectLocation> list) {
        List<ObjectLocationDTO> dtoList = new ArrayList<>();
        for (ObjectLocation unit : list) {
            ObjectLocationDTO objectLocationDTO = ObjectLocationDTO.from(unit);
            dtoList.add(objectLocationDTO);
        }
        return dtoList;
    }

    public String deleteLocationFromList(Integer id) {
        var counterId = objectLocationRepository.findById(id).orElseThrow().getCounterparty().getId();
        objectLocationRepository.delete(objectLocationRepository.findById(id).orElseThrow());
        return "redirect:/location/create";
    }

    public List<Document> getListDocument(Integer id) {

        var location = objectLocationRepository.findById(id).orElseThrow();
        List<Document> documents;
        documents = location.getDocuments();
        if (documents != null) {
            List<Document> filtered = new ArrayList<>();
            for (Document document : documents) {
                if (document.getDevice() == null) {
                    filtered.add(document);
                }

            }
            filtered.sort(Comparator.comparing(Document::getId).reversed());
            return filtered;
        }
        return null;
    }

    public int getDocumentsCount(Integer locationId) {
        return getListDocument(locationId).size();
    }

    public List<DeviceDTO> getNewDevices(Integer id) {
        List<DeviceDTO> devices = deviceService.getAllByLocationId(id);
        if (devices == null) {
            return new ArrayList<>();
        }
        devices.removeIf(device -> device.getStatus().getId() != 1);
        return devices;
    }

    public int getDevicesCount(Integer locationId) {
        return getNewDevices(locationId).size();
    }

    public List<DeviceDTO> getRewrittenDevices(Integer locationId, Integer applicationId) {
        List<ApplicationDevices> applicationDevices = applicationDevicesService.getDevices(applicationId);
        List<DeviceDTO> locationDevices = deviceService.getAllByLocationId(locationId);
        locationDevices.removeIf(locationDevice-> locationDevice.getStatus().getId() != 1);

        for (ApplicationDevices applicationDevice : applicationDevices) {
            locationDevices.removeIf(locationDevice -> Objects.equals(applicationDevice.getDevice().getId(), locationDevice.getId()));
        }
        return locationDevices;
    }

    public int getRewrittenDevicesCount(Integer locationId, Integer applicationId) {
        return getRewrittenDevices(locationId, applicationId).size();
    }

    public List<ObjectLocation> listAll() {
        return objectLocationRepository.findAll(Sort.by("id").ascending());
    }

    public void edit(Integer id, ObjectLocation edited, Principal principal) {
        ObjectLocation location = ObjectLocation.from(getById(id));
        location.setName(edited.getName());
        location.setAddress(edited.getAddress());
        location.setContactName(edited.getContactName());
        location.setPhoneNumber(edited.getPhoneNumber());
        location.setCounterparty(edited.getCounterparty());
        location.setObject(edited.getObject());
        objectLocationRepository.save(location);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editLocation(user.getFullName(), location.getId());
        mainHistoryRepository.save(history);
    }

    public void editFromCounterparty(Integer id, ObjectLocation edited, Principal principal) {
        ObjectLocation location = ObjectLocation.from(getById(id));
        location.setAddress(edited.getAddress());
        location.setContactName(edited.getContactName());
        location.setPhoneNumber(edited.getPhoneNumber());
        location.setObject(edited.getObject());
        objectLocationRepository.save(location);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editLocationFromCounterparty(user.getFullName(), location.getId());
        mainHistoryRepository.save(history);
    }

    public void editFromObject(Integer id, ObjectLocation edited, Principal principal) {
        ObjectLocation location = ObjectLocation.from(getById(id));
        location.setAddress(edited.getAddress());
        location.setContactName(edited.getContactName());
        location.setPhoneNumber(edited.getPhoneNumber());
        location.setObject(edited.getObject());
        objectLocationRepository.save(location);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editLocationFromObject(user.getFullName(), location.getId());
        mainHistoryRepository.save(history);
    }

    public List<ObjectLocationDTO> getAllLocationsFromCounterparty(Integer id) {
        Counterparty counterparty = counterpartyRepository.findById(id).orElseThrow();
        List<ObjectLocation> locations = objectLocationRepository.findAll();
        List<ObjectLocation> counterpartyLocationsList = new ArrayList<>();

        for (ObjectLocation location : locations) {
            if (location.getCounterparty().getId().equals(counterparty.getId())) {
                counterpartyLocationsList.add(location);
            }
        }
        counterpartyLocationsList.sort(Comparator.comparing(ObjectLocation::getId).reversed());
        return counterpartyLocationsList.stream().map(ObjectLocationDTO::from).collect(Collectors.toList());
    }

    public int getLocationsSize(Integer id) {
        return getAllLocationsFromCounterparty(id).size();
    }

    public void deleteLocationFromCounterparty(Integer counterpartyId, Integer locationId, Principal principal) {
        List<ObjectLocationDTO> counterpartyLocationsList = getAllLocationsFromCounterparty(counterpartyId);
        for (ObjectLocationDTO location : counterpartyLocationsList) {
            if (location.getId().equals(locationId)) {
                counterpartyLocationsList.remove(location);

                User user = userRepository.findByEmail(principal.getName()).orElseThrow();
                MainHistory history = MainHistory.deleteLocationFromCounterparty(user.getFullName(), location.getId());
                mainHistoryRepository.save(history);
                break;
            }
        }
    }

    public List<ObjectLocationDTO> getSearchLocations(String search) {
        return objectLocationRepository.findLocationsByNameIgnoreCaseContains(search)
                .stream().map(ObjectLocationDTO::from).collect(Collectors.toList());
    }
}