package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.exception.*;
import com.example.office.model.application.*;
import com.example.office.model.application.repository.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.user.*;
import com.example.office.model.user.User;
import com.example.office.model.user.repository.*;
import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.*;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
@PropertySource("classpath:telegram.properties")
public class UserService extends TelegramLongPollingBot {

    private final UserRepository userRepository;
    private final ApplicationRepository applicationRepository;
    private final ApplicationParticipantsRepository applicationParticipantsRepository;
    private final BCryptPasswordEncoder encoder;
    private final MainHistoryRepository historyRepository;

    public UserService(UserRepository userRepository, ApplicationRepository applicationRepository,
                       ApplicationParticipantsRepository applicationParticipantsRepository,
                       BCryptPasswordEncoder encoder,
                       MainHistoryRepository historyRepository) {
        this.userRepository = userRepository;
        this.applicationRepository = applicationRepository;
        this.applicationParticipantsRepository = applicationParticipantsRepository;
        this.encoder = encoder;
        this.historyRepository = historyRepository;
    }

    public List<UserDTO> getAll() {
        return userRepository.findAll(Sort.by("name").ascending()).stream().map(UserDTO::from).collect(Collectors.toList());
    }

    public List<UserDTO> getAllExecutors() {
        List<UserDTO> users = getAll();
        List<UserDTO> executors = new ArrayList<>();

        for (UserDTO user : users) {
            if (user.getPosition().getId() == 4) {
                executors.add(user);
            }
        }
        return executors;
    }

    public List<UserDTO> getAllParticipants() {
        List<UserDTO> users = getAll( );
        users.removeIf(user -> user.getRole().getId() == 3);
        return users;
    }

    public List<UserDTO> getUserSearchList(String search) {
        return userRepository.findUserByNameIgnoreCaseContains(search).stream().map(UserDTO::from).collect(Collectors.toList());
    }

//    public User findByIdAndChangePass(Integer id){
//        userRepository.findById(id);
//    }

    public UserDTO getById(Integer id) {
        return UserDTO.from(userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Пользователь с данным id не найден.")));
    }

    public User create(UserDTO userDTO, Principal principal) {

        User principalUser = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.addUser(principalUser.getFullName(), getLastId());
        historyRepository.save(history);

        User user = User
                .builder()
                .name(userDTO.getName())
                .surname(userDTO.getSurname())
                .email(userDTO.getEmail())
                .password(encoder.encode(userDTO.getPassword()))
                .chatId(userDTO.getChatId())
                .phone(userDTO.getPhone())
                .iin(userDTO.getIin())
                .role(Role.from(userDTO.getRole()))
                .position(Position.from(userDTO.getPosition()))
                .enabled(userDTO.isEnabled())
                .build();
        return userRepository.save(user);
    }

    public void edit(Integer id, User edited, Principal principal) {
        User user = User.from(getById(id));
        user.setName(edited.getName());
        user.setSurname(edited.getSurname());
        user.setEmail(edited.getEmail());
        user.setPhone(edited.getPhone());
        user.setPosition(edited.getPosition());
        user.setRole(edited.getRole());
        userRepository.save(user);

        User principalUser = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editUser(principalUser.getFullName(), user.getId());
        historyRepository.save(history);
    }

    public void deleteById(Integer userId, Principal principal) {
        userRepository.deleteById(userId);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deleteUser(user.getFullName(), userId);
        historyRepository.save(history);
    }

    public UserDTO findByIin(String iin) {
        UserDTO dto;
        try {
            User user = userRepository.findByIin(iin);
            dto = UserDTO.from(user);
        } catch (NullPointerException e) {
            e.printStackTrace();
            dto = null;
        }
        return dto;
    }

    public void findByEmailAndRecoverPass(String email) {
        User user = userRepository.findByEmail(email).orElseThrow();
        if (user == null) {
            log.debug("Пользователь с электронным адресом " + email + " не найден");
            return;
        }
        Long chatId = user.getChatId();
        if (chatId != null) {
            sendMessage(chatId.toString(), "Восстановление пароля: \n\nВаш Логин: " + user.getEmail() + "\nВаш Пароль: " + encoder.encode(user.getPassword()));
        } else {
            log.warn("Пользователь с Id: '" + user.getId() + "' и электронным адресом: '" + user.getEmail() + "' не зарегистрирован в телеграм боте");
        }
    }

    public List<Application> getApplicationByExecutorId(Integer id) {
        return applicationRepository.findAllByExecutorId(id);
    }

    public List<ApplicationParticipants> getApplicationByParticipantId(Integer id) {
        return applicationParticipantsRepository.getApplicationParticipantsByParticipantId(id);
    }

//TELEGRAM

    @Value("${bot.name}")
    private String botName;

    @Value("${bot.token}")
    private String botToken;

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();

        if (message.getText().equals("/start")) {
            execute(
                    SendMessage.builder()
                            .chatId(message.getChatId().toString())
                            .text("Доброго времени суток!\n\nПожалуйста введите ваш ИИН, если вас зарегистрировали в системе офиса.")
                            .build());
            return;
        } else if (message.getText().length() != 12 || !message.getText().matches("[0-9]{12}")) {
            execute(
                    SendMessage.builder()
                            .chatId(message.getChatId().toString())
                            .text("Пожалуйста введите ваш ИИН! \n\nВам будут высланы ваши Логин и Пароль.\n\n\n" +
                                    "Если это сообщение продолжает выходить, пожалуйста обратитесь к Администратору, за созданием аккаунта")
                            .build());
            return;
        }
        UserDTO find = findByIin(message.getText());
        long chatId = update.getMessage().getChatId();
        if (message.getText().equals(find.getIin())) {
            find.setChatId(chatId);
            userRepository.save(User.from(find));
            execute(
                    SendMessage.builder()
                            .chatId(message.getChatId().toString())
                            .text("Ваша информация обновлена!\n\nДобро пожаловать в коллектив!\n\nВаши данные для входа: \nЛогин: " + find.getEmail() + "\nПароль: За паролем обратитесь к Админу " + "\n\nПри создании вам заявки, вы будете уведомлены!")
                            .build());
        }
    }

    public void sendMessage(String chatId, String textMessage) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(textMessage);

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error("Не найден пользователь");
            e.printStackTrace();
        }
    }

    public List<ApplicationDTO> getAllApplicationsWithUser(Integer id) {
        User user = userRepository.findById(id).orElseThrow();
        List<Application> applications = applicationRepository.findAll();
        List<Application> userApplicationList = new ArrayList<>();

//        userApplicationList.addAll(applicationRepository.findAllByParticipantsContains(user));
//        userApplicationList.addAll(applicationRepository.findAllByExecutorId(user.getId()));

        for (Application application : applications) {
            if (application.getExecutor().getIin().equals(user.getIin()) || application.getParticipants().contains(user)) {
                userApplicationList.add(application);
            }
        }
        userApplicationList.sort(Comparator.comparing(Application::getId).reversed());
        return userApplicationList.stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public int getAllApplicationsWithUserCount(Integer userId) {
        return getAllApplicationsWithUser(userId).size();
    }

    public List<ApplicationDTO> getUserApplicationsSearchList(Integer id, String search) {
        User user = userRepository.findById(id).orElseThrow();
        List<Application> searched = applicationRepository.findApplicationsByNameIgnoreCaseContains(search);
        List<Application> userApplicationList = new ArrayList<>();

        for (Application application : searched) {
            if (application.getExecutor().getIin().equals(user.getIin()) || application.getParticipants().contains(user)) {
                userApplicationList.add(application);
            }
        }
        userApplicationList.sort(Comparator.comparing(Application::getId).reversed());
        return userApplicationList.stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

//    //Для профиля
//    public List<ApplicationDTO> getAllApplicationsWithUserProfile(Integer id) {
//        User user = userRepository.findById(id).orElseThrow();
//        List<Application> applications = applicationRepository.findAll();
//        List<Application> userApplicationList = new ArrayList<>();
//
////        userApplicationList.addAll(applicationRepository.findAllByParticipantsContains(user));
////        userApplicationList.addAll(applicationRepository.findAllByExecutorId(user.getId()));
//
//        for (Application application : applications) {
//            if (application.getExecutor().getIin().equals(user.getIin()) || application.getParticipants().contains(user)) {
//                userApplicationList.add(application);
//            }
//        }
//        userApplicationList.sort(Comparator.comparing(Application::getId).reversed());
//        return userApplicationList.stream().map(ApplicationDTO::from).collect(Collectors.toList());
//    }
//
//    public List<ApplicationDTO> getUserApplicationsSearchListProfile(Integer id, String search) {
//        User user = userRepository.findById(id).orElseThrow();
//        List<Application> searched = applicationRepository.findApplicationsByNameIgnoreCaseContains(search);
//        List<Application> userApplicationList = new ArrayList<>();
//
//        for (Application application : searched) {
//            if (application.getExecutor().getIin().equals(user.getIin()) || application.getParticipants().contains(user)) {
//                userApplicationList.add(application);
//            }
//        }
//        userApplicationList.sort(Comparator.comparing(Application::getId).reversed());
//        return userApplicationList.stream().map(ApplicationDTO::from).collect(Collectors.toList());
//    }

    public Integer closedApplicationsByUserCount(Integer userId) {
        User user = userRepository.findById(userId).orElseThrow();
        List<Application> applications = applicationRepository.findAll();
        List<Application> closedApplications = new ArrayList<>();

        for (Application application : applications) {
            if (application.getExecutor().getIin().equals(user.getIin()) || application.getParticipants().contains(user)) {
                if (application.getStatus().getName().contains("Выполнена")) {
                    closedApplications.add(application);
                }
            }
        }
        return closedApplications.size();
    }

    public Integer activeApplicationsByUserCount(Integer userId) {
        User user = userRepository.findById(userId).orElseThrow();
        List<Application> applications = applicationRepository.findAll();
        List<Application> activeApplications = new ArrayList<>();

        for (Application application : applications) {
            if (application.getExecutor().getIin().equals(user.getIin()) || application.getParticipants().contains(user)) {
                if (application.getStatus().getId().equals(2) || application.getStatus().getId().equals(1)) {
                    activeApplications.add(application);
                }
            }
        }
        return activeApplications.size();
    }

    public Integer allApplicationsByUserCount(Integer userId) {
        User user = userRepository.findById(userId).orElseThrow();
        List<Application> applications = applicationRepository.findAll();
        List<Application> allApplications = new ArrayList<>();

        for (Application application : applications) {
            if (application.getExecutor().getIin().equals(user.getIin()) || application.getParticipants().contains(user)) {
                if (!application.getStatus().getId().equals(4) || !application.getStatus().getId().equals(5)) {
                    allApplications.add(application);
                }
            }
        }
        return allApplications.size();
    }

    public Integer overdueApplicationsByUserCount(Integer userId) {
        User user = userRepository.findById(userId).orElseThrow();
        List<Application> applications = applicationRepository.findAll();
        List<Application> overdueApplications = new ArrayList<>();

        for (Application application : applications) {
            if (application.getExecutor().getIin().equals(user.getIin()) || application.getParticipants().contains(user)) {
                if (application.getStatus().getId() == 4) {
                    overdueApplications.add(application);
                }
            }
        }
        return overdueApplications.size();
    }

    public void changeUserPass(Integer id, String newPass, Principal principal) {
        User user = userRepository.findById(id).orElseThrow();
        user.setPassword(encoder.encode(newPass));
        userRepository.save(user);

        User principalUser = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editUserPassword(principalUser.getFullName(), user.getId());
        historyRepository.save(history);
    }

//    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//    String currentPrincipalName = authentication.getName();

//    public List<Application> getAllByExecutorOrParticipant(Integer id){
//        return applicationRepository.findByExecutorOrParticipants(id);
//    }

//    public Map<Application,User> getAllContainUser(Integer id) {
//        var user = userRepository.findById(id).orElseThrow();
//        Map<Application,User> applicationUserMap = new HashMap<>();
//
//        var applications = applicationRepository.findAll();
//
//        for (Application application : applications) {
//            if (application.getExecutor().getIin().equals(user.getIin()) || application.getParticipants().contains(user)) {
////                userApplications.add(new UserApplicationDTO(user.getId(), application.getId(), application.getStatus(), application.getPriority(), application.getClosedDate()));
////                userApplications.sort((o1, o2) -> );
//                applicationUserMap.put(application, user);
//            }
//        }
//        return applicationUserMap;
//    }

    public Integer getLastId() {
        return userRepository.findTopByOrderByIdDesc().getId();
    }

    public UserDTO getByEmail(String email) {
        return UserDTO.from(userRepository.findByEmail(email).orElseThrow());
    }

    public List<UserDTO> getAllForExcel() {
        return userRepository.findAll(Sort.by("id").ascending())
                .stream().map(UserDTO::from).collect(Collectors.toList());
    }

//    public void userExist(Model model, Principal principal) {
//        if (principal != null) {
//            if (userRepository.existsByEmail(principal.getName())) {
//                model.addAttribute("principal", true);
//            }
//        }
//    }

//    public static boolean onlyDigits(String string, Integer numbers) {
//        for (int i = 0; i < numbers; i++) {
//            if (string.charAt(i) >= '0'
//                    && string.charAt(i) <= '9') {
//                return true;
//            }
//            else {
//                return false;
//            }
//        }
//        return false;
//    }
//
//    public boolean isIinValid(String iin) {
//        if (!onlyDigits(iin, 12)) {
//            return false;
//        }
//
//        if (Integer.parseInt(iin.substring(4, 5)) > 3) {
//            return false;
//        }
//
//        if (Integer.parseInt(iin.substring(6, 7)) > 6) {
//            return false;
//        }
//
//        Integer s = 0;
//        for (int i = 0; i < 11; i++) {
//            s = s + (i + 1) * Integer.parseInt(iin.substring(i));
//        }
//
//        Integer k = s % 11;
//        if (k == 10) {
//            s = 0;
//            for (int i = 0; i < 11; i++) {
//                int t = (i + 3) % 11;
//                if (t == 0) {
//                    t = 11;
//                }
//                s = s + t * Integer.parseInt(iin.substring(i));
//            }
//            k = s % 11;
//            if (k == 10) {
//                return false;
//            }
//
//            return (k == Integer.parseInt(iin.substring(11, 12)));
//        }
//
//        return (k == Integer.parseInt(iin.substring(11, 12)));
//    }
}