package com.example.office.service;

import com.example.office.configuration.MinioConfiguration;
import com.example.office.configuration.Translit;
import com.example.office.model.document.Document;
import com.example.office.util.MinioUtil;
import io.minio.Result;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import lombok.*;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;

@Service
@AllArgsConstructor
public class MinioServiceImplement implements MinioService {

    private final MinioUtil minioUtil;
    private final MinioConfiguration minioConfiguration;


    @Override
    public String putObject(MultipartFile multipartFile, String bucketName, String fileType) {

        try {
            bucketName = StringUtils.isNotBlank(bucketName) ? bucketName : minioConfiguration.getBucketName();
            if (!this.bucketExists(bucketName)) {
                this.makeBucket(bucketName);
            }
            String fileName = multipartFile.getOriginalFilename();

            minioUtil.putObject(bucketName, multipartFile, fileName, fileType);

            return minioConfiguration.getEndpoint() + "/" + bucketName + "/" + fileName;
        } catch (Exception e) {
            e.printStackTrace();
            return "Загрузка не удалась" + e.getMessage();
        }
    }

    @Override
    public String copyObject(String bucketName, String objectName, String newBucketName) {
        return minioUtil.copyObject(bucketName,objectName,newBucketName);
    }

    @Override
    public String getObjectUrl(String bucketName, String objectName) {
        return minioUtil.getObjectUrl(bucketName, objectName);
    }

    @Override
    public void downloadNew(String bucketName, String objectName, String fileName) {
        minioUtil.downloadNewObject(bucketName,objectName,fileName);
    }

    @Override
    public InputStream downloadObject(String bucketName, String objectName) {
        return minioUtil.getObject(bucketName, objectName);
    }

    @Override
    public List<String> listBucketNames() {
        return minioUtil.listBucketNames();
    }

    @Override
    public List<Bucket> getBuckets() {
        return minioUtil.listAllBucket();
    }

    @Override
    public Iterable<Result<Item>> getListObjects(String bucketName) {
        return minioUtil.listDocumentsBucket(bucketName);
    }

    @Override
    public List<String> listObjectNames(String bucketName) {
        return minioUtil.listObjectNames(bucketName);
    }

    @Override
    public void makeBucket(String bucketName) {
        minioUtil.makeBucket(bucketName);
    }

    @Override
    public boolean isBucket(String name) {
        return minioUtil.bucketExists(name);
    }

    @Override
    public boolean bucketExists(String bucketName) {
        return minioUtil.makeBucket(bucketName);
    }

    @Override
    public boolean removeBucket(String bucketName) {
        return minioUtil.removeBucket(bucketName);
    }

    @Override
    public boolean removeObject(String bucketName, String objectName) {
        return minioUtil.removeObject(bucketName, objectName);
    }

    @Override
    public boolean removeListObject(String bucketName, List<String> objectNameList) {
        return false;
    }

    @Override
    public void putSubFolder(MultipartFile multipartFile, String bucketName, String fileType, String locationName) {
        String fileName = multipartFile.getOriginalFilename();
        minioUtil.putSubFolder(bucketName, multipartFile,fileName, fileType, locationName);
    }

    @Override
    public void putSubFolderFromDevice(MultipartFile multipartFile, String bucketName, String locationName, String deviceName) {
        String fileName = multipartFile.getOriginalFilename();
        String fileType2 = multipartFile.getContentType();
        minioUtil.putSubFolderFromDevice(bucketName,multipartFile,fileName,fileType2,locationName, deviceName);
    }
}