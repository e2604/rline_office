package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.application.Status;
import com.example.office.model.application.repository.StatusRepository;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class StatusService {
    private final StatusRepository statusRepository;

    public List<StatusDTO> getAll() {
        var a = statusRepository.findAll();
         return statusRepository.findAll().stream().map(status -> StatusDTO.from(status)).collect(Collectors.toList());
    }

    public Status getById(int id) {
        return statusRepository.findById(id).orElseThrow();
    }

    public StatusDTO getByName(String name) {
        return StatusDTO.from(statusRepository.findByName(name));
    }

    public void create(StatusDTO statusDTO) {
        Status status = Status
                .builder()
                .name(statusDTO.getName())
                .build();
        statusRepository.save(status);
    }

    public void deleteById(int id) {
        statusRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        statusRepository.deleteByName(name);
    }

    public Status createIndia(Status status) {
        return statusRepository.save(status);
    }


}