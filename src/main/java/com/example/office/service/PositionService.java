package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.user.Position;
import com.example.office.model.user.repository.PositionRepository;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
@AllArgsConstructor
public class PositionService {
    private PositionRepository positionRepository;

    public List<Position> getAll() {
        return positionRepository.findAll();
    }

    public Position getById(int id) {
        return positionRepository.getById(id);
    }

    public PositionDTO getByDtoId(int id) {
        return PositionDTO.from(positionRepository.findById(id).orElseThrow());
    }

    public PositionDTO getByName(String name) {
        return PositionDTO.from(positionRepository.findByName(name));
    }

    public Position create(PositionDTO positionDTO) {
        Position position = Position
                .builder()
                .name(positionDTO.getName())
                .build();
        return positionRepository.save(position);
    }

    public void deleteById(int id) {
        positionRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        positionRepository.deleteByName(name);
    }
}