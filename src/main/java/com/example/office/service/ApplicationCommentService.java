package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.application.*;
import com.example.office.model.application.repository.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.data.web.*;
import org.springframework.stereotype.*;
import org.springframework.data.domain.*;

import java.security.*;
import java.time.*;
import java.util.*;

@Service
@AllArgsConstructor
public class ApplicationCommentService {
    private final ApplicationCommentRepository applicationCommentRepository;
    private final ApplicationRepository applicationRepository;
    private final MainHistoryRepository mainHistoryRepository;
    private final UserRepository userRepository;

    public Page<ApplicationComment> getAllByApplicationId(Integer id, @PageableDefault(value = 3) Pageable pageable) {
        return applicationCommentRepository.findAllByApplicationId(id, pageable);
    }

    public List<ApplicationComment> getAll(Integer applicationId) {
        return applicationCommentRepository.findAllByApplicationId(applicationId);
    }

    public int getCommentsCount(Integer applicationId) {
        return applicationCommentRepository.findAllByApplicationId(applicationId).size();
    }

//    public ApplicationComment create(ApplicationCommentDTO commentDTO) {
//        User author = User.from(commentDTO.getAuthor());
//        Application application = Application.from(commentDTO.getApplication());
//        ApplicationComment comment = ApplicationComment
//                .builder()
//                .author(author)
//                .commentText(commentDTO.getCommentText())
//                .createdDate(LocalDateTime.now())
//                .application(application)
//                .build();
//        return applicationCommentRepository.save(comment);
//    }

    public ApplicationCommentDTO add(ApplicationCommentDTO commentDTO, Integer id, Principal principal) {
        var application = applicationRepository.findById(id).orElseThrow();
        User user = userRepository.findByEmail(principal.getName()).orElseThrow();

        ApplicationComment comment = ApplicationComment
                .builder()
                .author(user)
                .commentText(commentDTO.getCommentText())
                .createdDate(LocalDateTime.now())
                .application(application)
                .build();
        applicationCommentRepository.save(comment);
        var history = MainHistory.commentFromApplication(user.getFullName(), application.getId());
        mainHistoryRepository.save(history);
        return ApplicationCommentDTO.from(comment);
    }
}