package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.application.*;
import com.example.office.model.application.repository.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.project.*;
import com.example.office.model.project.repository.*;
import com.example.office.model.user.User;
import com.example.office.model.user.repository.*;
import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.web.*;
import org.springframework.http.*;
import org.springframework.data.domain.*;
import org.springframework.stereotype.*;
import org.springframework.web.server.*;

import java.security.*;
import java.time.*;
import java.util.*;
import java.util.stream.*;

@Service
@Log4j2
@AllArgsConstructor
public class ProjectService {
    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;
    private final StatusRepository statusRepository;
    private final ApplicationRepository applicationRepository;
    private final MainHistoryRepository mainHistoryRepository;

    public List<ProjectDTO> getAllDTO() {
        return projectRepository.findAll(Sort.by("id").descending())
                .stream().map(ProjectDTO::from).collect(Collectors.toList());
    }

    public List<ProjectDTO> getAllForExcel() {
        return projectRepository.findAll(Sort.by("id").ascending())
                .stream().map(ProjectDTO::from).collect(Collectors.toList());
    }

    public Page<Project> getAllProjectsPageList(@PageableDefault(value = 3) Pageable pageable) {
        return projectRepository.findAll(pageable);
    }

    public List<ProjectDTO> getProjectSearchList(String search) {
        return projectRepository.findProjectByNameIgnoreCaseContains(search).stream().map(ProjectDTO::from).collect(Collectors.toList());
    }

    public List<Project> getAll() {
        return projectRepository.findAll();
    }

    public ProjectDTO getProjectById(int id) {
        return ProjectDTO.from(projectRepository.findById(id).orElseThrow());
    }


    public List<Application> getApplicationsByProjectId(Integer id) {
        List<Application> projectApplications = applicationRepository.findAllByProjectId(id);
        projectApplications.sort(Comparator.comparing(Application::getId).reversed());
        return projectApplications;
    }

    public int getApplicationsCount(Integer projectId) {
        return getApplicationsByProjectId(projectId).size();
    }

    public String add(Project projectDTO, Principal principal) {
        var status = statusRepository.getById(1);
        var project = Project.builder()
                .name(projectDTO.getName())
                .description(projectDTO.getDescription())
                .createdDate(LocalDate.now())
                .closedDate(projectDTO.getClosedDate())
                .status(status)
                .build();
        projectRepository.save(project);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();

        var history = MainHistory.createFromProject(user.getFullName(), getLastId());
        mainHistoryRepository.save(history);
//        minioService.makeBucket(counterpartyDTO.getName());//Создается bucket с именем Контрагента
        return "redirect:/project/list";
    }

    public int getLastId() {
        return projectRepository.findById(projectRepository.findAll().size()).orElseThrow().getId();
    }

    public void updateProject(Integer id, String name, String description, Principal principal) {
        Project project = projectRepository.findById(id).orElseThrow();
        if (project != null) {
            project.setName(name);
            project.setDescription(description);
            projectRepository.save(project);
        } else {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Not found project" + name));
        }

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();

        MainHistory history = MainHistory.editProject(user.getFullName(), getLastId());
        mainHistoryRepository.save(history);
    }

    public void closeProject(Integer id, Principal principal) {
        Project project = projectRepository.findById(id).orElseThrow();

        Status status = statusRepository.getById(3);
        project.setClosedDate(LocalDate.now());
        project.setStatus(status);
        projectRepository.save(project);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.closedProject(user.getFullName(), id);
        mainHistoryRepository.save(history);
    }

    public boolean checkStatus(Integer id) {
        List<Application> listByProject = applicationRepository.findAllByProjectId(id);
        for (Application application : listByProject) {
            if (application.getStatus() != statusRepository.findByName("Выполнена"))
                return false;
        }
        return true;
    }


//    public List<Document> getListDocument(int id) {
//        Project project = projectRepository.findById(id).orElseThrow();
//        List<Document> documents;
//        documents =project.getDocuments();
//        return documents;
//    }


//    public List<ProjectDTO> getProjectByLocation(int locationId) {
//        var listProject = projectRepository.getProjectByLocationId(locationId).orElseThrow();
//        return remakeDto(listProject);
//    }

    public List<ProjectDTO> getProjectByStatus(int statusId) {
        var listProject = projectRepository.getProjectByStatusId(statusId).orElseThrow();
        return remakeDto(listProject);
    }

    public ProjectDTO getProjectByName(String name) {
        return ProjectDTO.from(projectRepository.getProjectByName(name).orElseThrow());
    }

    public void delete(Integer id, Principal principal) {
        projectRepository.deleteById(id);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deleteFromProject(user.getFullName(), id);
        mainHistoryRepository.save(history);
    }

    private List<ProjectDTO> remakeDto(List<Project> listProject) {
        List<ProjectDTO> dtoList = new ArrayList<>();
        for (Project project : listProject) {
            ProjectDTO projectDTO = ProjectDTO.from(project);
            dtoList.add(projectDTO);
        }
        return dtoList;
    }

    public int generateUniqueId() {
        UUID idOne = UUID.randomUUID();
        String str = "" + idOne;
        int uid = str.hashCode();
        String filterStr = "" + uid;
        str = filterStr.replaceAll("-", "");
        return Integer.parseInt(str);
    }
//
//    public void create(String projectName, LocalDate dueDate, ObjectLocation location) {
////        var user = UserDTO.from(userRepository.findByEmail(userEmail).orElseThrow());
//        var locationIn = ObjectLocationDTO.from(locationRepository.findById(1).orElseThrow());
//        var status = StatusDTO.from(statusRepository.findById(1).orElseThrow());
//
//        var projectDto = ProjectDTO.builder()
//                .id(generateUniqueId())
//                .name(projectName)
//                .createdDate(LocalDate.now().atStartOfDay())
//                .closedDate(null)
//                .status(status)
//                .build();
//
//        var project = Project.from(projectDto);
//        projectRepository.save(project);
//    }
//
//    public void createProjecty(Project project) {
////        var user = UserDTO.from(userRepository.findByEmail(userEmail).orElseThrow());
//        var locationIn = ObjectLocationDTO.from(locationRepository.findById(1).orElseThrow());
//        var status = StatusDTO.from(statusRepository.findById(1).orElseThrow());
//
//        var projectDto = ProjectDTO.builder()
//                .id(generateUniqueId())
//                .name(project.getName())
//                .createdDate(LocalDate.now().atStartOfDay())
//                .closedDate(null)
//                .status(status)
//                .build();
//
//        var projectNew = Project.from(projectDto);
//        projectRepository.save(projectNew);
//    }

//    public Project createIndia(Project project) {
////        var user = UserDTO.from(userRepository.findByEmail(userEmail).orElseThrow());
//        var locationIn = ObjectLocationDTO.from(locationRepository.findById(1).orElseThrow());
//        var status = StatusDTO.from(statusRepository.findById(1).orElseThrow());
//        project.setId(generateUniqueId());
//        project.setCreatedDate(LocalDate.now().atStartOfDay());
//        project.setStatus(statusRepository.findById(1).orElseThrow());
//        return projectRepository.save(project);
//    }
}