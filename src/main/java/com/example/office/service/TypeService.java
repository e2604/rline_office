package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.application.Type;
import com.example.office.model.application.repository.TypeRepository;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
@AllArgsConstructor
public class TypeService {
    private final TypeRepository typeRepository;

    public List<Type> getAll() {
        return typeRepository.findAll();
    }

    public TypeDTO getById(int id) {
        return TypeDTO.from(typeRepository.findById(id).orElseThrow());
    }

    public TypeDTO getByName(String name) {
        return TypeDTO.from(typeRepository.findByName(name));
    }

    public void create(TypeDTO typeDTO) {
        Type type = Type
                .builder()
                .name(typeDTO.getName())
                .build();
        typeRepository.save(type);
    }

    public void deleteById(int id) {
        typeRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        typeRepository.deleteByName(name);
    }
}