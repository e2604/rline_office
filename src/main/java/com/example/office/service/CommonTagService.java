package com.example.office.service;

import com.example.office.model.application.CommonTag;
import com.example.office.model.application.repository.*;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.List;

@Service
@AllArgsConstructor
public class CommonTagService {

    private final CommonTagRepository repository;

    public List<CommonTag> getAllByUser(Integer id) {
        return repository.findAllByUserId(id);
    }
}