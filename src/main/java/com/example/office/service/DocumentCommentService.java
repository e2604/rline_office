package com.example.office.service;

import com.example.office.DTO.ApplicationCommentDTO;
import com.example.office.DTO.DocumentCommentDTO;
import com.example.office.model.application.ApplicationComment;
import com.example.office.model.document.DocumentComment;
import com.example.office.model.document.repository.*;
import com.example.office.model.history.mainHistory.MainHistory;
import com.example.office.model.history.mainHistory.MainHistoryRepository;
import com.example.office.model.user.User;
import com.example.office.model.user.repository.UserRepository;
import lombok.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.*;

import java.security.Principal;
import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class DocumentCommentService {

    private final DocumentCommentRepository repository;
    private final DocumentService documentService;
    private final UserRepository userRepository;
    private final MainHistoryRepository mainHistoryRepository;
    public Page<DocumentComment> getAllByDocumentId(Integer id, @PageableDefault(value = 3) Pageable pageable) {
        return repository.findAllByDocumentId(id,pageable);
    }
    public DocumentCommentDTO add(DocumentCommentDTO commentDTO, Integer id, Principal principal) {
//        User author = User.from(commentDTO.getAuthor());
        var document = documentService.getById(id);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();

        DocumentComment comment = DocumentComment
                .builder()
                .author(user)
                .commentText(commentDTO.getCommentText())
                .createdDate(LocalDateTime.now())
                .document(document)
                .build();
        repository.save(comment);
        var history = MainHistory.commentFromDocument(user.getEmail(), document.getId());
        mainHistoryRepository.save(history);
        return DocumentCommentDTO.from(comment);
    }
}