package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.object.*;
import com.example.office.model.object.repository.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.stereotype.*;

import java.security.*;
import java.util.*;
import java.util.stream.*;

@Service
@AllArgsConstructor
public class ObjectCompanyService {

    private final ObjectCompanyRepository repository;
    private final ObjectLocationRepository objectLocationRepository;
    private final MainHistoryRepository mainHistoryRepository;
    private final UserRepository userRepository;

    public int getLastId() {
        return repository.findTopByOrderByIdDesc().getId();
    }

    public void add(ObjectCompanyDTO object, Principal principal) {
        repository.save(ObjectCompany.from(object));

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.createFromObject(user.getFullName(), getLastId());
        mainHistoryRepository.save(history);
    }

    public List<ObjectCompany> listAll() {
        return repository.findAll(Sort.by("id").ascending());
    }

    public List<ObjectCompany> getListObject() {
        return repository.findAll();
    }

    public ObjectCompanyDTO getById(Integer id) {
        return ObjectCompanyDTO.from(repository.findById(id).orElseThrow());
    }

    public ObjectCompany getOneObject(int id) {
        return repository.findById(id).orElseThrow();
    }

    public void edit(Integer id, ObjectCompany edited, Principal principal) {
        ObjectCompany object = ObjectCompany.from(getById(id));
        object.setName(edited.getName());
        object.setAddress(edited.getAddress());
        repository.save(object);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editObject(user.getFullName(), object.getId());
        mainHistoryRepository.save(history);
    }

    public List<ObjectCompanyDTO> getAll() {
        return repository.findAll(Sort.by("id").descending())
                .stream().map(ObjectCompanyDTO::from).collect(Collectors.toList());
    }

    public List<ObjectCompanyDTO> getAllForExcel() {
        return repository.findAll(Sort.by("id").ascending())
                .stream().map(ObjectCompanyDTO::from).collect(Collectors.toList());
    }

    public List<ObjectCompanyDTO> getSearchObjects(String search) {
        return repository.findObjectsByNameIgnoreCaseContains(search)
                .stream().map(ObjectCompanyDTO::from).collect(Collectors.toList());
    }

    public void delete(Integer id, Principal principal) {
        repository.deleteById(id);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deleteFromObject(user.getFullName(), id);
        mainHistoryRepository.save(history);
    }

    public void deleteLocationFromList(Integer objectId, ObjectLocation deleted, Principal principal) {
        List<ObjectLocation> locations = getOneObject(objectId).getLocations();
        ObjectLocation locationToDelete = objectLocationRepository.findById(deleted.getId()).orElseThrow();

        locations.remove(locationToDelete);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deleteLocationFromObject(user.getFullName(), deleted.getId());
        mainHistoryRepository.save(history);
    }
}