package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.configuration.*;
import com.example.office.model.application.*;
import com.example.office.model.application.repository.*;
import com.example.office.model.device.repository.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.user.User;
import com.example.office.model.user.repository.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.springframework.data.domain.*;
import org.springframework.data.web.*;
import org.springframework.stereotype.*;

import java.security.*;
import java.time.*;
import java.util.*;
import java.util.stream.*;

@Log4j2
@Service
@AllArgsConstructor
public class ApplicationService {
    private final ApplicationRepository applicationRepository;
    private final StatusService statusService;
    private final ApplicationDevicesService applicationDevicesService;
    private final UserService userService;
    private final ApplicationParticipantsService applicationParticipantsService;
    private final MainHistoryRepository mainHistoryRepository;
    private final UserRepository userRepository;
    private final DeviceRepository deviceRepository;

    public ApplicationDTO findOne(Integer id) {
        return ApplicationDTO.from(applicationRepository.getById(id));
    }

    public List<ApplicationDTO> findAll() {
        return applicationRepository.findAll(Sort.by("id").descending())
                .stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public List<ApplicationDTO> getAllForExcel() {
        return applicationRepository.findAll(Sort.by("id").ascending())
                .stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public List<ApplicationDTO> getAppSearchList(String search) {
        return applicationRepository.findApplicationByNameIgnoreCaseContains(search).stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public List<UserDTO> getUserSearchList(String search) {
        return userRepository.findUserByNameIgnoreCaseContains(search).stream().map(UserDTO::from).collect(Collectors.toList());
    }

    public Application add(Application application, Principal principal) {

        Translit translit = new Translit();
        var trimName = application.getName().trim();
        var nameExchange = trimName.replaceAll(" ", "-");

        var translitName = translit.cyr2lat(nameExchange.toUpperCase());
        var lowerName = translitName.toLowerCase();

        User principalUser = userRepository.findByEmail(principal.getName()).orElseThrow();

        application.setMinioName(lowerName);
        application.setFolder(application.getCounterparty().getFolder());
        application.setAuthor(principalUser);
        application.setStatus(statusService.getById(1));
        if (application.getDevices() != null)
            applicationDevicesService.createDevicesList(application);
        if (application.getParticipants() != null)
            applicationParticipantsService.createParticipantsList(application);
        if (application.getExecutor().getChatId() == null) {
            log.info("Пользователя " + application.getExecutor().getSurname() + " " + application.getExecutor().getName() + "нет в телеграм");
        } else {
            userService.sendMessage(application.getExecutor().getChatId().toString(), "Вам назначена новая Заявка: " + application.getName() + "\n\nПерейдите пожалуйста в онлайн-офис, для просмотра!");
        }
        return applicationRepository.save(application);
    }

    public void addParticipantToList(Integer id, Integer userId) {
        var application = applicationRepository.getById(id);
        var participant = userRepository.getById(userId);
        applicationParticipantsService.createParticipant(application, participant);
    }

    public void addDeviceToList(Integer applicationId, Integer deviceId) {
        var application = applicationRepository.getById(applicationId);
        var device = deviceRepository.getById(deviceId);
        applicationDevicesService.createDevice(application, device);
    }

    public void startApplication(Integer applicationId, Principal principal) {
        var application = applicationRepository.getById(applicationId);
        application.setActualStartDate(LocalDateTime.now());
        application.setStatus(Status.from(statusService.getByName("В работе")));

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var history = MainHistory.startFromApplication(user.getFullName(), application.getId());
        mainHistoryRepository.save(history);
        applicationRepository.save(application);
        sendApplicationStartMessageToParticipants(application);
    }

//    public void startApplicationAfterOverdue(Integer applicationId, Principal principal) {
//        var application = applicationRepository.getById(applicationId);
//        application.setActualStartDate(LocalDateTime.now());
//        application.setStatus(Status.from(statusService.getByName("Просрочена")));
//
//        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
//        var history = MainHistory.startFromApplication(user.getFullName(), application.getId());
//        mainHistoryRepository.save(history);
//        applicationRepository.save(application);
//        sendApplicationStartMessageToParticipants(application);
//    }

    public int getLastId() {
        return applicationRepository.findTopByOrderByIdDesc().getId();
    }

    public void cancelApplication(Integer applicationId, Principal principal) {
        Application application = applicationRepository.getById(applicationId);
        application.setStatus(Status.from(statusService.getByName("Отменена")));
        application.setClosedDate(LocalDateTime.now());

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.cancelFromApplication(user.getFullName(), applicationId);
        mainHistoryRepository.save(history);
        applicationRepository.save(application);
    }

//    public void cancelApplicationAfterOverdue(Integer applicationId, Principal principal) {
//        Application application = applicationRepository.getById(applicationId);
//        application.setStatus(Status.from(statusService.getByName("Отменена")));
//        application.setClosedDate(LocalDateTime.now());
//
//        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
//        MainHistory history = MainHistory.cancelFromApplication(user.getFullName(), applicationId);
//        mainHistoryRepository.save(history);
//        applicationRepository.save(application);
//    }

    public Application getOne(Integer applicationId) {
        return applicationRepository.getById(applicationId);
    }

    public int getAttachmentsCount(Integer applicationId) {
        return getOne(applicationId).getDocuments().size();
    }

    public void doneApplication(Integer applicationId, Principal principal) {
        var application = applicationRepository.getById(applicationId);
        application.setStatus(Status.from(statusService.getByName("Выполнена")));
        application.setClosedDate(LocalDateTime.now());
        application.setStatus(statusService.getById(3));

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var history = MainHistory.completeFromApplication(user.getFullName(), application.getId());
        mainHistoryRepository.save(history);
        applicationRepository.save(application);
    }

    public void putHistoryCreate(Principal principal, Integer id) {
        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var history = MainHistory.createFromApplication(user.getFullName(), id);
        mainHistoryRepository.save(history);
    }

    public void editApplication(Integer id, Application application, Principal principal) {
        var app = applicationRepository.getById(id);
        app.setName(application.getName());
        app.setProject(application.getProject());
        app.setExecutor(application.getExecutor());
        app.setPriority(application.getPriority());
        app.setDescription(application.getDescription());
        app.setPlanedStartDate(application.getPlanedStartDate());
        app.setPlanedDueDate(application.getPlanedDueDate());
        applicationRepository.save(app);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editApplication(user.getFullName(), getLastId());
        mainHistoryRepository.save(history);
    }

    public void sendReminder() {
        var applications = applicationRepository.findAll();
        for (var application : applications) {
            if (application.getStatus().getId() == 2 || application.getStatus().getId() == 4) {
                if (LocalDate.now().compareTo(application.getPlanedDueDate()) == 0) {
                    if (application.getExecutor().getChatId() == null)
                        log.info("Пользователя " + application.getExecutor().getSurname() + " " + application.getExecutor().getName() + "нет в телеграм");
                    else
                        userService.sendMessage(application.getExecutor().getChatId().toString(), "Срок выполнения заявки " + "\"" + application.getName() + "\"" + " приближается к концу! \n\nПерейдите пожалуйста в онлайн-офис, для просмотра!");
                    for (var participant : applicationParticipantsService.getParticipants(application.getId())) {
                        var partChatId = userService.getById(participant.getParticipant().getId());
                        if (partChatId.getChatId() == null)
                            log.info("Пользователя " + partChatId.getSurname() + " " + partChatId.getName() + " нет в телеграм");
                        else
                            userService.sendMessage(partChatId.getChatId().toString(), "Срок выполнения заявки " + "\"" + application.getName() + "\"" + " приближается к концу! \n\nПерейдите пожалуйста в онлайн-офис, для просмотра!");
                    }
                } else if (LocalDate.now().compareTo(application.getPlanedDueDate()) > 0) {
                    application.setStatus(Status.from(statusService.getByName("Просрочена")));
                    applicationRepository.save(application);
                    if (application.getExecutor().getChatId() == null)
                        log.info("Пользователя " + application.getExecutor().getSurname() + " " + application.getExecutor().getName() + " нет в телеграм");
                    else
                        userService.sendMessage(application.getExecutor().getChatId().toString(), "Заявка " + "\"" + application.getName() + "\"" + " просрочена! \n\nПерейдите пожалуйста в онлайн-офис, для просмотра!");
                    for (var participant : applicationParticipantsService.getParticipants(application.getId())) {
                        var partChatId = userService.getById(participant.getParticipant().getId());
                        if (partChatId.getChatId() == null)
                            log.info("Пользователя " + partChatId.getSurname() + " " + partChatId.getName() + " нет в телеграм");
                        else
                            userService.sendMessage(partChatId.getChatId().toString(), "Заявка " + "\"" + application.getName() + "\"" + " просрочена! \n\nПерейдите пожалуйста в онлайн-офис, для просмотра!");
                    }
                }
            }
        }
    }

    private void sendApplicationStartMessageToParticipants(Application application) {
        for (var participant : applicationParticipantsService.getParticipants(application.getId()))
            if (participant.getParticipant().getChatId() == null) {
                log.info("Пользователя " + participant.getParticipant().getSurname() + " " + participant.getParticipant().getName() + " нет в телеграм");
            } else {
                userService.sendMessage(participant.getParticipant().getChatId().toString(), "Вам назначена новая Заявка: " + application.getName() + "\n\nПерейдите пожалуйста в онлайн-офис, для просмотра!");
            }
    }

    public Page<Application> getAllApplicationsPageList(@PageableDefault(value = 3) Pageable pageable) {
        return applicationRepository.findAll(pageable);
    }

    public List<ApplicationDTO> getSearchList(String search) {
        return applicationRepository.findApplicationsByNameIgnoreCaseContains(search).stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public List<ApplicationDTO> findAllByStatusNew() {
        List<Application> newApplications = applicationRepository.findAllByStatusId(1);
        newApplications.sort(Comparator.comparing(Application::getPlanedDueDate));
        return newApplications.stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public List<ApplicationDTO> findAllByStatusInProgress() {
        List<Application> inProgressApplications = applicationRepository.findAllByStatusId(2);
        inProgressApplications.sort(Comparator.comparing(Application::getPlanedDueDate));
        return inProgressApplications.stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public List<ApplicationDTO> findAllByStatusOverdue() {
        List<Application> overdueApplications = applicationRepository.findAllByStatusId(4);
        overdueApplications.sort(Comparator.comparing(Application::getPlanedDueDate));
        return overdueApplications.stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public List<ApplicationDTO> findAllByStatusCompleted() {
        return applicationRepository.findAllByStatusId(3).stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public List<ApplicationDTO> getTop10ByStatusId() {
        return applicationRepository.findTop10ByStatusId(3).stream().map(ApplicationDTO::from).collect(Collectors.toList());
    }

    public List<Application> listAll() {
        return applicationRepository.findAll(Sort.by("id").ascending());
    }

    public List<ExecutorTotalSum> getTotalSum(Integer executorId) {
        return applicationRepository.getTotalSum(executorId);
    }

    public ExecutorTotalSum getTotalMonthSum(Integer executorId) {
        return applicationRepository.getTotalMonthSum(executorId);
    }

    public void changeStatusToOverdue() {
        List<Application> applications = applicationRepository.findAll();

        for (Application application : applications) {
            if (LocalDate.now().compareTo(application.getPlanedDueDate()) > 0 && application.getClosedDate() == null) {
                application.setStatus(statusService.getById(4));
                applicationRepository.save(application);
            }
        }
    }

//    public void overwriteStatus() {
//        List<Application> applications = applicationRepository.findAll();
//
//        for (Application application : applications) {
//            if (application.getClosedDate() != null) {
//                application.setStatus(statusService.getById(3));
//                applicationRepository.save(application);
//            }
//        }
//    }

    public void delete(Integer applicationId, Principal principal) {
        applicationRepository.deleteById(applicationId);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deleteFromApplication(user.getFullName(), applicationId);
        mainHistoryRepository.save(history);
    }
}