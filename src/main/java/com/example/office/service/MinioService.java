package com.example.office.service;

import java.io.*;
import java.util.*;

import io.minio.Result;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import org.springframework.web.multipart.MultipartFile;

public interface MinioService {
    String putObject(MultipartFile multipartFile, String bucketName,String fileType); //загрузка файлов

    String copyObject(String bucketName,String objectName,String newBucketName);//копирование объекта

    String getObjectUrl(String bucketName, String objectName); //путь к объекту

    void downloadNew(String bucketName, String objectName,String fileName);

    InputStream downloadObject(String bucketName, String objectName); //скачивание объекта

    List<String> listBucketNames(); //лист наименований всех бакетов

    List<Bucket> getBuckets();

    Iterable<Result<Item>> getListObjects(String bucketName);

    List<String> listObjectNames(String bucketName); //лист наименований всех объектов в бакете

    void makeBucket(String name); //создание бакета

    boolean isBucket(String name);

    boolean bucketExists(String bucketName); //проверка бакетов

    boolean removeBucket(String bucketName); //удаление бакета

    boolean removeObject(String bucketName, String objectName); //удаление объекта

    boolean removeListObject(String bucketName, List<String> objectNameList); //удаление вложений/объектов

    void putSubFolder(MultipartFile multipartFile, String bucketName, String fileType, String locationName);

    void putSubFolderFromDevice(MultipartFile multipartFile, String bucketName, String locationName, String deviceName);
}