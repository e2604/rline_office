package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.application.*;
import com.example.office.model.application.repository.*;
import com.example.office.model.user.*;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
@AllArgsConstructor
public class ApplicationParticipantsService {
    private final ApplicationParticipantsRepository repository;
    private final ApplicationRepository applicationRepository;
    private final UserService userService;

    public void createParticipantsList(Application application) {
        for (var participant : application.getParticipants()) {
            ApplicationParticipants.builder()
                    .application(application)
                    .participant(participant)
                    .build();
        }
    }

    public void createParticipant(Application application, User participant) {
        var part = ApplicationParticipants
                .builder()
                .participant(participant)
                .application(application)
                .build();
        repository.save(part);
    }

    public void deleteParticipantFromApplication(Integer applicationId, Integer id) {
        var parts = repository.getApplicationParticipantsByApplicationId(applicationId);
        for (var p : parts) {
            if (p.getParticipant().getId() == id) {
                repository.deleteById(p.getId());
                break;
            }
        }
    }

    public List<ApplicationParticipants> getParticipants(Integer applicationId) {
        return repository.getApplicationParticipantsByApplicationId(applicationId);
    }

    public List<UserDTO> getRewrittenParticipants(Integer applicationId) {
        List<ApplicationParticipants> applicationParticipants = getParticipants(applicationId);
        List<UserDTO> users = userService.getAll();
        Application application = applicationRepository.findById(applicationId).orElseThrow();

        users.removeIf(user -> user.getRole().getId() == 3);

        for (ApplicationParticipants participant : applicationParticipants) {
            users.removeIf(userDTO -> Objects.equals(participant.getParticipant().getId(), userDTO.getId())
                    || Objects.equals(application.getExecutor().getId(), userDTO.getId()));
        }

        return users;
    }

    public int getRewrittenParticipantsCount(Integer applicationId) {
        return getRewrittenParticipants(applicationId).size();
    }

    public int getParticipantsCount(Integer applicationId) {
        return getParticipants(applicationId).size();
    }

    public List<ApplicationParticipants> getApplications(Integer participantId) {
        return repository.getApplicationParticipantsByParticipantId(participantId);
    }

    public List<UserDTO> getUsersFromList(Integer id) {

        var application = applicationRepository.getById(id);
        var usersList = userService.getAll();
        var participants = repository.getApplicationParticipantsByApplicationId(id);
        List<UserDTO> users = new ArrayList<>();

        for (var user : usersList) {
            if (application.getExecutor().getId() == user.getId())
                users.add(user);
            else if (application.getAuthor().getId() == user.getId())
                users.add(user);
        }

        usersList.removeAll(users);

        if (participants.size() == 0)
            return usersList;
        for (var u : usersList) {
            for (var p : participants) {
                if (u.getId() == p.getParticipant().getId())
                    users.add(u);
            }
        }

        usersList.removeAll(users);

        return usersList;
    }
}
