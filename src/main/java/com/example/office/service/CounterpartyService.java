package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.configuration.*;
import com.example.office.model.document.*;
import com.example.office.model.folder.*;
import com.example.office.model.folder.repository.*;
import com.example.office.model.history.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.object.*;
import com.example.office.model.object.repository.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import com.example.office.model.сounterparty.*;
import com.example.office.model.сounterparty.repository.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.stereotype.*;

import java.security.*;
import java.util.*;
import java.util.stream.*;

@Service
@AllArgsConstructor
public class CounterpartyService {
    private final CounterpartyRepository counterpartyRepository;
    private final CounterpartyTypeRepository counterpartyTypeRepository;
    private final ObjectLocationRepository objectLocationRepository;
    private final UserRepository userRepository;
    private final MainHistoryRepository mainHistoryRepository;
    private final MinioService minioService;
    private final FolderRepository folderRepository;

    public List<CounterpartyDTO> getAll() {
        return remakeCounterpartyDto(counterpartyRepository.findAll());
    }

    public List<CounterpartyDTO> getAllDTO() {
        var listCounterparties = counterpartyRepository.findAll(Sort.by("id").descending());
        return listCounterparties.stream().map(CounterpartyDTO::from).collect(Collectors.toList());
    }

    public CounterpartyDTO getById(Integer id) {
        return CounterpartyDTO.from(counterpartyRepository.findById(id).orElseThrow());
    }

    public List<CounterpartyDTO> getCounterpartiesSearchList(String search) {
        return counterpartyRepository.findCounterpartiesByNameIgnoreCaseContains(search).stream().map(CounterpartyDTO::from).collect(Collectors.toList());
    }

    public String add(CounterpartyDTO counterpartyDTO, Principal principal) {
        var minioName = validate(counterpartyDTO.getName());
        var folderTest = folderRepository.existsByName(minioName);
        if (!folderTest) {
            Folder folder = Folder.builder() //сделать проверку на такое же имя folder
                    .name(minioName)
                    .size(0)
                    .auto(true)
                    .documentCount(0)
                    .pageName(counterpartyDTO.getName())
                    .build();

            folderRepository.save(folder);

            counterpartyDTO.setMinioName(minioName);
            counterpartyDTO.setFolder(folder.getName());
//            counterpartyDTO.setType(CounterpartyTypeDTO.from(counterpartyTypeRepository.findById(id).orElseThrow()));
            counterpartyRepository.save(Counterparty.from(counterpartyDTO));
            minioService.makeBucket(minioName);

            User user = userRepository.findByEmail(principal.getName()).orElseThrow();
            var history = MainHistory.createFromCounter(user.getFullName(), getLastId());
            mainHistoryRepository.save(history);

            return "redirect:/counterparty/list";
        } else {
            return "redirect:/pages-400-files";
        }
    }

    public int getLastId() {
        return counterpartyRepository.findTopByOrderByIdDesc().getId();
    }

    public int getLastIdLocat() {
        return objectLocationRepository.findTopByOrderByIdDesc().getId();
    }

    public void deleteById(Integer id, Principal principal) {
        var mainHistory = mainHistoryRepository.findAll();
        for (MainHistory history : mainHistory) {
            if (history.getEssence().equals(Essence.COUNTERPARTY.getName()) && history.getIdEntity() == id) {
                mainHistoryRepository.delete(history);
            }
        }
        counterpartyRepository.deleteById(id);
    }

    public List<CounterpartyTypeDTO> listType() {
        return remakeDto(counterpartyTypeRepository.findAll());
    }

    private List<CounterpartyTypeDTO> remakeDto(List<CounterpartyType> listTasks) {
        List<CounterpartyTypeDTO> dtoList = new ArrayList<>();
        for (CounterpartyType task : listTasks) {
            CounterpartyTypeDTO counterpartyTypeDTO = CounterpartyTypeDTO.from(task);
            dtoList.add(counterpartyTypeDTO);
        }
        return dtoList;
    }

    private List<CounterpartyDTO> remakeCounterpartyDto(List<Counterparty> list) {
        List<CounterpartyDTO> dtoList = new ArrayList<>();
        for (Counterparty unit : list) {
            CounterpartyDTO counterpartyDTO = CounterpartyDTO.from(unit);
            dtoList.add(counterpartyDTO);
        }
        return dtoList;
    }

    public CounterpartyDTO getOne(Integer id) {
        return CounterpartyDTO.from(counterpartyRepository.findById(id).orElseThrow());
    }

    public List<CounterpartyDTO> listCounters() {
        return remakeCounterpartyDto(counterpartyRepository.findAll());
    }

    public void addLocation(String address, String contactName,
                              String phoneNumber, Integer counterpartyId,
                              Principal principal) {
        var counterparty = counterpartyRepository.findById(counterpartyId).orElseThrow();

        var objectLocation = ObjectLocation.builder()
                .address(address)
                .contactName(contactName)
                .phoneNumber(phoneNumber)
                .counterparty(counterparty)
                .build();
        objectLocationRepository.save(objectLocation);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        var history = MainHistory.createFromLocation(user.getFullName(), getLastIdLocat());
        mainHistoryRepository.save(history);
    }

    public List<Document> getListDocument(Integer id) {
        var counterparty = counterpartyRepository.findById(id).orElseThrow();
        List<Document> documents;
        documents = counterparty.getDocuments();
        if (documents != null) {
            List<Document> filtered = new ArrayList<>();
            for (Document document : documents) {
                if (document.getLocation() == null && document.getApplication() == null && document.getDevice() == null) {
                    filtered.add(document);
                }
            }
            filtered.sort(Comparator.comparing(Document::getId).reversed());
            return filtered;
        }
        return null;
    }

    public int getAttachmentsCount(Integer counterpartyId) {
        return getListDocument(counterpartyId).size();
    }

    public List<ObjectLocationDTO> getCounterpartyLocation(Integer id) {
        var list = objectLocationRepository.findByCounterpartyId(id);
        List<ObjectLocationDTO> objectLocationDTOS = new ArrayList<>();

        for (ObjectLocation objectLocation : list) {
            ObjectLocationDTO locationDTO = ObjectLocationDTO.from(objectLocation);
            objectLocationDTOS.add(locationDTO);
        }

        return objectLocationDTOS;
    }

    public List<Counterparty> getAllBD() {
        return counterpartyRepository.findAll();
    }

    public List<Counterparty> getFilteredList(String search) {
        return counterpartyRepository.findCounterpartyByNameContains(search);
    }

    public void edit(Integer id, Counterparty edited, Principal principal) {
        Counterparty counterparty = Counterparty.from(getById(id));
        counterparty.setName(edited.getName());
        counterparty.setType(edited.getType());
        counterparty.setAddress(edited.getAddress());
        counterparty.setContactName(edited.getContactName());
        counterparty.setPhoneNumber(edited.getPhoneNumber());
        counterparty.setBin(edited.getBin());
        counterparty.setDescription(edited.getDescription());
        counterpartyRepository.save(counterparty);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editCounterparty(user.getFullName(), counterparty.getId());
        mainHistoryRepository.save(history);
    }

    public String createApi(CounterpartyDTOJS counterparty, Principal principal) {
        try {
            var minioName = validate(counterparty.getName());
            var counterpartyType = counterpartyTypeRepository.findById(counterparty.getType()).orElseThrow();

            var folderExist = folderRepository.existsByName(minioName);
            var counterpartyExist = counterpartyRepository.existsByMinioName(minioName);
            if (!folderExist && !counterpartyExist) {
                Folder folder = Folder.builder() //сделать проверку на такое же имя folder
                        .name(minioName)
                        .size(0)
                        .auto(true)
                        .documentCount(0)
                        .pageName(counterparty.getName())
                        .build();
                folderRepository.save(folder);

                counterparty.setMinioName(minioName);
                counterparty.setFolder(folder.getName());
                Counterparty counterpartyOriginal = Counterparty.fromJS(counterparty);
                counterpartyOriginal.setType(counterpartyType);
                counterpartyRepository.save(counterpartyOriginal);
                minioService.makeBucket(minioName);

                User user = userRepository.findByEmail(principal.getName()).orElseThrow();
                var history = MainHistory.createFromCounter(user.getFullName(), getLastId());
                mainHistoryRepository.save(history);

                return "Контрагент создан";
            } else {
                return "Имя контрагента должно быть уникальным";
            }
        } catch (Exception e) {
            System.out.println("Ошибка в создании Контрагента" + e.getMessage());
            return "Error: Перепроверьте данные";
        }
    }

    public String validate(String name) {
        Translit translit = new Translit();//
        var trimName = name.trim();
        String regex = trimName.replaceAll("[-+.^:~_@#$&*()`!,]", "");
        var trimTwo = regex.trim();
        var nameExchange = trimTwo.replaceAll("\\s+", "-");
        var transliteName = translit.cyr2lat(nameExchange.toUpperCase());
        return transliteName.toLowerCase();
    }

    public List<ObjectLocationDTO> getAllLocationsByCounterparty(Integer id) {
        var locations = objectLocationRepository.findByCounterpartyId(id);
        if (locations == null)
            return new ArrayList<>();
        locations.sort(Comparator.comparing(ObjectLocation::getId).reversed());
        return locations.stream().map(ObjectLocationDTO::from).collect(Collectors.toList());
    }

    public List<CounterpartyDTO> getAllForExcel() {
        return counterpartyRepository.findAll(Sort.by("id").ascending())
                .stream().map(CounterpartyDTO::from).collect(Collectors.toList());
    }
}