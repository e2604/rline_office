package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.application.*;
import com.example.office.model.application.repository.*;
import com.example.office.model.device.*;
import com.example.office.service.device.*;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
@AllArgsConstructor
public class ApplicationDevicesService {
    private final ApplicationDevicesRepository repository;
    private final DeviceService deviceService;

    public void createDevicesList(Application application) {
        for (var device : application.getDevices()) {
            ApplicationDevices.builder()
                    .application(application)
                    .device(device)
                    .build();
        }
    }

    public void createDevice(Application application, Device device) {
        var dev = ApplicationDevices
                .builder()
                .application(application)
                .device(device)
                .build();
        repository.save(dev);
    }

    public List<ApplicationDevices> getDevices(Integer applicationId) {
        return repository.getApplicationDevicesByApplicationId(applicationId);
    }

    public List<ApplicationDevices> getNewDevices(Integer applicationId) {
        List<ApplicationDevices> applicationDevices = repository.getApplicationDevicesByApplicationId(applicationId);
        applicationDevices.removeIf(device -> device.getDevice().getStatus().getId() != 1);
        return applicationDevices;
    }

    public int getDevicesCount(Integer applicationId) {
        return getDevices(applicationId).size();
    }

    public void deleteDeviceFromApplication(Integer applicationId, Integer id) {
        var devices = repository.getApplicationDevicesByApplicationId(applicationId);
        for (var d : devices) {
            if (Objects.equals(d.getDevice().getId(), id)) {
                repository.deleteById(d.getId());
                break;
            }
        }
    }

    public List<DeviceDTO> getDevicesFromList(Integer id) {
        var devicesList = deviceService.getAll();
        var appDevices = repository.getApplicationDevicesByApplicationId(id);
        List<DeviceDTO> devices = new ArrayList<>();

        if (appDevices.size() == 0)
            return devicesList;

        for (var d : devicesList) {
            for (var aP : appDevices) {
                if (d.getId() == aP.getDevice().getId())
                    devices.add(d);
            }
        }
        devicesList.removeAll(devices);

        return devicesList;
    }
}