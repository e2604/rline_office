package com.example.office.service.user;

import com.example.office.model.user.User;
import com.example.office.model.user.repository.*;
import lombok.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;

@Service
@AllArgsConstructor
public class AuthUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username).orElseThrow();

        if (user == null) {
            throw  new UsernameNotFoundException("User not found");
        }
        return user;
    }
}

//определяем откуда брать пользователей