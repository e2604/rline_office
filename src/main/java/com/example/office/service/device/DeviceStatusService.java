package com.example.office.service.device;

import com.example.office.DTO.*;
import com.example.office.model.device.*;
import com.example.office.model.device.repository.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import lombok.*;
import org.springframework.stereotype.*;

import java.security.*;
import java.util.*;

@Service
@AllArgsConstructor
public class DeviceStatusService {

    private final DeviceStatusRepository deviceStatusRepository;
    private final DeviceRepository deviceRepository;
    private final UserRepository userRepository;
    private final MainHistoryRepository mainHistoryRepository;

    List<DeviceStatus> getAll() {
        return deviceStatusRepository.findAll();
    }

    public List<DeviceStatusDTO> deviceStatusesDTOList() {
        return makeDeviceStatusesDTOList(getAll());
    }

    private List<DeviceStatusDTO> makeDeviceStatusesDTOList(List<DeviceStatus> deviceStatuses) {
        List<DeviceStatusDTO> deviceStatusesDTOList = new ArrayList<>();
        for (DeviceStatus status : deviceStatuses) {
            DeviceStatusDTO deviceStatusDTO = DeviceStatusDTO.from(status);
            deviceStatusesDTOList.add(deviceStatusDTO);
        }
        return deviceStatusesDTOList;
    }
    
    
    public void inProgressDevice(Integer deviceId, Principal principal) {
        Device device = deviceRepository.getById(deviceId);
        device.setStatus(deviceStatusRepository.getById(2));

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deviceInProgress(user.getFullName(), device.getId());
        mainHistoryRepository.save(history);
    }

    public void doneDevice(Integer deviceId, Principal principal) {
        Device device = deviceRepository.getById(deviceId);
        device.setStatus(deviceStatusRepository.getById(3));

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deviceDone(user.getFullName(), device.getId());
        mainHistoryRepository.save(history);
    }

    public void burnedDevice(Integer deviceId, Principal principal) {
        Device device = deviceRepository.getById(deviceId);
        device.setStatus(deviceStatusRepository.getById(4));

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deviceBurned(user.getFullName(), device.getId());
        mainHistoryRepository.save(history);
    }
}