package com.example.office.service.device;

import com.example.office.DTO.*;
import com.example.office.model.device.*;
import com.example.office.model.device.repository.*;
import com.example.office.model.user.User;
import lombok.*;
import org.springframework.stereotype.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.user.repository.*;

import java.security.*;
import java.time.*;
import java.util.*;

@Service
@AllArgsConstructor
public class DeviceCommentService {

    private final DeviceCommentRepository deviceCommentRepository;
    private final DeviceRepository deviceRepository;
    private final UserRepository userRepository;
    private final MainHistoryRepository mainHistoryRepository;

    public DeviceCommentDTO add(DeviceCommentDTO commentDTO, Integer id, Principal principal) {
        var device = deviceRepository.findById(id).orElseThrow();

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();

        DeviceComment comment = DeviceComment
                .builder()
                .author(user)
                .comment(commentDTO.getComment())
                .createdDate(LocalDateTime.now())
                .device(device)
                .build();
        deviceCommentRepository.save(comment);

        var history = MainHistory.commentFromDevice(user.getFullName(), device.getId());
        mainHistoryRepository.save(history);
        return DeviceCommentDTO.from(comment);
    }

    public List<DeviceComment> getAllDeviceComments(Integer deviceId) {
        List<DeviceComment> deviceComments = deviceCommentRepository.findAllByDeviceId(deviceId);
        deviceComments.sort(Comparator.comparing(DeviceComment::getCreatedDate).reversed());
        return deviceComments;
    }

    public int getCommentsCount(Integer deviceId) {
        return getAllDeviceComments(deviceId).size();
    }
}