package com.example.office.service.device;

import com.example.office.model.device.repository.*;
import lombok.*;
import org.springframework.stereotype.*;

@Service
@AllArgsConstructor
public class DeviceHistoryService {

    private final DeviceRepository repository;
}