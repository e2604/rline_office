package com.example.office.service.device;

import com.example.office.DTO.*;
import com.example.office.model.device.*;
import com.example.office.model.device.repository.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.user.User;
import com.example.office.model.user.repository.*;
import lombok.*;
import org.springframework.stereotype.*;

import java.security.Principal;
import java.util.*;

@Service
@AllArgsConstructor
public class DeviceTypeService {

    private final DeviceTypeRepository deviceTypeRepository;
    private final UserRepository userRepository;
    private final MainHistoryRepository historyRepository;

    public List<DeviceType> getAll() {
        return deviceTypeRepository.findAll();
    }

    public DeviceTypeDTO getById(Integer id) {
        return DeviceTypeDTO.from(deviceTypeRepository.findById(id).orElseThrow());
    }

    public List<DeviceTypeDTO> deviceTypesDTOList() {
        return makeDeviceTypesDTOList(getAll());
    }

    private List<DeviceTypeDTO> makeDeviceTypesDTOList(List<DeviceType> deviceTypes) {
        List<DeviceTypeDTO> deviceTypesDTOList = new ArrayList<>();
        for (DeviceType type : deviceTypes) {
            DeviceTypeDTO deviceTypeDTO = DeviceTypeDTO.from(type);
            deviceTypesDTOList.add(deviceTypeDTO);
        }
        return deviceTypesDTOList;
    }

    public void create(DeviceType type, Principal principal) {
        User user = userRepository.findByEmail(principal.getName()).orElseThrow();

        type.setAuthor(user);
        deviceTypeRepository.save(type);

        MainHistory history = MainHistory.createDeviceType(user.getFullName(), type.getId());
        historyRepository.save(history);
    }

    public void delete(Integer id, Principal principal) {
        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deleteDeviceType(user.getFullName(), id);
        historyRepository.save(history);
        deviceTypeRepository.deleteById(id);
    }

    public void edit(Integer id, DeviceType edited, Principal principal) {
        DeviceType type = DeviceType.from(getById(id));
        type.setName(edited.getName());
        type.setDescription(edited.getDescription());
        deviceTypeRepository.save(type);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editDeviceType(user.getFullName(), type.getId());
        historyRepository.save(history);
    }
}