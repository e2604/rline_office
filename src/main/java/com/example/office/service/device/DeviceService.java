package com.example.office.service.device;

import com.example.office.DTO.*;
import com.example.office.configuration.*;
import com.example.office.exception.*;
import com.example.office.model.device.*;
import com.example.office.model.device.repository.*;
import com.example.office.model.document.*;
import com.example.office.model.document.repository.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.object.*;
import com.example.office.model.object.repository.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.data.web.*;
import org.springframework.stereotype.*;

import java.security.*;
import java.util.*;
import java.util.stream.*;

@Repository
@AllArgsConstructor
public class DeviceService {

    private final DeviceRepository deviceRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final ObjectLocationRepository objectLocationRepository;
    private final DeviceStatusRepository deviceStatusRepository;
    private final UserRepository userRepository;
    private final MainHistoryRepository mainHistoryRepository;
    private final DocumentRepository documentRepository;

    public List<DeviceDTO> getAll() {
        return makeDevicesDTOList(deviceRepository.findAll());
    }

    public List<DeviceDTO> getAllByLocationId(Integer id) {
        List<DeviceDTO> locationDevices = makeDevicesDTOList(deviceRepository.findAllByLocationId(id));
        locationDevices.sort(Comparator.comparing(DeviceDTO::getId).reversed());
        return locationDevices;
    }

    public int getDevicesCount(Integer locationId) {
        return getAllByLocationId(locationId).size();
    }

    public List<DeviceDTO> getAllDTO() {
        return deviceRepository.findAll(Sort.by("id").descending()).stream().map(DeviceDTO::from).collect(Collectors.toList());
    }

    public List<DeviceDTO> getSearchDevices(String search) {
        return deviceRepository.findDevicesByNameIgnoreCaseContains(search).stream()
                .map(DeviceDTO::from).collect(Collectors.toList());
    }

    public Page<Device> getAllDevicesPageList(@PageableDefault(value = 3) Pageable pageable) {
        return deviceRepository.findAll(pageable);
    }

    public DeviceDTO getById(Integer id) {
        return DeviceDTO.from(deviceRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Устройство с данным id не найдено.")));
    }

    public void create(Device device, Principal principal) {

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();

        Translit translit = new Translit();
        var trimName = device.getName().trim();
        var nameExchange = trimName.replaceAll(" ", "-");

        var translitName = translit.cyr2lat(nameExchange.toUpperCase());
        var lowerName = translitName.toLowerCase();

        device.setAuthor(user);
        device.setMinioName(lowerName);
        device.setFolder(device.getLocation().getCounterparty().getFolder());
        device.setStatus(deviceStatusRepository.getById(1));
        deviceRepository.save(device);

        MainHistory history = MainHistory.createFromDevice(user.getFullName(), device.getId());
        mainHistoryRepository.save(history);
    }

    public List<DeviceTypeDTO> typesDTOList() {
        return makeTypesDTOList(deviceTypeRepository.findAll());
    }

    public Integer getLastId() {
        return deviceRepository.findTopByOrderByIdDesc().getId();
    }

    private List<DeviceTypeDTO> makeTypesDTOList(List<DeviceType> types) {
        List<DeviceTypeDTO> typesDTOList = new ArrayList<>();
        for (DeviceType type : types) {
            DeviceTypeDTO typesDTO = DeviceTypeDTO.from(type);
            typesDTOList.add(typesDTO);
        }
        return typesDTOList;
    }

    public List<ObjectLocationDTO> locationsDTOList() {
        return makeLocationsDTOList(objectLocationRepository.findAll());
    }

    private List<ObjectLocationDTO> makeLocationsDTOList(List<ObjectLocation> locations) {
        List<ObjectLocationDTO> locationsDTOList = new ArrayList<>();
        for (ObjectLocation location : locations) {
            ObjectLocationDTO locationsDTO = ObjectLocationDTO.from(location);
            locationsDTOList.add(locationsDTO);
        }
        return locationsDTOList;
    }

    public List<DeviceStatusDTO> statusesDTOList() {
        return makeStatusesDTOList(deviceStatusRepository.findAll());
    }

    private List<DeviceStatusDTO> makeStatusesDTOList(List<DeviceStatus> statuses) {
        List<DeviceStatusDTO> statusesDTOList = new ArrayList<>();
        for (DeviceStatus status : statuses) {
            DeviceStatusDTO statusesDTO = DeviceStatusDTO.from(status);
            statusesDTOList.add(statusesDTO);
        }
        return statusesDTOList;
    }

    public List<DeviceDTO> devicesDTOList() {
        return makeDevicesDTOList(deviceRepository.findAll());
    }

    private List<DeviceDTO> makeDevicesDTOList(List<Device> devices) {
        List<DeviceDTO> devicesDTOList = new ArrayList<>();
        for (Device device : devices) {
            DeviceDTO devicesDTO = DeviceDTO.from(device);
            devicesDTOList.add(devicesDTO);
        }
        return devicesDTOList;
    }

    public List<UserDTO> usersDTOList() {
        return makeUsersDTOList(userRepository.findAll());
    }

    private List<UserDTO> makeUsersDTOList(List<User> users) {
        List<UserDTO> usersDTOList = new ArrayList<>();
        for (User user : users) {
            UserDTO usersDTO = UserDTO.from(user);
            usersDTOList.add(usersDTO);
        }
        return usersDTOList;
    }

    public List<DocumentDTO> getListDocument(Integer id) {
        return remakeDocumentDTO(documentRepository.findAllByDeviceId(id));
    }

    public int getDocumentsCount(Integer deviceId) {
        return getListDocument(deviceId).size();
    }

    private List<DocumentDTO> remakeDocumentDTO(List<Document> list) {
        List<DocumentDTO> dtoList = new ArrayList<>();
        for (Document unit : list) {
            DocumentDTO documentDTO = DocumentDTO.from(unit);
            dtoList.add(documentDTO);
        }
        return dtoList;
    }

//    public ObjectLocation createLocation(ObjectLocationDTO locationDTO) {
//        return objectLocationRepository.save(ObjectLocation.from(locationDTO));
//    }

//    public void createObject(ObjectCompanyDTO objectDTO, Principal principal) {
//        objectCompanyRepository.save(ObjectCompany.from(objectDTO));
//
//        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
//        MainHistory history = MainHistory.createObjectFromDevice(user.getFullName(), objectDTO.getId());
//        mainHistoryRepository.save(history);
//    }
//
//    public void createLocation(ObjectLocationDTO objectLocationDTO, Principal principal) {
//
//        Translit translit = new Translit();
//        String trimName = objectLocationDTO.getAddress().trim();
//        String nameExchange = trimName.replaceAll(" ", "-");
//
//        String translitName = translit.cyr2lat(nameExchange.toUpperCase());
//        String lowerName = translitName.toLowerCase();
//
//        objectLocationDTO.setMinioName(lowerName);
//
//        objectLocationRepository.save(ObjectLocation.from(objectLocationDTO));
//
//        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
//        MainHistory history = MainHistory.createFromLocation(user.getFullName(), objectLocationDTO.getId());
//        mainHistoryRepository.save(history);
//    }

    public void delete(Integer id, Principal principal) {
        deviceRepository.deleteById(id);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.deleteFromDevice(user.getFullName(), id);
        mainHistoryRepository.save(history);
    }

    public void putHistoryCreate() {
        var history = MainHistory.createFromDevice(userRepository.findById(1).orElseThrow().getName(),getLastId());
        mainHistoryRepository.save(history);
    }

    public void edit(Integer id, Device edited, Principal principal) {
        Device device = Device.from(getById(id));
        device.setName(edited.getName());
        device.setDescription(edited.getDescription());
        device.setType(edited.getType());
        device.setInventoryNumber(edited.getInventoryNumber());
        device.setSerialNumber(edited.getSerialNumber());
        deviceRepository.save(device);

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        MainHistory history = MainHistory.editDevice(user.getFullName(), device.getId());
        mainHistoryRepository.save(history);
    }

    public List<DeviceDTO> getAllForExcel() {
        return deviceRepository.findAll(Sort.by("id").ascending()).stream().map(DeviceDTO::from).collect(Collectors.toList());
    }
}