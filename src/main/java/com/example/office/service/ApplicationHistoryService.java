package com.example.office.service;

import com.example.office.model.application.repository.*;
import lombok.*;
import org.springframework.stereotype.*;

@Service
@AllArgsConstructor
public class ApplicationHistoryService {
    private final ApplicationHistoryRepository repository;
}