package com.example.office.service;

import com.example.office.model.document.documentTrash.DocumentTrash;
import com.example.office.model.document.documentTrash.DocumentTrashRepository;
import com.example.office.model.document.repository.DocumentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DocumentTrashService {
    private final DocumentTrashRepository documentTrashRepository;
    private final DocumentRepository documentRepository;


    public List<DocumentTrash> getAllList() {
        var deleteList =  documentTrashRepository.findAll();
        for (DocumentTrash documentTrash : deleteList) {
            if (documentTrash.getLeftDays() <= 0) {
                documentTrashRepository.delete(documentTrash);
            }
        }
        return deleteList;
    }
}
