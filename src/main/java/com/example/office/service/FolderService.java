package com.example.office.service;

import com.example.office.DTO.CounterpartyDTO;
import com.example.office.DTO.DocumentDTO;
import com.example.office.configuration.Translit;
import com.example.office.model.document.Document;
import com.example.office.model.folder.Folder;
import com.example.office.model.folder.FolderDTO;
import com.example.office.model.folder.repository.FolderRepository;
import com.example.office.model.сounterparty.Counterparty;
import io.minio.Result;
import io.minio.messages.Item;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
@AllArgsConstructor
public class FolderService {
    private final FolderRepository folderRepository;
    private final MinioService minioService;

    public List<FolderDTO> getFolderAllAuto() {
        return remakeDto(folderRepository.findAllByAutoTrue());
    }

    private List<FolderDTO> remakeDto(List<Folder> list) {
        List<FolderDTO> dtoList = new ArrayList<>();
        for (Folder unit : list) {
            FolderDTO folderDTO = FolderDTO.from(unit);
            dtoList.add(folderDTO);
        }
        return dtoList;
    }

    public List<FolderDTO> getFilteredList(String search) {
        return remakeDto(folderRepository.findByPageNameIgnoreCaseContains(search));
    }

    public Map<String,List<DocumentDTO>> getCounterFiles(String name) {
        var mainList = folderRepository.findByName(name).orElseThrow().getDocuments();
        List<DocumentDTO> counterList = new ArrayList<>();
        List<DocumentDTO> locationsList = new ArrayList<>();
        List<DocumentDTO> deviceList = new ArrayList<>();
        List<DocumentDTO> appList = new ArrayList<>();
        Map<String,List<DocumentDTO>> listMap = new HashMap<>();
        for (int i = 0; i < mainList.size(); i++) {
            if (mainList.get(i).getCounterparty()!=null&&mainList.get(i).getLocation()==null&&mainList.get(i).getApplication()==null){
                counterList.add(DocumentDTO.from(mainList.get(i)));
            }else if(mainList.get(i).getCounterparty()!=null&&mainList.get(i).getApplication()!=null){
                appList.add(DocumentDTO.from(mainList.get(i)));
            }else if(mainList.get(i).getLocation()!=null&&mainList.get(i).getDevice()==null){
                locationsList.add(DocumentDTO.from(mainList.get(i)));
            }else if(mainList.get(i).getLocation()!=null&&mainList.get(i).getDevice()!=null){
                deviceList.add(DocumentDTO.from(mainList.get(i)));
            }
        }

        listMap.put("counters",counterList);
        listMap.put("apps", appList);
        listMap.put("devices", deviceList);
        listMap.put("locations",locationsList);

        return listMap;
    }

    public List<FolderDTO> getOwnFolders() {
        var folderMain = FolderDTO.from(folderRepository.findByName("general").orElseThrow());
        var folderBuh = FolderDTO.from(folderRepository.findByName("accountant").orElseThrow());
        var folders = remakeDto(folderRepository.findAllByAutoFalse());
        if (folders.contains(folderBuh)||folders.contains(folderMain)){
            folders.set(1,folderBuh);
            folders.set(2,folderMain);
        }
        return folders;
    }

    public FolderDTO getFolderByName(String name) {
        return FolderDTO.from(folderRepository.findByName(name).orElseThrow());
    }

    public String createFolder(String name) {
        try {
            if (name.equalsIgnoreCase("Общая")
                    ||name.equalsIgnoreCase("Бухгалтерия")
                    ||name.equalsIgnoreCase("general")
                    ||name.equalsIgnoreCase("accountant")){
                return "Нельзя использовать дефолтные значения";
            }

            if (name.trim().isEmpty()||name.length()<3) {
                return "минимальное кол-во 3 символа";
            }

            var nameGood = validate(name);

            var testFolder = folderRepository.existsByName(name);
            var testFolderTwo = folderRepository.existsByName(nameGood);
            var testMinio = minioService.isBucket(nameGood);

            if (!testFolder&&!testFolderTwo&&!testMinio){
                Folder folder = Folder.builder() //сделать проверку на такое же имя folder
                        .name(nameGood)
                        .size(0)
                        .auto(false)
                        .documentCount(0)
                        .pageName(name)
                        .build();
                folderRepository.save(folder);
                minioService.makeBucket(nameGood);
                return "Успешно создано";
            }else{
                return "Такое название папку уже используется";
            }
        }catch (Exception e){
            System.out.println("Ошибка при создании папки: " + e.getMessage());
            return "Что то пошло не так, при создании папки, проверьте входные данные";
        }

    }

    public String validate(String name){
        Translit translit = new Translit();//
        var trimName = name.trim();
        String regex = trimName.replaceAll("[-+.^:~_@#$&*()`!,]","");
        var trimTwo = regex.trim();
        var nameExchange = trimTwo.replaceAll("\\s+","-");
        var transliteName = translit.cyr2lat(nameExchange.toUpperCase());
        return transliteName.toLowerCase();
    }

    public String deleteFolder(String name) {
        try {
            if (name.equals("general")||name.equals("accountant")){
                return "Дефолтные папки не подлежат удалению";
            }
            var folderExist = folderRepository.existsByName(name);
            var minioFolderExist = minioService.isBucket(name);

            if (folderExist&&minioFolderExist){
                var folder = folderRepository.findByName(name).orElseThrow();
                var iterator = minioService.getListObjects(name);

                if (folder.getDocumentCount()>0&&folder.getDocuments()!=null){
                    return "Необходимо удалить все документы перед удаление папки";
                }else if (iterator.iterator().hasNext()) {
                    return "В Файловом хранилище присутствуют документы, удалите документы в профайлe Minio";
                }else {
                    folderRepository.delete(folderRepository.findByName(name).orElseThrow());
                    minioService.removeBucket(name);
                    return "Папка удаленна";
                }

            }else if(!folderExist&&minioFolderExist){
                return "Отсутствует папка с таким именем";
            }else {
                folderRepository.delete(folderRepository.findByName(name).orElseThrow());
                return "Папка удаленна, но в файловом хранилище отсутствовал необходимый элемент";
            }
        }catch (Exception e){
            System.out.println("Проблема с удалением папки: " + e.getMessage());
            return "Error with files";
        }
    }

    public FolderDTO getGeneralFolder() {
//        minioService.makeBucket("general");
        String bucketNameGeneral = "general";
        var testFolderGeneral = folderRepository.existsByName(bucketNameGeneral);
        if (!testFolderGeneral) {
            minioService.makeBucket(bucketNameGeneral);
            Folder folder = Folder.builder()
                    .name(bucketNameGeneral)
                    .size(0)
                    .auto(false)
                    .documentCount(0)
                    .pageName("Общая")
                    .build();
            folderRepository.save(folder);

            return FolderDTO.from(folder);
        }else {
            return FolderDTO.from(folderRepository.findByName(bucketNameGeneral).orElseThrow());
        }
    }

    public FolderDTO getAccountantFolder() {
//        minioService.makeBucket("general");
        String bucketNameAccountant = "accountant";
        var testFolderAccountant = folderRepository.existsByName(bucketNameAccountant);
        if (!testFolderAccountant) {
            Folder folder = Folder.builder()
                    .name(bucketNameAccountant)
                    .size(0)
                    .auto(false)
                    .documentCount(0)
                    .pageName("Бухгалтер")
                    .build();
            folderRepository.save(folder);
            minioService.makeBucket(bucketNameAccountant);
            return FolderDTO.from(folder);
        }else {
            return FolderDTO.from(folderRepository.findByName(bucketNameAccountant).orElseThrow());
        }
    }
}
