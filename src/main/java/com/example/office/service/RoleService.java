package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
@AllArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;

    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    public Role getById(int id) {
        return roleRepository.getById(id);
    }

    public RoleDTO getByDtoId(int id) {
        return RoleDTO.from(roleRepository.findById(id).orElseThrow());
    }

    public void create(RoleDTO roleDTO) {
        Role role = Role
                .builder()
                .role(roleDTO.getRole())
                .build();
        roleRepository.save(role);
    }

    public void deleteById(int id) {
        roleRepository.deleteById(id);
    }
}