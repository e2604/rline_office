package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.application.Priority;
import com.example.office.model.application.repository.PriorityRepository;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
@AllArgsConstructor
public class PriorityService {
    private final PriorityRepository priorityRepository;

    public List<Priority> getAll() {
        return priorityRepository.findAll();
    }

    public PriorityDTO getById(int id) {
        return PriorityDTO.from(priorityRepository.findById(id).orElseThrow());
    }

    public PriorityDTO getByName(String name) {
        return PriorityDTO.from(priorityRepository.findByName(name));
    }

    public void create(PriorityDTO priorityDTO) {
        Priority priority = Priority
                .builder()
                .name(priorityDTO.getName())
                .build();
        priorityRepository.save(priority);
    }

    public void deleteById(int id) {
        priorityRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        priorityRepository.deleteByName(name);
    }
}