package com.example.office.service;

import com.example.office.model.document.repository.*;
import lombok.*;
import org.springframework.stereotype.*;

@Service
@AllArgsConstructor
public class DocumentStatusService {

    private final DocumentStatusRepository repository;
}