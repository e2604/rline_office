package com.example.office.configuration;

import lombok.*;

import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.crypto.bcrypt.*;
import org.springframework.security.web.access.*;

import javax.sql.*;

@Configuration
@AllArgsConstructor
@EnableWebSecurity(debug = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    //источник данных, подключение к базе данных
    //в которой хранятся пользователи
    private final DataSource dataSource;

    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/applications/kanban")
                .authenticated()
                .antMatchers( "/user/create", "/file-manager-all",
                        "/minio-page")
                .hasAuthority("ADMIN")
                .antMatchers("/file-folder/accountant")
                .hasAnyAuthority("ADMIN", "ACCOUNTANT")
                .antMatchers("/file-manager")
                .hasAnyAuthority("ADMIN", "MANAGER", "ACCOUNTANT")
                .antMatchers("/user/list", "/counterparty/**",
                        "/object/**", "/location/**",
                        "/project/**", "/device/add", "/device/list", "/device-type/**",
                        "/applications/list", "/history-page", "/file-folder/**",
                        "/documentView/{id}", "/folderView/**", "/remove-list")
                .hasAnyAuthority("ADMIN", "MANAGER")
                .antMatchers("/applications/add")
                .hasAnyAuthority("ADMIN", "MANAGER", "EXECUTOR")
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler())
                .and()
                .formLogin()
                .loginPage("/user/authorization")
                .failureUrl("/user/authorization?error=true")
                .defaultSuccessUrl("/user/profile")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/user/authorization")
                .deleteCookies("JSESSIONID")
                .clearAuthentication(true) //удаляем информацию об авторизации юзера;
                .invalidateHttpSession(true); //удаляем сессию при выходе юзера
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                // Spring Security should completely ignore URLs starting with /resources/
                .antMatchers("/apiDocument/download/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authentication) throws Exception {
        //запрос для выборки пользователя
        String fetchUsersQuery = "SELECT email, password, enabled"
                + " FROM users"
                + " WHERE email = ?";

        //запрос для ролей пользователя
        String fetchRolesQuery = "SELECT users.email, roles.role"
                + " FROM users"
                + " INNER JOIN roles"
                + " ON roles.id = users.role_id"
                + " WHERE email = ?";

        authentication
                .jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(fetchUsersQuery)
                .authoritiesByUsernameQuery(fetchRolesQuery);
    }
}