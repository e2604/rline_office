package com.example.office.configuration;

import com.example.office.model.folder.Folder;
import com.example.office.model.folder.repository.FolderRepository;
import com.example.office.service.MinioService;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class DatabaseInitializer {

    private final FolderRepository folderRepository;
    private final MinioService minioService;

    @Bean
    public CommandLineRunner init() {
        return (args -> {
            String bucketNameGeneral = "general";
            String bucketNameAccountant = "accountant";
            var testFolderGeneral = folderRepository.existsByName(bucketNameGeneral);
            var testFolderAccount = folderRepository.existsByName(bucketNameAccountant);
            if (!testFolderGeneral){
                Folder folder = Folder.builder()
                        .name(bucketNameGeneral)
                        .size(0)
                        .auto(false)
                        .documentCount(0)
                        .pageName("Общая")
                        .build();
                folderRepository.save(folder);
            }
            minioService.makeBucket(bucketNameGeneral);
            if (!testFolderAccount){
                Folder folder2 = Folder.builder()
                        .name(bucketNameAccountant)
                        .size(0)
                        .auto(false)
                        .documentCount(0)
                        .pageName("Бухгалтер")
                        .build();
                folderRepository.save(folder2);
            }
            minioService.makeBucket(bucketNameAccountant);

        });
    }

}
