package com.example.office.configuration;

import org.slf4j.*;
import org.springframework.security.access.*;
import org.springframework.security.core.*;
import org.springframework.security.core.context.*;
import org.springframework.security.web.access.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    private static final Logger Log = LoggerFactory.getLogger(CustomAccessDeniedHandler.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException accessDeniedException) throws IOException, ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Log.info("User '" + authentication.getName() + "' attempted to access the URL: "
            + request.getRequestURI());
        }
        response.sendRedirect("/access-denied");
    }
}