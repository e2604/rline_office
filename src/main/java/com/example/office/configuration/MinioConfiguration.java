package com.example.office.configuration;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;

@Data
@Configuration
@ConfigurationProperties(prefix = "minio")
public class MinioConfiguration {

    private String endpoint; //URL, domain name ,IPv4, IPv6
    private Integer port; //TCP/IP порт
    private String accessKey; //accessKey - ID пользователя minIO
    private String secretKey; //secretKey - пароль пользователя minIO
    private boolean secure; //если true, использует https вместо http, дефолтное значение - true
    private String bucketName; //дефолтный бакет
    private long imageSize; //максимальный размер изображения
    private long fileSize; //максимальный размер остальных файлов

    @Bean
    public MinioClient minioClient() {
        return MinioClient
                .builder()
                .credentials(accessKey, secretKey)
                .endpoint(endpoint, port, secure)
                .build();
    }
}