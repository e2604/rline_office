package com.example.office.controller.object;

import com.example.office.DTO.*;
import com.example.office.model.object.*;
import com.example.office.service.*;
import com.example.office.xls.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.*;
import javax.validation.*;
import java.io.*;
import java.security.*;
import java.text.*;
import java.util.*;

@Controller
@RequestMapping("/object")
@AllArgsConstructor
public class ObjectCompanyController {

    private final ObjectCompanyService objectService;
    private final ObjectLocationService objectLocationService;
    private final CounterpartyService counterpartyService;

    @PostMapping("/add")
    public String add(@Valid ObjectCompany object, Principal principal) {
        objectService.add(ObjectCompanyDTO.from(object), principal);
        return "redirect:/object/list";
    }

    @PostMapping("/add-from-device")
    public String addFromDevice(@Valid ObjectCompany object, Principal principal) {
        objectService.add(ObjectCompanyDTO.from(object), principal);
        return "redirect:/device/add";
    }

    @PostMapping("/add_from_location")
    public String addFromLocation(@Valid ObjectCompany objectCompany, Principal principal) {
        objectService.add(ObjectCompanyDTO.from(objectCompany), principal);
        return "redirect:/location/create";
    }

//    @PostMapping("/add-from-object")
//    public String addLocationFromObject(@Valid ObjectCompany object,
//                                        @RequestParam Integer objectId,
//                                        @RequestParam Integer locationId,
//                                        Principal principal) {
//        ObjectLocation location = ObjectLocation.from(objectLocationService.getById(locationId));
//        objectService.add(ObjectCompanyDTO.from(object), principal);
//        return "object/overview/" + objectId;
//    }

    @GetMapping("/create")
    public String objectCreatePage(Model model) {
        model.addAttribute("listObjects", objectService.getListObject());
        return "object/object-create";
    }

    @GetMapping("/overview/{id}")
    private String showOneObject(Model model, @PathVariable Integer id) {
        model.addAttribute("oneObject", objectService.getOneObject(id));
        model.addAttribute("listObjects", objectService.getListObject());
        model.addAttribute("listCounterparty", counterpartyService.getAll());
        model.addAttribute("listLocations", objectLocationService.getAll());
//        model.addAttribute("sortedLocations", objectLocationService.getSortedList());
        return "object/object-overview";
    }

    @GetMapping("/list")
    public String listObjectsPage() {
        return "object/object-list";
    }

    @GetMapping("/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("/object/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=object_list_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        List<ObjectCompanyDTO> objectList = objectService.getAllForExcel();
        ObjectExcelExporter excelExporter = new ObjectExcelExporter(objectList);
        excelExporter.export(response);
    }

    @PostMapping("/overview/{id}/edit")
    public String edit(@PathVariable Integer id,
                       ObjectCompany edited,
                       Principal principal) {
        objectService.edit(id, edited, principal);
        return "redirect:/object/overview/" + id;
    }

    @PostMapping("/overview/{id}/delete")
    public String delete(@PathVariable Integer id, Principal principal) {
        objectService.delete(id, principal);
        return "redirect:/object/list";
    }

    @PostMapping("/overview/{id}/edit-location")
    public String editLocation(@PathVariable Integer id,
                       ObjectLocation edited,
                       Principal principal) {
        Integer objectId = objectLocationService.getById(id).getObject().getId();
        objectLocationService.editFromObject(id, edited, principal);
        return "redirect:/object/overview/" + objectId;
    }

    @PostMapping("/overview/{objectId}/delete-location-from-list")
    public String deleteLocationFromList(@PathVariable Integer objectId,
                                         ObjectLocation locationDelete,
                                         Principal principal) {
        objectService.deleteLocationFromList(objectId, locationDelete, principal);

        return "redirect:/object/overview/" + objectId;
    }
}