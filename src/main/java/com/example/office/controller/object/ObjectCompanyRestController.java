package com.example.office.controller.object;

import com.example.office.DTO.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@AllArgsConstructor
public class ObjectCompanyRestController {
    private final ObjectCompanyService objectCompanyService;

    @GetMapping("/all/objects")
    public List<ObjectCompanyDTO> getAllObjects() {
        return objectCompanyService.getAll();
    }

    @GetMapping("/search/objects/{search}")
    public List<ObjectCompanyDTO> searchObjects(@PathVariable String search) {
        return objectCompanyService.getSearchObjects(search);
    }

//    @GetMapping("/object/{objectId}/locations")
//    public List<ObjectLocation> getAllLocations(@PathVariable Integer objectId) {
//        return objectCompanyService.getAllLocationsByObject(objectId);
//    }

//    @GetMapping("/object")
//    public Integer getLocations(ObjectCompanyDTO object) {
//        return object.getCountLocations();
//    }
}