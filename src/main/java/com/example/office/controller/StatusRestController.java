package com.example.office.controller;

import com.example.office.DTO.StatusDTO;
import com.example.office.model.application.Status;
import com.example.office.service.*;
import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/status")
public class StatusRestController {
    private final StatusService statusService;

    @GetMapping
    public List<StatusDTO> findAllStatus() {
        return statusService.getAll();
    }

//    @GetMapping("/{statusId}")
//    public StatusDTO getById(@PathVariable int statusId) {
//        return statusService.getById(statusId);
//    }

    @PostMapping("/createInidia")
    public ResponseEntity<Status> createIndiaProject(@Valid @RequestBody Status status){
        Status saveProject = statusService.createIndia(status);
        return new ResponseEntity<Status>(saveProject, HttpStatus.CREATED);
    }
}