package com.example.office.controller;

import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@AllArgsConstructor
public class CommentRestController {
    private final ApplicationCommentService applicationCommentService;
}