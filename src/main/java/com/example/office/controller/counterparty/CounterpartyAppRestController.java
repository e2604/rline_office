package com.example.office.controller.counterparty;

import com.example.office.service.*;
import lombok.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("/api/counterparties")
@AllArgsConstructor
public class CounterpartyAppRestController {

    private final CounterpartyService counterpartyService;

    @GetMapping
    public ResponseEntity<List<?>> getAllApplications(){
        var counterparties = counterpartyService.getAll();
        return ResponseEntity.ok(counterparties);
    }

    @GetMapping("/{id}/locations")
    public ResponseEntity<List<?>> getAllLocations(@PathVariable int id){
        var locations = counterpartyService.getAllLocationsByCounterparty(id);
        return ResponseEntity.ok(locations);
    }
}
