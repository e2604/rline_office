package com.example.office.controller.counterparty;

import com.example.office.DTO.*;
import com.example.office.model.object.ObjectCompany;
import com.example.office.model.object.ObjectLocation;
import com.example.office.model.сounterparty.*;
import com.example.office.service.*;
import com.example.office.xls.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.*;
import java.io.*;
import java.security.*;
import java.text.*;
import javax.validation.*;
import java.util.*;

@Controller
@RequestMapping("/counterparty")
@AllArgsConstructor
public class CounterpartyController {
    private final CounterpartyService counterpartyService;
    private final ObjectLocationService locationService;
    private final ObjectCompanyService objectService;

    @GetMapping("/showAll")
    public List<CounterpartyDTO> showCounterparties() {
        return counterpartyService.getAll();
    }

//    @PostMapping("/add")
//    public String add(@Valid Counterparty counterparty) {
//        return counterpartyService.add(CounterpartyDTO.from(counterparty), counterparty.getType().getId());
//    }

    @PostMapping("/add")
    public String add(@Valid Counterparty counterparty, Principal principal) {
        return counterpartyService.add(CounterpartyDTO.from(counterparty), principal);
    }

    @PostMapping("overview/{id}/remove")
    private String delete(@PathVariable Integer id, Principal principal) {
        counterpartyService.deleteById(id, principal);
        return "redirect:/counterparty/list";
    }

    @PostMapping("/overview/{counterpartyId}/add-location")
    private String addLocationFromCounterparty(@PathVariable Integer counterpartyId,
                                               @Valid ObjectLocation location,
                                               @RequestParam Integer objectId,
                                               Principal principal) {
        if (objectId != 0) {
            ObjectCompany object = locationService.getObject(objectId);
            location.setObject(object);
        }

        location.setCounterparty(locationService.getCounter(counterpartyId));
        locationService.add(ObjectLocationDTO.from(location), principal);
        return "redirect:/counterparty/overview/" + counterpartyId;
    }

    @PostMapping("/overview/{counterpartyId}/add-object")
    private String addObjectFromCounterparty(@PathVariable Integer counterpartyId,
                                             ObjectCompanyDTO object,
                                             Principal principal) {
        objectService.add(object, principal);
        return "redirect:/counterparty/overview/" + counterpartyId;
    }

    @GetMapping("/create")
    public String contrAgentCreatePage(Model model) {
        model.addAttribute("types", counterpartyService.listType());
        return "counterparty/counterparty-create";
    }

    @GetMapping("/list")
    private String showListContrAgents() {
        return "counterparty/counterparty-list";
    }

    @GetMapping("/overview/{id}")
    private String showListContrAgents(Model model, @PathVariable Integer id,
                                       Principal principal) {
        if (principal != null) {
            model.addAttribute("principal", principal);
        }
        model.addAttribute("objects", objectService);
        model.addAttribute("locations", locationService);
        model.addAttribute("counterparties", counterpartyService);
        model.addAttribute("counterparty", counterpartyService.getById(id));
        return "counterparty/counterparty-overview";
    }

    @PostMapping("/overview/{id}/edit")
    public String edit(@PathVariable Integer id,
                       Counterparty edited,
                       Principal principal) {
        counterpartyService.edit(id, edited, principal);
        return "redirect:/counterparty/overview/" + id;
    }

    @PostMapping("/overview/{id}/edit-location")
    public String editLocation(@PathVariable Integer id,
                               ObjectLocation edited,
                               Principal principal) {
        Integer counterpartyId = locationService.getById(id).getCounterparty().getId();
        locationService.editFromCounterparty(id, edited, principal);
        return "redirect:/counterparty/overview/" + counterpartyId;
    }

    @PostMapping("/overview/{counterpartyId}/delete-location/{locationId}")
    public String deleteLocation(@PathVariable Integer locationId,
                                 Principal principal,
                                 @PathVariable Integer counterpartyId) {
        locationService.deleteLocationFromCounterparty(counterpartyId, locationId, principal);
        return "redirect:/counterparty/overview/" + counterpartyId;
    }

    @GetMapping("/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("/counterparty/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=counterparty_list_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<CounterpartyDTO> listCounterparties = counterpartyService.getAllForExcel();
        CounterpartyExcelExporter excelExporter = new CounterpartyExcelExporter(listCounterparties);

        excelExporter.export(response);
    }
}