package com.example.office.controller.counterparty;

import com.example.office.DTO.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;
import java.util.*;

@RestController
@RequestMapping("/apiCounterparty")
@AllArgsConstructor
public class CounterpartyRestController {

    private CounterpartyService counterpartyService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public String createCounterparty(@RequestBody CounterpartyDTOJS counterparty, Principal principal) {
        return counterpartyService.createApi(counterparty,principal);
    }

    @GetMapping("/allCounterparties")
    public List<CounterpartyDTO> getAllProjects() {
        return counterpartyService.getAllDTO();
    }

    @GetMapping("/searchCounterparties/{search}")
    public List<CounterpartyDTO> searchListCounterparties(@PathVariable String search) {
        return counterpartyService.getCounterpartiesSearchList(search);
    }
}