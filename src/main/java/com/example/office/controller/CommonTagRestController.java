package com.example.office.controller;

import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("tag")
@AllArgsConstructor
public class CommonTagRestController {

    private final CommonTagService service;
}