package com.example.office.controller;

import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;

@Controller
@AllArgsConstructor
public class StartPageController {
    @GetMapping("/")
    public String startPage(Principal principal) {
        if (principal == null) return "redirect:/user/authorization";
        return "redirect:/user/profile";
    }
}