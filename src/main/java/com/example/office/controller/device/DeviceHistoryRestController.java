package com.example.office.controller.device;

import com.example.office.service.device.DeviceHistoryService;
import lombok.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@AllArgsConstructor
public class DeviceHistoryRestController {

    private final DeviceHistoryService service;
}