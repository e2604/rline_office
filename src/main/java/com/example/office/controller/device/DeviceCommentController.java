package com.example.office.controller.device;

import com.example.office.DTO.*;
import com.example.office.model.device.repository.*;
import com.example.office.service.device.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;
import java.security.Principal;

@Controller
@AllArgsConstructor
@RequestMapping("/device")
public class DeviceCommentController {

    private final DeviceRepository deviceRepository;
    private final UserService userService;
    private final DeviceCommentService deviceCommentService;

    @PostMapping("/addComment/{id}")
    public String addComment(Model model,
                             @Valid DeviceCommentDTO dto,
                             @Valid @PathVariable Integer id,
                             Principal principal) {
        model.addAttribute("device", deviceRepository.findById(id));
        model.addAttribute("users", userService.getAll());
        deviceCommentService.add(dto, id, principal);
        return "redirect:/device/overview/" + id;
    }
}