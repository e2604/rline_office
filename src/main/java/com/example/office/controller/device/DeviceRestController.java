package com.example.office.controller.device;

import com.example.office.DTO.*;
import com.example.office.service.device.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@AllArgsConstructor
public class DeviceRestController {

    private final DeviceService deviceService;

    @GetMapping("/all/devices")
    public List<DeviceDTO> getAllDevices() {
        return deviceService.getAllDTO();
    }

    @GetMapping("/search/devices/{search}")
    public List<DeviceDTO> searchDevices(@PathVariable String search) {
        return deviceService.getSearchDevices(search);
    }
}