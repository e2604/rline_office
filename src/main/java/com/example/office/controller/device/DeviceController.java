package com.example.office.controller.device;

import com.example.office.DTO.*;
import com.example.office.model.device.*;
import com.example.office.model.user.repository.*;
import com.example.office.service.*;
import com.example.office.service.device.*;
import com.example.office.xls.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.*;
import javax.validation.*;
import java.io.*;
import java.security.*;
import java.text.*;
import java.util.*;

@Controller
@RequestMapping("/device")
@AllArgsConstructor
public class DeviceController {

    private final DeviceService deviceService;
    private final CounterpartyService counterpartyService;
    private final DeviceTypeService typeService;
    private final UserRepository userRepository;
    private final ObjectCompanyService objectService;
    private final DeviceCommentService deviceCommentService;
    private final DeviceStatusService deviceStatusService;
    private final ObjectLocationService objectLocationService;

    @GetMapping("/add")
    private String add(Model model, Principal principal) {

        if (principal != null) {
            model.addAttribute("principal", principal);
        }
        model.addAttribute("devices", deviceService.devicesDTOList());
        model.addAttribute("authors", deviceService.usersDTOList());
        model.addAttribute("types", deviceService.typesDTOList());
        model.addAttribute("locations", deviceService.locationsDTOList());
        model.addAttribute("statuses", deviceService.statusesDTOList());
        model.addAttribute("objects", objectService.getListObject());
        model.addAttribute("counterparties", counterpartyService.getAll());

        return "device/device-create";
    }

    @PostMapping("/add")
    public String add(@Valid Device device, Principal principal) {
        deviceService.create(device, principal);
        return "redirect:/device/list";
    }

//    @PostMapping("/add-location")
//    private String addLocation(@Valid ObjectLocation location, Principal principal) {
//        deviceService.createLocation(ObjectLocationDTO.from(location), principal);
//        return "redirect:/device/add";
//    }
//
//    @PostMapping("/add-object")
//    public String addObject(@Valid ObjectCompany object, Principal principal) {
//        deviceService.createObject(ObjectCompanyDTO.from(object), principal);
//        return "redirect:/device/add";
//    }

    @PostMapping("/add-device-type")
    public String addType(@Valid DeviceType type, Principal principal) {
        typeService.create(type, principal);
        return "redirect:/device/add";
    }

    @GetMapping("/list")
    private String showDevicesList() {
        return "device/device-list";
    }

    @GetMapping("/overview/{id}")
    private String deviceProfile(@PathVariable Integer id, Model model, Principal principal) {

        if (principal != null) {
            model.addAttribute("principal", principal);
        }

        model.addAttribute("device", deviceService.getById(id));
        model.addAttribute("deviceTypes", typeService);
        model.addAttribute("comments", deviceCommentService);
        model.addAttribute("attachments", deviceService);
        model.addAttribute("locations", objectLocationService);

        Integer roleId = userRepository.findByEmail(principal.getName()).orElseThrow().getRole().getId();

        if (roleId == 4 || roleId == 5) {
            return "device/device-overview-by-executor";
        } else {
            return "device/device-overview";
        }
    }

    @PostMapping("/overview/{id}/delete")
    private String delete(@PathVariable Integer id, Principal principal) {
        deviceService.delete(id, principal);
        return "redirect:/device/list";
    }

    @PostMapping("/overview/{id}/edit")
    public String edit(@PathVariable Integer id,
                       Device edited,
                       Principal principal) {
        deviceService.edit(id, edited, principal);
        return "redirect:/device/overview/" + id;
    }

    @GetMapping("/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("/device/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=device_list_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<DeviceDTO> devices = deviceService.getAllForExcel();
        DevicesExcelExport excelExporter = new DevicesExcelExport(devices);

        excelExporter.export(response);
    }

    @PostMapping("/overview/{deviceId}/in-progress")
    public String deviceInProgress(@PathVariable Integer deviceId, Principal principal) {
        deviceStatusService.inProgressDevice(deviceId, principal);
        return "redirect:/device/overview/" + deviceId;
    }

    @PostMapping("/overview/{deviceId}/done")
    public String deviceCompleted(@PathVariable Integer deviceId, Principal principal) {
        deviceStatusService.doneDevice(deviceId, principal);
        return "redirect:/device/overview/" + deviceId;
    }

    @PostMapping("/overview/{deviceId}/burned")
    public String deviceBurned(@PathVariable Integer deviceId, Principal principal) {
        deviceStatusService.burnedDevice(deviceId, principal);
        return "redirect:/device/overview/" + deviceId;
    }
}