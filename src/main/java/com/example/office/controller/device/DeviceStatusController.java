package com.example.office.controller.device;

import com.example.office.service.device.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/device-status")
@AllArgsConstructor
public class DeviceStatusController {

    private final DeviceStatusService deviceStatusService;

    @GetMapping("list")
    private String showDeviceStatusesList(Model model) {
        model.addAttribute("deviceStatusesList", deviceStatusService.deviceStatusesDTOList());

        return "device/device-statuses-list";
    }
}