package com.example.office.controller.device;

import com.example.office.model.device.DeviceType;
import com.example.office.service.device.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


@Controller
@RequestMapping("/device-type")
@AllArgsConstructor
public class DeviceTypeController {

    private final DeviceTypeService deviceTypeService;

    @GetMapping("list")
    private String showDeviceTypesList(Model model) {
        model.addAttribute("deviceTypesList", deviceTypeService.deviceTypesDTOList());

        return "device/device-types-list";
    }

    @PostMapping("list/{id}/delete")
    private String delete(@PathVariable Integer id, Principal principal) {
        deviceTypeService.delete(id, principal);
        return "redirect:/device-type/list";
    }

    @PostMapping("/list/{id}/edit")
    public String edit(@PathVariable Integer id,
                       DeviceType edited,
                       Principal principal) {
        deviceTypeService.edit(id, edited, principal);
        return "redirect:/device-type/list";
    }
}