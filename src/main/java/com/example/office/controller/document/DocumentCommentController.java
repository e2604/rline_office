package com.example.office.controller.document;

import com.example.office.DTO.*;
import com.example.office.model.document.repository.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;
import java.security.*;

@Controller
@RequestMapping("/document")
@AllArgsConstructor
public class DocumentCommentController {

    private final DocumentCommentService service;

    private final DocumentRepository documentRepository;
    private final UserService userService;
    @PostMapping("/addComment/{id}")
    public String addComment(Model model,
                             @Valid DocumentCommentDTO dto,
                             @Valid @PathVariable Integer id,
                             Principal principal){
        model.addAttribute("document",documentRepository.findById(id));
        model.addAttribute("users", userService.getAll());
       service.add(dto, id, principal);
        return "redirect:/documentView/"+id;
    }
}