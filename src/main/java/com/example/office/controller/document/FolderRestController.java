package com.example.office.controller.document;

import com.example.office.model.folder.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.*;

import java.security.*;
import java.util.*;

@RestController
@RequestMapping("/folder")
@AllArgsConstructor
public class FolderRestController {

    private FolderService folderService;
    private DocumentService documentService;

    @GetMapping("/all_auto")
    public List<FolderDTO> getFolderAll(){
        return folderService.getFolderAllAuto();
    }

    @GetMapping("/specialSearch/{search}")
    public List<FolderDTO> specialListSearch(@PathVariable String search){
        return folderService.getFilteredList(search);
    }

    @RequestMapping(value = "/folder-new", method = RequestMethod.POST)
    @ResponseBody
    public String createFolder(@RequestBody String name){
        return folderService.createFolder(name);
    }

    @GetMapping("/delete/file-folder/{name}")
    public String deleteFolder(@PathVariable String name){
        return folderService.deleteFolder(name);
    }

    @RequestMapping(value = "/file-upload/file-folder/{folderName}", method = RequestMethod.POST)
    @ResponseBody
    public String fileUpdate(@RequestBody MultipartFile multipartFile, @PathVariable String folderName, Principal principal){
        try {
            if (multipartFile.getSize()<=0){
                return "Файл отсутствует";
            }
            if(multipartFile.getSize()>9980000){
                return "Максимальный размер файла не должне превышать 10MB";
            }
            String fileType = multipartFile.getContentType();
            if (fileType != null) {
               return documentService.addDocumentFromFolderApi(folderName,multipartFile,fileType,principal);
            }else {
                return "Неизвестный тип файла";
            }
        }catch (Exception e){
            System.out.println("Ошибка при загрузке" + e.getMessage());
            return "Unknown error";
        }
    }

    @RequestMapping(value = "/file-upload/file-folder/admin", method = RequestMethod.POST)
    @ResponseBody
    public String uploadDocumentFromAdmin(@RequestBody MultipartFile multipartFile,Principal principal){
        try {
            if (multipartFile.getOriginalFilename().contains("#")){
                return "Не разрешенный символ в имени файла";
            }
            if (multipartFile.getSize()<=0){
                return "Файл отсутствует";
            }
            if(multipartFile.getSize()>9980000){
                return "Максимальный размер файла не должне превышать 10MB";
            }
            String fileType = multipartFile.getContentType();
            if (fileType != null) {
                return documentService.addDocumentFromAdminApi(multipartFile,principal);
            }else {
                return "Неизвестный тип файла";
            }
        }catch (Exception e){
            System.out.println("Ошибка при загрузке" + e.getMessage());
            return "Unknown error";
        }
    }

    @RequestMapping(value = "/file-upload/counterparty/overview/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String fileUpdateCounter(@RequestBody MultipartFile multipartFile, @PathVariable int id, Principal principal){
        try {
            if (multipartFile.getOriginalFilename().contains("#")){
                return "Не разрешенный символ в имени файла";
            }
            if (multipartFile.getSize()<=0){
                return "Файл отсутствует";
            }
            if(multipartFile.getSize()>9980000){
                return "Максимальный размер файла не должне превышать 10MB";
            }
            String fileType = multipartFile.getContentType();
            if (fileType != null) {
                return documentService.addDocumentFromCounterpartyApi(id,multipartFile,fileType,principal);
            }else {
                return "Неизвестный тип файла";
            }
        }catch (Exception e){
            System.out.println("Ошибка при загрузке" + e.getMessage());
            return "Unknown error";
        }
    }

    @RequestMapping(value = "/file-upload/location/overview/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String fileUpdateLocation(@RequestBody MultipartFile multipartFile, @PathVariable int id,Principal principal){
        try {
            if (multipartFile.getOriginalFilename().contains("#")){
                return "Не разрешенный символ в имени файла";
            }
            if (multipartFile.getSize()<=0){
                return "Файл отсутствует";
            }
            if(multipartFile.getSize()>9980000){
                return "Максимальный размер файла не должне превышать 10MB";
            }
            String fileType = multipartFile.getContentType();
            if (fileType != null) {
                return documentService.addDocumentFromLocationApi(id,multipartFile,fileType,principal);
            }else {
                return "Неизвестный тип файла";
            }
        }catch (Exception e){
            System.out.println("Ошибка при загрузке" + e.getMessage());
            return "Unknown error";
        }
    }

    @RequestMapping(value = "/file-upload/device/overview/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String fileUpdateDevice(@RequestBody MultipartFile multipartFile, @PathVariable int id, Principal principal){
        try {
            if (multipartFile.getOriginalFilename().contains("#")){
                return "Не разрешенный символ в имени файла";
            }
            if (multipartFile.getSize()<=0){
                return "Файл отсутствует";
            }
            if(multipartFile.getSize()>9980000){
                return "Максимальный размер файла не должне превышать 10MB";
            }
            String fileType = multipartFile.getContentType();
            if (fileType != null) {
                return documentService.addDocumentFromDeviceApi(id,multipartFile,fileType,principal);
            }else {
                return "Неизвестный тип файла";
            }
        }catch (Exception e){
            System.out.println("Ошибка при загрузке" + e.getMessage());
            return "Unknown error";
        }
    }

    @RequestMapping(value = "/file-upload/applications/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String fileUpdateApplication(@RequestBody MultipartFile multipartFile, @PathVariable int id, Principal principal){
        try {
            if (multipartFile.getOriginalFilename().contains("#")){
                return "Не разрешенный символ в имени файла";
            }
            if (multipartFile.getSize()<=0){
                return "Файл отсутствует";
            }
            if(multipartFile.getSize()>9980000){
                return "Максимальный размер файла не должне превышать 10MB";
            }
            String fileType = multipartFile.getContentType();
            if (fileType != null) {
                return documentService.addDocumentFromApplicationApi(id,multipartFile,fileType,principal);
            }else {
                return "Неизвестный тип файла";
            }
        }catch (Exception e){
            System.out.println("Ошибка при загрузке" + e.getMessage());
            return "Unknown error";
        }
    }
}