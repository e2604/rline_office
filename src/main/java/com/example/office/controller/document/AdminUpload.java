package com.example.office.controller.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AdminUpload {
    @JsonProperty("multipartFile")
    private File multipartFile;
    private String formRadios;
}
