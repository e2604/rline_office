package com.example.office.controller.document;

import com.example.office.DTO.*;
import com.example.office.model.document.*;
import com.example.office.model.сounterparty.*;
import com.example.office.service.*;
import lombok.*;
import org.apache.commons.io.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.*;


import javax.servlet.http.*;
import java.io.*;
import java.net.*;
import java.nio.charset.*;

import java.security.*;
import java.util.*;

@RestController
@RequestMapping("/apiDocument")
@AllArgsConstructor
public class DocumentRestController {
    private CounterpartyService counterpartyService;
    private DocumentService documentService;
    private MinioService minioService;

    @GetMapping("/countryAll")
    public List<Counterparty> getCounterAll() {
        return counterpartyService.getAllBD();
    }

    @GetMapping("/counterFilesCount/{id}")
    public int getCountFilesCounterparty(@PathVariable int id) {
        return documentService.getCountFilesCounterparty(id);
    }

    @GetMapping("/specialSearch/{search}")
    public List<Counterparty> specialListSearch(@PathVariable String search) {
        return counterpartyService.getFilteredList(search);
    }

    @RequestMapping(value = "/documentSearch", method = RequestMethod.POST)
    @ResponseBody
    public List<DocumentDTO> searchListDocumentTwo(@RequestBody SearchClass search) {
        return documentService.getSearchListGlobal(search);
    }

    @RequestMapping(value = "/documentSearchSpecialMarkDelete", method = RequestMethod.POST)
    @ResponseBody
    public List<DocumentDTO> searchListDocumentMarkDelete(@RequestBody SearchClass search) {
        return documentService.getSearchListMarkDelete(search);
    }

    @RequestMapping(value = "/search/file-folder/{folderName}", method = RequestMethod.POST)
    @ResponseBody
    public List<DocumentDTO> searchListDocumentFolder(@RequestBody SearchClass search, @PathVariable String folderName) {
        return documentService.getSearchListFolder(search, folderName);
    }

    @GetMapping("/all")
    public List<DocumentDTO> getAllDocument() {
        return documentService.getAllSortDTO();
    }

    @GetMapping("/mark-delete-all")
    public List<DocumentDTO> getMarkAllDocument() {
        return documentService.getMarkDeleteList();
    }

    @GetMapping("/file-folder/{folder}")
    public List<DocumentDTO> getFolderAllDocument(@PathVariable String folder) {
        return documentService.getFolderDocumentList(folder);
    }

    @GetMapping("/documentSearch/{search}")
    public List<DocumentDTO> searchListDocument(@PathVariable String search) {
        return documentService.getSearchList(search);
    }

    @GetMapping("/removeObject/{id}/{bucketName}/{objectName}")
    public String deleteObject(Principal principal, @PathVariable String bucketName, @PathVariable String objectName, @PathVariable int id) {
        try {
            var document = documentService.getById(id);
            return documentService.deleteDocument(document, bucketName, objectName);
        } catch (Exception e) {
            documentService.putError(id, principal);
            return "Что то пошло не так :(";
        }
    }

    @GetMapping("/removeObjectTrash/{id}/{objectName}")
    public String deleteObjectTrash(@PathVariable String objectName, @PathVariable int id) {
        try {
            documentService.deleteDocumentTrash(id);
            return minioService.removeObject("trash", objectName) ? "Объект удален" : "Не удалось удалить объект";
        } catch (Exception e) {
            return "Что то пошло не так :(";
        }
    }

    @RequestMapping("/download/{id}/{bucketName}/{fileName}")
    public String download(HttpServletResponse response, @PathVariable String bucketName, @PathVariable String fileName, @PathVariable int id, Principal principal) {
        InputStream in = null;
        var document = documentService.getById(id);
        try {
            in = minioService.downloadObject(bucketName, fileName);
            response.setHeader("Content-Disposition", "attachment: filename="
                    + URLEncoder.encode(fileName, StandardCharsets.UTF_8));
            response.setCharacterEncoding("UTF-8");
            IOUtils.copy(in, response.getOutputStream());
            if (principal != null) {
                documentService.putHistoryDownloadDocument(document.getId(), principal);
            }
            return "Успешно";
        } catch (UnsupportedEncodingException exception) {
            exception.printStackTrace();
            documentService.putError(id, principal);
            return "Не успешно ошибка1:" + exception.getMessage();
        } catch (IOException exception) {
            exception.printStackTrace();
            documentService.putError(id, principal);
            return "Не успешно ошибка2:" + exception.getMessage();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException exception) {
                    documentService.putError(id, principal);
                    exception.printStackTrace();
                }
            }
        }
    }

    @PostMapping("/upload/{id}")
    public String uploadFile(MultipartFile multipartFile, String bucketName, @PathVariable int id, Principal principal) {
        try {
            String fileType = multipartFile.getContentType();
            System.out.println("thisistype " + fileType);
            if (fileType != null) {
                minioService.putObject(multipartFile, bucketName, fileType);
                documentService.addDocument(id, multipartFile, principal);
                return "redirect:/counterparty/overview/" + id;
            }
            return "redirect:/counterparty/overview/" + id; //сюда страницу с ошибкой
        } catch (Exception e) {
            System.out.println(e);
            return "redirect:/pages-400-files";
        }
    }

    @GetMapping("/putMark/{id}")
    public String putMarkDeleteDocument(@PathVariable int id, Principal principal) {
        return documentService.putMarkDelete(id, principal);
    }

    @GetMapping("/link/{id}")
    public String getLinkDocument(@PathVariable int id, Principal principal) {
        try {
            var document = documentService.getById(id);
            if (document != null) {
                return documentService.getLink(document.getFolder().getName(), document.getName(), document.getId(), principal);
            } else {
                documentService.putError(id, principal);
                return "Документ не определен, отсутствует ссылка на документ";
            }
        } catch (Exception exception) {
            documentService.putError(id, principal);
            return "Что то пошло не так";
        }
    }

    @GetMapping("/linkTrash/{object}")
    public String getLinkTrashDocument(@PathVariable String object) {
        try {
            return documentService.getLinkTrash(object);
        } catch (
                Exception exception) {
            return "Документ не определен, отсутствует ссылка на документ";
        }
    }

    @RequestMapping(value = "/ex-name", method = RequestMethod.POST)
    @ResponseBody
    public void addMovieImage(@RequestBody RenameDocumentClass renameDocumentClass, Principal principal) {
        documentService.exchangeNameDocument(renameDocumentClass, principal);
    }

    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    @ResponseBody
    public String transferDocument(@RequestBody TransferDocument transferDocument, Principal principal) {
        try {
            if (transferDocument.getFileName().equals("")) {
                return "Выберите документ на оправку";
            }
//            var lastChar = transferDocument.getEntity().charAt(transferDocument.getEntity().length() - 1);
            String s1 = transferDocument.getEntity().substring(transferDocument.getEntity().indexOf("@") + 1);
            s1.trim();

            int idENtity = Integer.parseInt(s1);
//            int idENtity = lastChar - '0';
            return documentService.transfer(transferDocument, idENtity, principal);//здесь ID cущности;
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
            return ex.getMessage();
        }
    }
}