package com.example.office.controller.location;

import com.example.office.DTO.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/locations")
public class ObjectLocationRestController {
    private final ObjectLocationService locationService;

    @GetMapping("/{id}/devices")
    public ResponseEntity<List<?>> getAllDevices(@PathVariable Integer id){
        var devices = locationService.getNewDevices(id);
        return ResponseEntity.ok(devices);
    }

    @GetMapping("/all")
    public List<ObjectLocationDTO> getAllLocations() {
        return locationService.getAll();
    }

    @GetMapping("/search/{search}")
    public List<ObjectLocationDTO> searchLocations(@PathVariable String search) {
        return locationService.getSearchLocations(search);
    }
}