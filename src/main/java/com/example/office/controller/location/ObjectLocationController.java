package com.example.office.controller.location;

import com.example.office.DTO.*;
import com.example.office.model.device.*;
import com.example.office.model.object.*;
import com.example.office.model.сounterparty.*;
import com.example.office.service.*;
import com.example.office.service.device.*;
import com.example.office.xls.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.*;
import javax.validation.*;
import java.io.*;
import java.text.*;
import java.util.*;
import java.security.*;

@Controller
@RequestMapping("/location")
@AllArgsConstructor
public class ObjectLocationController {

    private final ObjectLocationService objectLocationService;
    private final CounterpartyService counterpartyService;
    private final UserService userService;
    private final DeviceService deviceService;
    private final ObjectCompanyService objectService;
    private final DeviceTypeService deviceTypeService;
    private final DeviceStatusService deviceStatusService;

    @PostMapping("/add")
    public String add(@Valid ObjectLocation objectLocation,
                      @RequestParam Integer objectId,
                      @RequestParam Integer counterpartyId,
                      Principal principal) {
        Counterparty counterparty = objectLocationService.getCounter(counterpartyId);

        if (objectId != 0) {
            ObjectCompany object = objectLocationService.getObject(objectId);
            objectLocation.setObject(object);
        }

        objectLocation.setCounterparty(counterparty);
        objectLocationService.add(ObjectLocationDTO.from(objectLocation), principal);
        return "redirect:/location/list";
    }

    @PostMapping("/add-from-counterparty")
    public String addLocationFromCounterparty(@Valid ObjectLocation objectLocation,
                      @RequestParam Integer objectId,
                      @RequestParam Integer counterpartyId,
                      Principal principal) {
        Counterparty counterparty = objectLocationService.getCounter(counterpartyId);

        if (objectId != 0) {
            ObjectCompany object = objectLocationService.getObject(objectId);
            objectLocation.setObject(object);
        }

        objectLocation.setCounterparty(counterparty);
        objectLocationService.add(ObjectLocationDTO.from(objectLocation), principal);
        return "redirect:/counterparty/overview/" + counterpartyId;
    }

    @PostMapping("/add-from-object")
    public String addLocationFromObject(@Valid ObjectLocation objectLocation,
                                        @RequestParam Integer objectId,
                                        @RequestParam Integer counterpartyId,
                                        Principal principal) {
        Counterparty counterparty = objectLocationService.getCounter(counterpartyId);

        if (objectId != 0) {
            ObjectCompany object = objectLocationService.getObject(objectId);
            objectLocation.setObject(object);
        }

        objectLocation.setCounterparty(counterparty);
        objectLocationService.add(ObjectLocationDTO.from(objectLocation), principal);
        return "redirect:/object/overview/" + objectId;
    }

    @PostMapping("/add-from-device")
    public String addLocationFromDevice(@Valid ObjectLocation objectLocation,
                                        @RequestParam Integer objectId,
                                        @RequestParam Integer counterpartyId,
                                        Principal principal) {
        Counterparty counterparty = objectLocationService.getCounter(counterpartyId);

        if (objectId != 0) {
            ObjectCompany object = objectLocationService.getObject(objectId);
            objectLocation.setObject(object);
        }

        objectLocation.setCounterparty(counterparty);
        objectLocationService.add(ObjectLocationDTO.from(objectLocation), principal);
        return "redirect:/device/add";
    }

    @PostMapping("/delete_from_counter/{id}")
    public String delete(@PathVariable int id) {
        return objectLocationService.deleteLocation(id);
    }

    @PostMapping("/delete_from_listCreate/{id}")
    public String deleteFromList(@PathVariable int id) {
        return objectLocationService.deleteLocationFromList(id);
    }

    @GetMapping("/create")
    public String locationCreatePage(Model model) {
        model.addAttribute("listCounterparty", counterpartyService.getAll());
        model.addAttribute("listObjects", objectService.getListObject());
        model.addAttribute("listLocations", objectLocationService.getAll());
        return "location/location-create";
    }

    @GetMapping("/list")
    public String listLocationsPage() {
        return "location/location-list";
    }

    @GetMapping("/overview/{id}")
    private String showOneLocation(Model model, @PathVariable Integer id, Principal principal) {

        Integer roleId = userService.getByEmail(principal.getName()).getRole().getId();

        model.addAttribute("objects", objectService);
        model.addAttribute("types", deviceTypeService);
        model.addAttribute("locations", deviceService);
        model.addAttribute("statuses", deviceStatusService);
        model.addAttribute("locationDevices", deviceService);
        model.addAttribute("counterparties", counterpartyService);
        model.addAttribute("locationDocuments", objectLocationService);
        model.addAttribute("location", objectLocationService.getOne(id));

        if (roleId == 4) {
            return "location/location-overview-by-executor";
        } else {
            return "location/location-overview";
        }
    }

    @PostMapping("/overview/{id}/edit")
    public String edit(@PathVariable Integer id,
                       ObjectLocation edited,
                       Principal principal) {
        objectLocationService.edit(id, edited, principal);
        return "redirect:/location/overview/" + id;
    }

    @GetMapping("/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("/location/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=location_list_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<ObjectLocationDTO> locations = objectLocationService.getAllForExcel();
        LocationExcelExporter excelExporter = new LocationExcelExporter(locations);
        excelExporter.export(response);
    }

    @PostMapping("/overview/{locationId}/add-device")
    public String addDevice(@PathVariable Integer locationId,
                            @Valid Device device,
                            Principal principal) {
        deviceService.create(device, principal);
        return "redirect:/location/overview/" + locationId;
    }
}