package com.example.office.controller.project;

import com.example.office.DTO.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@AllArgsConstructor
public class ProjectRestController {
    private final ProjectService projectService;

    @GetMapping("/allProjects")
    public List<ProjectDTO> getAllProjects() {
        return projectService.getAllDTO();
    }

    @GetMapping("/searchProjects/{search}")
    public List<ProjectDTO> searchListProjects(@PathVariable String search) {
        return projectService.getProjectSearchList(search);
    }
}