package com.example.office.controller.project;

import com.example.office.DTO.*;
import com.example.office.model.project.*;
import com.example.office.service.*;
import com.example.office.xls.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.data.web.*;
import org.springframework.http.*;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.validation.*;
import org.springframework.validation.annotation.*;
import org.springframework.web.bind.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.*;
import javax.validation.*;
import java.io.*;
import java.security.*;
import java.text.*;
import java.util.*;

@Validated
@Controller
@AllArgsConstructor
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;
    private final StatusService statusService;

//    @GetMapping("/all")
//    public List<Project> getAllProject() {
//        return projectService.getAll();
//    }

    @GetMapping("/{id}")
    public ProjectDTO getProjectByID(@PathVariable int id) {
        return projectService.getProjectById(id);
    }

    @GetMapping("/status/{statusId}")
    public List<ProjectDTO> getProjectByStatus(@PathVariable int statusId) {
        return projectService.getProjectByStatus(statusId);
    }

    @GetMapping("/name/{name}")
    public ProjectDTO getProjectByName(@PathVariable String name) {
        return projectService.getProjectByName(name);
    }

    @GetMapping("/list")
    public String getListProject(Model model, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
//        Page<Project> page = projectService.getAllProjectsPageList(pageable);
//        model.addAttribute("page", page);
//        model.addAttribute("url", "/project/list");
        return "project/project-list";
    }

    @PostMapping("/projects-overview/{id}/update")
    public String update(@PathVariable Integer id,
                         @RequestParam String name,
                         @RequestParam String description,
                         Principal principal) {
        projectService.updateProject(id, name, description, principal);
        return "redirect:/project/overview/" + id;
    }

//    @PostMapping("/add")
//    public String add(@Valid Project project, Principal principal) {
//        return projectService.add(ProjectDTO.from(project), principal);
//    }

    @PostMapping("/add")
    public String add(@Valid Project project, Principal principal) {
        var pro = projectService.add(project, principal);
        return "redirect:/project/list";
    }

    @PostMapping("/overview/{id}/close")
    public String closeProject(@PathVariable Integer id,
                               Principal principal) {
        projectService.closeProject(id, principal);
        return "redirect:/project/overview/" + id;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationEx(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @GetMapping("/create")
    public String projectCreatePage(Model model) {
        model.addAttribute("statuses", statusService.getAll());
        return "project/project-create";
    }

    @GetMapping("/overview/{id}")
    public String projectOverviewPage(@PathVariable Integer id, Model model) {
        model.addAttribute("projects", projectService);
        model.addAttribute("project", projectService.getProjectById(id));
        model.addAttribute("checkStatus", projectService.checkStatus(id));
        return "project/project-overview";
    }

    @GetMapping("/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("/project/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=project_list_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        List<ProjectDTO> projects = projectService.getAllForExcel();
        ProjectExcelExporter excelExporter = new ProjectExcelExporter(projects);
        excelExporter.export(response);
    }

    @PostMapping("/overview/{projectId}/delete")
    public String delete(@PathVariable Integer projectId,
                         Principal principal) {
        projectService.delete(projectId, principal);
        return "redirect:/project/list";
    }
}