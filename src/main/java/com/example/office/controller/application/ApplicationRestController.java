package com.example.office.controller.application;

import com.example.office.DTO.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class ApplicationRestController {

    ApplicationService applicationService;

    @GetMapping("/applications/apiApp/allApps")
    public List<ApplicationDTO> getAllApp() {
        return applicationService.findAll();
    }

    @GetMapping("/applications/apiApp/searchApps/{search}")
    public List<ApplicationDTO> searchAppList(@PathVariable String search) {
        return applicationService.getAppSearchList(search);
    }
}