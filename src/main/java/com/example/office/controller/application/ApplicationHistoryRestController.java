package com.example.office.controller.application;

import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@AllArgsConstructor
public class ApplicationHistoryRestController {
    private final ApplicationHistoryService service;
}