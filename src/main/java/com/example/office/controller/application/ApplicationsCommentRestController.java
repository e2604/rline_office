package com.example.office.controller.application;

import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("api/applications")
public class ApplicationsCommentRestController {
    private final ApplicationCommentService applicationCommentService;

    //    @PostMapping("/createComment")
//    public ResponseEntity<?> addComment(@Valid @RequestBody ApplicationCommentDTO applicationComment, BindingResult bindingResult) {
//        if (bindingResult.hasFieldErrors()) {
//            applicationCommentService.create(applicationComment);
//            return ResponseEntity.ok("Comment invalid");
//        } else {
//            applicationCommentService.create(applicationComment);
//            return ResponseEntity.ok("comment is valid");
//        }
//    }
//    @GetMapping("/commentCount/{id}")
//    public int getCountComments(@PathVariable int id) {
//        return applicationCommentService.getCountComments(id);
//    }

//    @GetMapping("/getAllComment/{id}")
//    public List<ApplicationComment> getCounterAll(@PathVariable int id) {
//        return applicationCommentService.getAllByApplicationId(id);
//    }
}