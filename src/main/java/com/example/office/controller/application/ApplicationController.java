package com.example.office.controller.application;

import com.example.office.DTO.*;
import com.example.office.model.application.*;
import com.example.office.model.application.repository.*;
import com.example.office.model.user.repository.*;
import com.example.office.service.*;
import com.example.office.service.device.*;
import com.example.office.xls.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.data.web.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.*;
import javax.validation.*;

import java.io.*;
import java.security.*;
import java.text.*;
import java.time.format.*;
import java.util.*;

@Controller
@AllArgsConstructor
@RequestMapping("/applications")
public class ApplicationController {
    private final ApplicationService applicationService;
    private final CounterpartyService counterpartyService;
    private final UserService userService;
    private final DeviceService deviceService;
    private final ObjectLocationService objectLocationService;
    private final ProjectService projectService;
    private final TypeService typeService;
    private final PriorityService priorityService;
    private final ApplicationRepository applicationRepository;
    private final ApplicationCommentService applicationCommentService;
    private final ApplicationDevicesService applicationDevicesService;
    private final ApplicationParticipantsService applicationParticipantsService;
    private final UserRepository userRepository;
    private final DeviceStatusService deviceStatusService;

//    @GetMapping
//    public String showApplicationsList(Model model, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable){
//        Page<Application> page=applicationService.getAllApplicationsPageList(pageable);
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
////        model.addAttribute("applications", applicationService.findAll());
//        model.addAttribute("formatter", formatter);
//        model.addAttribute("page", page);
////        model.addAttribute("filter", filter);
//        model.addAttribute("url", "/applications");
//        return "application/applications-list";
//    }

    @GetMapping("/list")
    public String showApplicationsList() {
        applicationService.changeStatusToOverdue();
        return "application/application-list";
    }

    @GetMapping("/{id}")
    public String showApplicationPage(@PathVariable Integer id, Model model,
                                      @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
                                      Principal principal) {

        if (principal != null) {
            model.addAttribute("principal", principal);
        }

        applicationService.changeStatusToOverdue();
        Page<ApplicationComment> page = applicationCommentService.getAllByApplicationId(id, pageable);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        model.addAttribute("page", page);
        model.addAttribute("formatter", formatter);
        model.addAttribute("users", userService.getAll());
        model.addAttribute("applications", applicationService);
        model.addAttribute("application", applicationService.findOne(id));
        model.addAttribute("participants", applicationParticipantsService);
        model.addAttribute("devices", applicationDevicesService);
        model.addAttribute("locationDevices", objectLocationService);
        model.addAttribute("priorities", priorityService.getAll());
        model.addAttribute("projects", projectService.getAll());
        model.addAttribute("comments", applicationCommentService);
        model.addAttribute("url", "/applications/" + id);

        Integer roleId = userRepository.findByEmail(principal.getName()).orElseThrow().getRole().getId();

        if (roleId == 4 || roleId == 5) {
            return "application/application-overview-by-executor";
        } else {
            return "application/application-overview";
        }
    }

    @PostMapping("/addComment/{id}")
    public String addTheme(Model model,
                           @Valid ApplicationCommentDTO dto,
                           @Valid @PathVariable Integer id,
                           Principal principal) {
        model.addAttribute("application", applicationRepository.findById(id));
        model.addAttribute("users", userService.getAll());
        applicationCommentService.add(dto, id, principal);
        return "redirect:/applications/" + id;
    }

    @GetMapping("/add")
    public String showApplicationAddPage(Model model, Principal principal) {

        if (principal != null) {
            model.addAttribute("principal", principal);
        }

        model.addAttribute("users", userService);
        model.addAttribute("devices", deviceService.getAllDTO());
        model.addAttribute("locations", objectLocationService.getAll());
        model.addAttribute("counterparties", counterpartyService.getAll());
        model.addAttribute("projects", projectService.getAll());
        model.addAttribute("types", typeService.getAll());
        model.addAttribute("priorities", priorityService.getAll());
        model.addAttribute("participants", applicationParticipantsService);

        Integer roleId = userRepository.findByEmail(principal.getName()).orElseThrow().getRole().getId();

        if (roleId == 4) {
            return "application/application-create-by-executor";
        } else {
            return "application/application-create";
        }
    }

    @PostMapping("/{id}/edit")
    public String editApplication(@PathVariable Integer id, Application application, Principal principal) {
        applicationService.editApplication(id, application, principal);
        return "redirect:/applications/" + id;
    }

    @PostMapping
    public String create(@Valid Application application, Principal principal) {

        var a = applicationService.add(application, principal);
        applicationService.putHistoryCreate(principal, a.getId());
        return "redirect:/applications/" + a.getId();
    }

    @PostMapping("/start/{id}")
    public String applicationStart(@PathVariable Integer id, Principal principal) {
        List<ApplicationDevices> devices = applicationDevicesService.getDevices(id);
        for (ApplicationDevices device : devices) {
            if (device.getDevice().getStatus().getId() == 1) {
                deviceStatusService.inProgressDevice(device.getDevice().getId(), principal);
            }
        }
        applicationService.startApplication(id, principal);
        return "redirect:/applications/" + id;
    }

    @PostMapping("/cancel/{id}")
    public String applicationCancel(@PathVariable Integer id, Principal principal) {
        applicationService.cancelApplication(id, principal);
//        applicationService.cancelApplicationAfterOverdue(id, principal);
        return "redirect:/applications/" + id;
    }

    @PostMapping("/done/{id}")
    public String applicationDone(@PathVariable Integer id, Principal principal) {
        List<ApplicationDevices> devices = applicationDevicesService.getDevices(id);
        for (ApplicationDevices device : devices) {
            if (device.getDevice().getStatus().getId() == 2) {
                deviceStatusService.doneDevice(device.getDevice().getId(), principal);
            }
        }
        applicationService.doneApplication(id, principal);
        return "redirect:/applications/" + id;
    }

    @GetMapping("/kanban")
    public String kanbanPage(Model model) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//       model.addAttribute("participants",applicationParticipantsService.getParticipants(id));
        model.addAttribute("listApp", applicationService.findAllByStatusNew());
        model.addAttribute("listAppProgress", applicationService.findAllByStatusInProgress());
        model.addAttribute("formatter", formatter);
        model.addAttribute("listAppOverdue", applicationService.findAllByStatusOverdue());
        model.addAttribute("completedApplications", applicationService.getTop10ByStatusId());
        return "application/kanban-board";
    }

    @PostMapping("/{id}/participant")
    public String addParticipant(@PathVariable Integer id, Integer userId) {
        applicationService.addParticipantToList(id, userId);
        return "redirect:/applications/" + id;
    }

    @PostMapping("/{id}/device")
    public String addDevice(@PathVariable Integer id, Integer deviceId) {
        var app = applicationService.findOne(id);
        var device = deviceService.getById(deviceId);
        applicationService.addDeviceToList(id, deviceId);
        return "redirect:/applications/" + id;
    }

    @PostMapping("/{applicationId}/participant/{id}/delete")
    public String deleteParticipant(@PathVariable Integer applicationId, @PathVariable Integer id) {
        applicationParticipantsService.deleteParticipantFromApplication(applicationId, id);
        return "redirect:/applications/" + applicationId;
    }

    @GetMapping("/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("/application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=application_list_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        List<ApplicationDTO> listApplications = applicationService.getAllForExcel();
        ApplicationExcelExporter excelExporter = new ApplicationExcelExporter(listApplications);
        excelExporter.export(response);
    }

    @PostMapping("/{applicationId}/device/{id}/delete")
    public String deleteDevice(@PathVariable Integer applicationId, @PathVariable Integer id) {
        applicationDevicesService.deleteDeviceFromApplication(applicationId, id);
        return "redirect:/applications/" + applicationId;
    }

    @PostMapping("/overview/{applicationId}/delete")
    public String delete(@PathVariable Integer applicationId, Principal principal) {
        applicationService.delete(applicationId, principal);
        return "redirect:/applications/list";
    }
}