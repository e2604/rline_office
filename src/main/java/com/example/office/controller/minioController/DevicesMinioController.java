package com.example.office.controller.minioController;

import com.example.office.service.device.*;
import com.example.office.service.*;
import com.example.office.service.*;
import com.example.office.util.*;
import lombok.*;
import org.apache.commons.io.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.*;

import javax.servlet.http.*;
import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.security.Principal;

@Controller
@RequestMapping("/deviceMinio")
@AllArgsConstructor
public class DevicesMinioController {

    private final MinioService minioService;
    private final DeviceService deviceService;
    private final DocumentService documentService;


    @PostMapping("/upload/{id}")
    public String uploadFile(MultipartFile multipartFile, String bucketName, @PathVariable int id, Principal principal) {
        try {
            if (multipartFile.getSize()<=0){
                return "redirect:/pages-400-files";
            }
            String fileType = multipartFile.getContentType();
            var idDevice = deviceService.getById(id).getId();
            if (fileType != null) {
                minioService.putObject(multipartFile, bucketName, fileType);
                documentService.addDocumentFromDevice(id,multipartFile,principal);

            return "redirect:/device/overview/" + idDevice ;
        }
            return "redirect:/device/overview/" + idDevice; //сюда страницу с ошибкой
        }catch (Exception e){
            System.out.println(e);
            return "redirect:/pages-400-files" ;
        }

    }
}
