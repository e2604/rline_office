package com.example.office.controller.minioController;

import com.example.office.service.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.*;

import java.security.Principal;

@Controller
@RequestMapping("/counterpartyRest")
@AllArgsConstructor
public class CounterpartyMinioController {

    private final MinioService minioService;
    private final CounterpartyService counterpartyService;
    private final DocumentService documentService;


    @PostMapping("/upload/{id}")
    public String uploadFile(MultipartFile multipartFile, String bucketName, @PathVariable int id, Principal principal) {
        try {
            if (multipartFile.getSize()<=0){
                return "redirect:/pages-400-files";
            }
            String fileType = multipartFile.getContentType();
            if (fileType != null) {
                minioService.putObject(multipartFile, bucketName, fileType);
                documentService.addDocument(id, multipartFile, principal);
                return "redirect:/counterparty/overview/" + id;
            }
            return "redirect:/counterparty/overview/" + id; //сюда страницу с ошибкой
        }catch (Exception e){
            System.out.println(e);
            return "redirect:/pages-400-files" ;
        }

    }


}
