package com.example.office.controller.minioController;

import com.example.office.service.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.*;

import java.security.*;


@Controller
@RequestMapping("/objectLocationMinio")
@AllArgsConstructor
public class ObjectLocationMinioController {

    private final MinioService minioService;
    private final ObjectLocationService objectLocationService;
    private final DocumentService documentService;

    @PostMapping("/upload/{id}")
    public String uploadFile(MultipartFile multipartFile, String bucketName,
                             @PathVariable Integer id, Principal principal) {
       try {
           if (multipartFile.getSize()<=0){
               return "redirect:/pages-400-files";
           }
           String fileType = multipartFile.getContentType();
           var idLocat = objectLocationService.getOne(id).getId();
           if (fileType != null) {
               minioService.putObject(multipartFile, bucketName, fileType);
               documentService.addDocumentFromLocation(id,multipartFile,principal);

               return "redirect:/location/overview/" + idLocat ;
           }
           return "redirect:/location/overview/" + idLocat; //сюда страницу с ошибкой
       }catch (Exception e){
           return "redirect:/pages-400-files" ;
       }
    }
}