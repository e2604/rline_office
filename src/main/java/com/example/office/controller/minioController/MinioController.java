package com.example.office.controller.minioController;

import com.example.office.configuration.Translit;
import com.example.office.service.MinioService;
import com.example.office.util.FileTypeMinioUtil;
import lombok.*;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

@RestController
@RequestMapping("/minio")
@AllArgsConstructor
public class MinioController {
    private final MinioService minioService;

    @PostMapping("/upload")
    public String uploadFile(MultipartFile multipartFile, String bucketName) {
        String fileType = FileTypeMinioUtil.getFileType(multipartFile);
        if (fileType != null) {
            return minioService.putObject(multipartFile, bucketName, fileType);
        }
        return "Неподдерживаемый формат файла. Пожалуйста, проверьте файл и загрузите снова";
    }

    @PostMapping("/addBucket")
    public String addBucket(@RequestParam String bucketName) {
        minioService.makeBucket(bucketName);
        return "Папка удачно создана";
    }

    @GetMapping("/showAttachments")
    public List<String> showAttachments(@RequestParam String bucketName) {
        return minioService.listObjectNames(bucketName);
    }

    @GetMapping("/showBucketName")
    public List<String> showBucketName() {
        return minioService.listBucketNames();
    }

    @GetMapping("/showListObjectNameAndDownloadUrl")
    public Map<String, String> showListObjectNameAndDownloadUrl(@RequestParam String bucketName) {
        Map<String, String> map = new HashMap<>();

        List<String> listObjectNames = minioService.listObjectNames(bucketName);

        String url = "localhost:8898/minio/download/" + bucketName + "/";

        listObjectNames.forEach(System.out::println);

        for (String listObjectName : listObjectNames) {
            map.put(listObjectName, url + listObjectName);
        }

        return map;
    }

    @DeleteMapping("/removeBucket")
    public String deleteByBucketName(@RequestParam String bucketName) {
        return minioService.removeBucket(bucketName) ? "Папка удалена" : "Не удалось удалить папку";
    }

    @DeleteMapping("/removeObject")
    public String deleteObject(@RequestParam String bucketName, @RequestParam String objectName) {
        return minioService.removeObject(bucketName, objectName) ? "Объект удален" : "Не удалось удалить объект";
    }

    @RequestMapping("/download")
    public void download(HttpServletResponse response, @RequestParam String bucketName, @RequestParam String objectName) {
        InputStream in = null;

        try {
            in = minioService.downloadObject(bucketName, objectName);
            response.setHeader("Content-Disposition", "attachment: filename="
            + URLEncoder.encode(objectName, StandardCharsets.UTF_8));
            response.setCharacterEncoding("UTF-8");

            IOUtils.copy(in, response.getOutputStream());
        } catch (UnsupportedEncodingException exception) {
            exception.printStackTrace();
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        }
    }
}