package com.example.office.controller.minioController;

import com.example.office.service.*;
import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
public class MinioPageController {
    private final DocumentService documentService;

    @GetMapping("/minio-page")
    public String getMinioPage(Model model) {
        model.addAttribute("buckets", documentService.getAllBucketList());
        model.addAttribute("documents", documentService);
        model.addAttribute("counteBucketFiles", documentService);
        model.addAttribute("counteSystemFiles", documentService);
        model.addAttribute("testIs", documentService);
        model.addAttribute("testFolderPresent", documentService);
        model.addAttribute("testMinioPresent", documentService);
        model.addAttribute("similarDoc", documentService);
        return "minio-page";
    }

    @GetMapping("/minio-delete-minio/{bucketName}/{fileName}")
    public String deleteMinioPageMinio(@PathVariable String bucketName, @PathVariable String fileName) {
        documentService.deleteFromMinio(bucketName, fileName);
//        return "/minio-page";
        return "redirect:/minio-page";
    }

    @GetMapping("/minio-delete-system/{bucketName}/{fileName}")
    public String deleteMinioPageSystem(@PathVariable String bucketName, @PathVariable String fileName) {
        documentService.deleteFromSystem(bucketName, fileName);
//        return "/minio-page";
        return "redirect:/minio-page";
    }
}