package com.example.office.controller.minioController;


import com.example.office.service.ApplicationService;
import com.example.office.service.DocumentService;
import com.example.office.service.MinioService;
import com.example.office.service.ObjectLocationService;
import com.example.office.util.FileTypeMinioUtil;
import lombok.AllArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.Principal;

@Controller
@RequestMapping("/applicationMinio")
@AllArgsConstructor
public class ApplicationMinioController {

    private final MinioService minioService;
    private final ApplicationService applicationService;
    private final DocumentService documentService;


    @PostMapping("/upload/{id}")
    public String uploadFile(MultipartFile multipartFile, String bucketName, @PathVariable int id, Principal principal) {
        try {
            if (multipartFile.getSize()<=0){
                return "redirect:/pages-400-files";
            }
            String fileType = multipartFile.getContentType();
            var idLocat = applicationService.findOne(id).getId();
            if (fileType != null) {

                minioService.putObject(multipartFile, bucketName, fileType);
                documentService.addDocumentFromApplication(id,multipartFile,principal);

                return "redirect:/applications/" + idLocat ;
            }
            return "redirect:/applications/" + idLocat; //сюда страницу с ошибкой
        }catch (Exception e){
            System.out.println(e);
            return "redirect:/pages-400-files" ;
        }


    }

}
