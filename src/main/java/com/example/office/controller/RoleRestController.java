package com.example.office.controller;

import com.example.office.DTO.*;
import com.example.office.model.user.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@AllArgsConstructor
@RequestMapping("/role")
public class RoleRestController {
    private final RoleService roleService;

    @GetMapping("/showAll")
    public List<Role> showRoles() {
        return roleService.getAll();
    }

    @GetMapping("/showById")
    public RoleDTO showById(@RequestParam Integer id) {
        return roleService.getByDtoId(id);
    }

    @PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@RequestBody Role role) {
        roleService.create(RoleDTO.from(role));

        return ResponseEntity.status(HttpStatus.CREATED)
                .body("Роль " + role.getRole() + " добавлена");
    }

    @DeleteMapping("/remove")
    public ResponseEntity<?> delete(@RequestParam Integer id) {
        roleService.deleteById(id);

        return ResponseEntity.status(HttpStatus.CREATED)
                .body("Роль удалена");
    }
}