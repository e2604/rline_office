package com.example.office.controller;

import com.example.office.model.document.*;
import com.example.office.model.history.mainHistory.*;
import com.example.office.model.user.repository.*;
import com.example.office.service.*;
import com.example.office.service.device.*;
import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.*;
import org.springframework.data.web.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;
import java.time.format.*;

@Log4j2
@Controller
@AllArgsConstructor
public class FrontendController {

    private final DocumentService documentService;
    private final UserService userService;
    private final FolderService folderService;
    private final MainHistoryService mainHistoryService;
    private final DocumentTrashService documentTrashService;
    private final DocumentCommentService documentCommentService;
    private final CounterpartyService counterpartyService;
    private final ObjectLocationService objectLocationService;
    private final DeviceService deviceService;
    private final ApplicationService applicationService;
    private final UserRepository userRepository;

    //КОНТАКТЫ
    @GetMapping("/contacts-grid")
    public String contantGridPage() {
        return "contacts-grid";
    }

    @GetMapping("/pages-400-files")
    public String page500Show() {
        return "400pageFile";
    }

    @GetMapping("/pages-400-size")
    public String page400size() {
        return "400pageFile-maxSize";
    }

    @GetMapping("/contacts-list")
    public String contactListPage() {
        return "contacts-list";
    }

    @GetMapping("/contacts-profile")
    public String contactProfilePage() {
        return "contacts-profile";
    }

    @GetMapping("/test")
    public String getTestPage(Model model) {
//        model.addAttribute("lastActions",counterpartyService.getActionsLast());
        return "test";
    }

    //ФАЙЛОВЫЙ МЕНЕДЖЕР, ДОКУМЕНТЫ
    @GetMapping("/file-manager")
    private String showFileManagerPage(Model model, Principal principal) {
        if (principal == null) return "redirect:/user/authorization";

        Integer roleId = userRepository.findByEmail(principal.getName()).orElseThrow().getRole().getId();

//        documentService.makeDefaultBucket();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        model.addAttribute("recentDocuments", documentService.getResentDocuments());
        model.addAttribute("formatter", formatter);
        model.addAttribute("mapToCategoryFile", documentService.getSizeFilesFromCategory());
        model.addAttribute("foldersUser", folderService.getOwnFolders());
        model.addAttribute("lastActions", mainHistoryService.getAllDocumentHistory(principal));
        model.addAttribute("generalFolder", folderService.getGeneralFolder());
        model.addAttribute("accountant", folderService.getAccountantFolder());

        if (roleId == 3) {
            return "apps-file-manager-for-accountant";
        } else {
            return "apps-filemanager";
        }
    }

    @GetMapping("/file-manager-all")
    private String showAllFilePage(Model model, Principal principal) {
        if (principal == null) return "redirect:/user/authorization";
//        counterpartyService.putAllCounter();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        model.addAttribute("recentDocuments", documentService.getResentDocuments());
        model.addAttribute("formatter", formatter);
        model.addAttribute("mapToCategoryFile", documentService.getSizeFilesFromCategory());
        model.addAttribute("counterpartyList", counterpartyService.getAll());
        model.addAttribute("locationsList", objectLocationService.getAll());
        model.addAttribute("devicesList", deviceService.getAll());
        model.addAttribute("applicationsList", applicationService.findAll());

        return "apps-filemanager-all";
    }

    @PostMapping("/ex-name")
    public String addMovieImage(@RequestParam String exName, @RequestParam int idoc, Principal principal) {
        return documentService.exchangeNameDocumentMVC(exName, idoc, principal);
    }

    @GetMapping("/documentView/{id}")
    public String getDocumentPage(Model model,
                                  @PathVariable int id,
                                  @PageableDefault(sort = {"id"},
                                          direction = Sort.Direction.DESC) Pageable pageable,
                                  Principal principal) {
        if (principal == null) return "redirect:/user/authorization";
        Page<DocumentComment> page = documentCommentService.getAllByDocumentId(id, pageable);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        model.addAttribute("document", documentService.getOneDocument(id));
        model.addAttribute("lastActions", mainHistoryService.getDocumentHistory(id, principal));
        model.addAttribute("formatter", formatter);
        model.addAttribute("page", page);
        model.addAttribute("url", "/documentView/" + id);
        model.addAttribute("users", userService.getAll());

        Integer roleId = userRepository.findByEmail(principal.getName()).orElseThrow().getRole().getId();

        if (roleId == 3) {
            return "document-overview-accountant";
        } else {
            return "document-overview";
        }
    }

    @GetMapping("/folderView/{name}")
    public String getDocumentPage(Model model, @PathVariable String name) {
        model.addAttribute("counterDocs", folderService.getCounterFiles(name).get("counters"));
        model.addAttribute("appsDocs", folderService.getCounterFiles(name).get("apps"));
        model.addAttribute("devicesDocs", folderService.getCounterFiles(name).get("devices"));
        model.addAttribute("locationsDocs", folderService.getCounterFiles(name).get("locations"));
        return "folder-overview";
    }

    @GetMapping("/remove-list")
    public String getRemoveList(Model model) {
        model.addAttribute("duplicates", documentService.getDuplicate());
        model.addAttribute("deleted", documentTrashService.getAllList());
        return "remove-list";
    }

    @GetMapping("/file-folder/{name}")
    public String getPageFolder(Model model, @PathVariable String name, Principal principal) {
        if (principal == null) return "redirect:/user/authorization";
        var folder = folderService.getFolderByName(name);
        model.addAttribute("folder", folder);
        model.addAttribute("counterpartyList", counterpartyService.getAll());
        model.addAttribute("locationsList", objectLocationService.getAll());
        model.addAttribute("devicesList", deviceService.getAll());
        model.addAttribute("applicationsList", applicationService.findAll());

        Integer roleId = userRepository.findByEmail(principal.getName()).orElseThrow().getRole().getId();

        if (roleId == 3) {
            return "apps-own-folder-accountant";
        } else {
            return "apps-own-folder";
        }
    }

    @GetMapping("/history-page")
    public String getHistoryPage(Model model) {
        model.addAttribute("histories", mainHistoryService.getAllActions());
        return "history-page";
    }
}