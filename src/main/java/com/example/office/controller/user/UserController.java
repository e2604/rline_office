package com.example.office.controller.user;

import com.example.office.DTO.*;
import com.example.office.model.user.*;
import com.example.office.model.user.repository.*;
import com.example.office.service.*;
import com.example.office.xls.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.*;

import javax.servlet.http.*;
import javax.validation.*;
import java.io.*;
import java.security.*;
import java.text.*;
import java.util.*;

@Log4j2
@Controller
@AllArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserService userService;
    private final RoleService roleService;
    private final UserRepository userRepository;
    private final PositionService positionService;
    private final ApplicationService applicationService;

//    @GetMapping("/showById") //works
//    public UserDTO showUser(@RequestParam Integer id) {
//        return userService.getById(id);
//    }
//
//    @GetMapping("/showAll")
//    public List<UserDTO> showUsers() {
//        return userService.getAll();
//    }

    @GetMapping("/authorization") //страница авторизации пользователя
    public String authorizationPage(Model model,
                                    @RequestParam(required = false, name = "error") Boolean error) {
        model.addAttribute("error", error);
        return "authentication/authorization";
    }

    @GetMapping("/create")
    public String registerPage(Model model, Principal principal) {
        if (principal == null) return "redirect:/user/authorization";
        model.addAttribute("roles", roleService.getAll());
        model.addAttribute("positions", positionService.getAll());
        return "authentication/registration";
    }

    @PostMapping("/add") //регистрация нового пользователя
    public String create(@Valid User user,
                         Principal principal,
                         BindingResult bindingResult,
                         RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("user", user);

        boolean isUserExists = userRepository.existsByEmail(user.getEmail());
        boolean isIinExists = userRepository.existsByIin(user.getIin());
//        boolean isIinValid = userService.isIinValid(user.getIin());

        if (isUserExists || isIinExists) {
            if (isUserExists) {
                bindingResult.addError(new FieldError("user", "email", "Пользователь с таким почтовым адресом уже зарегистрирован"));
            }

            if (isIinExists) {
                bindingResult.addError(new FieldError("user", "iin", "Пользователь с таким ИИН уже зарегистрирован"));
            }
//            else if (!isIinValid) {
//                bindingResult.addError(new FieldError("user", "iin", "ИИН введен не верно"));
//        }

            redirectAttributes.addFlashAttribute("errors", bindingResult.getFieldErrors());
            return "redirect:/user/create";
        }

        userService.create(UserDTO.from(user), principal);
        return "redirect:/user/list";
    }

    @PostMapping("/profile/{userId}/delete")
    public String delete(@PathVariable Integer userId, Principal principal) {
        userService.deleteById(userId, principal);
        return "redirect:/user/list";
    }

    @GetMapping("/list")
    public String userList(Model model, Principal principal) {
        if (principal == null) return "redirect:/user/authorization";
        model.addAttribute("users", userService.getAll());
        return "authentication/user-list";
    }

    @GetMapping("/profile")
    public String profile(Model model, Principal principal) {

        if (principal == null) return "redirect:/user/authorization";

        User user = userRepository.findByEmail(principal.getName()).orElseThrow();
        Integer roleId = userRepository.findById(user.getId()).orElseThrow().getRole().getId();

        applicationService.changeStatusToOverdue();

        model.addAttribute("user", user);
        model.addAttribute("users", userService);
        model.addAttribute("roles", roleService);
        model.addAttribute("positions", positionService);
        model.addAttribute("applications", applicationService);

        if (roleId == 4 || roleId == 5) {
            return "authentication/executor-users-profile";
        } else if (roleId == 3) {
            return "authentication/accountant-profile";
        } else {
            return "authentication/admin-manager-profile";
        }
    }

    @GetMapping("/profile/{userId}")
    public String userProfilePage(@PathVariable Integer userId, Model model, Principal principal) {
        if (principal == null) return "redirect:/user/authorization";

        UserDTO user = userService.getById(userId);
        User authorizedUser = userRepository.findByEmail(principal.getName()).orElseThrow();
        Integer roleId = userRepository.findById(userId).orElseThrow().getRole().getId();

        applicationService.changeStatusToOverdue();

        model.addAttribute("user", user);
        model.addAttribute("users", userService);
        model.addAttribute("roles", roleService);
        model.addAttribute("positions", positionService);
        model.addAttribute("applications", applicationService);

//        model.addAttribute("user", userService.getById(id));
//        model.addAttribute("containing", userService.getAllContainUser(id));
//        model.addAttribute("applications", userService.getAllApplicationsWithUser(id));
//        model.addAttribute("doneApp", userService.doneAppCount(id));
//        model.addAttribute("activeApp", userService.activeAppCount(id));
//        model.addAttribute("allApp", userService.allAppCount(id));
//        model.addAttribute("role", roleService.getAll());
//        model.addAttribute("position", positionService.getAll());
//        model.addAttribute("totalSum", applicationService.getTotalSum(id));
//        model.addAttribute("totalMonthSum", applicationService.getTotalMonthSum(id));

        if (roleId == 4 || roleId == 5) {
            return "authentication/executor-users-profile-by-admin";
        } else if (authorizedUser.getRole().getId() == 1 && roleId == 1 || authorizedUser.getRole().getId() == 2 && roleId == 2) {
            return "redirect:/user/profile";
        } else {
            return "authentication/manager-accountant-profile-by-admin";
        }
    }

    //ДОРАБОТАТЬ ВОССТАНОВЛЕНИЕ ПАРОЛЯ
//    @GetMapping("/recoverpw")
//    public String recoverPassPage() {
//        return "authentication/recover-password";
//    }

//    @PostMapping("/recoverpw")
//    public ResponseEntity<?> recoverPassword(@RequestParam String email) {
//        userService.findByEmailAndRecoverPass(email);
//        return ResponseEntity.ok("Пароль будет отправлен в телеграм бот, если вы зарегистрированы в системе");
//    }

//    @GetMapping("/changePass")
//    public String changePass() {
//        return "authentication/auth-change-pass";
//    }

//    @PostMapping("/{id}/changePass")
//    public ResponseEntity<?> changePassword(@RequestParam int id){
//        userService.getById(id);
//        return ResponseEntity.ok("Пароль отправлен в телеграм бот");
//    }

    @PostMapping("/profile/{userId}/edit")
    public String editUserInformation(@PathVariable Integer userId, User edited, Principal principal) {
        userService.edit(userId, edited, principal);
        return "redirect:/user/profile/" + userId;
    }

    @PostMapping("/profile/{userId}/edit-admin")
    public String editAdminInformation(@PathVariable Integer userId, User edited, Principal principal) {
        userService.edit(userId, edited, principal);
        return "redirect:/user/profile";
    }

    @GetMapping("/{id}/changePass")
    public String changePassPage(@PathVariable Integer id, Model model) {
        model.addAttribute("user", userService.getById(id));
        return "authentication/auth-change-pass";
    }

    @PostMapping("/{id}/changePass")
    public String changePass(@PathVariable Integer id,
                             @RequestParam String newPass,
                             Principal principal) {
        userService.changeUserPass(id, newPass, principal);
        return "redirect:/user/profile/{id}";
    }

    @PostMapping("/profile/{userId}/change-password")
    public String changePassword(@PathVariable Integer userId,
                                 @RequestParam String newPassword,
                                 Principal principal) {
        userService.changeUserPass(userId, newPassword, principal);

        User authorizedUser = userRepository.findByEmail(principal.getName()).orElseThrow();
        User user = User.from(userService.getById(userId));

        if (authorizedUser.getRole().getId() == 1 && user.getRole().getId() == 1 || authorizedUser.getRole().getId() == 2 && user.getRole().getId() == 2) {
            return "redirect:/user/profile";
        } else {
            return "redirect:/user/profile/" + userId;
        }
    }

//    @GetMapping("/changePass")
//    public String changePass() {
//        return "authentication/auth-change-pass";
//    }

//    @PostMapping("/{id}/changePass")
//    public ResponseEntity<?> changePassword(@RequestParam int id){
//        userService.getById(id);
//        return ResponseEntity.ok("Пароль отправлен в телеграм бот");
//    }

//    @PostMapping("/recoverpw")
//    public ResponseEntity<?> recoverPassword(@RequestParam String email){
//        userService.findByEmailAndRecoverPass(email);
//        return ResponseEntity.ok("Пароль будет отправлен в телеграм бот, если вы зарегистрированы в системе");
//    }

//    @PostMapping("{id}/remove") //NotWorks
//    public String delete(@PathVariable Integer id) {
//        userService.deleteById(id);
//        return "redirect:/user/list";
//    }

//    @GetMapping("/{id}/changePass")
//    public String changePassPage (@PathVariable int id, Model model){
//        model.addAttribute("user", userService.getById(id));
//        return "authentication/auth-change-pass";
//    }

//    @PostMapping("/{id}/changePass")
//    public String changePass(@PathVariable Integer id, @RequestParam String newPass){
//        userService.changeUserPass(id, newPass);
//        return "redirect:/user/profile/{id}";
//    }

    @PostMapping("/{id}/sendMessage")
    public String sendMessageForUser(@PathVariable Integer id, @RequestParam("message") String message) {
        var user = userService.getById(id);
        if (user.getChatId() != null) {
            if (message != null) {
                userService.sendMessage(user.getChatId().toString(), message);
            } else {
                log.info("Сообщение пустое! " + user.getName() + " не получил сообщение в телеграм");
            }
            return "redirect:/user/profile/" + id;
        } else {
            return "404SendMessageError";
        }
    }

    @GetMapping("/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("/user/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=user_list_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        List<UserDTO> users = userService.getAllForExcel();
        UserExcelExporter excelExporter = new UserExcelExporter(users);
        excelExporter.export(response);
    }
}