package com.example.office.controller.user;

import com.example.office.DTO.*;
import com.example.office.model.user.repository.*;
import com.example.office.service.*;
import lombok.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;
import java.util.*;

@RestController
@AllArgsConstructor
public class UserRestController {

    private UserService userService;
    private UserRepository userRepository;

    //user-applications.js
    @GetMapping("/user/profile/{id}/apiApp/all")
    public List<ApplicationDTO> getAllApp(@PathVariable Integer id) {
        return userService.getAllApplicationsWithUser(id);
    }

    //user-applications.js
    @GetMapping("/user/profile/{id}/apiApp/search/{search}")
    public List<ApplicationDTO> searchListDocument(@PathVariable Integer id, @PathVariable String search) {
        return userService.getUserApplicationsSearchList(id, search);
    }

    //user-applications.js
    @GetMapping("/user/profile/apiApp/all")
    public List<ApplicationDTO> getAllAppProfile(Principal principal) {
        Integer id = userRepository.findByEmail(principal.getName()).orElseThrow().getId();
        return userService.getAllApplicationsWithUser(id);
    }

    //user-applications.js
    @GetMapping("/user/profile/apiApp/search/{search}")
    public List<ApplicationDTO> searchListAppProfile(Principal principal, @PathVariable String search) {
        Integer id = userRepository.findByEmail(principal.getName()).orElseThrow().getId();
        return userService.getUserApplicationsSearchList(id, search);
    }

    //user-list.js
    @GetMapping("/apiAppUser/allUsers")
    public List<UserDTO> getAllUser() {
        return userService.getAll();
    }

    //user-list.js
    @GetMapping("/apiAppUser/searchUsers/{search}")
    public List<UserDTO> searchListUser(@PathVariable String search) {
        return userService.getUserSearchList(search);
    }
}