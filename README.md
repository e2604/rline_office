# RLine Office

Система автоматизации офиса.

## Инструкция запуска

Для запуска приложения необходимо прописать команды mvn install, docker-compose build и docker-compose up.

Для корректного запуска приложения необходимо убедиться, что порты minIO и postgres запущены.

Приложения запускаются на портах:

3005:8898 - web приложение;  
1235:9001 - minIO;  
2000:5432 - postgres;

## Пароли

Данные для входа в офис:  
логин - admin@gmail.com   
пароль - admin;

Данные для входа на сервер minIO:  
логин - minioadmin  
пароль - minioadmin;

Данные для входа в базу данных:  
логин - postgres  
пароль - postgres;