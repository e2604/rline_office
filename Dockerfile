FROM openjdk:11
WORKDIR /app
#указываем путь к собранному файлу, когда мы сбилдим проект, данный файл появится в папке target
ARG JAR_FILE=target/spring-boot-docker.jar
#добавляем файл JAR_FILE в докер контейнер, внутри докера он будет называться app.jar
ADD ${JAR_FILE} app.jar
#комнда запуска
ENTRYPOINT ["java", "-jar", "app.jar"]